<?php

namespace App\Jobs;

use App\Exceptions\InvalidReceivableUnitException;
use App\Exceptions\NotFoundException;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\CancelContractEffectServiceContract;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\ReceivableUnits\ReceivableUnitRegisterFromContractEffectService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ContractEffectApply implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $contractEffect;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ContractEffect $contractEffect)
    {
        $this->contractEffect = $contractEffect;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $receivableUnit = ReceivableUnit::where([
                'final_recipient_document_number' => $this->contractEffect->final_recipient_document_number,
                'holder_document_number' => $this->contractEffect->final_recipient_document_number,
                'code_arrangement_payment' => $this->contractEffect->code_arrangement_payment,
                'settlement_date' => $this->contractEffect->settlement_date
            ])->first();

            if (! $receivableUnit) {
                Log::info('Not found receivable unit to apply effect. Creating not constituted now.');

                $receivableUnit = (new ReceivableUnitRegisterFromContractEffectService())
                    ->store($this->contractEffect);
            }

            app(ContractEffectServiceContract::class, [
                'effect_type' => $this->contractEffect->effect_type,
                'has_committed_value' => $this->hasCommittedValue()
            ])->apply($this->contractEffect, $receivableUnit);

        } catch (InvalidReceivableUnitException | NotFoundException $e) {
            Log::warning('contractEffectApplyJob', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'contractEffectId' => $this->contractEffect->id
            ]);

        } catch (\Exception $e) {
            Log::critical('contractEffectApplyJob', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'contractEffectId' => $this->contractEffect->id
            ]);
        }
    }

    private function hasCommittedValue(): bool
    {
        return $this->contractEffect->committed_value > 0;
    }
}
