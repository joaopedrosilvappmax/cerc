<?php

namespace App\Jobs;

use App\Models\Entities\ReceivableUnit;
use App\Models\Services\ReceivableUnits\ReceivableUnitIntegrateService;
use App\Support\EnumTypes\OperationType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class IntegrationSendReceivableUnit implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $receivableUnitId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $receivableUnitId)
    {
        $this->receivableUnitId = $receivableUnitId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ReceivableUnitIntegrateService $integrationService)
    {
        try {
            $receivableUnit = ReceivableUnit::find($this->receivableUnitId);
            $receivableUnit = $this->updateOperationType($receivableUnit);

            $integrationService->send($receivableUnit);

        } catch (\Exception $e) {
            Log::critical('integrationSendReceivableUnitJob', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'receivableUnitId' => $this->receivableUnitId
            ]);
        }
    }

    private function updateOperationType($receivableUnit): ReceivableUnit
    {
        if ($receivableUnit->integrated_at && $receivableUnit->operation_type != OperationType::WRITE_OFF) {
            $receivableUnit->update([ 'operation_type' => OperationType::UPDATE ]);
        }

        return $receivableUnit->refresh();
    }
}
