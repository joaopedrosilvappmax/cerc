<?php

namespace App\Jobs;

use App\Models\Entities\Anticipation;
use App\Support\EnumTypes\OperationType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class WriteOffAnticipationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Anticipation $anticipation;

    public function __construct(Anticipation $anticipation)
    {
        $this->anticipation = $anticipation;
    }

    public function handle(): void
    {
        try {
            $this->anticipation->update([
                'operation_type' => OperationType::WRITE_OFF
            ]);

        } catch (\Exception $e) {
            Log::error('WriteOffAnticipation.job', [
                'anticipationId' => $this->anticipation->id,
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }
    }
}
