<?php

namespace App\Jobs;

use App\Models\Entities\Company;
use App\Models\Services\Companies\CompanyIntegrateService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class IntegrationSendCompany implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $companyId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CompanyIntegrateService $integrationService)
    {
        try {
            $integrationService->send(Company::find($this->companyId));
        } catch (\Exception $e) {
            Log::critical('integrationSendCompanyJob', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'companyId' => $this->companyId
            ]);
        }
    }
}
