<?php

namespace App\Jobs;

use App\Models\Services\ContractEffects\ContractEffectReadFileService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ContractEffectReadFileProcess implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    protected $path;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ContractEffectReadFileService $contractEffectReadFileService)
    {
        try {
            $contractEffectReadFileService->read(['path' => $this->path]);
        } catch (\Exception $e) {
            Log::critical('contractEffectReadFileJob', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage()
            ]);
        }
    }
}
