<?php

namespace App\Jobs;

use App\Models\Entities\Anticipation;
use App\Models\Services\Anticipations\AnticipationIntegrateService;
use App\Support\EnumTypes\OperationType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class IntegrationSendAnticipation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $anticipationId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $anticipationId)
    {
        $this->anticipationId = $anticipationId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AnticipationIntegrateService $integrationService)
    {
        try {
            $anticipation = Anticipation::find($this->anticipationId);
            $anticipation = $this->updateOperationType($anticipation);

            $integrationService->send($anticipation);
        } catch (\Exception $e) {
            Log::critical('integrationSendAnticipationJob', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'anticipationId' => $this->anticipationId
            ]);
        }
    }

    private function updateOperationType($anticipation): Anticipation
    {
        if ($anticipation->integrated_at && $anticipation->operation_type != OperationType::WRITE_OFF) {
            $anticipation->update([ 'operation_type' => OperationType::UPDATE ]);
        }

        return $anticipation->refresh();
    }
}
