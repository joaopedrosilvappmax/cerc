<?php

namespace App\Jobs;

use App\Models\Services\Reports\GenerateSyntheticConciliationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class GenerateSyntheticConciliationReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(GenerateSyntheticConciliationService $generateSyntheticConciliation)
    {
        try {
            $generateSyntheticConciliation->process();
        } catch (\Exception $e) {
            Log::error('generateSyntheticConciliation.job', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }

    }
}
