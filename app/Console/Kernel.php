<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\IntegrationCompany::class,
        Commands\IntegrationAnticipation::class,
        Commands\IntegrationReceivableUnit::class,
        Commands\GenerateSyntheticConciliation::class,
        Commands\GenerateReceivableUnitValueToAnalyticConciliation::class,
        Commands\IntegrationSyntheticConciliation::class,
        Commands\WriteOffReceivableUnit::class,
        Commands\WriteOffPaidAnticipation::class,
        Commands\VerifyLastCreatedReceivableUnitOrder::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('company:integrate')
            ->dailyAt(config('cerc.company_hour_integrate'))
            ->withoutOverlapping();

        $schedule->command('receivable-unit:write-off')
            ->dailyAt(config('cerc.receivable_unit_hour_write_off'))
            ->withoutOverlapping();

        $schedule->command('receivable-unit:integrate')
            ->dailyAt(config('cerc.receivable_unit_hour_integrate'))
            ->withoutOverlapping();

        $schedule->command('receivable-unit:integrate')
            ->dailyAt(config('cerc.receivable_unit_hour_integrate_retry'))
            ->withoutOverlapping();

        $schedule->command('anticipation:integrate')
            ->hourly()
            ->between('9:00', '19:59')
            ->withoutOverlapping();

        $schedule->command('anticipation:write-off-paid')
            ->dailyAt(config('cerc.anticipation_hour_write_off'))
            ->withoutOverlapping();

        $schedule->command('synthetic-conciliation:generate')
            ->dailyAt(config('cerc.synthetic_conciliation_hour_generate'))
            ->withoutOverlapping();

        $schedule->command('synthetic-conciliation:integrate')
            ->dailyAt(config('cerc.synthetic_conciliation_hour_integrate'))
            ->withoutOverlapping();

        $schedule->command('receivable-unit-conciliation:generate')
            ->dailyAt(config('cerc.analytic_conciliation.receivable_unit_hour_generate'))
            ->withoutOverlapping();

        $schedule->command('anticipation-conciliation:generate')
            ->dailyAt(config('cerc.analytic_conciliation.anticipation_hour_generate'))
            ->withoutOverlapping();

        $schedule->command('contract-effect:ap008')
            ->everyThirtyMinutes()
            ->withoutOverlapping();

        $schedule->command('report:daily-via-slack')
            ->dailyAt(config('notifications.daily_reports_time'))
            ->withoutOverlapping();

        $schedule->command('report:daily-conciliation-records-via-slack')
            ->dailyAt(config('notifications.daily_reports_time'))
            ->withoutOverlapping();

        $schedule->command('report:daily-via-discord')
            ->dailyAt(config('notifications.daily_reports_time'))
            ->withoutOverlapping();

        $schedule->command('report:daily-conciliation-records-via-discord')
            ->dailyAt(config('notifications.daily_reports_time'))
            ->withoutOverlapping();

        $schedule->command('receivable-unit-order:verify-last-created')
            ->everyMinute()
            ->withoutOverlapping();

        $schedule->command('notification:critical-erros-slack')
            ->everyMinute()
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
