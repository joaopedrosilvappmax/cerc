<?php

namespace App\Console\Commands;

use App\Jobs\IntegrationSendAnticipation;
use App\Models\Entities\Anticipation;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class IntegrationAnticipation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anticipation:integrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send anticipations to CERC';


    public function handle()
    {
        $now = Carbon::now();
        $start = Carbon::createFromTimeString(config('cerc.anticipation_limit_time.start'));
        $end = Carbon::createFromTimeString(config('cerc.anticipation_limit_time.end'));

        if (! $now->between($start, $end)) {
            return false;
        }

        $counter = 0;

        Anticipation::where(function ($query) {
            return $query->whereColumn('integrated_at', '<', 'updated_at')
                ->orWhereNull('integrated_at');
        })
        ->where('total_constituted_value', '>', 0)
        ->get('id')
        ->each(function ($anticipation) use (&$counter) {
            IntegrationSendAnticipation::dispatch($anticipation->id);
            $counter++;
        });

        Log::info("Sending {$counter} anticipations");

        return true;
    }
}
