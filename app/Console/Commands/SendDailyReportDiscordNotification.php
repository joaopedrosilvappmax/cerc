<?php

namespace App\Console\Commands;

use App\Models\Entities\Anticipation;
use App\Models\Entities\Company;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\ContractEffects\ContractEffectService;
use App\Models\Services\Contracts\NotificationServiceContract;
use App\Support\EnumTypes\NotificationSenderType;
use App\Support\ValueObjects\DiscordNotificationBody;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendDailyReportDiscordNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:daily-via-discord';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send information about some daily insertions to a Discord server.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            app(
                NotificationServiceContract::class,
                ['class' => NotificationSenderType::getBindedClassByType(NotificationSenderType::DISCORD)]
            )->notify(
                'Relatório Diário',
                $this->getMessageData()
            );
        } catch (Exception $e) {
            Log::error('sendDailyReportDiscordNotification.handle', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }

        return 0;
    }

    private function getMessageData(): array
    {
        $companiesCreated = Company::dailyCreated()->count();
        $companiesUpdated = Company::dailyUpdated()->count();
        $companiesIntegrated = Company::dailyIntegrated()->count();
        $receivableUnitsCreated = ReceivableUnit::dailyCreated()->count();
        $receivableUnitsUpdated = ReceivableUnit::dailyUpdated()->count();
        $receivableUnitsIntegrated = ReceivableUnit::dailyIntegrated()->count();
        $receivableUnitsWriteOff = ReceivableUnit::dailyWriteOff()->count();
        $anticipationsCreated = Anticipation::dailyCreated()->count();
        $anticipationsUpdated = Anticipation::dailyUpdated()->count();
        $anticipationsIntegrated = Anticipation::dailyIntegratedAnticipation()->count();
        $anticipationsWriteOff = Anticipation::dailyWriteOffAnticipation()->count();
        $contractEffectCreated = ContractEffect::dailyCreated()->count();
        $contractEffectsApplied = ContractEffect::dailyApplied()->count();
        $contractEffectsAnnulled = ContractEffect::dailyAnnulled()->count();
        $contractEffectsErrors = ContractEffect::dailyWithError()->count();

        return [
            [
                'title' => ':clock8: Relatório Diário',
                'color' => DiscordNotificationBody::APPMAX_DEFAULT_COLOR,
                'fields' => [
                    DiscordNotificationBody::field(
                        ':office: Empresas',
                        "**Criadas:** $companiesCreated\n**Atualizadas:** $companiesUpdated\n**Integradas:** $companiesIntegrated"
                    ),
                    DiscordNotificationBody::field(
                        ':moneybag: Unidades Recebíveis',
                        "**Criadas:** $receivableUnitsCreated\n**Atualizadas:** $receivableUnitsUpdated\n**Baixadas:** $receivableUnitsWriteOff\n**Integradas:** $receivableUnitsIntegrated"
                    ),
                    DiscordNotificationBody::field(
                        ':fast_forward: Antecipações',
                        "**Criadas:** $anticipationsCreated\n**Atualizadas:** $anticipationsUpdated\n**Baixadas:** $anticipationsWriteOff\n**Integradas:** $anticipationsIntegrated"
                    ),
                    DiscordNotificationBody::field(
                        ':scroll: Efeitos de Contrato',
                        "**Recebidos:** $contractEffectCreated\n**Aplicados:** $contractEffectsApplied\n**Anulados:** $contractEffectsAnnulled\n**Falhados:** $contractEffectsErrors"
                    ),
                ]
            ]
        ];
    }
}
