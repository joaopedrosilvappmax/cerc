<?php

namespace App\Console\Commands;

use App\Models\Entities\Company;
use App\Models\Entities\LogNotificationError;
use App\Support\EnumTypes\CompanyType;
use App\Support\EnumTypes\LogNotificationErrorType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class LogNotificationCriticalErrorsSlack extends Command
{
    protected $signature = 'notification:critical-erros-slack';

    protected $description = 'Report critical errors occurring 10 times or more to the slack channel';

    public function handle()
    {
        $company = Company::firstWhere('type_id', CompanyType::INTERNAL);

        $criticalError = LogNotificationError::where('type', LogNotificationErrorType::CRITICAL)
            ->where('quantity', '>=', config('log_notification_errors.max_critical_count'))
            ->first();

        if (! $criticalError || $company->has_critical_error) {
            return;
        }

        $company->update(['has_critical_error' => true]);

        Log::channel('slack')->critical(
            ":exclamation: Hello, everyone! We have a problem at CERC."
        );
    }
}
