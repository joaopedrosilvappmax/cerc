<?php

namespace App\Console\Commands;

use App\Models\Entities\SyntheticConciliationRecord;
use App\Models\Services\Contracts\NotificationServiceContract;
use App\Support\EnumTypes\NotificationSenderType;
use App\Support\ValueObjects\Money;
use App\Support\ValueObjects\SlackNotificationBody;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendDailyConciliationRecordSlackNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:daily-conciliation-records-via-slack';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily Conciliation Records insertions to a Slack channel.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            app(
                NotificationServiceContract::class,
                ['class' => NotificationSenderType::getBindedClassByType(NotificationSenderType::SLACK)]
            )->notify(
                'Relatório diário',
                $this->getMessageData()
            );
        } catch (Exception $e) {
            Log::error('sendDailyReportSlackNotification.handle', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }

        return 0;
    }

    private function getMessageData(): array
    {
        $syntheticConciliationRecordCreated = SyntheticConciliationRecord::where(
            'reference_date',
            '=',
            Carbon::now()->subDay()->format('Y-m-d')
        )->get();

        $message = [
            SlackNotificationBody::header(':book: Conciliação Agenda'),
            SlackNotificationBody::divider(),
        ];

        if ($syntheticConciliationRecordCreated->isNotEmpty()) {
            $syntheticConciliationRecordCreated->each(function ($record) use (&$message) {
                $message[] = SlackNotificationBody::columns(
                    "Data de Integração",
                    $record->integrated_at ? $record->integrated_at->format('d/m/Y H:i') : "-"
                );
                $message[] = SlackNotificationBody::columns(
                    "Código Arranjo de Pagamento",
                    $record->code_arrangement_payment
                );
                $message[] = SlackNotificationBody::columns(
                    "Valor Total",
                    (new Money($record->total_gross_value))->toMoneyFormat()
                );
                $message[] = SlackNotificationBody::columns(
                    "Valor Total Constituído",
                    (new Money($record->total_constituted_value))->toMoneyFormat()
                );
                $message[] = SlackNotificationBody::columns(
                    "Valor Pré Contratado",
                    (new Money($record->pre_contracted_value))->toMoneyFormat()
                );
                $message[] = SlackNotificationBody::columns(
                    "Valor Antecipação",
                    (new Money($record->anticipation_not_settled_value))->toMoneyFormat()
                );
                $message[] = SlackNotificationBody::divider();
            });

            return $message;
        }

        $message[] = SlackNotificationBody::section("Nada a reportar");

        return $message;
    }
}
