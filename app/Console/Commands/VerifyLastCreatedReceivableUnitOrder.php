<?php

namespace App\Console\Commands;

use App\Models\Services\ReceivableUnits\ReceivableUnitOrderVerifyLastCreatedByTimeService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class VerifyLastCreatedReceivableUnitOrder extends Command
{
    protected $signature = 'receivable-unit-order:verify-last-created';
    protected $description = 'check the last unit order received';

    public function handle(ReceivableUnitOrderVerifyLastCreatedByTimeService $service)
    {
        try {
            $service->verify();
        } catch (\Exception $e) {
            Log::error('VerifyLastCreatedReceivableUnitOrderCommand.verify', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }
    }
}
