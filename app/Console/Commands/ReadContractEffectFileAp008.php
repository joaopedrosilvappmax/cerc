<?php

namespace App\Console\Commands;

use App\Models\Services\Contracts\ContractEffectReadFileServiceContract;
use App\Models\Services\Files\RegisterContractEffectFilesService;
use App\Support\EnumTypes\ClassBindResolution;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ReadContractEffectFileAp008 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contract-effect:ap008
                            {date? : File date on YYYYMMDD format. (i.e.: 20211124)}
                            {sequential-number? : Sequential number greater than 0. (i.e.: 1)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Submission of contract effects applicable to receivable units for settlement purposes.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): bool
    {
        if (config('cerc_ap008.read_file_inactive_schedule')) {
            return false;
        }

        $date = $this->argument('date') ?? Carbon::now()->format('Ymd');
        $sequentialNumber = $this->argument('sequential-number')
            ?? app(RegisterContractEffectFilesService::class)
                ->getLastSequentialNumber() + 1;

        try {
            app(ContractEffectReadFileServiceContract::class, [
                'class' => ClassBindResolution::CONTRACT_EFFECT_READ_S3BUCKET_FILE
            ])->read([
                'date' => $date,
                'sequential_number' => $sequentialNumber
            ]);

            $this->info('Success!');
        } catch (Exception $e) {
            Log::error('command.readContractEffectFile.handle', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'date' => $date,
                'sequential_number' => $sequentialNumber
            ]);

            $this->error($e->getMessage());
        }

        return true;
    }
}
