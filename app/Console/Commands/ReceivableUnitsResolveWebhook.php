<?php

namespace App\Console\Commands;

use App\Models\Entities\WebhookRequest;
use App\Models\Services\ReceivableUnits\ReceivableUnitWebhookResolverService;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ReceivableUnitsResolveWebhook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'receivable-units:resolve-webhook
                            {date? : File date on YYYY-MM-DD format. (i.e.: 2022-01-20)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        if (! $date = $this->argument('date')) {
            $this->error('Missing date argument.');

            return 0;
        }

        try {
            $date = Carbon::createFromFormat('Y-m-d', $date);

            WebhookRequest::where('created_at', '>=', $date->startOfDay()->toDateTimeString())
                ->where('created_at', '<=', $date->endOfDay()->toDateTimeString())
                ->get()
                ->each(function ($webhookRequest) {
                    foreach ($webhookRequest->request->toArray() as $data) {
                        if ($data['tipoEvento'] == 'unidadeRecebivel') {
                            app(ReceivableUnitWebhookResolverService::class)
                                ->resolveWebhookData($data, $webhookRequest->id);
                        }
                    }
                });
        } catch (Exception $exception) {
            Log::error('receivableUnitsResolveWebhook.handle', [
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
            ]);
        }

        return 1;
    }
}
