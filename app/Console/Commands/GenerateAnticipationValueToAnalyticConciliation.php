<?php

namespace App\Console\Commands;

use App\Models\Services\Reports\GenerateAnalyticConciliationFromAnticipationValueService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GenerateAnticipationValueToAnalyticConciliation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'anticipation-conciliation:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate data to synthetic conciliation to CERC';

    public function handle(GenerateAnalyticConciliationFromAnticipationValueService $generateAnalyticConciliation): bool
    {
        Log::info("Analytic conciliation to anticipation values running");

        try {
            $generateAnalyticConciliation->process();
        } catch (\Exception $e) {
            Log::error('generateAnalyticConciliationCommand.process', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }

        return true;
    }
}
