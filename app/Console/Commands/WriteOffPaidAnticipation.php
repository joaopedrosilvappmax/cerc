<?php

namespace App\Console\Commands;

use App\Jobs\WriteOffAnticipationJob;
use App\Models\Entities\Anticipation;
use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class WriteOffPaidAnticipation extends Command
{
    protected $signature = 'anticipation:write-off-paid';

    protected $description = 'Write off paid anticipations';

    public function handle()
    {
        $anticipations = $this->getPaidAnticipations();

        $anticipations->map(function ($anticipation) {
            dispatch(new WriteOffAnticipationJob($anticipation));
        });

        $this->info('Anticipations found: ' . $anticipations->count());
    }

    private function getPaidAnticipations(): Collection
    {
        return Anticipation::select('anticipations.id')
            ->join('payments', 'anticipations.id', '=', 'payments.anticipation_id')
            ->where('anticipations.created_at', '<', Carbon::now()->subDay()->endOfDay())
            ->where('anticipations.operation_type', '!=', OperationType::WRITE_OFF)
            ->whereNotNull('integrated_at')
            ->where(function ($query) {
                return $query
                    ->whereNotNull('payments.paid_at')
                    ->orWhere('settlement_date', '<=', Carbon::now());
            })
            ->get();
    }
}
