<?php

namespace App\Console\Commands;

use App\Models\Services\Reports\ReadAnalyticConciliationService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ReadAnalyticConciliation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'analytic-conciliation:read {--date=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read data to analytic conciliation to CERC';

    public function handle(ReadAnalyticConciliationService $readAnalyticConciliation): bool
    {
        Log::info("Analytic conciliation reading");

        try {
            if (! $this->option('date')) {
                $this->error("Option {date} is required");
            }

            $readAnalyticConciliation->read(
                Carbon::createFromFormat('Y-m-d', $this->option('date'))
            );

        } catch (\Exception $e) {

            Log::error('readAnalyticConciliationCommand.process', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }

        return true;
    }
}
