<?php

namespace App\Console\Commands;

use App\Models\Entities\SyntheticConciliationRecord;
use App\Models\Services\Contracts\NotificationServiceContract;
use App\Support\EnumTypes\NotificationSenderType;
use App\Support\ValueObjects\DiscordNotificationBody;
use App\Support\ValueObjects\Money;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendDailyConciliationRecordDiscordNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:daily-conciliation-records-via-discord';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daily Conciliation Records insertions to a Discord server.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        try {
            app(
                NotificationServiceContract::class,
                ['class' => NotificationSenderType::getBindedClassByType(NotificationSenderType::DISCORD)]
            )->notify(
                'Relatório Diário',
                $this->getMessageData()
            );
        } catch (Exception $e) {
            Log::error('sendDailyConciliationRecordDiscordNotification.handle', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }

        return 0;
    }

    private function getMessageData(): array
    {
        $syntheticConciliationRecordCreated = SyntheticConciliationRecord::where(
            'reference_date',
            '=',
            Carbon::now()->subDay()->format('Y-m-d')
        )->get();

        $message = [
            [
                'title' => ':book: Conciliação Agenda',
                'color' => DiscordNotificationBody::APPMAX_DEFAULT_COLOR,
                'fields' => []
            ]
        ];

        if ($syntheticConciliationRecordCreated->isNotEmpty()) {
            $syntheticConciliationRecordCreated->each(function ($record) use (&$message) {
                $message[0]['fields'][] = DiscordNotificationBody::field(
                    '_',
                    '**Data de Integração: **' . ($record->integrated_at ? $record->integrated_at->format('d/m/Y H:i') : '-') . "\n" .
                    '**Código Arranjo de Pagamento: **' . $record->code_arrangement_payment . "\n" .
                    '**Valor Total: **' . (new Money($record->total_gross_value))->toMoneyFormat() . "\n" .
                    '**Valor Total Constituído: **' . (new Money($record->total_constituted_value))->toMoneyFormat() . "\n" .
                    '**Valor Pré Contratado: **' . (new Money($record->pre_contracted_value))->toMoneyFormat() . "\n" .
                    '**Valor Antecipação: **' . (new Money($record->anticipation_not_settled_value))->toMoneyFormat()
                );
            });

            return $message;
        }

        $message[0]['fields'][] = DiscordNotificationBody::field('Nada a reportar', ':robot_face:');

        return $message;
    }
}
