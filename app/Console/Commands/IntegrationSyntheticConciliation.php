<?php

namespace App\Console\Commands;

use App\Models\Services\Reports\SyntheticConciliationIntegrateService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class IntegrationSyntheticConciliation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'synthetic-conciliation:integrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate data to synthetic conciliation to CERC';

    public function handle(): void
    {
        Log::info("Send synthetic conciliation running");

        (new SyntheticConciliationIntegrateService())->send();
    }
}
