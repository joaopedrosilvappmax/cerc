<?php

namespace App\Console\Commands;

use App\Models\Entities\Company;
use App\Models\Entities\LogNotificationError;
use App\Support\EnumTypes\CompanyType;
use App\Support\EnumTypes\LogNotificationErrorType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ClearHasCriticalError extends Command
{
    protected $signature = 'has-critical-error:clear';
    protected $description = 'clear has critical error';

    public function handle()
    {
        try {
            Company::firstWhere('type_id', CompanyType::INTERNAL)
                ->update(['has_critical_error' => false]);

            LogNotificationError::firstWhere('type', LogNotificationErrorType::CRITICAL)
                ->update(['quantity' => 0]);

        } catch (\Exception $e) {
            Log::error('ClearHasCriticalErrorCommand', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }
    }
}
