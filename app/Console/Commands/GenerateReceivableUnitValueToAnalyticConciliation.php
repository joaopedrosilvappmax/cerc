<?php

namespace App\Console\Commands;

use App\Models\Services\Reports\GenerateAnalyticConciliationFromReceivableUnitValueService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GenerateReceivableUnitValueToAnalyticConciliation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'receivable-unit-conciliation:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate data to synthetic conciliation to CERC';

    public function handle(GenerateAnalyticConciliationFromReceivableUnitValueService $generateAnalyticConciliation): bool
    {
        Log::info("Analytic conciliation to receivable unit values running");

        try {
            $generateAnalyticConciliation->process();
        } catch (\Exception $e) {
            Log::error('generateAnalyticConciliationCommand.process', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }

        return true;
    }
}
