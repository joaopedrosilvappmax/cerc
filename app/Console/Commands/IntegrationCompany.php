<?php

namespace App\Console\Commands;

use App\Jobs\IntegrationSendCompany;
use App\Models\Entities\Company;
use App\Support\EnumTypes\CompanyType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class IntegrationCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:integrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send companies to CERC';

    public function handle(): bool
    {
        $counter = 0;

        Company::where('type_id', CompanyType::PLATFORM)
            ->where(function($query) {
                return $query->whereColumn('integrated_at', '<', 'updated_at')
                    ->orWhereNull('integrated_at');
            })
            ->get('id')
            ->map(function ($company) use (&$counter) {
                IntegrationSendCompany::dispatch($company->id);
                $counter++;
            });

        Log::info("Sending {$counter} companies");

        return true;
    }
}
