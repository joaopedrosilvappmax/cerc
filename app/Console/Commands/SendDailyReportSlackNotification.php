<?php

namespace App\Console\Commands;

use App\Models\Entities\Anticipation;
use App\Models\Entities\Company;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\ContractEffects\ContractEffectService;
use App\Models\Services\Contracts\NotificationServiceContract;
use App\Support\EnumTypes\NotificationSenderType;
use App\Support\ValueObjects\SlackNotificationBody;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendDailyReportSlackNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:daily-via-slack';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send information about some daily insertions to a Slack channel.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        try {
            app(
                NotificationServiceContract::class,
                ['class' => NotificationSenderType::getBindedClassByType(NotificationSenderType::SLACK)]
            )->notify(
                'Relatório diário',
                $this->getMessageData()
            );
        } catch (Exception $e) {
            Log::error('sendDailyReportSlackNotification.handle', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }

        return 0;
    }

    private function getMessageData(): array
    {
        $companiesCreated = Company::dailyCreated()->count();
        $companiesUpdated = Company::dailyUpdated()->count();
        $companiesIntegrated = Company::dailyIntegrated()->count();
        $receivableUnitsCreated = ReceivableUnit::dailyCreated()->count();
        $receivableUnitsUpdated = ReceivableUnit::dailyUpdated()->count();
        $receivableUnitsIntegrated = ReceivableUnit::dailyIntegrated()->count();
        $receivableUnitsWriteOff = ReceivableUnit::dailyWriteOff()->count();
        $anticipationsCreated = Anticipation::dailyCreated()->count();
        $anticipationsUpdated = Anticipation::dailyUpdated()->count();
        $anticipationsIntegrated = Anticipation::dailyIntegratedAnticipation()->count();
        $anticipationsWriteOff = Anticipation::dailyWriteOffAnticipation()->count();
        $contractEffectCreated = ContractEffect::dailyCreated()->count();
        $contractEffectsApplied = ContractEffect::dailyApplied()->count();
        $contractEffectsAnnulled = ContractEffect::dailyAnnulled()->count();
        $contractEffectsErrors = ContractEffect::dailyWithError()->count();

        return [
            SlackNotificationBody::header(':clock8: Relatório Diário'),
            SlackNotificationBody::divider(),
            SlackNotificationBody::header(':office: Empresas'),
            SlackNotificationBody::section("*Criadas:* $companiesCreated\n*Atualizadas:* $companiesUpdated\n*Integradas:* $companiesIntegrated"),
            SlackNotificationBody::divider(),
            SlackNotificationBody::header(':moneybag: Unidades Recebíveis'),
            SlackNotificationBody::section("*Criadas:* $receivableUnitsCreated\n*Atualizadas:* $receivableUnitsUpdated\n*Baixadas:* $receivableUnitsWriteOff\n*Integradas:* $receivableUnitsIntegrated"),
            SlackNotificationBody::divider(),
            SlackNotificationBody::header(':fast_forward: Antecipações'),
            SlackNotificationBody::section("*Criadas:* $anticipationsCreated\n*Atualizadas:* $anticipationsUpdated\n*Baixadas:* $anticipationsWriteOff\n*Integradas:* $anticipationsIntegrated"),
            SlackNotificationBody::divider(),
            SlackNotificationBody::header(':scroll: Efeitos de Contrato'),
            SlackNotificationBody::section("*Recebidos:* $contractEffectCreated\n*Aplicados:* $contractEffectsApplied\n*Anulados:* $contractEffectsAnnulled\n*Falhados:* $contractEffectsErrors\n"),
            SlackNotificationBody::divider()
        ];
    }
}
