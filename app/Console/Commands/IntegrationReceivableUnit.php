<?php

namespace App\Console\Commands;

use App\Jobs\IntegrationSendReceivableUnit;
use App\Models\Entities\ReceivableUnit;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class IntegrationReceivableUnit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'receivable-unit:integrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send receivable units to CERC';

    public function handle(): bool
    {
        $counter = 0;

        ReceivableUnit::where('created_at', '<', Carbon::now()->startOfDay())
            ->where(function ($query) {
                return $query->whereColumn('integrated_at', '<', 'updated_at')
                    ->orWhereNull('integrated_at');
            })
            ->get(['id'])
            ->each(function ($receivableUnit) use (&$counter) {
                IntegrationSendReceivableUnit::dispatch($receivableUnit->id);
                $counter++;
            });


        Log::info("Sending {$counter} receivable units");

        return true;
    }
}
