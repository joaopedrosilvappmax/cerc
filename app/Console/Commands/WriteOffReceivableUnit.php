<?php

namespace App\Console\Commands;

use App\Models\Entities\ReceivableUnit;
use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class WriteOffReceivableUnit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'receivable-unit:write-off';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Write-off receivable units';

    public function handle(): bool
    {
        Log::info("Receivable units write-off running");

        ReceivableUnit::where('operation_type', '<>', OperationType::WRITE_OFF)
            ->where('settlement_date', '<', Carbon::now()->format('Y-m-d'))
            ->get()
            ->map(function ($receivableUnit) {
                $receivableUnit->update(['operation_type' => OperationType::WRITE_OFF]);
            });

        return true;
    }
}
