<?php

namespace App\Console\Commands;

use App\Models\Services\Reports\GenerateSyntheticConciliationService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GenerateSyntheticConciliation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'synthetic-conciliation:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate data to synthetic conciliation to CERC';

    public function handle(): bool
    {
        (new GenerateSyntheticConciliationService())->process();

        Log::info("Synthetic conciliation running");

        return true;
    }
}
