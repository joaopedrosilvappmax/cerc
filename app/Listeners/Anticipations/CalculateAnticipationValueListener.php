<?php

namespace App\Listeners\Anticipations;

use App\Events\Anticipations\AnticipationRegisterEvent;
use App\Models\Services\Anticipations\AnticipationCalculateValueService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class CalculateAnticipationValueListener implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Handle the event.
     *
     * @param AnticipationRegisterEvent $event
     * @return void
     */
    public function handle(AnticipationRegisterEvent $event)
    {
        try {
            (new AnticipationCalculateValueService())
                ->calculate($event->anticipation);
        } catch (\Exception $e) {
            Log::error('calculateAnticipationValueListener', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'anticipationId' => $event->anticipation->id
            ]);
        }
    }
}
