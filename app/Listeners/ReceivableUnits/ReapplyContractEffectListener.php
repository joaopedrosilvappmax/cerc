<?php

namespace App\Listeners\ReceivableUnits;

use App\Events\ReceivableUnits\ReceivableUnitUpdatedWithContractEffectEvent;
use App\Models\Services\Contracts\UpdatePaymentFromContractEffectServiceContract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class ReapplyContractEffectListener implements ShouldQueue
{
    protected $delay = 1;
    /**
     * Handle the event.
     *
     * @param ReceivableUnitUpdatedWithContractEffectEvent $event
     * @return void
     */
    public function handle(ReceivableUnitUpdatedWithContractEffectEvent $event)
    {
        try {
            app(UpdatePaymentFromContractEffectServiceContract::class)
                ->update($event->receivableUnit);
        } catch (\Exception $e) {
            Log::error('ReapplyContractEffectListener', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'anticipationId' => $event->receivableUnit->id
            ]);
        }
    }
}
