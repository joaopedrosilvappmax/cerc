<?php

namespace App\Traits;

use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

trait DailyReport
{
    public static function dailyCreated(): Collection
    {
        return self::whereDate('created_at', Carbon::now()->subDay())
            ->get();
    }

    public static function dailyUpdated(): Collection
    {
        return self::whereDate('updated_at', Carbon::now()->subDay())
            ->whereDate('created_at', '!=', Carbon::now()->subDay())
            ->where('operation_type', '!=', OperationType::WRITE_OFF)
            ->get();
    }

    public static function dailyIntegrated(): Collection
    {
        return self::whereDate('integrated_at', Carbon::now())
            ->get();
    }

    public static function dailyIntegratedAnticipation(): Collection
    {
        return self::whereDate('integrated_at', Carbon::now()->subDay())
            ->get();
    }

    public static function dailyWriteOff(): Collection
    {
        return self::whereDate('integrated_at', Carbon::now())
            ->where('operation_type', OperationType::WRITE_OFF)
            ->get();
    }

    public static function dailyWriteOffAnticipation(): Collection
    {
        return self::whereDate('integrated_at', Carbon::now()->subDay())
            ->where('operation_type', OperationType::WRITE_OFF)
            ->get();
    }

    public static function dailyApplied(): Collection
    {
        return self::whereDate('applied_at', Carbon::now()->subDay())
            ->get();
    }

    public static function dailyAnnulled(): Collection
    {
        return self::whereDate('created_at', Carbon::now()->subDay())
            ->where('committed_value', 0)
            ->get();
    }

    public static function dailyWithError(): Collection
    {
        return self::whereDate('created_at', Carbon::now()->subDay())
            ->whereNull('applied_at')
            ->get();
    }
}
