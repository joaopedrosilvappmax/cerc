<?php

namespace App\Providers;

use App\Events\Anticipations\AnticipationRegisterEvent;
use App\Events\ContractEffects\ContractEffectDeletedEvent;
use App\Events\ReceivableUnits\ReceivableUnitUpdatedWithContractEffectEvent;
use App\Listeners\Anticipations\CalculateAnticipationValueListener;
use App\Listeners\ContractEffects\CancelContractEffectAnticipationListener;
use App\Listeners\ReceivableUnits\ReapplyContractEffectListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ReceivableUnitUpdatedWithContractEffectEvent::class => [
            ReapplyContractEffectListener::class,
        ],
        AnticipationRegisterEvent::class => [
            CalculateAnticipationValueListener::class
        ],
        ContractEffectDeletedEvent::class => [
            CancelContractEffectAnticipationListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
