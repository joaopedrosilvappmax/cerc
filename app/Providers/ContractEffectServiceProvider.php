<?php

namespace App\Providers;

use App\Exceptions\BindException;
use App\Models\Services\ContractEffectOrders\ContractEffectOrderGetByExternalCashOutService;
use App\Models\Services\ContractEffectOrders\ContractEffectOrderGetByOrderService;
use App\Models\Services\ContractEffectOrders\ContractEffectOrderGetByPaymentNoAnticipationService;
use App\Models\Services\ContractEffectOrders\ContractEffectOrderNoSettledGetByOrdersService;
use App\Models\Services\ContractEffectOrders\ContractEffectOrderSettledGetByOrdersService;
use App\Models\Services\ContractEffectOrders\GetTotalContractEffectAnticipatedHolderChangeService;
use App\Models\Services\ContractEffects\Cancel\CancelAnticipationContractEffectOnusService;
use App\Models\Services\ContractEffects\Cancel\CancelContractEffectService;
use App\Models\Services\ContractEffects\DivisionRuleApplicators\DivisionRuleFixedValueApplicatorForHolderChangeService;
use App\Models\Services\ContractEffects\DivisionRuleApplicators\DivisionRuleFixedValueApplicatorForOnusService;
use App\Models\Services\ContractEffects\DivisionRuleApplicators\DivisionRulePercentageValueApplicatorForHolderChangeService;
use App\Models\Services\ContractEffects\DivisionRuleApplicators\DivisionRulePercentageValueApplicatorForOnusService;
use App\Models\Services\ContractEffects\Recalculate\RecalculateEffectContractCourtBlockService;
use App\Models\Services\ContractEffects\Recalculate\RecalculateEffectContractService;
use App\Models\Services\ContractEffects\ReceivableUnitContractEffectHolderChangeService;
use App\Models\Services\ContractEffects\ReceivableUnitContractEffectOnusService;
use App\Models\Services\Contracts\CancelAnticipationContractEffectServiceContract;
use App\Models\Services\Contracts\ContractEffectApplyServiceContract;
use App\Models\Services\Contracts\ContractEffectOrderGetByOrderServiceContract;
use App\Models\Services\Contracts\ContractEffectOrderGetByOrdersServiceContract;
use App\Models\Services\Contracts\ContractEffectOrderGetByPaymentServiceContract;
use App\Models\Services\Contracts\ContractEffectOrderGetByServiceContract;
use App\Models\Services\Contracts\ContractEffectReadFileServiceContract;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\DivisionRuleApplicatorForHolderChangeServiceContract;
use App\Models\Services\Contracts\DivisionRuleApplicatorForOnusServiceContract;
use App\Models\Services\Contracts\GetTotalContractEffectAnticipatedServiceContract;
use App\Models\Services\Contracts\RecalculateContractEffectContract;
use App\Support\EnumTypes\ContractEffectOrderType;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use Illuminate\Support\ServiceProvider;

class ContractEffectServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRecalculateContractEffect();

        $this->registerDivisionRuleApplicatorForOnus();

        $this->registerDivisionRuleApplicatorForHolderChange();

        $this->registerContractEffectApplyContract();

        $this->app->bind(CancelAnticipationContractEffectServiceContract::class, CancelAnticipationContractEffectOnusService::class);

        $this->app->bind(ContractEffectReadFileServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Contract Effect class name not found");
            }

            return $app->make($data['class']);
        });

        $this->app->bind(ContractEffectServiceContract::class, function ($app, $data) {
            if (! isset($data['effect_type'])) {
                throw new BindException("Contract effect class name not found");
            }

            $hasCommittedValue = isset($data['has_committed_value']) ? $data['has_committed_value'] : true;

            if (! $hasCommittedValue) {
                return $app->make(CancelContractEffectService::class);
            }

            if ($data['effect_type'] == ContractEffectType::HOLDER_CHANGE) {
                return $app->make(ReceivableUnitContractEffectHolderChangeService::class);
            }

            return $app->make(ReceivableUnitContractEffectOnusService::class);
        });

        $this->app->bind(ContractEffectOrderGetByOrdersServiceContract::class, function ($app, $data) {
            if (! isset($data['type'])) {
                throw new BindException("Contract effect class name not found");
            }

            if ($data['type'] == ContractEffectOrderType::NO_SETTLED) {
                return $app->make(ContractEffectOrderNoSettledGetByOrdersService::class);
            }

            return $app->make(ContractEffectOrderSettledGetByOrdersService::class);
        });

        $this->app->bind(ContractEffectOrderGetByPaymentServiceContract::class, function ($app, $data) {
            if (! isset($data['type']) || $data['type'] != ContractEffectOrderType::NO_ANTICIPATION) {
                throw new BindException("Contract effect order class name not found");
            }

            return $app->make(ContractEffectOrderGetByPaymentNoAnticipationService::class);
        });

        $this->app->bind(GetTotalContractEffectAnticipatedServiceContract::class,
            GetTotalContractEffectAnticipatedHolderChangeService::class);

        $this->app->bind(ContractEffectOrderGetByServiceContract::class,
            ContractEffectOrderGetByExternalCashOutService::class);

        $this->app->bind(ContractEffectOrderGetByOrderServiceContract::class,
            ContractEffectOrderGetByOrderService::class);
    }

    private function registerRecalculateContractEffect()
    {
        $this->app->bind(RecalculateContractEffectContract::class, function ($app, $data) {
            if (isset($data['type']) && $data['type'] == ContractEffectType::COURT_BLOCK) {
                return $app->make(RecalculateEffectContractCourtBlockService::class);
            }

            return $app->make(RecalculateEffectContractService::class);
        });
    }

    private function registerDivisionRuleApplicatorForOnus()
    {
        $this->app->bind(DivisionRuleApplicatorForOnusServiceContract::class, function ($app, $data) {
            if (! isset($data['division_rule'])) {
                throw new BindException("Division Rule Applicator class name not found");
            }

            if ($data['division_rule'] == DivisionRule::PERCENTAGE_VALUE) {
                return $app->make(DivisionRulePercentageValueApplicatorForOnusService::class);
            }

            return $app->make(DivisionRuleFixedValueApplicatorForOnusService::class);
        });
    }

    private function registerDivisionRuleApplicatorForHolderChange()
    {
        $this->app->bind(DivisionRuleApplicatorForHolderChangeServiceContract::class, function ($app, $data) {
            if (! isset($data['division_rule'])) {
                throw new BindException("Division Rule Applicator For Holder Change class name not found");
            }

            if ($data['division_rule'] == DivisionRule::PERCENTAGE_VALUE) {
                return $app->make(DivisionRulePercentageValueApplicatorForHolderChangeService::class);
            }

            return $app->make(DivisionRuleFixedValueApplicatorForHolderChangeService::class);
        });
    }

    private function registerContractEffectApplyContract()
    {
        $this->app->bind(ContractEffectApplyServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Apply Contract Effect class name not found");
            }

            return $app->make($data['class']);
        });
    }
}
