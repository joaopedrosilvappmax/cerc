<?php

namespace App\Providers;

use App\Exceptions\BindException;
use App\Models\Services\Contracts\NotificationServiceContract;
use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(NotificationServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Notification class name not found");
            }

            return $app->make($data['class']);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
