<?php

namespace App\Providers;

use App\Exceptions\BindException;
use App\Models\Services\Contracts\WebhookResolverServiceContract;
use Illuminate\Support\ServiceProvider;

class WebhookServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(WebhookResolverServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Webhook resolver class name not found");
            }

            return $app->make($data['class']);
        });
    }
}
