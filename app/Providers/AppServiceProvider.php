<?php

namespace App\Providers;

use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Observers\PaymentObserver;
use App\Observers\ReceivableUnitObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadObservers();
        $this->loadHelpers();
    }

    private function loadObservers(): void
    {
        ReceivableUnit::observe(ReceivableUnitObserver::class);
        Payment::observe(PaymentObserver::class);
    }

    private function loadHelpers(): void
    {
        require app_path('Support/Helpers/Http.php');
    }
}
