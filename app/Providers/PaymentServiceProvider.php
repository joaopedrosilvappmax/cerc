<?php

namespace App\Providers;

use App\Exceptions\BindException;
use App\Models\Services\Contracts\PaymentConfirmationServiceContract;
use App\Models\Services\Contracts\PaymentCreateFromServiceContract;
use App\Models\Services\Contracts\PaymentGetSettledServiceContract;
use App\Models\Services\Contracts\PaymentGetTotalValueServiceContract;
use App\Models\Services\Contracts\UpdatePaymentFromContractEffectServiceContract;
use App\Models\Services\Payments\PaymentGetSettledNoHolderService;
use App\Models\Services\Payments\PaymentGetTotalValueNoSettledHolderChangeService;
use App\Models\Services\Payments\PaymentGetTotalValueNoSettledNoHolderService;
use App\Models\Services\Payments\PaymentGetTotalValueSettledHolderChangeService;
use App\Models\Services\Payments\PaymentGetTotalValueSettledNoHolderService;
use App\Models\Services\Payments\UpdatePaymentFromContractEffectService;
use App\Support\EnumTypes\PaymentTotalValueCalculateType;
use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PaymentGetTotalValueServiceContract::class, function ($app, $data) {
            if (! isset($data['calculate_type'])) {
                throw new BindException("Payment get total calculate type not found");
            }

            if ($data['calculate_type'] == PaymentTotalValueCalculateType::NO_SETTLED_HOLDER_CHANGE) {
                return $app->make(PaymentGetTotalValueNoSettledHolderChangeService::class);
            }

            if ($data['calculate_type'] == PaymentTotalValueCalculateType::SETTLED_HOLDER_CHANGE) {
                return $app->make(PaymentGetTotalValueSettledHolderChangeService::class);
            }

            if ($data['calculate_type'] == PaymentTotalValueCalculateType::NO_HOLDER) {
                return $app->make(PaymentGetTotalValueNoSettledNoHolderService::class);
            }

            return $app->make(PaymentGetTotalValueSettledNoHolderService::class);
        });

        $this->app->bind(UpdatePaymentFromContractEffectServiceContract::class, UpdatePaymentFromContractEffectService::class);

        $this->app->bind(PaymentGetSettledServiceContract::class, PaymentGetSettledNoHolderService::class);

        $this->app->bind(PaymentConfirmationServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException('Invalid class', 400);
            }

            return $app->make($data['class']);
        });

        $this->app->bind(PaymentCreateFromServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException('Invalid class', 400);
            }

            return $app->make($data['class']);
        });
    }
}
