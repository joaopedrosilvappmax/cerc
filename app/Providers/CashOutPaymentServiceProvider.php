<?php

namespace App\Providers;

use App\Exceptions\BindException;
use App\Exceptions\NotFoundException;
use App\Models\Entities\Payment;
use App\Models\Services\CashOutPayment\CashOutPaymentAnticipationRegisterService;
use App\Models\Services\CashOutPayment\CashOutPaymentSettledRegisterService;
use App\Models\Services\Contracts\CashOutPaymentRegisterServiceContract;
use App\Models\Services\Contracts\CashOutRefuseServiceContract;
use App\Support\EnumTypes\CashOutPaymentType;
use Exception;
use Illuminate\Support\ServiceProvider;

class CashOutPaymentServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CashOutPaymentRegisterServiceContract::class, function ($app, $data) {
            if (!isset($data['type'])) {
                throw new BindException("Register CashOutPayment class name not found");
            }

            if ($data['type'] == CashOutPaymentType::SETTLED) {
                return $app->make(CashOutPaymentSettledRegisterService::class);
            }

            return $app->make(CashOutPaymentAnticipationRegisterService::class);
        });
    }
}
