<?php

namespace App\Providers;

use App\Exceptions\BindException;
use App\Models\Services\Authentications\AuthenticationService;
use App\Models\Services\Contracts\AuthenticationServiceContract;
use App\Models\Services\Contracts\ExportServiceContract;
use App\Models\Services\Contracts\GuaranteeProcessServiceContract;
use App\Models\Services\Contracts\IndexServiceContract;
use App\Models\Services\Contracts\ReceivableUnitPaymentServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Models\Services\Contracts\StorageServiceContract;
use App\Models\Services\Contracts\WebhookServiceContract;
use App\Models\Services\Storages\LocalStorageService;
use App\Models\Services\Webhooks\WebhookService;
use App\Support\Adapters\Contracts\DataRegisterAdapterContract;
use Illuminate\Support\ServiceProvider;

class ServiceLayerProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RegisterServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Data register class name not found");
            }

            return $app->make($data['class']);
        });

        $this->app->bind(IndexServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Index class name not found");
            }

            return $app->make($data['class']);
        });

        $this->app->bind(ExportServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Export class name not found");
            }

            return $app->make($data['class']);
        });

        $this->app->bind(ReceivableUnitPaymentServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Receivable Unit Payment Service not found");
            }

            return $app->make($data['class']);
        });

        $this->app->bind(GuaranteeProcessServiceContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Guarantee service contract class name not found");
            }

            return $app->make($data['class']);
        });

        $this->app->bind(StorageServiceContract::class,LocalStorageService::class);
        $this->app->bind(AuthenticationServiceContract::class, AuthenticationService::class);
        $this->app->bind(WebhookServiceContract::class, WebhookService::class);

        $this->app->bind(DataRegisterAdapterContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Adapter class name not found");
            }

            return $app->make($data['class']);
        });
    }
}
