<?php

namespace App\Providers;

use App\Exceptions\BindException;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\DataTransferObjects\Contracts\FilterContract;
use Illuminate\Support\ServiceProvider;

class DataTransferObjectLayerProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DataRegisterContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("RegisterData class name not found");
            }

            return $app->make($data['class']);
        });

        $this->app->bind(FilterContract::class, function ($app, $data) {
            if (! isset($data['class'])) {
                throw new BindException("Filter class name not found");
            }

            return $app->make($data['class']);
        });
    }
}
