<?php

namespace App\Providers;

use App\Http\Responses\AuthenticationResponse;
use App\Http\Responses\Contracts\AuthenticationResponseContract;
use App\Http\Responses\Contracts\IntegrationResponseContract;
use App\Http\Responses\IntegrationResponse;
use App\Models\Services\ContractEffects\ContractEffectAppmaxSenderContractEffectService;
use App\Models\Services\Contracts\AppmaxIntegrationContractEffectServiceContract;
use App\Models\Services\Contracts\IntegrationBodyServiceContract;
use App\Models\Services\Contracts\IntegrationServiceContract;
use Exception;
use Illuminate\Support\ServiceProvider;

class HttpServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(IntegrationResponseContract::class, IntegrationResponse::class);
        $this->app->bind(AuthenticationResponseContract::class, AuthenticationResponse::class);
        $this->app->bind(AppmaxIntegrationContractEffectServiceContract::class, ContractEffectAppmaxSenderContractEffectService::class);

        $this->app->bind(IntegrationServiceContract::class, function ($app, $data) {
            if (! $data || ! $data['class']) {
                throw new Exception("Integration service contract class name not found");
            }

            return $app->make($data['class']);
        });

        $this->app->bind(IntegrationBodyServiceContract::class, function ($app, $data) {
            if (! $data || ! $data['class']) {
                throw new Exception("Integration body service contract class name not found");
            }

            return $app->make($data['class']);
        });
    }
}
