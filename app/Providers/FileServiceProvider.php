<?php

namespace App\Providers;

use App\Models\Services\Contracts\FileServiceContract;
use App\Models\Services\Files\S3FileService;
use Illuminate\Support\ServiceProvider;

class FileServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FileServiceContract::class, S3FileService::class);
    }
}
