<?php

namespace App\Support\Traits;

trait CastData
{
    public function castObjectToArrayRecursive($data): array
    {
        $result = [];

        $data = get_object_vars($data);

        foreach ($data as $key => $value) {
            if (is_object($value)) {
                $value = self::castObjectToArrayRecursive($value);
            }

            $result[$key] = $value;
        }

        return $result;
    }
}
