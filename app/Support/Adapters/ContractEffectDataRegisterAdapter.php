<?php

namespace App\Support\Adapters;

use App\Support\Adapters\Contracts\DataRegisterAdapterContract;
use App\Support\DataTransferObjects\ContractEffects\ContractEffectDataRegister;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use Carbon\Carbon;

class ContractEffectDataRegisterAdapter implements DataRegisterAdapterContract
{
    public function addData(array $data): ContractEffectDataRegister
    {
        $dataRegister = [
            'event_datetime' => Carbon::createFromDate($data['dataHoraEvento'])->format('Y-m-d H:i:s'),
            'external_reference_receivable_unit_id' => $data['evento']['referenciaExternaUnidadeRecebivel'] ?? null,
            'external_contract_id' => $data['evento']['efeitosContrato']['identificadorContrato'],
            'contract_generator_event_id' => $data['evento']['identificadorContratoGeradorEvento'],
            'protocol' => $data['evento']['efeitosContrato']['protocolo'],
            'indicator' => $data['evento']['efeitosContrato']['indicadorEfeitosContrato'],
            'accrediting_company_document_number' => $data['evento']['cnpjCredenciadora'],
            'constituted' => $data['evento']['constituicaoUnidadeRecebivel'],
            'final_recipient_document_number' => $data['evento']['documentoUsuarioFinalRecebedor'],
            'code_arrangement_payment' => $data['evento']['codigoArranjoPagamento'],
            'settlement_date' => $data['evento']['dataLiquidacao'],
            'registration_entity_document_number' => $data['evento']['efeitosContrato']['cnpjEntidadeRegistradora'],
            'holder_beneficiary_document_number' => $data['evento']['efeitosContrato']['cnpjBeneficiarioTitular'],
            'effect_type' => $data['evento']['efeitosContrato']['tipoEfeito'],
            'division_rule' => $data['evento']['efeitosContrato']['regraDivisao'],
            'committed_value' => $data['evento']['efeitosContrato']['valorComprometido'], //CAST INT
            'holder_document_number' => $data['evento']['efeitosContrato']['domicilioPagamento']['documentoTitularDomicilio'],
            'holder_name' => $data['evento']['efeitosContrato']['domicilioPagamento']['nomeTitularDomicilio'] ?? null,
            'bank_account_type' => $data['evento']['efeitosContrato']['domicilioPagamento']['tipoConta'],
            'compe' => $data['evento']['efeitosContrato']['domicilioPagamento']['compe'] ?? null,
            'ispb' => $data['evento']['efeitosContrato']['domicilioPagamento']['ispb'],
            'bank_branch' => $data['evento']['efeitosContrato']['domicilioPagamento']['agencia'],
            'bank_account' => $data['evento']['efeitosContrato']['domicilioPagamento']['numeroConta'],
            'webhook_request_id' => $data['webhook_request_id'],
        ];

        return app(DataRegisterContract::class, ['class' => ClassBindResolution::CONTRACT_EFFECT_DATA])
            ->addData($dataRegister);
    }
}
