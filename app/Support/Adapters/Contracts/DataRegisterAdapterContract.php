<?php

namespace App\Support\Adapters\Contracts;

use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

interface DataRegisterAdapterContract
{
    public function addData(array $data): DataRegisterContract;
}
