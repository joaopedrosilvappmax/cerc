<?php

namespace App\Support\ValueObjects;

use App\Support\EnumTypes\Math;
use App\Support\ValueObjects\Contracts\MoneyContract;

class Money implements MoneyContract
{
    private mixed $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function toDecimal(): float
    {
        return round($this->value * Math::DECIMAL, 2);
    }

    public function toMoneyFormat(): string
    {
        return 'R$ ' . number_format($this->toDecimal(), 2, ',', '.');
    }

    public function toInteger(): int
    {
        return (int) round($this->value * 100, 2);
    }
}
