<?php

namespace App\Support\ValueObjects;

use App\Models\Entities\Bank as Model;

class Bank
{
    protected ?int $id = null;
    protected ?int $compe = null;
    protected ?int $ispb = null;
    protected ?Model $bank = null;

    public function __construct(int $compe = null)
    {
        if ($compe) {
            $this->setByCompe($compe);
        }
    }

    public function setByCompe(int $compe): Bank
    {
        $bank = Model::where('compe', $compe)->first();

        if (! $bank) {
            return $this;
        }

        $this->bank = $bank;
        $this->id = $bank->id;
        $this->compe = $bank->compe;
        $this->ispb = $bank->ispb;

        return $this;
    }

    public function getFormattedIspb($length = 8): string
    {
        return str_pad((string) $this->ispb , $length , '0' , STR_PAD_LEFT);
    }
}
