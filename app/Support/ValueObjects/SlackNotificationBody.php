<?php

namespace App\Support\ValueObjects;

class SlackNotificationBody
{
    public static function header(string $text): array
    {
        return [
            "type" => "header",
            "text" => [
                "type" => "plain_text",
                "text" => "$text",
                "emoji" => true
            ],
        ];
    }

    public static function divider(): array
    {
        return [
            "type" => "divider",
        ];
    }

    public static function section(string $text): array
    {
        return [
            "type" => "section",
            "fields" => [
                [
                    "type" => "mrkdwn",
                    "text" => $text,
                ],
            ],
        ];
    }

    public static function columns(string $title, string $text): array
    {
        return [
            "type" => "section",
            "fields" => [
                [
                    "type" => "mrkdwn",
                    "text" => "*$title*",
                ],
                [
                    "type" => "mrkdwn",
                    "text" => "$text",
                ],
            ],
        ];
    }
}
