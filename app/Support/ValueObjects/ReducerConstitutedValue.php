<?php

namespace App\Support\ValueObjects;


use App\Models\Entities\ReceivableUnitOrder;
use App\Support\EnumTypes\ContractEffectType;
use Illuminate\Database\Eloquent\Collection;

class ReducerConstitutedValue
{
    private int $guaranteeValue;
    private int $reversalValue;
    private int $debitValue;
    private ReceivableUnitOrder $receivableUnitOrder;
    private int $contractEffectsHolderChangeTotalValue;

    public function __construct() {
        $this->guaranteeValue = 0;
        $this->reversalValue = 0;
        $this->debitValue = 0;
    }

    public function addContractEffectsHolderChangeTotalValue(Collection $contractEffectOrdersHolderChange): static
    {
        if ($contractEffectOrdersHolderChange->isEmpty()) {
            $this->contractEffectsHolderChangeTotalValue = 0;

            return $this;
        }

        $this->contractEffectsHolderChangeTotalValue = $contractEffectOrdersHolderChange
            ->filter(function($contractEffectOrder) {
                return $contractEffectOrder->contractEffect->effect_type == ContractEffectType::HOLDER_CHANGE;
            })
            ->sum('value');

        return $this;
    }

    public function addReceivableUnitOrder(ReceivableUnitOrder $receivableUnitOrder): static
    {
        $this->receivableUnitOrder = $receivableUnitOrder;

        return $this;
    }

    public function addReversalValue($reversalValue): static
    {
        if ($this->receivableUnitOrder->hasDebitValue() && $reversalValue) {
            $this->reversalValue = $this->receivableUnitOrder->getRawOriginal('value')
                - $this->receivableUnitOrder->debit_value;

            return $this;
        }

        $this->reversalValue = $reversalValue;

        return $this;
    }

    public function addGuaranteeValue($guaranteeValue): static
    {
        if ($this->receivableUnitOrder->hasDebitValue() && $guaranteeValue) {
            $this->guaranteeValue = $this->receivableUnitOrder->getRawOriginal('value')
                - $this->receivableUnitOrder->debit_value;

            return $this;
        }

        $this->guaranteeValue = $guaranteeValue;

        return $this;
    }

    public function addDebitValue($debitValue): static
    {
        if (! empty($debitValue)) {
             $this->debitValue = $debitValue - $this->contractEffectsHolderChangeTotalValue;
        }

        return $this;
    }

    public function getValueToReduce(): int
    {
        if (! $this->receivableUnitOrder || $this->receivableUnitOrder->anticipation_id) {
            return 0;
        }

        if ($this->reversalValue) {
            return ! $this->receivableUnitOrder->guarantee_value
                ? $this->reversalValue - $this->contractEffectsHolderChangeTotalValue
                : 0;
        }

        if ($this->guaranteeValue) {
            return ! $this->receivableUnitOrder->reversal_value
                ? $this->guaranteeValue - $this->contractEffectsHolderChangeTotalValue
                : 0;
        }

        if (! empty($this->debitValue)) {
            return $this->debitValue - $this->contractEffectsHolderChangeTotalValue;
        }

        return 0;
    }
}
