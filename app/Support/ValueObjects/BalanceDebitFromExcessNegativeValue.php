<?php

namespace App\Support\ValueObjects;

use App\Models\Entities\ReceivableUnitOrder;
use App\Support\EnumTypes\Math;

class BalanceDebitFromExcessNegativeValue
{
    private int $value;
    private ?int $reversalValue;
    private ?int $guaranteeValue;
    private ReceivableUnitOrder $receivableUnitOrder;

    public function __construct() {
        $this->value = 0;
    }

    public function addReceivableUnitOrder(ReceivableUnitOrder $receivableUnitOrder): static
    {
        $this->receivableUnitOrder = $receivableUnitOrder;

        return $this;
    }

    public function addGuaranteeValue($guaranteeValue): static
    {
        $this->guaranteeValue = $guaranteeValue;

        return $this;
    }

    public function addReversalValue($reversalValue): static
    {
        $this->reversalValue = $reversalValue;

        return $this;
    }

    public function getCalculatedValue()
    {
        if ($this->receivableUnitOrder->reversal_value > 0) {
            $this->reversalValue = 0;
        }

        if ($this->receivableUnitOrder->value && $this->reversalValue) {
            return $this->reversalValue - $this->receivableUnitOrder->value;
        }

        if ($this->receivableUnitOrder->value && $this->guaranteeValue) {
            return $this->guaranteeValue - $this->receivableUnitOrder->value;
        }

        return 0;
    }

    public function toDecimal(): float
    {
        return round($this->value * Math::DECIMAL, 2);
    }
}
