<?php

namespace App\Support\ValueObjects;

class DiscordNotificationBody
{
    const APPMAX_DEFAULT_COLOR = 10185466;

    public static function field(string $name, string $value): array
    {
        return [
            'name' => $name,
            'value' => $value
        ];
    }
}
