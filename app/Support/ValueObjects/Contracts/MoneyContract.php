<?php

namespace App\Support\ValueObjects\Contracts;

interface MoneyContract
{
    public function toDecimal(): float;
}
