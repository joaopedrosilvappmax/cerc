<?php

namespace App\Support\ValueObjects;

use App\Models\Entities\Payment;
use App\Models\Services\Contracts\IntegrationBodyServiceContract;
use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ReceivableUnitIntegrationBodyV14 implements IntegrationBodyServiceContract
{
    public function mountBody(Model $entity): array
    {
        return [
            'tipoOperacao' => $entity->operation_type,
            'referenciaExterna' => $entity->id,
            'cnpjCredenciadora' => $entity->accrediting_company_document_number,
            'documentoUsuarioFinalRecebedor' => $entity->final_recipient_document_number,
            'documentoTitular' => $entity->holder_document_number,
            'codigoArranjoPagamento' => $entity->code_arrangement_payment,
            'dataLiquidacao' => $entity->settlement_date,
            'valorBrutoTotal' => (new Money($entity->total_gross_value))->toDecimal(),
            'valorConstituidoTotal' => (new Money($entity->total_constituted_value))->toDecimal(),
            'valorConstituidoPreContratado' => (new Money($entity->pre_contracted_value))->toDecimal(),
            'valorBloqueado' => (new Money($entity->blocked_value))->toDecimal(),
            'pagamentos' => $this->mountPayments($entity)
        ];
    }

    private function mountPayments(Model $entity): array
    {
        $payAddress = [];
        $operationType = $entity->operation_type;

        $entity->payments->each(function ($payment) use (&$payAddress, $operationType) {
            $paymentData = [
                'DomicilioPagamento' => [
                    'numeroDocumentoTitular' => $payment->holder_document_number,
                    'tipoConta' => $payment->bank_account_type,
                    'ispb' => $payment->ispb,
                    'compe' => $payment->compe,
                    'agencia' => $payment->bank_branch,
                    'numeroConta' => $payment->bank_account
                ],
                'valorAPagar' => (new Money($payment->payment_value))->toDecimal()
            ];

            if ($this->isSettled($operationType)) {
               $payment->update(['paid_at' => Carbon::now()]);
               $payment->refresh();
            }

            if ($this->hasPaidAt($payment)) {
                $paymentData['dataLiquidacaoEfetiva'] = $payment->paid_at;
                $paymentData['valorLiquidacaoEfetiva'] = (new Money($payment->payment_value))->toDecimal();
            }

            $payAddress[] = $paymentData;
        });

        return $payAddress;
    }

    private function isSettled(string $operationType): bool
    {
        return $operationType == OperationType::WRITE_OFF;
    }

    private function hasPaidAt(Payment $payment)
    {
        return $payment->paid_at;
    }
}
