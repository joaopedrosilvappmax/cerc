<?php

namespace App\Support\ValueObjects;


class Cnpj
{
    private int $value;

    public function __construct(int $value) {
        $this->value = $value;
    }

    public function getISBP(): float
    {
        return substr($this->value, 0, 8);
    }
}
