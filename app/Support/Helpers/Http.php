<?php

use App\Support\EnumTypes\VersionsCERC;

if (! function_exists("simplify_error_code")) {
    function simplify_error_code($code)
    {
        $code = (int) $code;
        return $code > 500 || $code < 100 ? 422 : $code;
    }
}

if (! function_exists("cerc_route_by_config_version")) {
    function cerc_route_by_config_version($route)
    {
        if (config('cerc.api_version') != VersionsCERC::V14) {
            return config('cerc.api_version') . "/{$route}";
        }
        return $route;
    }
}
