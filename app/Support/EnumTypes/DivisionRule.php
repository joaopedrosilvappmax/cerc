<?php


namespace App\Support\EnumTypes;


class DivisionRule
{
    const FIXED_VALUE = 1;
    const PERCENTAGE_VALUE = 2;
}
