<?php

namespace App\Support\EnumTypes;

class VersionsCERC
{
    const V14 = 'v14';
    const V15 = 'v15';
}
