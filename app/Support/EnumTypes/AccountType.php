<?php

namespace App\Support\EnumTypes;

class AccountType
{
    const CHECKING_ACCOUNT = 'CC';
    const DEPOSIT_ACCOUNT = 'CD';
    const PAYMENT_ACCOUNT = 'PG';
    CONST SAVINGS_ACCOUNT = 'PP';
}
