<?php

namespace App\Support\EnumTypes;

use App\Models\Services\ContractEffects\ReceivableUnitContractEffectHolderChangeService;
use App\Models\Services\ContractEffects\ReceivableUnitContractEffectOnusService;

class EffectClassBindResolution
{
    const HOLDER_CHANGE = ReceivableUnitContractEffectHolderChangeService::class;

    const ONUS = ReceivableUnitContractEffectOnusService::class;
}
