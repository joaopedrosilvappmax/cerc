<?php

namespace App\Support\EnumTypes;

class PaymentTotalValueCalculateType
{
    const SETTLED_NO_HOLDER = 'settled-no-holder';
    const NO_HOLDER = 'no-holder';
    const NO_SETTLED_HOLDER_CHANGE = 'no-settled-holder-change';
    const SETTLED_HOLDER_CHANGE = 'settled-holder-change';
}
