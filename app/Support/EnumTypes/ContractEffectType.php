<?php

namespace App\Support\EnumTypes;

class ContractEffectType
{
    const HOLDER_CHANGE = 1;

    const ONUS_FIDUCIARY_ASSIGNMENT = 2;

    const ONUS_OTHERS = 3;

    const COURT_BLOCK = 4;

    public static function allOnus(): array
    {
        return [
            self::ONUS_FIDUCIARY_ASSIGNMENT,
            self::ONUS_OTHERS,
            self::COURT_BLOCK
        ];
    }
}
