<?php

namespace App\Support\EnumTypes;

use App\Models\Services\Notifications\DiscordNotificationService;
use App\Models\Services\Notifications\SlackNotificationService;

class NotificationSenderType
{
    const DISCORD = 'discord';
    const SLACK = 'slack';

    public static function getBindedClassByType(string $type): ?string
    {
        return match ($type) {
            self::DISCORD => DiscordNotificationService::class,
            self::SLACK => SlackNotificationService::class,
            default => null,
        };
    }
}
