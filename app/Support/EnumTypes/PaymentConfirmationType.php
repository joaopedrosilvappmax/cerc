<?php

namespace App\Support\EnumTypes;

class PaymentConfirmationType
{
    const ANTICIPATION = 'anticipation';
    const ANTICIPATION_HOLDER = 'anticipation-holder';
    const AVAILABLE = 'available';

    public static function getBindServiceClass($type): ?string
    {
        switch ($type) {
            case self::ANTICIPATION:
                return ClassBindResolution::PAYMENT_CONFIRMATION_ANTICIPATION;

            case self::ANTICIPATION_HOLDER:
                return ClassBindResolution::PAYMENT_CONFIRMATION_ANTICIPATION_HOLDER;

            case self::AVAILABLE:
                return ClassBindResolution::PAYMENT_CONFIRMATION_AVAILABLE;

            default:
                return null;
        }
    }
}
