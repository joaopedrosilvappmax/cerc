<?php

namespace App\Support\EnumTypes;

class CompanyType
{
    const PLATFORM = 1;
    const INTERNAL = 2;
}
