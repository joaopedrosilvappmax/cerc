<?php

namespace App\Support\EnumTypes;

class ReceivableUnitConstituteStatus
{
    const CONSTITUTED = 1;
    const TO_CONSTITUTE = 2;
}
