<?php

namespace App\Support\EnumTypes;

use App\Models\Services\ContractEffects\ContractEffectWebhookResolverService;
use App\Models\Services\ReceivableUnits\ReceivableUnitWebhookResolverService;

class WebhookEventType
{
    const CONTRACT_EFFECT = 'efeitoContrato';
    const RECEIVABLE_UNIT = 'unidadeRecebivel';

    public static function getBindServiceClass(string $type): ?string
    {
        return match ($type) {
            self::CONTRACT_EFFECT => ContractEffectWebhookResolverService::class,
            self::RECEIVABLE_UNIT => ReceivableUnitWebhookResolverService::class,
            default => null,
        };
    }
}
