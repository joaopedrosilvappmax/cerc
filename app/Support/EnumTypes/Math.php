<?php

namespace App\Support\EnumTypes;

class Math
{
    const DIVISION_PERCENTAGE_INTEGER = 0.0001;
    const DECIMAL = 0.01;
}
