<?php

namespace App\Support\EnumTypes;

class CashOutPaymentType
{
    const ANTICIPATION = 1;

    const SETTLED = 2;
}
