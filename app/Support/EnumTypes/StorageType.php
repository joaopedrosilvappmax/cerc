<?php


namespace App\Support\EnumTypes;


class StorageType
{
    const LOCAL = 'local';
    const S3 = 's3';
}
