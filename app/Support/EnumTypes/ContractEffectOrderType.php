<?php

namespace App\Support\EnumTypes;

class ContractEffectOrderType
{
    const NO_SETTLED = 'no-settled';
    const SETTLED = 'settled';
    const NO_ANTICIPATION = 'no-anticipation';
}
