<?php

namespace App\Support\EnumTypes;

class RoutesCERC
{
    const RECEIVABLE_UNITS = 'unidades_recebiveis';
    const ANTICIPATION = 'pos_contratadas';
    const COMPANIES = 'estabelecimento';
    const SCHEDULE_CONCILIATION = 'conciliacao/agenda';
}
