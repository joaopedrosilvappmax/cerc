<?php

namespace App\Support\EnumTypes;

class Status
{
    const ACTIVE = 1;
    const INACTIVE = 2;
}
