<?php

namespace App\Support\EnumTypes;

class CompanyDocumentType
{
    const INDIVIDUAL = "1";
    const COMPANY = "2";
}
