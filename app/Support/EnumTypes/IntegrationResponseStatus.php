<?php

namespace App\Support\EnumTypes;

use App\Models\Entities\Company;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\SyntheticConciliationRecord;
use Illuminate\Database\Eloquent\Model;
use JetBrains\PhpStorm\Pure;

class IntegrationResponseStatus
{
    const SUCCESS = 0;
    const FAILED = 1;

    const RECEIVABLE_UNIT_INVALID_OPERATION = "102802";
    const RECEIVABLE_UNIT_CREATED = "102803";
    const ESTABLISHMENT_INVALID_OPERATION = "101802";
    const ESTABLISHMENT_CREATED = "101803";
    const ANTICIPATION_INVALID_OPERATION = "103802";
    const ANTICIPATION_CREATED = "103803";
    const SCHEDULE_CONCILIATION_INVALID_OPERATION = "110802";
    const SCHEDULE_CONCILIATION_CREATED = "110803";

    public static function receivableUnitCreated(): array
    {
        return [
            self::RECEIVABLE_UNIT_INVALID_OPERATION,
            self::RECEIVABLE_UNIT_CREATED
        ];
    }

    public static function establishmentCreated(): array
    {
        return [
            self::ESTABLISHMENT_INVALID_OPERATION,
            self::ESTABLISHMENT_CREATED,
        ];
    }

    public static function anticipationCreated(): array
    {
        return [
            self::ANTICIPATION_INVALID_OPERATION,
            self::ANTICIPATION_CREATED,
        ];
    }

    public static function scheduleConciliationCreated(): array
    {
        return [
            self::SCHEDULE_CONCILIATION_INVALID_OPERATION,
            self::SCHEDULE_CONCILIATION_CREATED,
        ];
    }

    #[Pure] public static function createdByEntity(Model $entity): array
    {
        if ($entity instanceof Company) {
            return self::establishmentCreated();
        }

        if ($entity instanceof ReceivableUnit) {
            return self::receivableUnitCreated();
        }

        if ($entity instanceof SyntheticConciliationRecord) {
            return self::scheduleConciliationCreated();
        }

        return self::anticipationCreated();
    }
}
