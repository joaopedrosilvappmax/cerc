<?php

namespace App\Support\EnumTypes;

class BalanceType
{
    const CREDIT = 'C';
    const DEBIT = 'D';
}
