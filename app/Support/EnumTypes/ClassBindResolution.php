<?php

namespace App\Support\EnumTypes;

use App\Models\Services\Anticipations\AnticipationRegisterService;
use App\Models\Services\Balances\BalanceRegisterService;
use App\Models\Services\Companies\CompanyRegisterService;
use App\Models\Services\ContractEffects\Apply\ContractEffectApplyS3FileService;
use App\Models\Services\ContractEffects\Apply\ContractEffectApplyWebhookService;
use App\Models\Services\ContractEffects\ContractEffectReadFileService;
use App\Models\Services\ContractEffects\ContractEffectReadS3BucketFileService;
use App\Models\Services\Guarantee\GuaranteeProcessService;
use App\Models\Services\Payments\PaymentConfirmationAnticipationHolderService;
use App\Models\Services\Payments\PaymentConfirmationAnticipationService;
use App\Models\Services\Payments\PaymentConfirmationAvailableService;
use App\Support\Adapters\ContractEffectDataRegisterAdapter;
use App\Models\Services\ContractEffects\ContractEffectRegisterService;
use App\Models\Services\ReceivableUnits\ReceivableUnitRegisterService;
use App\Support\DataTransferObjects\Anticipations\AnticipationDataRegister;
use App\Support\DataTransferObjects\Balances\BalanceDataRegister;
use App\Support\DataTransferObjects\CashOutPayments\CashOutPaymentsDataRegister;
use App\Support\DataTransferObjects\Companies\CompanyDataRegister;
use App\Support\DataTransferObjects\ContractEffects\ContractEffectDataRegister;
use App\Support\DataTransferObjects\Guarantees\GuaranteesDataRegister;
use App\Support\DataTransferObjects\ReceivableUnits\ReceivableUnitDataRegister;
use App\Support\ValueObjects\ReceivableUnitIntegrationBodyV14;

class ClassBindResolution
{
    const COMPANY_DATA = CompanyDataRegister::class;
    const COMPANY_SERVICE = CompanyRegisterService::class;

    const RECEIVABLE_UNIT_DATA = ReceivableUnitDataRegister::class;
    const RECEIVABLE_UNIT_SERVICE = ReceivableUnitRegisterService::class;
    const RECEIVABLE_UNIT_INTEGRATION_BODY_V14 = ReceivableUnitIntegrationBodyV14::class;

    const ANTICIPATION_DATA = AnticipationDataRegister::class;
    const ANTICIPATION_SERVICE = AnticipationRegisterService::class;

    const CONTRACT_EFFECT_DATA = ContractEffectDataRegister::class;
    const CONTRACT_EFFECT_DATA_ADAPTER = ContractEffectDataRegisterAdapter::class;
    const CONTRACT_EFFECT_SERVICE = ContractEffectRegisterService::class;
    const CONTRACT_EFFECT_READ_FILE = ContractEffectReadFileService::class;
    const CONTRACT_EFFECT_READ_S3BUCKET_FILE = ContractEffectReadS3BucketFileService::class;
    const CONTRACT_EFFECT_APPLY_WEBHOOK = ContractEffectApplyWebhookService::class;
    const CONTRACT_EFFECT_APPLY_S3 = ContractEffectApplyS3FileService::class;

    const BALANCE_DATA = BalanceDataRegister::class;
    const BALANCE_SERVICE = BalanceRegisterService::class;

    const GUARANTEE_SERVICE = GuaranteeProcessService::class;
    const GUARANTEE_DATA = GuaranteesDataRegister::class;

    const CASH_OUT_PAYMENT_DATA = CashOutPaymentsDataRegister::class;

    const PAYMENT_CONFIRMATION_ANTICIPATION = PaymentConfirmationAnticipationService::class;
    const PAYMENT_CONFIRMATION_ANTICIPATION_HOLDER = PaymentConfirmationAnticipationHolderService::class;
    const PAYMENT_CONFIRMATION_AVAILABLE = PaymentConfirmationAvailableService::class;
}
