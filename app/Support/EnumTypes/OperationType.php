<?php

namespace App\Support\EnumTypes;

class OperationType
{
    const CREATE = 'C';
    const UPDATE = 'A';
    const INACTIVE = 'I';
    const WRITE_OFF = 'B';

    static function getAllActiveAndSettled(): array
    {
        return [
            self::CREATE,
            self::UPDATE,
            self::WRITE_OFF,
        ];
    }

    static function getAllInactive(): array
    {
        return [
            self::INACTIVE,
            self::WRITE_OFF,
        ];
    }
}
