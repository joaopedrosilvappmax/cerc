<?php


namespace App\Support\EnumTypes;


class PaymentArrangement
{
    const ELO_ARRANGEMENT           = 'ECC';

    const MASTERCARD_ARRANGEMENT    = 'MCC';

    const VISA_ARRANGEMENT          = 'VCC';

    const AMEX_ARRANGEMENT          = 'ACC';

    const HIPER_ARRANGEMENT         = 'HCC';

    const DINERS_ARRANGEMENT        = 'DCC';
}
