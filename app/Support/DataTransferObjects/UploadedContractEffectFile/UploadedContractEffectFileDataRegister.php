<?php

namespace App\Support\DataTransferObjects\UploadedContractEffectFile;

use App\Support\DataTransferObjects\Abstracts\DataRegisterAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

class UploadedContractEffectFileDataRegister extends DataRegisterAbstract implements DataRegisterContract
{
    protected string $file_name;
    protected string $date;
    protected string $sequential_number;

    public function addData(array $data): static
    {
        $this->file_name = $data['file_name'];
        $this->date = $data['date'];
        $this->sequential_number = $data['sequential_number'];

        return $this;
    }
}
