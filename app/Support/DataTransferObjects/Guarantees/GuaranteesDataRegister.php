<?php

namespace App\Support\DataTransferObjects\Guarantees;

use App\Support\DataTransferObjects\Abstracts\DataRegisterAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

class GuaranteesDataRegister extends DataRegisterAbstract implements DataRegisterContract
{
    protected int $order_id;
    protected int $value;

    public function addData(array $data): static
    {
        $this->order_id = trim($data['order_id']);
        $this->value = trim($data['value']);

        return $this;
    }
}
