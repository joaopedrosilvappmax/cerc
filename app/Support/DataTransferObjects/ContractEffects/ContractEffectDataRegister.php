<?php

namespace App\Support\DataTransferObjects\ContractEffects;

use App\Support\DataTransferObjects\Abstracts\DataRegisterAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

class ContractEffectDataRegister extends DataRegisterAbstract implements DataRegisterContract
{
    protected ?string $external_reference_receivable_unit_id;
    protected ?string $external_contract_id;
    protected ?string $contract_generator_event_id;
    protected string $indicator;
    protected string $protocol;
    protected string $accrediting_company_document_number;
    protected int $constituted;
    protected string $final_recipient_document_number;
    protected string $code_arrangement_payment;
    protected string $settlement_date;
    protected string $registration_entity_document_number;
    protected string $holder_beneficiary_document_number;
    protected int $effect_type;
    protected int $division_rule;
    protected int $committed_value;
    protected string $holder_document_number;
    protected ?string $holder_name;
    protected string $bank_account_type;
    protected ?string $compe;
    protected string $ispb;
    protected string $bank_branch;
    protected string $bank_account;
    protected ?string $event_datetime;
    protected ?int $uploaded_contract_effect_file_id;
    protected ?int $webhook_request_id;

    public function addData(array $data): static
    {
        $this->external_reference_receivable_unit_id = $data['external_reference_receivable_unit_id'] ?? null;
        $this->external_contract_id = $data['external_contract_id'] ?? null;
        $this->contract_generator_event_id = $data['contract_generator_event_id'] ?? null;
        $this->protocol = $data['protocol'];
        $this->indicator = $data['indicator'];
        $this->accrediting_company_document_number = $data['accrediting_company_document_number'];
        $this->constituted = $data['constituted'];
        $this->final_recipient_document_number = $data['final_recipient_document_number'];
        $this->code_arrangement_payment = $data['code_arrangement_payment'];
        $this->settlement_date = $data['settlement_date'];
        $this->registration_entity_document_number = $data['registration_entity_document_number'];
        $this->holder_beneficiary_document_number = $data['holder_beneficiary_document_number'];
        $this->effect_type = $data['effect_type'];
        $this->division_rule = $data['division_rule'];
        $this->committed_value = (int) ($data['committed_value'] * 100);
        $this->holder_document_number = $data['holder_document_number'];
        $this->holder_name = $data['holder_name'] ?? null;
        $this->bank_account_type = $data['bank_account_type'];
        $this->compe = $data['compe'];
        $this->ispb = $data['ispb'];
        $this->bank_branch = $data['bank_branch'];
        $this->bank_account = $data['bank_account'];
        $this->indicator = $data['indicator'];
        $this->event_datetime = $data['event_datetime'] ?? null;
        $this->uploaded_contract_effect_file_id = $data['uploaded_contract_effect_file_id'] ?? null;
        $this->webhook_request_id = $data['webhook_request_id'] ?? null;

        return $this;
    }
}
