<?php

namespace App\Support\DataTransferObjects\CashOutPayments;

use App\Support\DataTransferObjects\Abstracts\DataRegisterAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

class CashOutPaymentsDataRegister extends DataRegisterAbstract implements DataRegisterContract
{
    protected int $external_cash_out_id;
    protected ?int $external_cash_out_company_id;
    protected int $receivable_unit_id;
    protected int $payment_id;
    protected int $value;

    public function addData(array $data): static
    {
        $this->external_cash_out_id = $data['external_cash_out_id'];
        $this->external_cash_out_company_id = $data['external_cash_out_company_id'] ?? null;
        $this->receivable_unit_id = $data['receivable_unit_id'];
        $this->payment_id = $data['payment_id'];
        $this->value = $data['payment_value'];

        return $this;
    }
}
