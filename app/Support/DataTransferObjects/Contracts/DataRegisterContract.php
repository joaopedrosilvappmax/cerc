<?php

namespace App\Support\DataTransferObjects\Contracts;

interface DataRegisterContract extends DataRegisterAbstractContract
{
    public function addData(array $data): static;
}
