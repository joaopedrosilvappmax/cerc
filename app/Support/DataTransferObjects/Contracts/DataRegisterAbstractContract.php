<?php

namespace App\Support\DataTransferObjects\Contracts;

interface DataRegisterAbstractContract
{
    public function toArray(): array;
}
