<?php

namespace App\Support\DataTransferObjects\Abstracts;

use App\Support\Traits\CastData;

class DataRegisterAbstract
{
    use CastData;

    public function __get($name) {
        return $this->$name;
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function toArray(): array
    {
        return $this->castObjectToArrayRecursive($this);
    }
}
