<?php

namespace App\Support\DataTransferObjects\ReceivableUnits;

use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\ReceivableUnitConstituteStatus;
use App\Support\DataTransferObjects\Abstracts\DataRegisterAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

class ReceivableUnitDataRegister extends DataRegisterAbstract implements DataRegisterContract
{
    protected string $external_id;
    protected ?int $company_id;
    protected ?int $accrediting_company_id;
    protected ?int $receivable_unit_linked_id;
    protected ?int $contract_effect_id;
    protected ?string $accrediting_company_document_number;
    protected string $operation_type;
    protected string $final_recipient_document_number;
    protected string $holder_document_number;
    protected string $code_arrangement_payment;
    protected string $compe;
    protected int $constituted;
    protected int $total_constituted_value;
    protected int $pre_contracted_value;
    protected int $total_gross_value;
    protected int $reversal_value;
    protected int $guarantee_value;
    protected ?int $debit_value;
    protected int $blocked_value;
    protected string $settlement_date;
    protected ?string $paid_at;

    public function addData(array $data): static
    {
        $this->external_id = $data['external_id'];
        $this->operation_type = OperationType::CREATE;
        $this->final_recipient_document_number = $data['final_recipient_document_number'];
        $this->holder_document_number = $data['final_recipient_document_number'];
        $this->code_arrangement_payment = $data['code_arrangement_payment'];
        $this->constituted = ReceivableUnitConstituteStatus::CONSTITUTED;
        $this->total_constituted_value = $data['value'];
        $this->pre_contracted_value = $data['value'];
        $this->total_gross_value = $data['total_gross_value'];
        $this->reversal_value = $data['reversal_value'];
        $this->guarantee_value = $data['guarantee_value'] ?? 0;
        $this->debit_value = ($data['debit_value']) ?? 0;
        $this->blocked_value = $data['blocked_value'];
        $this->settlement_date = $data['settlement_date'];
        $this->paid_at = $data['paid_at'];

        return $this;
    }
}
