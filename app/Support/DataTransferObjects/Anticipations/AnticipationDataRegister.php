<?php

namespace App\Support\DataTransferObjects\Anticipations;

use App\Support\EnumTypes\OperationType;
use App\Support\DataTransferObjects\Abstracts\DataRegisterAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

class AnticipationDataRegister extends DataRegisterAbstract implements DataRegisterContract
{
    protected int $external_order_id;
    protected int $external_cash_out_id;
    protected string $final_recipient_document_number;
    protected string $holder_document_number;
    protected string $code_arrangement_payment;
    protected string $total_constituted_value;
    protected string $request_date;
    protected ?string $settlement_date;
    protected int $total_value;
    protected int $paid_value;
    protected string $accrediting_company_id;
    protected string $accrediting_company_document_number;
    protected string $operation_type;

    public function addData(array $data): static
    {
        $this->external_order_id = $data['external_order_id'];
        $this->external_cash_out_id = $data['external_cash_out_id'];
        $this->accrediting_company_id = config('accrediting_company.appmax_id');
        $this->accrediting_company_document_number = config('accrediting_company.appmax_document_number');
        $this->final_recipient_document_number = $data['final_recipient_document_number'];
        $this->holder_document_number = $data['final_recipient_document_number'];
        $this->operation_type = OperationType::CREATE;
        $this->code_arrangement_payment = $data['code_arrangement_payment'];
        $this->total_constituted_value = $data['value'];
        $this->total_value = $data['value'];
        $this->paid_value = $data['value'];
        $this->request_date = $data['request_date'];
        $this->settlement_date = null;

        return $this;
    }
}
