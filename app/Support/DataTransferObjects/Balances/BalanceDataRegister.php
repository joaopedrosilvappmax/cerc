<?php

namespace App\Support\DataTransferObjects\Balances;

use App\Support\DataTransferObjects\Abstracts\DataRegisterAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

class BalanceDataRegister extends DataRegisterAbstract implements DataRegisterContract
{
    protected string $external_id;
    protected string $document_number;
    protected string $type;
    protected string $value;

    public function addData(array $data): static
    {
        $this->external_id = $data['external_id'];
        $this->document_number = $data['document_number'];
        $this->type = $data['type'];
        $this->value = $data['value'];

        return $this;
    }
}
