<?php

namespace App\Support\DataTransferObjects\Companies;

use App\Support\EnumTypes\CompanyType;
use App\Support\DataTransferObjects\Abstracts\DataRegisterAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\ValueObjects\Bank;

class CompanyDataRegister extends DataRegisterAbstract implements DataRegisterContract
{
    protected int $type_id;
    protected string $name;
    protected string $operation_type;
    protected string $document_number;
    protected string $ispb;
    protected string $status;
    protected string $email;
    protected string $phone;
    protected string $compe;
    protected string $integrated_at;
    protected string $bank_account_type;
    protected string $bank_branch;
    protected string $bank_account;

    public function addData(array $data): static
    {
        $this->type_id = CompanyType::PLATFORM;
        $this->name = $data['name'];
        $this->operation_type = $data['operation_type'];
        $this->document_number = $data['document_number'];
        $this->ispb = (new Bank($data['compe']))->getFormattedIspb();
        $this->compe = $data['compe'];
        $this->status = $data['status'];
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        $this->bank_account_type = $data['bank_account_type'];
        $this->bank_branch = $data['bank_branch'];
        $this->bank_account = $data['bank_account'];

        return $this;
    }
}
