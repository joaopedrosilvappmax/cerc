<?php

namespace App\Events\ReceivableUnits;

use App\Models\Entities\ReceivableUnit;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ReceivableUnitUpdatedWithContractEffectEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public ReceivableUnit $receivableUnit;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ReceivableUnit $receivableUnit)
    {
        $this->receivableUnit = $receivableUnit;
    }
}
