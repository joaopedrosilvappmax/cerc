<?php

namespace App\Events\Anticipations;

use App\Models\Entities\Anticipation;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AnticipationRegisterEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Anticipation $anticipation;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Anticipation $anticipation)
    {
        $this->anticipation = $anticipation;
    }
}
