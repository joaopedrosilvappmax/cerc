<?php

namespace App\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Exceptions\ReceivableUnitOrderNoHasValueAvailableException;
use App\Http\Requests\AnticipationRequest;
use App\Models\Services\Anticipations\AnticipationRegisterService;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class AnticipationController extends Controller
{
    public function store(AnticipationRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'success' => true,
                'data' => app(RegisterServiceContract::class, [
                            'class' => ClassBindResolution::ANTICIPATION_SERVICE
                ])->store(
                    app(DataRegisterContract::class, [
                        'class' => ClassBindResolution::ANTICIPATION_DATA
                    ])->addData($request->all())
                )
            ]);
        } catch (ReceivableUnitOrderNoHasValueAvailableException $e) {
            Log::info('anticipationController.store', [
                'request' => $request->all(),
                'message' => $e->getmessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            return response()->json([
                'success' => true,
            ]);
        } catch (NotFoundException $e) {
            Log::error('anticipationController.store', [
                'request' => $request->all(),
                'message' => $e->getmessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            return response()->json([
                'success' => false,
                'message' => $e->getmessage()
            ], 404);
        } catch (Exception $e) {
            Log::error('anticipationController.store', [
                'request' => $request->all(),
                'message' => $e->getmessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error!'
            ], 422);
        }
    }
}
