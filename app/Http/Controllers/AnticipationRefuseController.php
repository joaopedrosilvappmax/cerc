<?php

namespace App\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Http\Requests\AnticipationRefuseStoreRequest;
use App\Models\Services\Anticipations\AnticipationRefuseService;
use App\Support\EnumTypes\HttpCode;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class AnticipationRefuseController extends Controller
{
    public function store(AnticipationRefuseStoreRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'success' => true,
                'data' => (new AnticipationRefuseService())->refuse($request->external_cash_out_id)
            ]);
        } catch (NotFoundException $e) {
            Log::error('anticipationController.store', [
                'request' => $request->all(),
                'message' => $e->getmessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            return response()->json([
                'success' => false,
                'message' => $e->getmessage()
            ], HttpCode::NOT_FOUND);
        } catch (Exception $e) {
            Log::error('anticipationController.store', [
                'request' => $request->all(),
                'message' => $e->getmessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error!'
            ], HttpCode::UNPROCESSABLE_ENTITY);
        }
    }
}
