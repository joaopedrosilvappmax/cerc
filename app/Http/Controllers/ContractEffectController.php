<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContractEffectRequest;
use App\Models\Services\ContractEffects\ContractEffectService;
use App\Models\Services\Contracts\ContractEffectReadFileServiceContract;
use App\Support\EnumTypes\ClassBindResolution;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ContractEffectController extends Controller
{
    public function upload(Request $request): JsonResponse
    {
        try{
            app(ContractEffectReadFileServiceContract::class, [
                'class' => ClassBindResolution::CONTRACT_EFFECT_READ_FILE
            ])->upload($request);

            return response()->json([
                'message' => 'Successfully uploaded contract effect file'
            ]);

        } catch(Exception $e) {
            Log::error('contractEffectController.upload', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            return response()->json([
                'message' => 'Unprocessable Entity'
            ],422);
        }
    }

    public function index(ContractEffectRequest $request): JsonResponse
    {
        try {
            $data = app(ContractEffectService::class)
                ->getContractEffects($request->get('contract_effect_ids'));

            return response()->json([
                'success' => true,
                'data' => $data
            ]);
        } catch (Exception $exception) {
            Log::error('contractEffectController.contractEffectsFromOrders', [
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error on getting contract effects.'
            ]);
        }
    }
}
