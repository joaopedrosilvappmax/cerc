<?php

namespace App\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Http\Requests\PaymentConfirmationRequest;
use App\Http\Requests\PaymentSettledRequest;
use App\Http\Requests\PaymentTotalValueRequest;
use App\Models\Entities\Payment;
use App\Models\Services\Contracts\PaymentConfirmationServiceContract;
use App\Models\Services\Contracts\PaymentGetSettledServiceContract;
use App\Models\Services\Contracts\PaymentGetTotalValueServiceContract;
use App\Support\EnumTypes\PaymentConfirmationType;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    public function show($id): JsonResponse
    {
        try {
            return response()->json([
                'message' => 'Successfully',
                'data' => Payment::find($id),
            ]);

        } catch(Exception $e) {
            Log::error('paymentController.store', [
                'message' => $e->getmessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            return response()->json([
                'message' => 'Unprocessable Entity'
            ], 422);
        }
    }

    public function getPaymentTotals(PaymentTotalValueRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'message' => 'Successfully',
                'data' => app(PaymentGetTotalValueServiceContract::class, ['calculate_type' => $request->calculate_type])
                    ->getTotal($request->holder_document_number),
            ]);

        } catch(Exception $e) {
            Log::error('paymentController.getTotalPaymentValueFromReceivableUnits', [
                'message' => $e->getmessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            return response()->json([
                'message' => 'Unprocessable Entity'
            ], 422);
        }
    }

    public function paymentsSettled(PaymentSettledRequest $request) : JsonResponse
    {
        try {
            return response()->json([
                'message' => 'Successfully',
                'data' => app(PaymentGetSettledServiceContract::class)->getPayments($request->get('date')),
            ]);
        } catch (Exception $e) {
            Log::error('paymentController.paymentsSettled', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error on getting the payments Settled.'
            ], 422);
        }
    }

    public function paymentConfirmation(PaymentConfirmationRequest $request): JsonResponse
    {
        try {
            app(PaymentConfirmationServiceContract::class, [
                'class' => PaymentConfirmationType::getBindServiceClass($request->get('type'))
            ])->confirmPayment($request->get('external_cash_out_id'));

            return response()->json([
                'success' => true
            ]);
        } catch (NotFoundException $e) {
            Log::error('paymentController.paymentConfirmation', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'confirmation_type' => $request->get('type'),
                'payment_id' => $request->get('payment_id'),
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Payment not found.'
            ], 404);
        } catch (Exception $e) {
            Log::error('paymentController.paymentConfirmation', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'confirmation_type' => $request->get('type'),
                'payment_id' => $request->get('payment_id'),
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error on confirming payment.'
            ], 422);
        }
    }
}
