<?php

namespace App\Http\Controllers;

use App\Exceptions\CompanyException;
use App\Http\Requests\ReceivableUnitRequest;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class ReceivableUnitController extends Controller
{
    public function store(ReceivableUnitRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'message' => 'Successfully registered receivable unit',
                'data' => app(RegisterServiceContract::class, [
                    'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
                ])->store(
                    app(DataRegisterContract::class, ['class' => ClassBindResolution::RECEIVABLE_UNIT_DATA])
                        ->addData($request->all())
                )
            ]);

        } catch (CompanyException $e) {
            Log::info('receivableUniteController.store', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'documentNumber' => $request->final_recipient_document_number
            ]);

            return response()->json([
                'message' => 'Unprocessable Company'
            ], simplify_error_code($e->getCode()));

        } catch (Exception $e) {
            Log::error('receivableUniteController.store', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'documentNumber' => $request->final_recipient_document_number
            ]);

            return response()->json([
               'message' => 'Unprocessable Entity'
            ], simplify_error_code($e->getCode()));
        }
    }
}
