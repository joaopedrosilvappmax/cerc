<?php

namespace App\Http\Controllers;

use App\Http\Requests\DebitRequest;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\BalanceType;
use App\Support\EnumTypes\ClassBindResolution;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class DebitController extends Controller
{
    private RegisterServiceContract $balanceRegisterService;

    public function __construct()
    {
        $this->balanceRegisterService = app(RegisterServiceContract::class, [
            'class' => ClassBindResolution::BALANCE_SERVICE
        ]);
    }

    public function store(DebitRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'message' => 'Successfully registered debit',
                'data' => $this->balanceRegisterService->store(
                    app(DataRegisterContract::class, ['class' => ClassBindResolution::BALANCE_DATA])
                        ->addData(
                            array_merge($request->all(), [
                                'type' => BalanceType::DEBIT
                            ])
                        )
                )
            ], 200);
        } catch (\Exception $e) {
            Log::error('debitController.store', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);

            return response()->json([
                'message' => 'Unprocessable Entity'
            ], 422);
        }
    }
}
