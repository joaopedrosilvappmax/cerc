<?php

namespace App\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Http\Requests\CashOutPaymentStoreRequest;
use App\Models\Services\Contracts\CashOutPaymentRegisterServiceContract;
use App\Models\Services\Notifications\LogNotificationErrorRegisterService;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class CashOutPaymentController extends Controller
{
    public function store(CashOutPaymentStoreRequest $request): JsonResponse
    {
        try {
            app(CashOutPaymentRegisterServiceContract::class, [
                'type' => $request->get('type')
            ])->store(
                app(DataRegisterContract::class, ['class' => ClassBindResolution::CASH_OUT_PAYMENT_DATA])
                    ->addData($request->all())
            );

            return response()->json([
                'success' => true
            ]);
        } catch (NotFoundException $e) {
            Log::error('cashOutPayment.controller.store', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'request' => $request->all()
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Payment not found.'
            ], 404);
        } catch (Exception $e) {
            app(LogNotificationErrorRegisterService::class)->incrementCriticalError();

            Log::error('cashOutPayment.controller.store', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'request' => $request->all()
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error while processing Cash Out data.'
            ], 422);
        }
    }
}
