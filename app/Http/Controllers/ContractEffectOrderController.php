<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContractEffectOrderGetByOrderRequest;
use App\Http\Requests\ContractEffectOrderGetByPaymentRequest;
use App\Models\Services\Contracts\ContractEffectOrderGetByOrderServiceContract;
use App\Models\Services\Contracts\ContractEffectOrderGetByOrdersServiceContract;
use App\Models\Services\Contracts\ContractEffectOrderGetByPaymentServiceContract;
use App\Models\Services\Contracts\ContractEffectOrderGetByServiceContract;
use App\Support\EnumTypes\HttpCode;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class ContractEffectOrderController extends Controller
{
    public function getByOrders(ContractEffectOrderGetByOrderRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'success' => true,
                'data' =>  app(ContractEffectOrderGetByOrdersServiceContract::class, [
                    'type' => $request->type
                ])->getByOrdersId($request->orders),
            ]);
        } catch(Exception $e) {
            Log::error('contractEffectController.getByOrders', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'order_id' => $request->order_id
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error while getting contract effect order.'
            ], 422);
        }
    }

    public function getByOrder($orderId): JsonResponse
    {
        try {
            return response()->json([
                'success' => true,
                'data' =>  app(ContractEffectOrderGetByOrderServiceContract::class)
                    ->getBy($orderId),
            ]);
        } catch(Exception $e) {
            Log::error('contractEffectController.getByOrder', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'order_id' => $orderId
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error while getting contract effect order.'
            ], 422);
        }
    }

    public function getByPaymentId(ContractEffectOrderGetByPaymentRequest $request, $id): JsonResponse
    {
        try {
            return response()->json([
                'success' => true,
                'data' =>  app(ContractEffectOrderGetByPaymentServiceContract::class, [
                    'type' => $request->type
                ])->getBy($id),
            ]);
        } catch(Exception $e) {
            Log::error('contractEffectController.contractEffectOrderGetByPaymentId', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'payment_id' => $request->payment_id
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error while getting contract effect order.'
            ], 422);
        }
    }

    public function getByExternalCashOutId($externalCashOutId): JsonResponse
    {
        try {
            return response()->json([
                'success' => true,
                'data' =>  app(ContractEffectOrderGetByServiceContract::class)
                    ->getBy($externalCashOutId),
            ]);

        } catch(Exception $e) {
            Log::error('contractEffectOrdersController.getByExternalCashOutId', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'external_cash_out_id' => $externalCashOutId
            ]);

            return response()->json([
                'success' => false,
                'message' => 'Error while getting contract effect orders.'
            ], HttpCode::UNPROCESSABLE_ENTITY);
        }
    }
}
