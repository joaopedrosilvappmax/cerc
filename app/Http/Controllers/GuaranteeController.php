<?php

namespace App\Http\Controllers;

use App\Exceptions\GuaranteeAlreadyException;
use App\Exceptions\GuaranteeException;
use App\Http\Requests\GuaranteeRequest;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class GuaranteeController extends Controller
{
    private mixed $guaranteeService;

    public function __construct()
    {
        $this->guaranteeService = app(RegisterServiceContract::class, [
            'class' => ClassBindResolution::GUARANTEE_SERVICE
        ]);
    }

    public function process(GuaranteeRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'message'=> 'Successfully registered guarantee',
                'data' => $this->guaranteeService->process(
                    app(DataRegisterContract::class, ['class' => ClassBindResolution::GUARANTEE_DATA])
                        ->addData([
                            'order_id' => $request->get('order_id'),
                            'value' => $request->get('value')
                        ])
                )
            ]);
        } catch (GuaranteeException | GuaranteeAlreadyException $e) {
            Log::info('guaranteeController.process', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'orderId' => $request->get('order_id'),
            ]);

            return response()->json([
                'message'=> 'Unprocessable Entity'
            ]);
        } catch (\Exception $e) {
            Log::error('guaranteeController.process', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);

            return response()->json([
                'message'=> 'Unprocessable Entity'
            ]);
        }
    }
}
