<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\Status;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class CompanyController extends Controller
{
    private RegisterServiceContract $companyRegisterService;

    public function __construct()
    {
        $this->companyRegisterService = app(RegisterServiceContract::class, [
            'class' => ClassBindResolution::COMPANY_SERVICE
        ]);
    }

    public function store(CompanyRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'message'=> 'Successfully registered company',
                'data' => $this->companyRegisterService->store(
                    app(DataRegisterContract::class, ['class' => ClassBindResolution::COMPANY_DATA])
                        ->addData(
                            array_merge($request->all(), [
                                'operation_type' => OperationType::CREATE,
                                'status' => Status::ACTIVE
                            ])
                        )
                )
            ]);
        } catch (\Exception $e) {
            Log::error('companyController.store', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);

            return response()->json([
                'message'=> 'Unprocessable Entity'
            ], 422);
        }
    }
}
