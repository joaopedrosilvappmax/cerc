<?php

namespace App\Http\Controllers;

use App\Models\Services\Contracts\WebhookServiceContract;
use Error;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WebhookController extends Controller
{
    public function store(Request $request): JsonResponse
    {
        try {
            app(WebhookServiceContract::class)->registerEvent($request->all());

            return response()->json([
                'message' => 'Registration successfully'
            ], 200);

        } catch (Exception $e) {
            Log::error('webhookController.store', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'request' => $request->all()
            ]);

            return response()->json([
                'message' => 'Unprocessable Entity'
            ], 422);
        } catch (Error $e) {
            Log::error('webhookController.store', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'request' => $request->all()
            ]);

            return response()->json([
                'message' => 'Unprocessable Entity'
            ], 422);
        }
    }
}
