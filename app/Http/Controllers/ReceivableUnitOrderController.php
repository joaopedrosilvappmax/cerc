<?php

namespace App\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Models\Services\Contracts\GetTotalContractEffectAnticipatedServiceContract;
use App\Support\EnumTypes\HttpCode;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ReceivableUnitOrderController extends Controller
{

    public function getTotalAnticipated(Request $request): JsonResponse
    {
        try {
            return response()->json([
                'value' => app(GetTotalContractEffectAnticipatedServiceContract::class)
                    ->getTotal($request->get('receivableUnitId'))
            ]);

        } catch (NotFoundException $e) {
            Log::error('ReceivableUnitOrderController.getTotalAnticipated', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'receivableUnitId' => $request->get('receivableUnitId')
            ]);

            return response()->json([
               'message' => 'Receivable Unit Not Found'
            ], HttpCode::NOT_FOUND);

        } catch (Exception $e) {
            Log::error('ReceivableUnitOrderController.getTotalAnticipated', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'receivableUnitId' => $request->get('receivableUnitId')
            ]);

            return response()->json([
               'message' => 'Unprocessable Entity'
            ], simplify_error_code($e->getCode()));
        }
    }
}
