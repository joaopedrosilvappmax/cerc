<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContractEffectOrderGetByPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => [
                'required',
                'string',
                Rule::in([
                    'no-anticipation'
                ]),
            ],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'type.required' => 'A type is required',
        ];
    }
}
