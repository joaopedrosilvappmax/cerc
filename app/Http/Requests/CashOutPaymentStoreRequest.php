<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CashOutPaymentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'external_cash_out_id' => 'required',
            'receivable_unit_id' => 'required',
            'payment_id' => 'required',
            'payment_value' => 'required',
            'type' => 'required'
        ];
    }
}
