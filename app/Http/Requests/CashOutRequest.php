<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CashOutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'external_cash_out_id' => 'required',
            'external_order_id' => 'required',
            'final_recipient_document_number' => 'required|max:14',
            'code_arrangement_payment' => 'required',
            'value' => 'required|integer',
            'request_date' => 'required',
        ];
    }
}
