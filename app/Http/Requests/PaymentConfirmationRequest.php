<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PaymentConfirmationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'external_cash_out_id' => 'required|int',
            'type' => [
                'required',
                'string',
                Rule::in([
                    'anticipation',
                    'anticipation-holder',
                    'available',
                ])
            ],
        ];
    }
}
