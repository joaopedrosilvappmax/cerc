<?php

namespace App\Http\Requests;

use App\Rules\CpfOrCnpj;
use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'document_number' => ['required', new CpfOrCnpj],
            'email' => 'required',
            'phone' => 'required',
            'compe' => 'required',
            'bank_account_type' => 'required',
            'bank_branch' => 'required',
            'bank_account' => 'required'
        ];
    }
}
