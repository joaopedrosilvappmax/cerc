<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReceivableUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'external_id' => 'required',
            'final_recipient_document_number' => 'required',
            'code_arrangement_payment' => 'required',
            'value' => 'required|integer',
            'total_gross_value' => 'required|integer',
            'blocked_value' => 'required|integer',
            'settlement_date' => 'required',
            'paid_at' => 'nullable'
        ];
    }
}
