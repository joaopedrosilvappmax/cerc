<?php

namespace App\Http\Responses;

use App\Http\Responses\Contracts\AuthenticationResponseContract;
use App\Support\DataTransferObjects\Abstracts\DataRegisterAbstract;

class AuthenticationResponse extends DataRegisterAbstract implements AuthenticationResponseContract
{

    protected ?string $access_token;

    protected ?string $token_type;

    protected ?string $expires_in;

    protected ?string $scope;

    protected ?int $status;

    public function loadResponse(array $response): static
    {
        $this->access_token = isset($response['access_token']) ? $response['access_token'] : null;
        $this->token_type = isset($response['token_type']) ? $response['token_type'] : null;
        $this->expires_in = isset($response['expires_in']) ? $response['expires_in'] : null;
        $this->scope = isset($response['scope']) ? $response['scope'] : null;
        $this->status = isset($response['status']) ? $response['status'] : null;

        if (! empty($response[0])) {
            $this->status = isset($response[0]['status']) ? (int) $response[0]['status'] : null;
        }

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }
}
