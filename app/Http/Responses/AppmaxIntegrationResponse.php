<?php

namespace App\Http\Responses;

use App\Http\Responses\Abstracts\IntegrationResponseAbstract;

class AppmaxIntegrationResponse extends IntegrationResponseAbstract
{
    public array $requestBody;
    public array $responseBody;

    public function loadResponse(array $response): static
    {
        $this->status = isset($response['status']) ? $response['status'] : null;
        $this->requestBody = isset($response['requestBody']) ? $response['requestBody'] : null;
        $this->responseBody = isset($response['responseBody']) ? $response['responseBody'] : null;

        return $this;
    }

    public function getResponse(): ?object
    {
        return (object) $this->responseBody;
    }

    public function getRequest(): ?object
    {
        return (object) $this->requestBody;
    }
}
