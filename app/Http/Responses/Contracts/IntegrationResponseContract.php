<?php

namespace App\Http\Responses\Contracts;

interface IntegrationResponseContract
{
    public function loadResponse(array $response): static;

    public function getStatus(): ?int;

    public function getErrorCode(): ?string;

    public function getMessage(): ?string;
}
