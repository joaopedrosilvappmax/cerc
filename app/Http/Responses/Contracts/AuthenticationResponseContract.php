<?php

namespace App\Http\Responses\Contracts;

interface AuthenticationResponseContract
{
    public function loadResponse(array $response): static;

    public function getStatus(): ?int;
}
