<?php

namespace App\Http\Responses\Abstracts;

abstract class IntegrationResponseAbstract
{
    protected ?int $status;

    protected ?string $error_code;

    protected ?array $errors;

    protected ?string $message;

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function getErrorCode(): ?string
    {
        return $this->error_code;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getErrors(): ?array
    {
        return $this->errors;
    }
}
