<?php

namespace App\Http\Responses;

use App\Http\Responses\Abstracts\IntegrationResponseAbstract;
use App\Http\Responses\Contracts\IntegrationResponseContract;

class IntegrationResponse extends IntegrationResponseAbstract implements IntegrationResponseContract
{
    public function loadResponse(array $response): static
    {
        if (isset($response[0])) {
            $this->status = isset($response[0]['status']) ? (int) $response[0]['status'] : null;
            $this->error_code = isset($response[0]['erros'][0]['codigo']) ? $response[0]['erros'][0]['codigo'] : null;
            $this->message = isset($response[0]['erros'][0]['mensagem']) ? $response[0]['erros'][0]['mensagem'] : null;
            $this->errors = isset($response[0]['erros']) ? $response[0]['erros'] : null;

            return $this;
        }

        $this->status = isset($response['status']) ? (int) $response['status'] : null;
        $this->error_code = isset($response['erros'][0]->codigo) ? $response['erros'][0]->codigo : null;
        $this->message = isset($response['erros'][0]->mensagem) ? $response['erros'][0]->mensagem : null;
        $this->errors = isset($response['erros']) ? $response['erros'] : null;

        return $this;
    }
}
