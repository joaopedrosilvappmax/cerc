<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class WebhookToken
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('chave') != config('cerc_ap008.webhook_token')) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 403);
        }

        return $next($request);
    }
}
