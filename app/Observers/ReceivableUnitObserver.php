<?php

namespace App\Observers;

use App\Models\Entities\ReceivableUnit;

class ReceivableUnitObserver
{
    public function saving(ReceivableUnit $receivableUnit)
    {
        if ($this->hasNegativeValue($receivableUnit)) {
            $receivableUnit->total_constituted_value = 0;
            $receivableUnit->pre_contracted_value = 0;
            $receivableUnit->total_gross_value = 0;
        }
    }

    private function hasNegativeValue(ReceivableUnit $receivableUnit): bool
    {
        return $receivableUnit->total_constituted_value < 0
            || $receivableUnit->pre_contracted_value < 0
            || $receivableUnit->total_gross_value < 0;
    }
}
