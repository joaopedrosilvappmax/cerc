<?php

namespace App\Observers;

use App\Models\Entities\Payment;

class PaymentObserver
{
    public function saving(Payment $payment)
    {
        if ($payment->payment_value < 0) {
            $payment->payment_value = 0;
        }
    }
}
