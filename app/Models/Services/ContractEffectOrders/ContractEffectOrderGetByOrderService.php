<?php

namespace App\Models\Services\ContractEffectOrders;

use App\Models\Entities\ContractEffectOrder;
use App\Models\Services\Contracts\ContractEffectOrderGetByOrderServiceContract;
use Illuminate\Support\Collection;

class ContractEffectOrderGetByOrderService implements ContractEffectOrderGetByOrderServiceContract
{
    public function getBy(int $externalOrderId): ?Collection
    {
        if (! $externalOrderId) {
            return null;
        }

        return ContractEffectOrder::select([
            'external_order_id',
            'value'
        ])->where('external_order_id', $externalOrderId)
            ->get();
    }
}
