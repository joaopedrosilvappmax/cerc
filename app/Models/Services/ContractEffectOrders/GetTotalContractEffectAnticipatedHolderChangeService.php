<?php

namespace App\Models\Services\ContractEffectOrders;

use App\Exceptions\NotFoundException;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\GetTotalContractEffectAnticipatedServiceContract;
use App\Support\EnumTypes\ContractEffectType;

class GetTotalContractEffectAnticipatedHolderChangeService implements GetTotalContractEffectAnticipatedServiceContract
{

    public function getTotal(int $receivableUnitId): int
    {
        $receivableUnitParent = $this->getReceivableUnitParent($receivableUnitId);

        if (! $receivableUnitParent) {
            return 0;
        }

        return $receivableUnitParent->receivableUnitOrders()
            ->whereNotNull('anticipation_id')
            ->get(['id', 'external_order_id'])
            ->sum(function ($receivableUnitOrder) {
                return $receivableUnitOrder->contractEffectOrders()
                    ->whereHas('contractEffect', function ($query) {
                        return $query->where('effect_type', ContractEffectType::HOLDER_CHANGE);
                    })
                    ->sum('value');
            });
    }

    private function getReceivableUnitParent(int $receivableUnitId): ?ReceivableUnit
    {
        $receivableUnit = ReceivableUnit::find($receivableUnitId);

        if (! $receivableUnit) {
            throw new NotFoundException('Receivable unit not found');
        }

        return $receivableUnit->receivableUnitParent;
    }
}
