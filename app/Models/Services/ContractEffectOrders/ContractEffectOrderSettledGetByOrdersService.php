<?php

namespace App\Models\Services\ContractEffectOrders;

use App\Models\Entities\ContractEffectOrder;
use App\Models\Services\Contracts\ContractEffectOrderGetByOrdersServiceContract;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ContractEffectOrderSettledGetByOrdersService implements ContractEffectOrderGetByOrdersServiceContract
{
    public function getByOrdersId(array $ordersId): ?Collection
    {
        if (empty($ordersId)) {
            return null;
        }

        return ContractEffectOrder::selectRaw('
                contract_effect_orders.payment_id,
                payments.compe,
                payments.document_number,
                payments.bank_account_type,
                payments.bank_branch,
                payments.bank_account,
                contract_effects.effect_type,
                sum(contract_effect_orders.value) value
            ')
            ->whereIn('external_order_id', $ordersId)
            ->where('receivable_units.settlement_date', '<', Carbon::now())
            ->join('receivable_units', 'contract_effect_orders.receivable_unit_id', '=', 'receivable_units.id')
            ->join('contract_effects', 'contract_effect_orders.contract_effect_id', '=', 'contract_effects.id')
            ->join('payments', 'contract_effect_orders.payment_id', '=', 'payments.id')
            ->groupBy([
                'contract_effect_orders.payment_id',
                'payments.compe',
                'payments.document_number',
                'payments.bank_account_type',
                'payments.bank_branch',
                'payments.bank_account',
                'contract_effects.effect_type'
            ])
            ->get();
    }
}
