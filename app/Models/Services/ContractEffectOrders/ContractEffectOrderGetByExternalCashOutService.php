<?php

namespace App\Models\Services\ContractEffectOrders;

use App\Models\Entities\Anticipation;
use App\Models\Services\Contracts\ContractEffectOrderGetByServiceContract;
use Illuminate\Support\Collection;

class ContractEffectOrderGetByExternalCashOutService implements ContractEffectOrderGetByServiceContract
{
    public function getBy(int $externalCashOutId): ?Collection
    {
        return Anticipation::select('contract_effect_orders.external_order_id', 'contract_effect_orders.value')
            ->where('anticipations.external_cash_out_id', $externalCashOutId)
            ->join('receivable_unit_orders', 'anticipations.id', '=', 'receivable_unit_orders.anticipation_id')
            ->join('contract_effect_orders', 'receivable_unit_orders.external_order_id', '=', 'contract_effect_orders.external_order_id')
            ->get();
    }
}
