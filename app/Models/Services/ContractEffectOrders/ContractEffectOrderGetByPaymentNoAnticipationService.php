<?php

namespace App\Models\Services\ContractEffectOrders;

use App\Models\Entities\ContractEffectOrder;
use App\Models\Services\Contracts\ContractEffectOrderGetByPaymentServiceContract;
use Illuminate\Support\Collection;

class ContractEffectOrderGetByPaymentNoAnticipationService implements ContractEffectOrderGetByPaymentServiceContract
{
    public function getBy(int $paymentId): ?Collection
    {
        if (! $paymentId) {
            return null;
        }

        return ContractEffectOrder::select(
            'contract_effect_orders.payment_id',
            'contract_effect_orders.external_order_id',
            'contract_effect_orders.value',
        )->join('receivable_unit_orders', 'contract_effect_orders.external_order_id', '=', 'receivable_unit_orders.external_order_id', )
            ->where('contract_effect_orders.payment_id', $paymentId)
            ->whereNull('receivable_unit_orders.anticipation_id')
            ->get();
    }
}
