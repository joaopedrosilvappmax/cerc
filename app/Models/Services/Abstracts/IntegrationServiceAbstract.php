<?php

namespace App\Models\Services\Abstracts;

use App\Exceptions\IntegrationAuthenticationException;
use App\Models\Services\Contracts\AuthenticationServiceContract;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

abstract class IntegrationServiceAbstract
{
    private string $url;
    private ?string $token;

    public function __construct() {
        $this->url = config('cerc.api_url');
        $this->token = app(AuthenticationServiceContract::class)->getToken();
    }

    /**
     * @throws IntegrationAuthenticationException
     */
    public function put($endpoint, $body): Response
    {
        if (! $this->token) {
            throw new IntegrationAuthenticationException("CERC Token is null");
        }

        return Http::timeout(config('cerc.timeout'))->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => $this->token,
        ])->put($this->url . $endpoint, $body);
    }

    /**
     * @throws IntegrationAuthenticationException
     */
    public function post($endpoint, $body): Response
    {
        if (! $this->token) {
            throw new IntegrationAuthenticationException("CERC Token is null");
        }

        return Http::timeout(config('cerc.timeout'))->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => $this->token,
        ])->post($this->url . $endpoint, $body);
    }
}
