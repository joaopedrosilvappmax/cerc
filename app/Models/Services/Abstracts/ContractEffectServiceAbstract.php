<?php

namespace App\Models\Services\Abstracts;

use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

abstract class ContractEffectServiceAbstract
{
    protected function getPaymentFromReceivableUnitHolder(ReceivableUnit $receivableUnit): ?Payment
    {
        return Payment::where([
            'receivable_unit_id' => $receivableUnit->id,
            'holder_document_number' => $receivableUnit->final_recipient_document_number
        ])->first();
    }

    protected function getAnticipationPaymentHolder(Anticipation $anticipation): ?Payment
    {
        return Payment::where([
            'anticipation_id' => $anticipation->id,
            'holder_document_number' => $anticipation->final_recipient_document_number
        ])->first();
    }

    protected function fillApplyDateAndReceivableUnitId(
        ContractEffect $contractEffect,
        ReceivableUnit $receivableUnit
    ): void
    {
        $contractEffect->update([
            'receivable_unit_id' => $receivableUnit->id,
            'applied_at' => Carbon::now()
        ]);
    }

    public function getOrdersWithValuesCalculatedFromContractEffectApplied(ReceivableUnit $receivableUnit, int $paymentValue = 0): array
    {
        if (empty($receivableUnit->orders)) {
            return [];
        }

        $percentageDebit = $this->getPercentageDebit($receivableUnit->orders, $paymentValue);

        $orders = collect();

        $receivableUnit->orders->each(function($order) use (&$orders, $percentageDebit) {
            if ($order->anticipation_id) {
                return;
            }

            $valueDebit = (int) ceil($order->value * $percentageDebit);

            $orders->push((object) [
                'order_id' => $order->external_order_id,
                'value' => $valueDebit
            ]);
        });

        if ($orders->sum('value') != $paymentValue && $orders->sum('value') > 0) {
            $this->decimalDiffValueLastOrder($orders, $paymentValue);
        }

        return $orders->toArray();
    }

    public function storeContractEffectOrders(
        Payment $payment,
        ReceivableUnit $receivableUnit,
        ContractEffect $contractEffect,
        array $orders
    ) {
        if (! $orders) {
            return;
        }

        array_map(function($order) use ($receivableUnit, $payment, $contractEffect) {
           ContractEffectOrder::updateOrCreate(
                [
                    'external_order_id' => $order->order_id,
                    'receivable_unit_id' => $receivableUnit->id,
                    'payment_id' => $payment->id,
                    'contract_effect_id' => $contractEffect->id
                ],
                [
                    'value' => $order->value,
                    'reversal_value' => 0,
                ]);

        }, $orders);
    }

    private function getPercentageDebit($receivableUnitOrders, $paymentValue): float
    {
        $sumValueOrders = $receivableUnitOrders->sum('value');

        if ($sumValueOrders > 0) {
            return $paymentValue / $sumValueOrders;
        }

        return 0.00;
    }

    private function decimalDiffValueLastOrder(&$orders, $paymentValue)
    {
        $diff = $orders->sum('value') - $paymentValue;

        $orders->last()->value -= $diff;
    }
}
