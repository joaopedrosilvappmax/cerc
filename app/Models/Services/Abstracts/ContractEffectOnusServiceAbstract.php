<?php

namespace App\Models\Services\Abstracts;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\Math;

abstract class ContractEffectOnusServiceAbstract extends ContractEffectServiceAbstract
{
    public function applyDivisionRule(ContractEffect $contractEffect, Payment $payment, ReceivableUnit $receivableUnit): int
    {
        if ($contractEffect->division_rule == DivisionRule::PERCENTAGE_VALUE) {
            return $this->applyDivisionPercentageValue($contractEffect, $payment, $receivableUnit);
        }

        return $this->applyDivisionFixedValue($contractEffect, $payment);
    }

    private function applyDivisionFixedValue(ContractEffect $contractEffect, Payment $payment): int
    {
        $payment->update([
            'payment_value' => ($payment->payment_value - $contractEffect->committed_value)
        ]);

        return $contractEffect->committed_value;
    }

    private function applyDivisionPercentageValue(ContractEffect $contractEffect, Payment $payment, ReceivableUnit $receivableUnit): int
    {
        $committedValue =  (int) ceil(
            $receivableUnit->total_constituted_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER)
        );

        $value = $receivableUnit->total_constituted_value - $committedValue;

        if ($value < 0) {
            $value = 0;
            $contractEffect->committed_value = $payment->payment_value;
        }

        $valueTotal = $value - $receivableUnit->payments()
                ->where('receivable_unit_id', '=', $receivableUnit->id)
                ->where('holder_document_number', '<>', $receivableUnit->final_recipient_document_number)
                ->sum('payment_value');

        $payment->update(['payment_value' => $valueTotal]);

        return $committedValue;
    }
}
