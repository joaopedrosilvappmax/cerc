<?php

namespace App\Models\Services\Abstracts;

use App\Exceptions\IntegrationAuthenticationException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

abstract class IntegrationAppmaxServiceAbstract
{
    private string $url;
    private string $token;
    private array $headers;

    public function __construct() {
        $this->url = config('appmax.api_url');
        $this->token = config('appmax.api_token');

        $this->headers = [
            'Content-Type' => 'application/json',
            'Authorization' => $this->token,
        ];

        if (! $this->token) {
            throw new IntegrationAuthenticationException("Appmax Token is null");
        }
    }

    public function post($endpoint, $body): Response
    {
        return Http::withHeaders($this->headers)
            ->post($this->url . $endpoint, $body);
    }

    protected function delete($endpoint): Response
    {
        return Http::withHeaders($this->headers)
            ->delete($this->url . $endpoint);
    }
}
