<?php

namespace App\Models\Services\ReceivableUnits\Abstracts;

use App\Models\Entities\Balance;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\ReceivableUnits\ReceivableUnitGetCalculatedDebitValueService;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\BalanceType;
use App\Support\EnumTypes\OperationType;
use App\Support\ValueObjects\BalanceDebitFromExcessNegativeValue;
use Illuminate\Support\Facades\Log;

class ReceivableUnitRegisterStoreServiceAbstract
{
    protected bool $existsReversalValue;

    protected function registerOrder(ReceivableUnit $receivableUnit, DataRegisterContract $data): ReceivableUnitOrder
    {
        $receivableUnitOrder = ReceivableUnitOrder::firstWhere('external_order_id', $data->external_id);

        $this->checkReversalValueInOrder($receivableUnitOrder);

        $this->createBalanceFromExcessDebitValue($receivableUnit, $receivableUnitOrder, $data);

        $receivableUnitOrderData = ['receivable_unit_id' => $receivableUnit->id];

        if ($data->total_gross_value > 0) {
            $receivableUnitOrderData['gross_value'] = $data->total_gross_value;
        }

        if ($data->total_constituted_value > 0) {
            $receivableUnitOrderData['value'] = $data->total_constituted_value;
            $receivableUnitOrderData['debit_value'] = app(ReceivableUnitGetCalculatedDebitValueService::class)
                ->getValue(
                    $receivableUnit,
                    $receivableUnitOrderData
                );
            $data->debit_value = $receivableUnitOrderData['debit_value'];
        }

        if ($data->reversal_value > 0) {
            $data->reversal_value = $this->getValueReversalValue($receivableUnitOrder, $data);
            $receivableUnitOrderData['reversal_value'] = $data->reversal_value;
        }

        if ($data->guarantee_value > 0) {
            $receivableUnitOrderData['guarantee_value'] = $data->guarantee_value;
        }

        if (! $receivableUnitOrder) {
            return ReceivableUnitOrder::create(
                array_merge($receivableUnitOrderData, ['external_order_id' => $data->external_id])
            );
        }

        $receivableUnitOrder->update($receivableUnitOrderData);

        if ($this->existsReversalValue) {
            $data->reversal_value = 0;
        }

        return $receivableUnitOrder->fresh();
    }

    private function createBalanceFromExcessDebitValue(
        ReceivableUnit $receivableUnit,
        ? ReceivableUnitOrder $receivableUnitOrder,
        DataRegisterContract $data
    ): void
    {
        if (! $receivableUnitOrder) {
            return;
        }

        $debitValue = (new BalanceDebitFromExcessNegativeValue())
            ->addReceivableUnitOrder($receivableUnitOrder)
            ->addGuaranteeValue($data->guarantee_value)
            ->addReversalValue($data->reversal_value)
            ->getCalculatedValue();

        if (! $debitValue) {
            return;
        }

        Balance::create([
            'document_number' => $receivableUnit->holder_document_number,
            'type' => BalanceType::DEBIT,
            'value' => $debitValue
        ]);
    }

    protected function isSettled(ReceivableUnit $receivableUnit): bool
    {
        return $receivableUnit->operation_type == OperationType::WRITE_OFF;
    }

    protected function warnSettled(ReceivableUnit $receivableUnit): void
    {
        Log::info('receivableUnitRegisterService', [
            'message' => 'Receivable Unit is settled',
            'receivableUnitId' => $receivableUnit->id
        ]);
    }

    protected function getValueReversalValue($receivableUnitOrder, $data): int
    {
        if (! $receivableUnitOrder) {
            return $data->total_constituted_value;
        }

        return $receivableUnitOrder->getRawOriginal('value');
    }

    protected function checkReversalValueInOrder($receivableUnitOrder)
    {
        if ($receivableUnitOrder && $receivableUnitOrder->reversal_value > 0) {
            $this->existsReversalValue = true;

            return;
        }

        $this->existsReversalValue = false;
    }
}
