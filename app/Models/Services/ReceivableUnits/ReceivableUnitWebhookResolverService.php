<?php

namespace App\Models\Services\ReceivableUnits;

use App\Models\Entities\AnalyticConciliationRecord;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\WebhookResolverServiceContract;
use App\Support\EnumTypes\IntegrationResponseStatus;
use App\Support\EnumTypes\OperationType;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class ReceivableUnitWebhookResolverService implements WebhookResolverServiceContract
{
    public function resolveWebhookData(array $data, int $webhookRequestId): void
    {
        if ($this->verifyBodyData($data)) {
            Log::error('receivableUnitWebhookResolverService.resolveWebhookData', [
                'message' => 'Webhook data is empty',
                $data
            ]);

            return;
        }

        $receivableUnit = ReceivableUnit::find($data['evento']['referenciaExterna']);

        if (! $receivableUnit) {
            Log::error('receivableUnitWebhookResolverService.resolveWebhookData', [
                'message' => 'Receivable unit not found',
                $data
            ]);

            return;
        }

        if ($this->verifyReceivableUnitIsSettledWithError($receivableUnit, $data)) {

            $receivableUnit->update([
                'operation_type' => $receivableUnit->integrated_at ? OperationType::UPDATE : OperationType::CREATE
            ]);

            $this->storeAnalyticRecord($receivableUnit);

            Log::info('receivableUnitWebhookResolverService.resolveWebhookData', [
                'message' => 'Receivable unit is not settled',
                'receivableUnitId' => $receivableUnit->id
            ]);

            return;
        }


        if ($data['evento']['status'] != IntegrationResponseStatus::SUCCESS) {

            $this->removeAnalyticRecordWhenHasIntegrationError($receivableUnit);

            Log::error('receivableUnitWebhookResolverService.resolveWebhookData', [
                'message' => 'Webhook event status receivable units',
                $data
            ]);

            return;
        }

        $receivableUnit->update([ 'integrated_at' => Carbon::now() ]);
    }

    private function verifyBodyData(array $data): bool
    {
        return empty($data['evento']['referenciaExterna']);
    }

    private function verifyReceivableUnitIsSettledWithError($receivableUnit, array $data): bool
    {
        return $receivableUnit->settlement_date < Carbon::now() && $data['evento']['status'] != IntegrationResponseStatus::SUCCESS;
    }

    private function storeAnalyticRecord(ReceivableUnit $receivableUnit): void
    {
        AnalyticConciliationRecord::updateOrCreate(
            [
                'receivable_unit_id' => $receivableUnit->id,
                'reference_date' => Carbon::now()->format('Y-m-d'),
                'final_recipient_document_number' => $receivableUnit->final_recipient_document_number,
                'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
                'settlement_date' => $receivableUnit->settlement_date,
                'holder_document_number' => $receivableUnit->holder_document_number,
            ],
            [
                'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
                'total_gross_value' => $receivableUnit->total_gross_value,
                'total_constituted_value' => $receivableUnit->total_constituted_value,
                'pre_contracted_value' => $receivableUnit->pre_contracted_value,
                'anticipation_not_settled_value' => 0,
                'is_not_settled' => true
            ]
        );
    }

    private function removeAnalyticRecordWhenHasIntegrationError($receivableUnit)
    {
        if (! $receivableUnit->integrated_at) {
            AnalyticConciliationRecord::firstWhere([
                'reference_date' => Carbon::now()->format('Y-m-d'),
                'receivable_unit_id' => $receivableUnit->id,
            ])->delete();
        }
    }
}
