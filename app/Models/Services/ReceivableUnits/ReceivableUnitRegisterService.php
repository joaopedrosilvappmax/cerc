<?php

namespace App\Models\Services\ReceivableUnits;

use App\Events\ReceivableUnits\ReceivableUnitUpdatedWithContractEffectEvent;
use App\Exceptions\CompanyException;
use App\Models\Entities\Company;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Models\Services\Contracts\RegisterStoreServiceContract;
use App\Models\Services\ReceivableUnits\Abstracts\ReceivableUnitRegisterStoreServiceAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use Carbon\Carbon;

class ReceivableUnitRegisterService extends ReceivableUnitRegisterStoreServiceAbstract implements RegisterServiceContract, RegisterStoreServiceContract
{
    /**
     * @throws CompanyException
     */
    public function store(DataRegisterContract $data): ?ReceivableUnit
    {
        if ($receivableUnit = $this->findByPaymentArrangement($data)) {
            return app(ReceivableUnitRegisterUpdateService::class)
                ->update($receivableUnit, $data);
        }

        $data = $this->setCompanyInformation($data);

        $receivableUnit = ReceivableUnit::create($data->toArray());

        $receivableUnitOrder = $this->registerOrder($receivableUnit, $data);

        $this->createPayment($receivableUnit);

        if ($receivableUnitOrder->hasDebitValue()) {

            $receivableUnit->update([
                'total_constituted_value' => $receivableUnit->total_constituted_value - $receivableUnitOrder->debit_value,
                'pre_constituted_value' => $receivableUnit->pre_constituted_value - $receivableUnitOrder->debit_value,
                'total_gross_value' => $receivableUnit->total_gross_value - $receivableUnitOrder->gross_value,
                'updated_at' => Carbon::now()
            ]);
        }

        $receivableUnit = $receivableUnit->fresh();

        ReceivableUnitUpdatedWithContractEffectEvent::dispatch($receivableUnit);

        return $receivableUnit;
    }

    private function findByPaymentArrangement(DataRegisterContract $data): ?ReceivableUnit
    {
        return ReceivableUnit::where([
            'final_recipient_document_number' => $data->final_recipient_document_number,
            'code_arrangement_payment' => $data->code_arrangement_payment,
            'settlement_date' => $data->settlement_date,
            'holder_document_number' => $data->holder_document_number
        ])->first();
    }

    private function createPayment($receivableUnit): void
    {
        Payment::create([
            'receivable_unit_id' => $receivableUnit->id,
            'holder_document_number' => $receivableUnit->holder_document_number,
            'ispb' => $receivableUnit->company->ispb,
            'compe' => $receivableUnit->company->compe,
            'bank_account_type' => $receivableUnit->company->bank_account_type,
            'bank_branch' => $receivableUnit->company->bank_branch,
            'bank_account' => $receivableUnit->company->bank_account,
            'payment_value' => $receivableUnit->total_constituted_value
        ]);
    }

    /**
     * @throws CompanyException
     */
    private function setCompanyInformation(DataRegisterContract $data): DataRegisterContract
    {
        $company = Company::where('document_number', $data->final_recipient_document_number)->first();

        if (! $company) {
            throw new CompanyException("Unregistered company", 404);
        }

        if (! $company->compe) {
            throw new CompanyException("Company {{$company->id}} without compe value", 404);
        }

        $data->company_id = $company->id;
        $data->compe = $company->compe;
        $data->accrediting_company_id = config('accrediting_company.appmax_id');
        $data->accrediting_company_document_number = config('accrediting_company.appmax_document_number');

        return $data;
    }
}
