<?php

namespace App\Models\Services\ReceivableUnits;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;

class ReceivableUnitRegisterFromContractEffectService
{
    public function store(ContractEffect $contractEffect): ?ReceivableUnit
    {
        return app(RegisterServiceContract::class, [
            'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
        ])->store(
            app(DataRegisterContract::class, ['class' => ClassBindResolution::RECEIVABLE_UNIT_DATA])
                ->addData([
                    'external_id' => 0,
                    'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
                    'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
                    'value' => 0,
                    'total_gross_value' => 0,
                    'reversal_value' => 0,
                    'blocked_value' => 0,
                    'settlement_date' => $contractEffect->settlement_date,
                    'paid_at' => null,
                    'guarantee_value' => 0,
                ])
        );
    }
}
