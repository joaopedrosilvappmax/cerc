<?php

namespace App\Models\Services\ReceivableUnits;

use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\ReceivableUnitPaymentServiceContract;
use Carbon\Carbon;

class ReceivableUnitPaymentService implements ReceivableUnitPaymentServiceContract
{
    /**
     * @param string $holderDocumentNumber
     * @return int
     */
    public function receivableUnitPaymentsSum(string $holderDocumentNumber): int
    {
        return ReceivableUnit::with('payments')
            ->where([
                ['holder_document_number', '=', $holderDocumentNumber],
                ['settlement_date', '>', Carbon::now()]
            ])
            ->get()
            ->sum(function ($receivableUnit) use ($holderDocumentNumber) {
                return $receivableUnit->payments
                    ->where('holder_document_number', '!=', $holderDocumentNumber)
                    ->sum('payment_value');
            });
    }
}
