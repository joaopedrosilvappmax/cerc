<?php

namespace App\Models\Services\ReceivableUnits;

use App\Models\Entities\Balance;
use App\Models\Entities\ReceivableUnit;
use App\Support\EnumTypes\BalanceType;

class ReceivableUnitGetCalculatedDebitValueService
{
    public function getValue(
        ReceivableUnit $receivableUnit,
        array $receivableUnitOrderData
    ): int
    {
        $balances = Balance::where('document_number', $receivableUnit->final_recipient_document_number)
            ->where('type', BalanceType::DEBIT)
            ->where('value', '>', 0)
            ->get();

        if ($balances->isEmpty()) {
            return 0;
        }

        $debitValue = 0;
        $remainingValue = $receivableUnitOrderData['value'];

        $balances->each(function ($balance) use ($receivableUnit, &$remainingValue, &$debitValue) {

            if (! $remainingValue) {
                return false;
            }

            if ($balance->value > $remainingValue) {
                $debitValue += $remainingValue;
                $subtractValue = $remainingValue;
                $this->applyDebitOnReceivableUnit($receivableUnit, $balance, $subtractValue);
                return false;
            }

            if ($remainingValue < 0) {
                $debitValue += $remainingValue;
                $subtractValue = $remainingValue;
                $this->applyDebitOnReceivableUnit($receivableUnit, $balance, $subtractValue);
                return false;
            }

            $debitValue += $balance->value;
            $subtractValue = $balance->value;
            $remainingValue -= $balance->value;

            $this->applyDebitOnReceivableUnit($receivableUnit, $balance, $subtractValue);
        });

        return $debitValue;
    }

    private function applyDebitOnReceivableUnit(ReceivableUnit $receivableUnit, $balance, int $subtractValue): void
    {
        $balance->update(['value' => ($balance->value - $subtractValue)]);

        $balance->receivableUnits()
            ->attach([
                $receivableUnit->id => [
                    'applied_value' => $subtractValue
                ]
            ]);
    }
}
