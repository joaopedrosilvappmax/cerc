<?php

namespace App\Models\Services\ReceivableUnits;

use App\Events\ReceivableUnits\ReceivableUnitUpdatedWithContractEffectEvent;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\ReceivableUnits\Abstracts\ReceivableUnitRegisterStoreServiceAbstract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\ValueObjects\ReducerConstitutedValue;
use Illuminate\Support\Collection;

class ReceivableUnitRegisterUpdateService extends ReceivableUnitRegisterStoreServiceAbstract
{
    private Collection $contractEffectOrders;
    private int $reduceConstitutedValue;

    public function update(ReceivableUnit $receivableUnit, DataRegisterContract $data): ?ReceivableUnit
    {
        if ($this->isSettled($receivableUnit)) {
            $this->warnSettled($receivableUnit);

            return null;
        }

        $receivableUnitOrder = $this->registerOrder($receivableUnit, $data);

        $this->setContractEffectOrders($receivableUnitOrder);
        $this->setReduceConstitutedValue($receivableUnitOrder, $data);

        $data = $this->changeValueTotalConstituted($receivableUnit, $data);
        $data = $this->changeValuePreContracted($receivableUnit, $data);
        $data = $this->changeValueTotalGross($receivableUnit, $data);

        $receivableUnit->update($data->toArray());
        $receivableUnit = $receivableUnit->fresh();

        if ($receivableUnit->hasContractEffect()) {
            $this->reduceConstitutedValueOnHolderChangeReceivableUnits($receivableUnitOrder);
            $this->reduceValueContractEffectOrders();

            $receivableUnit = $receivableUnit->fresh();

            ReceivableUnitUpdatedWithContractEffectEvent::dispatch($receivableUnit);

            return $receivableUnit;
        }

        $this->updatePayment($receivableUnit);

        return $receivableUnit->fresh();
    }

    private function updatePayment(ReceivableUnit $receivableUnit): void
    {
        sleep(config('cerc.sleep_seconds_update_competition'));

        $noHolderPayments = $receivableUnit->payments
            ->where('holder_document_number', '!=', $receivableUnit->holder_document_number)
            ->sum('payment_value');

        Payment::where([
            'receivable_unit_id' => $receivableUnit->id,
            'holder_document_number' => $receivableUnit->holder_document_number
        ])
        ->first()
        ->update([
            'payment_value' => $receivableUnit->total_constituted_value - $noHolderPayments
        ]);
    }

    private function changeValueTotalConstituted(
        ReceivableUnit $receivableUnit,
        DataRegisterContract $data
    ): DataRegisterContract
    {
        $data->total_constituted_value += $receivableUnit->total_constituted_value;
        $data->total_constituted_value -= $this->reduceConstitutedValue;

        return $data;
    }

    private function changeValuePreContracted(
        ReceivableUnit $receivableUnit,
        DataRegisterContract $data
    ): DataRegisterContract
    {
        $data->pre_contracted_value += $receivableUnit->pre_contracted_value;
        $data->pre_contracted_value -= $this->reduceConstitutedValue;

        return $data;
    }

    private function changeValueTotalGross(
        ReceivableUnit $receivableUnit,
        DataRegisterContract $data
    ): DataRegisterContract
    {
        $data->total_gross_value += $receivableUnit->total_gross_value;

        return $data;
    }

    private function setContractEffectOrders(ReceivableUnitOrder $receivableUnitOrder): void
    {
        $this->contractEffectOrders = ContractEffectOrder::select('id','receivable_unit_id', 'contract_effect_id', 'value')
            ->where('external_order_id', $receivableUnitOrder->external_order_id)
            ->get()
            ->filter(function($contractEffectOrder) {
                return $contractEffectOrder->contractEffect->effect_type == ContractEffectType::HOLDER_CHANGE;
            });
    }

    private function reduceConstitutedValueOnHolderChangeReceivableUnits(ReceivableUnitOrder $receivableUnitOrder)
    {
        $this->contractEffectOrders->each(function ($contractEffectOrder) use ($receivableUnitOrder) {
            $receivableUnit = $contractEffectOrder->receivableUnit;

            $receivableUnit->update([
                'total_gross_value' => $receivableUnit->total_gross_value - $receivableUnitOrder->gross_value,
                'total_constituted_value' =>  $receivableUnit->total_constituted_value - $contractEffectOrder->value,
                'pre_contracted_value' =>  $receivableUnit->pre_contracted_value - $contractEffectOrder->value
            ]);

            $receivableUnit = $receivableUnit->fresh();

            $payment = $receivableUnit->payments()
                ->first();

            $payment->update(['payment_value' => $receivableUnit->total_constituted_value]);
        });
    }

    private function reduceValueContractEffectOrders(): void
    {
        $this->contractEffectOrders->each(function ($contractEffectOrder) {
            $contractEffectOrder->update(['value' => 0]);
        });
    }

    private function setReduceConstitutedValue(
        ReceivableUnitOrder $receivableUnitOrder,
        DataRegisterContract $data
    ): void
    {
        $this->reduceConstitutedValue = (new ReducerConstitutedValue())
            ->addContractEffectsHolderChangeTotalValue($this->contractEffectOrders)
            ->addReceivableUnitOrder($receivableUnitOrder)
            ->addReversalValue($data->reversal_value)
            ->addGuaranteeValue($data->guarantee_value)
            ->addDebitValue($data->debit_value)
            ->getValueToReduce();
    }
}
