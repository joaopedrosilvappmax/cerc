<?php

namespace App\Models\Services\ReceivableUnits;

use App\Exceptions\NotFoundException;
use App\Models\Entities\Company;
use App\Models\Entities\ReceivableUnitOrder;
use App\Support\EnumTypes\CompanyType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ReceivableUnitOrderVerifyLastCreatedByTimeService
{
    public function verify(): void
    {
        $internalCompany = $this->findInternalCompany();

        if (! $internalCompany) {
            throw new NotFoundException("Internal company not found");
        }

        if ($internalCompany->has_critical_error) {
            return;
        }

        $lastReceivableUnitOrder = $this->findLastReceivableUnitOrder();

        if (! $lastReceivableUnitOrder) {
            throw new NotFoundException("Receivable unit order not found");
        }


        $diff = $this->getDiffInMinutesNowToLastOrder($lastReceivableUnitOrder);

        if ($this->verifyNotificationNeedsToBeSent($diff)) {
            Log::channel('slack')->critical(":exclamation: Did not receive orders within the last 10 minutes.");

            $internalCompany->update(['has_critical_error' => true]);
        }
    }

    private function findLastReceivableUnitOrder(): ?ReceivableUnitOrder
    {
        return ReceivableUnitOrder::withTrashed()
            ->orderByDesc('updated_at')
            ->first(['updated_at']);
    }

    private function getDiffInMinutesNowToLastOrder(ReceivableUnitOrder $lastOrder): int
    {
        return Carbon::now()->diffInMinutes(
            Carbon::createFromFormat('Y-m-d H:i:s', $lastOrder->updated_at)
        );
    }

    private function findInternalCompany(): ?Company
    {
        return Company::firstWhere('type_id', CompanyType::INTERNAL);
    }

    private function verifyNotificationNeedsToBeSent(int $diff): bool
    {
        return $diff >= config('receivable_unit.order.diff_last_created_time');
    }
}
