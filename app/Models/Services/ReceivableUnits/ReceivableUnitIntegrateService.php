<?php

namespace App\Models\Services\ReceivableUnits;

use App\Exceptions\IntegrationException;
use App\Http\Responses\Contracts\IntegrationResponseContract;
use App\Models\Entities\AnalyticConciliationRecord;
use App\Models\Entities\IntegrationError;
use App\Models\Services\Abstracts\IntegrationServiceAbstract;
use App\Models\Services\Contracts\IntegrationBodyServiceContract;
use App\Models\Services\Contracts\IntegrationServiceContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\IntegrationResponseStatus;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\RoutesCERC;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ReceivableUnitIntegrateService extends IntegrationServiceAbstract implements IntegrationServiceContract
{
    public function send(Model $entity): IntegrationResponseContract
    {
        $response = app(IntegrationResponseContract::class);

        try {
            $body = $this->getBody($entity);

            $response = $response->loadResponse(
                json_decode(
                    $this->put(cerc_route_by_config_version(RoutesCERC::RECEIVABLE_UNITS), $body)
                        ->getBody()
                        ->getContents(),
                    true
                )
            );

            $this->verifyErrors($entity, $body, $response);

            $this->updateAnalyticConciliationRecord($entity);

            return $response;

        } catch (IntegrationException $e) {
            Log::error('integrationReceivableUnitService', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'companyId' => $entity->id
            ]);

            return $response;
        }
    }

    private function getBody(Model $entity): array
    {
        $serviceVersion = ClassBindResolution::RECEIVABLE_UNIT_INTEGRATION_BODY_V14;

        return app(IntegrationBodyServiceContract::class, [
            'class' => $serviceVersion
        ])->mountBody($entity);
    }

    private function verifyErrors(Model $entity, array $request, IntegrationResponseContract $response): void
    {
        if ($response->getStatus() !== IntegrationResponseStatus::SUCCESS) {

            IntegrationError::create([
                'receivable_unit_id' => $entity->id,
                'code' => $response->getErrorCode() ?: 0,
                'request' => json_encode($request),
                'response' => json_encode($response->getErrors())
            ]);
        }
    }

    private function updateAnalyticConciliationRecord(Model $entity)
    {
        if (in_array($entity->operation_type, OperationType::getAllInactive())) {
            return;
        }

        AnalyticConciliationRecord::updateOrCreate(
            [
                'reference_date' => Carbon::now()->format('Y-m-d'),
                'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
                'final_recipient_document_number' => $entity->final_recipient_document_number,
                'code_arrangement_payment' => $entity->code_arrangement_payment,
                'settlement_date' => $entity->settlement_date,
                'holder_document_number' => $entity->holder_document_number,
            ],
            [
                'receivable_unit_id' => $entity->id,
                'total_gross_value' => $entity->total_gross_value,
                'total_constituted_value' => $entity->total_constituted_value,
                'pre_contracted_value' => $entity->pre_contracted_value,
            ]
        );
    }
}
