<?php

namespace App\Models\Services\Reports;

use App\Models\Entities\AnalyticConciliationRecord;
use App\Models\Entities\SyntheticConciliationRecord;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class GenerateSyntheticConciliationService
{
    public function process(): void
    {
        try {

            $calculatedAnalyticData = $this->getCalculatedAnalyticRecords();

            $calculatedAnalyticData->each(function ($data) {
                SyntheticConciliationRecord::create([
                    'reference_date' => Carbon::now()->format('Y-m-d'),
                    'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
                    'code_arrangement_payment' => $data->code_arrangement_payment,
                    'total_gross_value' => $data->total_gross_value,
                    'total_constituted_value' => $data->total_constituted_value,
                    'pre_contracted_value' => $data->pre_contracted_value,
                    'anticipation_not_settled_value' => $data->anticipation_not_settled_value,
                ]);
            });
        } catch (\Exception $e) {
            Log::error('generateSyntheticConciliationService.process', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }
    }

    private function getCalculatedAnalyticRecords(): Collection
    {
        return AnalyticConciliationRecord::where('reference_date', Carbon::now()->format('Y-m-d'))
            ->groupBy('code_arrangement_payment')
            ->selectRaw('
                code_arrangement_payment,
                sum(total_gross_value) total_gross_value,
                sum(total_constituted_value) total_constituted_value,
                sum(pre_contracted_value) pre_contracted_value,
                sum(anticipation_not_settled_value) anticipation_not_settled_value
            ')
            ->get();
    }
}
