<?php

namespace App\Models\Services\Reports;

use App\Models\Entities\AnalyticConciliationRecord;
use App\Models\Entities\Anticipation;
use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class GenerateAnalyticConciliationFromAnticipationValueService
{
    public function process(): void
    {
        try {
            $this->getAnticipations()
                ->each(function ($anticipation) {
                    AnalyticConciliationRecord::updateOrCreate(
                        [
                            'reference_date' => Carbon::now()->format('Y-m-d'),
                            'final_recipient_document_number' => $anticipation->final_recipient_document_number,
                            'code_arrangement_payment' => $anticipation->code_arrangement_payment,
                            'settlement_date' => $anticipation->settlement_date,
                            'holder_document_number' => $anticipation->holder_document_number,
                            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
                        ],
                        [
                            'anticipation_not_settled_value' => $anticipation->total_constituted_value,
                        ]
                    );
                });
        } catch (\Exception $e) {
            Log::error('generateAnalyticConciliationService.process', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }
    }

    private function getAnticipations()
    {
        return Anticipation::whereNotIn('operation_type', OperationType::getAllInactive())
            ->whereNotNull('integrated_at')
            ->get([
                'id',
                'final_recipient_document_number',
                'code_arrangement_payment',
                'settlement_date',
                'holder_document_number',
                'total_constituted_value',
            ]);
    }
}
