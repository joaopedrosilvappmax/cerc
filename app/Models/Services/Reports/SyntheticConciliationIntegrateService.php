<?php

namespace App\Models\Services\Reports;

use App\Http\Responses\Contracts\IntegrationResponseContract;
use App\Models\Entities\SyntheticConciliationRecord;
use App\Models\Services\Abstracts\IntegrationServiceAbstract;
use App\Support\EnumTypes\IntegrationResponseStatus;
use App\Support\EnumTypes\RoutesCERC;
use App\Support\ValueObjects\Money;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use JetBrains\PhpStorm\NoReturn;

class SyntheticConciliationIntegrateService extends IntegrationServiceAbstract
{
    #[NoReturn] public function send(): void
    {
        $entities = SyntheticConciliationRecord::where(
            'reference_date',
            Carbon::now()->subDay()->format('Y-m-d')
        )->get();

        try {
            $body = [];

            $entities->each(function ($entity) use (&$body) {
                $body[] = $this->mountBody($entity);
            });

            $response = app(IntegrationResponseContract::class);

            $response = $response->loadResponse(
                json_decode(
                    $this->post(RoutesCERC::SCHEDULE_CONCILIATION, $body)
                        ->getBody()
                        ->getContents(),
                    true
                )
            );

            $entities->each(function ($entity) use ($response) {
                $this->updateIntegratedAt($entity, $response);
            });

        } catch (\Exception $e) {
            Log::error('integrationSyntheticConciliationService', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'recordsId' => [$entities->pluck('id')]
            ]);
        }
    }

    private function mountBody(Model $entity): array
    {
        return [
            'referenciaExterna' => $entity->id,
            'dataReferencia' => $entity->reference_date,
            'cnpjCredenciadora' => $entity->accrediting_company_document_number,
            'codigoArranjoPagamento' => $entity->code_arrangement_payment,
            'valorBrutoTotal' => (new Money($entity->total_gross_value))->toDecimal(),
            'valorConstituidoTotal' => (new Money($entity->total_constituted_value))->toDecimal(),
            'valorConstituidoPreContratado' => (new Money($entity->pre_contracted_value))->toDecimal(),
            'valorLiquidadoPosContratadas' => (new Money($entity->anticipation_not_settled_value))->toDecimal()
        ];
    }

    private function updateIntegratedAt(Model $entity, IntegrationResponseContract $response): void
    {
        if ($response->getStatus() !== IntegrationResponseStatus::SUCCESS
            && in_array($response->getErrorCode(), IntegrationResponseStatus::createdByEntity($entity))) {

            $entity->update([
                'integrated_at' => Carbon::now()
            ]);
        }

        if ($response->getStatus() !== IntegrationResponseStatus::SUCCESS) {

            Log::error('syntheticConciliationIntegrateService', [
                'message' => 'Synthetic conciliation error integration',
                'response' => [$response]
            ]);

            return;
        }

        $entity->update(['integrated_at' => Carbon::now()]);
    }
}
