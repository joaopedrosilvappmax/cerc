<?php

namespace App\Models\Services\Reports;

use App\Models\Entities\AnalyticConciliationRecord;
use App\Models\Entities\ReceivableUnit;
use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class GenerateAnalyticConciliationFromReceivableUnitValueService
{
    public function process(): void
    {
        try {
            $this->getReceivableUnits()
                ->each(function ($receivableUnit) {
                    AnalyticConciliationRecord::updateOrCreate(
                        [
                            'reference_date' => Carbon::now()->format('Y-m-d'),
                            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
                            'final_recipient_document_number' => $receivableUnit->final_recipient_document_number,
                            'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
                            'settlement_date' => $receivableUnit->settlement_date,
                            'holder_document_number' => $receivableUnit->holder_document_number,
                        ],
                        [
                            'receivable_unit_id' => $receivableUnit->id,
                            'total_gross_value' => $receivableUnit->total_gross_value,
                            'total_constituted_value' => $receivableUnit->total_constituted_value,
                            'pre_contracted_value' => $receivableUnit->pre_contracted_value,
                        ]
                    );
                });
        } catch (\Exception $e) {
            Log::error('generateAnalyticConciliationService.process', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }
    }

    private function getReceivableUnits()
    {
        return ReceivableUnit::where('created_at', '<', Carbon::now()->startOfDay())
            ->whereNotIn('operation_type', OperationType::getAllInactive())
            ->get([
                'id',
                'final_recipient_document_number',
                'code_arrangement_payment',
                'settlement_date',
                'holder_document_number',
                'total_gross_value',
                'total_constituted_value',
                'pre_contracted_value',
            ]);
    }
}
