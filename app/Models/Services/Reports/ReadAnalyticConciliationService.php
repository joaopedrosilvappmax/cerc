<?php

namespace App\Models\Services\Reports;

use App\Models\Entities\AnalyticConciliationRecord;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\FileServiceContract;
use App\Support\ValueObjects\Money;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ReadAnalyticConciliationService
{
    public function read(Carbon $date): void
    {
        try {
            $fileName = $this->getMountedFileName($date);

            $columns = config('cerc_ap009.file_columns');

            $file = app(FileServiceContract::class)
                ->getStream("regulatorio/envio_conciliacao_agenda/saida/{$fileName}");

            $row = 0;

            while ($line = fgetcsv($file, 1000, ";")) {
                if ($row++ == 0) {
                    continue;
                }

                $conciliationRecord = $this->getAnalyticConciliationRecord(
                    $date, $line[$columns['reference_external_id']]
                );

                if (! $conciliationRecord) {
                    $this->createNotSettledRecord($line, $columns);
                    continue;
                }

                $conciliationRecord->update([
                    'value_is_divergent' => $this->checkDivergenceInValues($conciliationRecord, $line, $columns),
                    'real_total_constituted_value' => (new Money($line[$columns['total_constituted_value']]))->toInteger(),
                    'real_pre_contracted_value' => (new Money($line[$columns['pre_contracted_value']]))->toInteger(),
                    'real_total_gross_value' => (new Money($line[$columns['total_gross_value']]))->toInteger(),
                    'real_anticipation_not_settled_value' => (new Money($line[$columns['anticipation_not_settled_value']]))->toInteger(),
                ]);

            }

        } catch (\Exception $e) {
            Log::error('readAnalyticConciliationService.process', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine()
            ]);
        }
    }

    public function getMountedFileName(Carbon $date): string
    {
        $isbp = substr(config('accrediting_company.appmax_document_number'), 0, 8);
        $sequentialNumber = config('cerc_ap009.sequential_number_file');

        return "CERC-AP009_{$isbp}_{$date->format('Ymd')}_{$sequentialNumber}_ret.csv";
    }

    public function getAnalyticConciliationRecord(
        Carbon $referenceDate,
        int $referenceExternalId
    ): ?AnalyticConciliationRecord
    {
        return AnalyticConciliationRecord::where([
            'reference_date' => $referenceDate->format('Y-m-d'),
            'receivable_unit_id' => $referenceExternalId
        ])->first();
    }

    public function checkDivergenceInValues($conciliationRecord, $line, array $columns): bool
    {
        return $conciliationRecord->total_gross_value != (new Money($line[$columns['total_gross_value']]))->toInteger()
            || $conciliationRecord->total_constituted_value != (new Money($line[$columns['total_constituted_value']]))->toInteger()
            || $conciliationRecord->pre_contracted_value != (new Money($line[$columns['pre_contracted_value']]))->toInteger()
            || $conciliationRecord->anticipation_not_settled_value != (new Money($line[$columns['anticipation_not_settled_value']]))->toInteger();
    }

    public function createNotSettledRecord(array $line, array $columns): ?AnalyticConciliationRecord
    {
        $receivableUnit = $this->getReceivableUnit($line[$columns['reference_external_id']]);

        if (! $receivableUnit) {
            Log::info('receivableUnitNotExists', [
                'receivableUnitId' => $line[$columns['reference_external_id']]
            ]);

            return null;
        }

        return AnalyticConciliationRecord::create([
            'receivable_unit_id' => $line[$columns['reference_external_id']],
            'reference_date' => $line[$columns['reference_date']],
            'accrediting_company_document_number' => $line[$columns['accrediting_company_document_number']],
            'final_recipient_document_number' => $line[$columns['final_recipient_document_number']],
            'code_arrangement_payment' => $line[$columns['code_arrangement_payment']],
            'settlement_date' => $line[$columns['settlement_date']],
            'holder_document_number' => $line[$columns['holder_document_number']],
            'total_constituted_value' => (new Money($line[$columns['total_constituted_value']]))->toInteger(),
            'pre_contracted_value' => (new Money($line[$columns['pre_contracted_value']]))->toInteger(),
            'total_gross_value' => (new Money($line[$columns['total_gross_value']]))->toInteger(),
            'anticipation_not_settled_value' => (new Money($line[$columns['anticipation_not_settled_value']]))->toInteger(),
            'real_total_constituted_value' => (new Money($line[$columns['total_constituted_value']]))->toInteger(),
            'real_pre_contracted_value' => (new Money($line[$columns['pre_contracted_value']]))->toInteger(),
            'real_total_gross_value' => (new Money($line[$columns['total_gross_value']]))->toInteger(),
            'real_anticipation_not_settled_value' => (new Money($line[$columns['anticipation_not_settled_value']]))->toInteger(),
            'is_not_settled' => true
        ]);
    }

    public function getReceivableUnit(int $referenceExternalId): ?ReceivableUnit
    {
        return ReceivableUnit::find($referenceExternalId);
    }
}
