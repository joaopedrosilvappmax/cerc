<?php

namespace App\Models\Services\Authentications;

use App\Exceptions\IntegrationAuthenticationException;
use App\Http\Responses\Contracts\AuthenticationResponseContract;
use App\Models\Services\Contracts\AuthenticationServiceContract;
use App\Support\EnumTypes\IntegrationResponseStatus;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use App\Models\Entities\Authentication;
use Illuminate\Support\Facades\Log;

class AuthenticationService implements AuthenticationServiceContract
{
    /**
     * @throws IntegrationAuthenticationException
     */
    public function getToken(): string
    {
        $authentication = Authentication::first();

        if ($authentication && $authentication->updated_at->addSeconds($authentication->expires_in) <= Carbon::now()) {
            $authentication = $this->authenticate();
        }

        if (! $authentication) {
            $authentication = $this->authenticate();
        }

        if (! $authentication) {
            throw new IntegrationAuthenticationException("Unable to authenticate to CERC server ");
        }

        return "{$authentication->token_type} {$authentication->access_token}";
    }

    private function authenticate()
    {
        try {
            $client_id = config('cerc.client_id');
            $client_secret = config('cerc.client_secret');

            $body = ['grant_type' => 'client_credentials'];

            $response = Http::asForm()->withHeaders([
                'Authorization' => 'Basic ' . base64_encode("{$client_id}:{$client_secret}")
            ])->post(config('cerc.api_url') . "oauth/token", $body);

            $response = app(AuthenticationResponseContract::class)
                ->loadResponse(json_decode($response->getBody()->getContents(), true));

            if ($response->getStatus() != IntegrationResponseStatus::SUCCESS) {
                return null;
            }

            return Authentication::updateOrCreate(
                ['token_type' => $response->token_type],
                array_merge($response->toArray(), ['updated_at' => Carbon::now()])
            );
        } catch (\Exception $e) {
            Log::error('authenticationService', [
                'message' => $e->getMessage()
            ]);

            return null;
        }
    }
}
