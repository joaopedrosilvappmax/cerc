<?php

namespace App\Models\Services\Notifications;

use App\Models\Entities\LogNotificationError;
use App\Support\EnumTypes\LogNotificationErrorType;
use Exception;
use Illuminate\Support\Facades\Log;

class LogNotificationErrorRegisterService
{
    public function incrementCriticalError()
    {
        try {
            $log = LogNotificationError::firstWhere('type', LogNotificationErrorType::CRITICAL);

            return $log->update(['quantity' => $log->quantity + 1]);
        } catch (Exception $e) {
            Log::error('logNotificationErrorRegisterService.incrementCriticalError', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }
    }
}
