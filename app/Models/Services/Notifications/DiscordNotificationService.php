<?php

namespace App\Models\Services\Notifications;

use App\Exceptions\DiscordNotificationException;
use App\Models\Services\Contracts\NotificationServiceContract;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class DiscordNotificationService implements NotificationServiceContract
{
    public function notify(string $message, array $data = null): void
    {
        if (config('app.env') == 'local') {
            return;
        }

        if (! $serverlUrl = config('notifications.discord.servers.cerc_notifications.webhook_url')) {
            return;
        }

        try {
            $response = Http::withHeaders(['Content-Type' => 'application/json'])
                ->post($serverlUrl, [
                    'content' => null,
                    'embeds' => $data
                ]);

            if (! $response->ok()) {
                throw new DiscordNotificationException('Discord notification fail to server', 400);
            }
        } catch (Exception $e) {
            Log::error('slackNotificationService.notify', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }
    }
}
