<?php

namespace App\Models\Services\Notifications;

use App\Exceptions\SlackNotificationException;
use App\Models\Services\Contracts\NotificationServiceContract;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SlackNotificationService implements NotificationServiceContract
{
    public function notify(string $message, array $data = null): void
    {
        if (config('app.env') == 'local') {
            return;
        }

        if (! $channelUrl = config('notifications.slack.channels.cerc_notifications.webhook_url')) {
            return;
        }

        try {
            $response = Http::withHeaders(['Content-type' => 'application/json'])
                ->post($channelUrl, [
                    'text' => $message,
                    'blocks' => $data
                ]);

            if (! $response->ok()) {
                throw new SlackNotificationException('Slack notification fail to channel', 400);
            }
        } catch (Exception $e) {
            Log::error('slackNotificationService.notify', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }
    }
}
