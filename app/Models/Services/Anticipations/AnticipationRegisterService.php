<?php

namespace App\Models\Services\Anticipations;

use App\Events\Anticipations\AnticipationRegisterEvent;
use App\Exceptions\NotFoundException;
use App\Exceptions\ReceivableUnitOrderNoHasValueAvailableException;
use App\Models\Entities\Anticipation;
use App\Models\Entities\Company;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Models\Services\Contracts\RegisterStoreServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\OperationType;
use App\Support\ValueObjects\Cnpj;

class AnticipationRegisterService implements RegisterServiceContract, RegisterStoreServiceContract
{
    /**
     * @throws NotFoundException|ReceivableUnitOrderNoHasValueAvailableException
     */
    public function store(DataRegisterContract $data): Anticipation
    {
        $receivableUnitOrder = $this->getReceivableUnitOrder($data->external_order_id);

        $data = $this->setSettlementDate($receivableUnitOrder, $data);

        if ($anticipation = $this->findByPaymentArrangement($data)) {
            $this->updateOperationTypeAnticipation($anticipation);
            $this->updateOperationTypeReceivableUnit($receivableUnitOrder->receivableUnit);
            $this->updateReceivableUnitOrder($receivableUnitOrder, $anticipation);

            AnticipationRegisterEvent::dispatch($anticipation);

            return $anticipation;
        }

        $anticipation = $this->createAnticipation($data);
        $this->createPaymentFrom($anticipation);
        $this->updateOperationTypeReceivableUnit($receivableUnitOrder->receivableUnit);
        $this->updateReceivableUnitOrder($receivableUnitOrder, $anticipation);

        AnticipationRegisterEvent::dispatch($anticipation);

        return $anticipation->fresh('payments');
    }

    /**
     * @throws NotFoundException|ReceivableUnitOrderNoHasValueAvailableException
     */
    private function getReceivableUnitOrder(int $orderId): ?ReceivableUnitOrder
    {
        $receivableUnitOrder = ReceivableUnitOrder::where('external_order_id', $orderId)
            ->with('receivableUnit')
            ->first();

        if (! $receivableUnitOrder) {
            throw new NotFoundException('Receivable Unit not found.');
        }

        if (! $receivableUnitOrder->receivableUnit) {
            throw new NotFoundException("No receivable unit found to cash out anticipation", 404);
        }

        if (! $receivableUnitOrder->receivableUnit->total_constituted_value) {
            throw new ReceivableUnitOrderNoHasValueAvailableException(
                "No value receivable unit to cash out anticipation",
                404
            );
        }

        return $receivableUnitOrder;
    }

    private function findByPaymentArrangement(DataRegisterContract $data): ?Anticipation
    {
        return Anticipation::where([
            'external_cash_out_id' => $data->external_cash_out_id,
            'holder_document_number' => $data->holder_document_number,
            'code_arrangement_payment' => $data->code_arrangement_payment,
            'settlement_date' => $data->settlement_date,
        ])->where('operation_type', '<>', OperationType::WRITE_OFF)->first();
    }

    private function setSettlementDate(
        ReceivableUnitOrder $receivableUnitOrder,
        DataRegisterContract $data
    ): DataRegisterContract
    {
        $data->settlement_date = $receivableUnitOrder->receivableUnit->settlement_date;

        return $data;
    }

    private function createAnticipation(DataRegisterContract $data): Anticipation
    {
        $data->company_id = Company::where('document_number', $data->final_recipient_document_number)->first()->id;
        $data->accrediting_company_id = config('accrediting_company.appmax_id');
        $data->accrediting_company_document_number = config('accrediting_company.appmax_document_number');

        return Anticipation::create($data->toArray());
    }

    private function updateReceivableUnitOrder(
        ReceivableUnitOrder $receivableUnitOrder,
        Anticipation $anticipation
    ): void
    {
        $receivableUnitOrder->update([
            'anticipation_id' => $anticipation->id,
            'anticipation_value' => $receivableUnitOrder->getRawOriginal('value'),
        ]);
    }

    private function updateOperationTypeAnticipation(Anticipation $anticipation): void
    {
        $anticipation->update([
            'operation_type' => $anticipation->integrated_at ? OperationType::UPDATE : OperationType::CREATE
        ]);
    }

    private function updateOperationTypeReceivableUnit(ReceivableUnit $receivableUnit): void
    {
        if ($receivableUnit->operation_type == OperationType::WRITE_OFF) {
            return;
        }

        $receivableUnit->update([
            'operation_type' => $receivableUnit->integrated_at ? OperationType::UPDATE : OperationType::CREATE
        ]);
    }

    private function createPaymentFrom(Anticipation $anticipation): void
    {
        $receivableUnit = ReceivableUnit::where([
            'holder_document_number' => $anticipation->holder_document_number,
            'code_arrangement_payment' => $anticipation->code_arrangement_payment,
            'settlement_date' => $anticipation->settlement_date,
        ])->first();

        $paymentFromReceivableUnit = $receivableUnit->payments()
            ->where('holder_document_number', $receivableUnit->holder_document_number)
            ->first();

        Payment::create([
            'receivable_unit_payment_id' => $paymentFromReceivableUnit->id,
            'external_cash_out_company_id' => $anticipation->external_cash_out_id,
            'anticipation_id' => $anticipation->id,
            'holder_document_number' => $anticipation->holder_document_number,
            'ispb' => (new Cnpj($anticipation->holder_document_number))->getISBP(),
            'compe' => $anticipation->company->compe,
            'bank_account_type' => $anticipation->company->bank_account_type,
            'bank_branch' => $anticipation->company->bank_branch,
            'bank_account' => $anticipation->company->bank_account,
            'payment_value' => $anticipation->total_constituted_value,
        ]);
    }
}
