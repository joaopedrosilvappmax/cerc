<?php

namespace App\Models\Services\Anticipations;

use App\Models\Entities\Anticipation;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Support\EnumTypes\ContractEffectType;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class AnticipationCalculateValueService
{
    public function calculate(Anticipation $anticipation)
    {
        $noHolderPaymentValue = $anticipation->payments()
            ->where('holder_document_number', '<>', $anticipation->holder_document_number)
            ->sum('payment_value');

        $paymentAnticipationHolder = $anticipation->getPaymentHolder();

        if (! $paymentAnticipationHolder) {
            Log::info('paymentUpdateFromAnticipationObserverService', [
                'message' => 'Anticipation payment not found',
                'anticipationId' => $anticipation->id
            ]);

            return null;
        }

        $totalConstitutedValue = ReceivableUnitOrder::where('anticipation_id', $anticipation->id)
            ->get()
            ->sum(function ($receivableUnitOrder) {
                $calculatedAnticipationValue = $receivableUnitOrder->contractEffectOrders()
                    ->whereHas('contractEffect', function ($query) {
                        return $query->where('effect_type', ContractEffectType::HOLDER_CHANGE);
                    })
                    ->sum('value');

                return $receivableUnitOrder->getRawOriginal('value') - $calculatedAnticipationValue - $receivableUnitOrder->debit_value;
            });

        $anticipation->update([
            'total_constituted_value' => $totalConstitutedValue,
            'total_value' => $totalConstitutedValue,
            'paid_value' => $totalConstitutedValue,
        ]);

        $paymentAnticipationHolder->update([ 'payment_value' => $totalConstitutedValue - $noHolderPaymentValue]);

        $receivableUnit = $this->findReceivableUnitBy($anticipation);

        $receivableUnitOrders = $receivableUnit->receivableUnitOrders;

        $this->updateTotalConstituted($receivableUnit, $receivableUnitOrders);

        $this->updatePaymentFromReceivableUnit($anticipation, $receivableUnit, $receivableUnitOrders);
    }

    private function findReceivableUnitBy(Anticipation $anticipation): ?ReceivableUnit
    {
        return ReceivableUnit::firstWhere([
            'holder_document_number' => $anticipation->holder_document_number,
            'code_arrangement_payment' => $anticipation->code_arrangement_payment,
            'settlement_date' => $anticipation->settlement_date,
        ]);
    }

    private function updatePaymentFromReceivableUnit(
        Anticipation $anticipation,
        ReceivableUnit $receivableUnit,
        Collection $receivableUnitOrders
    )
    {
        $payment = $receivableUnit->payments
            ->where('holder_document_number', $anticipation->holder_document_number)
            ->first();

        $payment->update([
            'payment_value' => $receivableUnitOrders
                    ->where('receivable_unit_id', $receivableUnit->id)
                    ->whereNull('anticipation_id')
                    ->sum(function ($receivableUnitOrder) {
                        return $receivableUnitOrder->net_value;
                    })
        ]);
    }

    private function updateTotalConstituted(
        ReceivableUnit $receivableUnit,
        Collection $receivableUnitOrders
    ): void
    {
        $valueNoAnticipatedHolderChange = $receivableUnitOrders->whereNull('anticipation_id')
            ->sum(function ($receivableUnitOrder) {
                return $receivableUnitOrder->contractEffectOrders()
                    ->whereHas('contractEffect', function ($query) {
                        return $query->where('effect_type', ContractEffectType::HOLDER_CHANGE);
                    })->sum('value');
            });

        $receivableUnit->update([
            'total_constituted_value' => $receivableUnitOrders->sum('value') - $valueNoAnticipatedHolderChange
        ]);
    }

    private function getCalculatedAnticipatedOrders(Anticipation $anticipation)
    {
        return ReceivableUnitOrder::where('anticipation_id', $anticipation->id)
            ->get(['id','external_order_id'])
            ->sum(function ($receivableUnitOrder) {
                return $receivableUnitOrder->contractEffectOrders()
                    ->whereHas('contractEffect', function ($query) {
                        return $query->where('effect_type', ContractEffectType::HOLDER_CHANGE);
                    })
                    ->sum('value');
            });
    }
}
