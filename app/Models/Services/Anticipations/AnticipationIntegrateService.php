<?php

namespace App\Models\Services\Anticipations;

use App\Exceptions\IntegrationException;
use App\Http\Responses\Contracts\IntegrationResponseContract;
use App\Models\Entities\IntegrationError;
use App\Models\Services\Abstracts\IntegrationServiceAbstract;
use App\Models\Services\Contracts\IntegrationServiceContract;
use App\Support\EnumTypes\IntegrationResponseStatus;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\RoutesCERC;
use App\Support\ValueObjects\Money;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class AnticipationIntegrateService extends IntegrationServiceAbstract implements IntegrationServiceContract
{
    public function send(Model $entity): IntegrationResponseContract
    {
        $response = app(IntegrationResponseContract::class);

        try {
            $request = $this->mountBody($entity);

            $response = $response->loadResponse(
                json_decode(
                    $this->put(RoutesCERC::ANTICIPATION, $request)
                        ->getBody()
                        ->getContents(),
                    true
                )
            );

            $this->updateIntegratedAt($entity, $request, $response);

            return $response;

        } catch (IntegrationException $e) {
            Log::error('integrationAnticipationService', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'companyId' => $entity->id
            ]);

            return $response;
        }
    }

    private function mountBody(Model $entity): array
    {
        return [
            'tipoOperacao' => $entity->operation_type,
            'referenciaExterna' => $entity->id,
            'instituicaoCredenciadora' => $entity->accrediting_company_document_number,
            'documentoUsuarioFinalRecebedor' => $entity->final_recipient_document_number,
            'codigoArranjoPagamento' => $entity->code_arrangement_payment,
            'dataContratadaAntecipacao' => $entity->created_at->format('Y-m-d'),
            'dataLiquidacao' => $entity->settlement_date,
            'valorConstituidoAntecipado' => (new Money($entity->total_constituted_value))->toDecimal(),
            'valorAntecipado' => (new Money($entity->paid_value))->toDecimal(),
            'pagamentos' => $this->mountPayments($entity, $entity->payments)
        ];
    }

    private function mountPayments(Model $entity, Collection $payments): array
    {
        $payAddress = [];

        $payments->each(function ($payment) use (&$payAddress, $entity) {
            $payAddress[] = [
                'domicilioPagamento' => [
                    'numeroDocumentoTitular' => $payment->holder_document_number,
                    'tipoConta' => $payment->bank_account_type,
                    'ispb' => $payment->ispb,
                    'compe' => $payment->compe,
                    'agencia' => $payment->bank_branch,
                    'numeroConta' => $payment->bank_account
                ],
                'dataPagamento' => $entity->request_date,
                'valorPago' => (new Money($entity->paid_value))->toDecimal()
            ];
        });

        return $payAddress;
    }

    private function updateIntegratedAt(Model $entity, array $request, IntegrationResponseContract $response): bool
    {
        if ($response->getStatus() !== IntegrationResponseStatus::SUCCESS
            && in_array($response->getErrorCode(), IntegrationResponseStatus::createdByEntity($entity))) {
            $entity->update([
                'operation_type' => OperationType::UPDATE,
                'integrated_at' => Carbon::now()
            ]);
        }

        if ($response->getStatus() !== IntegrationResponseStatus::SUCCESS) {

            IntegrationError::create([
                'anticipation_id' => $entity->id,
                'code' => $response->getErrorCode() ?: 0,
                'request' => json_encode($request),
                'response' => json_encode($response->getErrors())
            ]);

            return false;
        }

        return $entity->update(['integrated_at' => Carbon::now()]);
    }
}
