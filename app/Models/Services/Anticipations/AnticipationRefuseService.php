<?php

namespace App\Models\Services\Anticipations;

use App\Events\ReceivableUnits\ReceivableUnitUpdatedWithContractEffectEvent;
use App\Exceptions\NotFoundException;
use App\Models\Entities\Anticipation;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Support\EnumTypes\HttpCode;
use App\Support\EnumTypes\OperationType;

class AnticipationRefuseService
{
    /**
     * @throws NotFoundException
     */
    public function refuse(int $externalCashOutId): Anticipation
    {
        $anticipation = Anticipation::firstWhere('external_cash_out_id', $externalCashOutId);

        if (! $anticipation) {
            throw new NotFoundException("Anticipation not found", HttpCode::NOT_FOUND);
        }

        $anticipation->update(['operation_type' => OperationType::INACTIVE]);

        $this->removeAnticipationValueFromOrders($anticipation);

        $receivableUnit = $this->returnValuesToReceivableUnit($anticipation);

        $this->returnValuesToPayments($anticipation, $receivableUnit);

        ReceivableUnitUpdatedWithContractEffectEvent::dispatch($receivableUnit);

        return $anticipation->refresh();
    }

    private function removeAnticipationValueFromOrders(Anticipation $anticipation)
    {
        ReceivableUnitOrder::where('anticipation_id', $anticipation->id)
            ->update([
                'anticipation_id' => null,
                'anticipation_value' => 0
            ]);
    }

    private function returnValuesToReceivableUnit($anticipation): ReceivableUnit
    {
        $receivableUnit = ReceivableUnit::firstWhere([
            'final_recipient_document_number' => $anticipation->final_recipient_document_number,
            'holder_document_number' => $anticipation->holder_document_number,
            'code_arrangement_payment' => $anticipation->code_arrangement_payment,
            'settlement_date' => $anticipation->settlement_date,
        ]);

        $receivableUnit->update([
            'total_constituted_value' => $receivableUnit->total_constituted_value + $anticipation->total_constituted_value
        ]);

        return $receivableUnit->refresh();
    }

    private function returnValuesToPayments(Anticipation $anticipation, ReceivableUnit $receivableUnit)
    {
        $receivableUnitPayments = $receivableUnit->payments;

        $anticipation->payments->each(function ($payment) use ($receivableUnitPayments) {
            $receivableUnitPayment = $receivableUnitPayments
                ->where('id', $payment->receivable_unit_payment_id)
                ->first();

            $receivableUnitPayment->update([
                'payment_value' => $receivableUnitPayment->payment_value + $payment->payment_value
            ]);
        });
    }
}
