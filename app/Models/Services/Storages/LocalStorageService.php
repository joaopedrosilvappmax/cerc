<?php

namespace App\Models\Services\Storages;

use App\Models\Services\Contracts\StorageServiceContract;
use Illuminate\Http\Request;

class LocalStorageService implements StorageServiceContract
{
    public function upload(Request $request): string
    {
        return storage_path('app/' . $request->file('file')
            ->store("contract-effects"));
    }

    public function delete(string $path): bool
    {
        return unlink($path);
    }
}
