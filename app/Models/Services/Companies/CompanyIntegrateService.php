<?php

namespace App\Models\Services\Companies;

use App\Exceptions\IntegrationException;
use App\Http\Responses\Contracts\IntegrationResponseContract;
use App\Models\Entities\IntegrationError;
use App\Models\Services\Abstracts\IntegrationServiceAbstract;
use App\Models\Services\Contracts\IntegrationServiceContract;
use App\Support\EnumTypes\IntegrationResponseStatus;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\RoutesCERC;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class CompanyIntegrateService extends IntegrationServiceAbstract implements IntegrationServiceContract
{
    public function send(Model $entity): IntegrationResponseContract
    {
        $response = app(IntegrationResponseContract::class);

        try {
            $request = $this->mountBody($entity);

            $response = $response->loadResponse(
                json_decode(
                    $this->put(RoutesCERC::COMPANIES, $request)
                        ->getBody()
                        ->getContents(),
                    true
                )
            );

            $this->updateIntegratedAt($entity, $request, $response);

            return $response;

        } catch (IntegrationException $e) {
            Log::error('integrationCompanyService', [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'message' => $e->getMessage(),
                'companyId' => $entity->id
            ]);

            return $response;
        }
    }

    private function mountBody(Model $entity): array
    {
        return [
            'tipoOperacao' => $entity->operation_type,
            'referenciaExterna' => $entity->id,
            'razaoSocial' => $entity->name,
            'tipoDocumentoUsuarioFinalRecebedor' => $entity->documentType,
            'documentoUsuarioFinalRecebedor' => $entity->document_number,
            'tipoUsuarioFinalRecebedor' => '1',
            'status' => $entity->status,
            'codigoMCC' => 'N/D',
            'bloqueioLiquidacao' => '0',
            'contato' => [
                'email' => $entity->email,
                'celular' => $entity->phone ?: 'N/D',
            ]
        ];
    }

    private function updateIntegratedAt(Model $entity, array $request, IntegrationResponseContract $response): bool
    {
        if ($response->getStatus() !== IntegrationResponseStatus::SUCCESS
            && in_array($response->getErrorCode(), IntegrationResponseStatus::createdByEntity($entity))) {
            $entity->update([
                'operation_type' => OperationType::UPDATE,
                'integrated_at' => Carbon::now()
            ]);
        }

        if ($response->getStatus() !== IntegrationResponseStatus::SUCCESS) {

            IntegrationError::create([
                'company_id' => $entity->id,
                'code' => $response->getErrorCode() ?: 0,
                'request' => json_encode($request),
                'response' => json_encode($response->getErrors())
            ]);

            return false;
        }

        return $entity->update(['integrated_at' => Carbon::now()]);
    }
}
