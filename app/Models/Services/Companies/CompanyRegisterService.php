<?php

namespace App\Models\Services\Companies;

use App\Models\Entities\Company;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Models\Services\Contracts\RegisterStoreServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\Status;

class CompanyRegisterService implements RegisterServiceContract, RegisterStoreServiceContract
{
    public function store(DataRegisterContract $data): Company
    {
        $company = Company::where('document_number', $data->document_number)->first();

        if ($company) {
            $data->status = Status::ACTIVE;
            $data->operation_type = $company->integrated_at ? OperationType::UPDATE : OperationType::CREATE;
        }

        return Company::updateOrCreate(
            ['document_number' => $data->document_number],
            $data->toArray()
        );
    }
}
