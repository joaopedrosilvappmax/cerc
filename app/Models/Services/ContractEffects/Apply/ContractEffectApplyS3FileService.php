<?php

namespace App\Models\Services\ContractEffects\Apply;

use App\Jobs\ContractEffectApply;
use App\Models\Entities\ContractEffect;
use App\Models\Services\Contracts\ContractEffectApplyServiceContract;

class ContractEffectApplyS3FileService implements ContractEffectApplyServiceContract
{
    public function apply(int $id): void
    {
        ContractEffect::where('uploaded_contract_effect_file_id', $id)
            ->get()
            ->sortBy('indicator')
            ->each(function (ContractEffect $contractEffect) {
                ContractEffectApply::dispatch($contractEffect);
            });
    }
}
