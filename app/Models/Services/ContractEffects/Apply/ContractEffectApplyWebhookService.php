<?php

namespace App\Models\Services\ContractEffects\Apply;

use App\Jobs\ContractEffectApply;
use App\Models\Entities\ContractEffect;
use App\Models\Services\Contracts\ContractEffectApplyServiceContract;

class ContractEffectApplyWebhookService implements ContractEffectApplyServiceContract
{
    public function apply(int $id): void
    {
        ContractEffect::where('webhook_request_id', $id)
            ->orderBy('indicator', 'asc')
            ->get()
            ->each(function (ContractEffect $contractEffect) {
                ContractEffectApply::dispatch($contractEffect);
            });
    }
}
