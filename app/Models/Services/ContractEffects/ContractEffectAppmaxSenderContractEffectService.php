<?php

namespace App\Models\Services\ContractEffects;

use App\Http\Responses\AppmaxIntegrationResponse;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\IntegrationError;
use App\Models\Entities\IntegrationSuccess;
use App\Models\Services\Abstracts\IntegrationAppmaxServiceAbstract;
use App\Models\Services\Contracts\AppmaxIntegrationContractEffectServiceContract;
use App\Support\EnumTypes\RoutesAppmax;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ContractEffectAppmaxSenderContractEffectService extends IntegrationAppmaxServiceAbstract implements AppmaxIntegrationContractEffectServiceContract
{
    public function send(ContractEffect $contractEffect, int $paymentValue, array $orders): ?AppmaxIntegrationResponse
    {
        $requestBody = $this->mountBody($contractEffect, $paymentValue, $orders);

        $request = $this->post(RoutesAppmax::CONTRACT_EFFECT, $requestBody);

        $response = app(AppmaxIntegrationResponse::class)->loadResponse([
            'successful' =>$request->successful(),
            'status' => $request->status(),
            'requestBody' => $requestBody,
            'responseBody' => json_decode($request->body(), true)
        ]);

        if ($request->successful()) {
            IntegrationSuccess::create([
                'external_id' => $response->getResponse()->result,
                'contract_effect_id' => $contractEffect->id,
            ]);
        }

        if ($request->failed()) {
            Log::warning('contract-effect.appmax.sender.service.send', [
                'contractEffectId' => $contractEffect->id,
                'responseStatusCode' => $response->getStatus(),
                'response' => $request->body()
            ]);

            IntegrationError::create([
                'contract_effect_id' => $contractEffect->id,
                'code' => $response->getStatus(),
                'request' => json_encode($requestBody),
                'response' => $request->body()
            ]);
        }

        return $response;
    }

    private function mountBody(Model $contractEffect, int $paymentValue, $orders): array
    {
        return [
            'effect_contract_id' => $contractEffect->id,
            'effect_contract_type' => $contractEffect->effect_type,
            'document_number' => $contractEffect->final_recipient_document_number,
            'settlement_date' => $contractEffect->settlement_date,
            'orders' => $orders,
        ];
    }
}
