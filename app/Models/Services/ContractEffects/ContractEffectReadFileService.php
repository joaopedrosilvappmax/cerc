<?php

namespace App\Models\Services\ContractEffects;

use App\Jobs\ContractEffectApply;
use App\Jobs\ContractEffectReadFileProcess;
use App\Models\Entities\ContractEffect;
use App\Models\Services\Contracts\ContractEffectReadFileServiceContract;
use App\Models\Services\Contracts\StorageServiceContract;
use App\Support\EnumTypes\ReceivableUnitConstituteStatus;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class ContractEffectReadFileService implements ContractEffectReadFileServiceContract
{
    public function upload($request): void
    {
        $path = app(StorageServiceContract::class)
            ->upload($request);

        ContractEffectReadFileProcess::dispatch($path);
    }

    public function read(array $data): void
    {
        $separator = ';';
        $columns = config('cerc_ap008.fields');
        $contractEffectColumns = config('cerc_ap008.contract_effects_fields');

        $file = fopen($data['path'], 'r');

        if (! $file) {
            return;
        }

        $count = 1;

        Log::info('START READING CONTRACT EFFECTS');

        while (!feof($file)) {
            $row = fgetcsv($file, 0, $separator);

            if (! $row) {
                continue;
            }

            $row = array_combine($columns, $row);
            $row['contract_effects'] = explode('|', $row['contract_effects']);

            foreach ($row['contract_effects'] as $key => $stringData) {
                $row['contract_effects'][$key] = explode(';', $stringData);
                $row['contract_effects'][$key] = array_combine($contractEffectColumns, $row['contract_effects'][$key]);

                try {
                    $this->storeContractEffect($row, $key);
                } catch (\Exception $e) {
                    Log::critical('contractEffectReadFile', [
                        'message' => $e->getMessage(),
                        'file' => $data['path'],
                        'row' => $count
                    ]);
                }
            }

            $count++;
        }

        fclose($file);

        app(StorageServiceContract::class)->delete($data['path']);

        Log::info('END READING CONTRACT EFFECTS');
    }

    private function storeContractEffect(array $row, int $contractEffectKey): void
    {
        $contractEffect = ContractEffect::updateOrCreate(
            [
                'accrediting_company_document_number' => $row['accrediting_company_document_number'],
                'final_recipient_document_number' => $row['final_recipient_document_number'],
                'code_arrangement_payment' => $row['code_arrangement_payment'],
                'settlement_date' => $row['settlement_date']
            ],
            [
                'constituted' => $row['constituted'],
                'holder_document_number' => $row['holder_document_number'],
                'receivable_unit_id' => ! empty($row['external_reference_receivable_unit_id']) ? $row['external_reference_receivable_unit_id'] : null,
                'contract_generator_event_id' => $row['contract_generator_event_id'],
                'event_datetime' => date('Y-m-d H:i:s', strtotime($row['event_datetime'])),
                'protocol' => $row['contract_effects'][$contractEffectKey]['protocol'],
                'indicator' => $row['contract_effects'][$contractEffectKey]['indicator'],
                'registration_entity_document_number' => $row['contract_effects'][$contractEffectKey]['registration_entity_document_number'],
                'effect_type' => $row['contract_effects'][$contractEffectKey]['effect_type'],
                'holder_beneficiary_document_number' => $row['contract_effects'][$contractEffectKey]['holder_beneficiary_document_number'],
                'division_rule' => $row['contract_effects'][$contractEffectKey]['division_rule'],
                'committed_value' => $row['contract_effects'][$contractEffectKey]['committed_value'],
                'payment_holder_document_number' => $row['contract_effects'][$contractEffectKey]['payment_holder_document_number'],
                'bank_account_type' => $row['contract_effects'][$contractEffectKey]['bank_account_type'],
                'compe' => $row['contract_effects'][$contractEffectKey]['compe'],
                'ispb' => $row['contract_effects'][$contractEffectKey]['ispb'],
                'bank_branch' => $row['contract_effects'][$contractEffectKey]['bank_branch'],
                'bank_account' => $row['contract_effects'][$contractEffectKey]['bank_account'],
                'holder_name' => $row['contract_effects'][$contractEffectKey]['holder_name'],
                'external_contract_id' => $row['contract_effects'][$contractEffectKey]['external_contract_id'],
            ]
        );

        $this->applyEffect($contractEffect);
    }

    private function applyEffect(ContractEffect $contractEffect)
    {
        if ($contractEffect->constituted == ReceivableUnitConstituteStatus::CONSTITUTED) {
            ContractEffectApply::dispatch($contractEffect);
        }
    }
}
