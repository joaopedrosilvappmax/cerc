<?php

namespace App\Models\Services\ContractEffects\Cancel;

use App\Exceptions\NotFoundException;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Support\EnumTypes\HttpCode;

class ReturnValuesOfContractEffectService
{
    public function returnValues(ReceivableUnit $receivableUnit): void
    {
        $paymentHolder = $this->findPaymentHolder($receivableUnit);

        $this->returnValuesFromHolderChange($receivableUnit, $paymentHolder);

        $this->returnValuesFromOnus($receivableUnit, $paymentHolder);
    }

    private function findPaymentHolder(ReceivableUnit $receivableUnit)
    {
        $payment = $receivableUnit->payments
            ->where('holder_document_number', $receivableUnit->holder_document_number)
            ->first();

        if (! $payment) {
            throw new NotFoundException('Payment holder not found', HttpCode::NOT_FOUND);
        }

        return $payment;
    }

    private function returnValuesFromHolderChange(ReceivableUnit $receivableUnit, Payment $paymentHolder)
    {
        ReceivableUnit::where([
            'receivable_unit_linked_id' => $receivableUnit->id,
        ])->get()->each(function ($receivableUnitChild) {

            $receivableUnitChild->update([
                'total_constituted_value' => 0,
                'total_gross_value' => 0,
                'pre_contracted_value' => 0
            ]);

            $paymentChild = $receivableUnitChild->payments->first();

            $paymentChild->update([
                'payment_value' => 0
            ]);

            $this->updateContractEffectOrders($paymentChild);
        });

        $totalConstitutedValue = $receivableUnit->receivableUnitOrders->sum('value');

        $receivableUnit->update([
            'total_constituted_value' => $totalConstitutedValue
        ]);

        $paymentHolder->update([
            'payment_value' => $totalConstitutedValue
        ]);
    }

    private function returnValuesFromOnus(ReceivableUnit $receivableUnit, Payment $paymentHolder)
    {
        $paymentContractEffects = $receivableUnit->payments()
            ->where('holder_document_number', '<>', $receivableUnit->holder_document_number)
            ->get();

        $paymentHolder->update([
            'payment_value' => $receivableUnit->total_constituted_value
        ]);

        $paymentContractEffects->each(function ($paymentContractEffect) {
            $paymentContractEffect->update(['payment_value' => 0]);
            $this->updateContractEffectOrders($paymentContractEffect);
        });
    }

    private function updateContractEffectOrders(Payment $payment): void
    {
        $payment->contractEffectOrders->each(function ($contractEffectOrder) {
            if (isset($contractEffectOrder->receivableUnitOrder->anticipation_id)) {
                return;
            }

            $contractEffectOrder->update(['value' => 0]);
        });
    }
}
