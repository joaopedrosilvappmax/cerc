<?php

namespace App\Models\Services\ContractEffects\Cancel;

use App\Http\Responses\AppmaxIntegrationResponse;
use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\IntegrationError;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Abstracts\IntegrationAppmaxServiceAbstract;
use App\Models\Services\Contracts\CancelAnticipationContractEffectServiceContract;
use App\Support\EnumTypes\AppmaxWebhookEvent;
use App\Support\EnumTypes\OperationType;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use App\Support\EnumTypes\RoutesAppmax;

class CancelAnticipationContractEffectOnusService extends IntegrationAppmaxServiceAbstract implements CancelAnticipationContractEffectServiceContract
{
    public function apply(ContractEffect $contractEffect): void
    {
        $this->getAnticipations($contractEffect)->each(function ($anticipation) use ($contractEffect) {
            $contractEffectPayment = $anticipation->payments
                ->where('holder_document_number', $contractEffect->holder_beneficiary_document_number)
                ->whereNull('paid_at')
                ->first();

            if (! $contractEffectPayment) {
                Log::info('CancelAnticipationContractEffectOnusService.apply', [
                    'message' => 'Anticipation paid',
                    'anticipationId' => $anticipation->id,
                    'contractEffectCanceledId' => $contractEffect->id
                ]);

                return;
            }

            $canceledValue = $this->getCanceledValue($contractEffect, $anticipation);

            $this->updateHolderPayment($anticipation, $canceledValue);

            $this->reduceValues($contractEffectPayment, $canceledValue);

            $contractEffectOrdersAnticipated = $this->getContractEffectOrdersAnticipated($anticipation->id, $contractEffect->id);

            $this->sendWebhook($contractEffectPayment, $contractEffectOrdersAnticipated);
        });

        $this->deleteContractEffectOrders($contractEffect);
    }

    private function getAnticipations(ContractEffect $contractEffect)
    {
        return Anticipation::with('payments')
            ->where('settlement_date', $contractEffect->settlement_date)
            ->where('code_arrangement_payment', $contractEffect->code_arrangement_payment)
            ->where('holder_document_number', $contractEffect->final_recipient_document_number)
            ->where('operation_type', '<>', OperationType::WRITE_OFF)
            ->get();
    }

    private function getCanceledValue(ContractEffect $contractEffect, Anticipation $anticipation)
    {
        $valueContractEffectOrders = 0;

        ReceivableUnitOrder::where('anticipation_id', $anticipation->id)->get()
            ->each(function($receivableUnitOrder) use ($contractEffect, $anticipation, &$valueContractEffectOrders){
                $valueContractEffectOrders += $receivableUnitOrder->contractEffectOrders
                    ->where('contract_effect_id', $contractEffect->id)
                    ->sum('value');
            });

        return $valueContractEffectOrders;
    }

    private function updateHolderPayment(Anticipation $anticipation, int $canceledValue): int
    {
        $holderPayment = $anticipation->payments
            ->where('holder_document_number', $anticipation->holder_document_number)
            ->first();

        $holderPayment->update([
            'payment_value' => $holderPayment->payment_value + $canceledValue
        ]);

        return $canceledValue;
    }

    private function reduceValues(Payment $payment, int $canceledValue): void
    {
        $payment->update([
            'payment_value' => $payment->payment_value - $canceledValue
        ]);
    }

    private function deleteContractEffectOrders(ContractEffect $contractEffect)
    {
        $contractEffectOrders = ContractEffectOrder::where('contract_effect_orders.contract_effect_id', $contractEffect->id)
            ->with(['receivableUnitOrder.anticipation.payments'])
            ->get();

        $contractEffectOrders->each(function ($contractEffectOrder) use ($contractEffect) {
            if (! $contractEffectOrder->receivableUnitOrder->anticipation_id) {
                $contractEffectOrder->delete();
                return;
            }

            $contractEffectOrderWithAnticipationPay = $contractEffectOrder->receivableUnitOrder->anticipation->payments
                ->where('holder_document_number', '=', $contractEffect->holder_beneficiary_document_number)
                ->whereNull('paid_at')
                ->first();

            if ($contractEffectOrderWithAnticipationPay) {
                $contractEffectOrder->delete();
            }
        });
    }

    private function sendWebhook(Payment $contractEffectPayment, Collection $contractEffectOrdersAnticipated)
    {
        try {
            $body = [
                'event' => AppmaxWebhookEvent::CANCEL_CONTRACT_EFFECT,
                'data' => [
                    'cercCashOutRequestId' => $contractEffectPayment->external_cash_out_id,
                    'orders' => $contractEffectOrdersAnticipated->toArray()
                ]
            ];

            $request = $this->post(RoutesAppmax::CONTRACT_EFFECT, $body);

            $response = app(AppmaxIntegrationResponse::class)->loadResponse([
                'successful' =>$request->successful(),
                'status' => $request->status(),
                'requestBody' => ['externalCashOutId' => $contractEffectPayment->external_cash_out_id],
                'responseBody' => json_decode($request->body(), true)
            ]);

            if ($request->failed()) {
                Log::warning('CancelAnticipationContractEffectOnusService.sendWebhook', [
                    'paymentId' => $contractEffectPayment->id,
                    'externalCashOutId' => $contractEffectPayment->external_cash_out_id,
                    'responseStatusCode' => $response->getStatus(),
                    'response' => $request->body()
                ]);

                IntegrationError::create([
                    'anticipation_id' => $contractEffectPayment->anticipation_id,
                    'code' => $response->getStatus(),
                    'request' => json_encode($body),
                    'response' => $request->body()
                ]);
            }
        } catch (Exception $exception) {
            Log::error('CancelAnticipationContractEffectOnusService.sendWebhook', [
                'anticipationId' => $contractEffectPayment->anticipation_id,
                'externalCashOutId' => $contractEffectPayment->external_cash_out_id,
                'message' => $exception->getMessage(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine()
            ]);

            IntegrationError::create([
                'anticipation_id' => $contractEffectPayment->anticipation_id,
                'code' => $exception->getCode(),
                'request' => json_encode($body),
                'response' => json_encode(['exceptionError' => $exception->getMessage()])
            ]);
        }
    }

    public function getContractEffectOrdersAnticipated($anticipationId, $contractEffectId): Collection
    {
        return ContractEffectOrder::select(
            'contract_effect_orders.external_order_id',
            'contract_effect_orders.value',
        )->join('receivable_unit_orders', 'contract_effect_orders.external_order_id', '=', 'receivable_unit_orders.external_order_id', )
            ->where('contract_effect_orders.contract_effect_id', $contractEffectId)
            ->where('receivable_unit_orders.anticipation_id', $anticipationId)
            ->get();
    }
}
