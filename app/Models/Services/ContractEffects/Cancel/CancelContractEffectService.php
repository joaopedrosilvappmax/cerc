<?php

namespace App\Models\Services\ContractEffects\Cancel;

use App\Events\ReceivableUnits\ReceivableUnitUpdatedWithContractEffectEvent;
use App\Exceptions\NotFoundException;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Abstracts\ContractEffectServiceAbstract;
use App\Models\Services\Contracts\CancelAnticipationContractEffectServiceContract;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Support\EnumTypes\HttpCode;

class CancelContractEffectService extends ContractEffectServiceAbstract implements ContractEffectServiceContract
{
    public function apply(ContractEffect $contractEffect, ReceivableUnit $receivableUnit): void
    {
        $contractEffectOriginal = $this->findContractEffectOriginal($contractEffect);

        $this->fillApplyDateAndReceivableUnitId($contractEffect, $receivableUnit);

        $contractEffectOriginal->delete();

        app(CancelAnticipationContractEffectServiceContract::class)->apply($contractEffectOriginal);

        ReceivableUnitUpdatedWithContractEffectEvent::dispatch($receivableUnit);
    }

    private function findContractEffectOriginal(ContractEffect $contractEffect): ContractEffect
    {
        $contractEffectOriginal = ContractEffect::where([
                'holder_document_number' => $contractEffect->holder_document_number,
                'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
                'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
                'settlement_date' => $contractEffect->settlement_date,
                'effect_type' => $contractEffect->effect_type
            ])
            ->where('committed_value', '>', 0)
            ->first();

        if (! $contractEffectOriginal) {
            throw new NotFoundException('Contract effect original not found', HttpCode::NOT_FOUND);
        }

        return $contractEffectOriginal;
    }
}
