<?php

namespace App\Models\Services\ContractEffects;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ReceivableUnit;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ContractEffectService
{
    public function getContractEffects(array $contractEffectIds): ?Collection
    {
        if (empty($contractEffectIds)) {
            return null;
        }

        return ContractEffect::whereIn('external_contract_id', $contractEffectIds)
            ->select(
                'id',
                'bank_account',
                'bank_account_type',
                'bank_branch',
                'committed_value',
                'compe',
                'contract_generator_event_id',
                'effect_type',
                'external_contract_id',
                'external_reference_receivable_unit_id',
                'final_recipient_document_number',
                'holder_beneficiary_document_number',
                'holder_document_number',
                'holder_name',
                'ispb',
                'protocol',
                'registration_entity_document_number'
            )
            ->get();
    }

    public function getDailyContractEffectsFromWebhookOrFile()
    {
        $start = Carbon::now()->subDay()->toDateTimeString();
        $finish = Carbon::now()->toDateTimeString();

        return ContractEffect::orWhereHas('uploadedFile', function ($query) use ($start, $finish) {
                $query->where([
                    ['created_at', '>=', $start],
                    ['created_at', '<=', $finish]
                ]);
            })
            ->orWhereHas('webhookRequest', function ($query) use ($start, $finish) {
                $query->where([
                    ['created_at', '>=', $start],
                    ['created_at', '<=', $finish]
                ]);
            })
            ->get();
    }
}
