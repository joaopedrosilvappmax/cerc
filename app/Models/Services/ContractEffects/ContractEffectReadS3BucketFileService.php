<?php

namespace App\Models\Services\ContractEffects;

use App\Models\Entities\UploadedContractEffectFile;
use App\Models\Services\Contracts\ContractEffectApplyServiceContract;
use App\Models\Services\Contracts\ContractEffectReadFileServiceContract;
use App\Models\Services\Contracts\FileServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Models\Services\Files\RegisterContractEffectFilesService;
use App\Support\DataTransferObjects\ContractEffects\ContractEffectDataRegister;
use App\Support\DataTransferObjects\UploadedContractEffectFile\UploadedContractEffectFileDataRegister;
use App\Support\EnumTypes\ClassBindResolution;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;

class ContractEffectReadS3BucketFileService implements ContractEffectReadFileServiceContract
{
    public function read(array $data): void
    {
        $row = 0;
        $columns = config('cerc_ap008.fields');
        $contractEffectColumns = config('cerc_ap008.contract_effects_fields');
        $fileName = $this->getFileName($data['date'], $data['sequential_number']);

        $file = app(FileServiceContract::class)
            ->getStream("regulatorio/efeitos_contrato/saida/{$fileName}");

        $uploadedFileInfo = $this->storeUploadInfo([
            'file_name' => $fileName,
            'date' => Carbon::createFromFormat('Ymd', $data['date'])->format('Y-m-d'),
            'sequential_number' => $data['sequential_number'],
        ]);

        while ($line = fgetcsv($file, 1000, ";")) {
            if ($row++ == 0) {
                continue;
            }

            $line = array_combine($columns, $line);
            $line['contract_effects'] = explode('|', $line['contract_effects']);

            foreach ($line['contract_effects'] as $key => $stringData) {
                $line['contract_effects'][$key] = explode(';', $stringData);
                $line['contract_effects'][$key] = array_combine($contractEffectColumns, $line['contract_effects'][$key]);

                try {
                    $this->storeContractEffect($line, $key, $uploadedFileInfo->id);
                } catch (Exception $e) {
                    Log::error('contractEffect.service.contractEffectReadFileService.read', [
                        'message' => $e->getMessage(),
                        'file' => $e->getFile(),
                        'line' => $e->getLine(),
                        'file_line' => $line
                    ]);
                }
            }
        }

        $this->applyContractEffects($uploadedFileInfo->id);
    }

    private function getFileName(string $date, int $sequentialNumber): string
    {
        $isbp = substr(config('accrediting_company.appmax_document_number'), 0, 8);
        $number = str_pad($sequentialNumber, 7, '0', STR_PAD_LEFT);

        return "CERC-AP008_{$isbp}_{$date}_{$number}_ret.csv";
    }

    private function storeUploadInfo(array $data): UploadedContractEffectFile
    {
        return app(RegisterContractEffectFilesService::class)
            ->register((new UploadedContractEffectFileDataRegister)->addData($data));
    }

    private function storeContractEffect(array $line, int $contractEffectKey, int $uploadedContractEffectFileId): void
    {
        app(RegisterServiceContract::class, ['class' => ClassBindResolution::CONTRACT_EFFECT_SERVICE])
            ->store(
                (new ContractEffectDataRegister)->addData([
                    'accrediting_company_document_number' => $line['accrediting_company_document_number'],
                    'final_recipient_document_number' => $line['final_recipient_document_number'],
                    'code_arrangement_payment' => $line['code_arrangement_payment'],
                    'settlement_date' => $line['settlement_date'],
                    'constituted' => $line['constituted'],
                    'holder_document_number' => $line['holder_document_number'],
                    'receivable_unit_id' => ! empty($line['external_reference_receivable_unit_id']) ? $line['external_reference_receivable_unit_id'] : null,
                    'contract_generator_event_id' => $line['contract_generator_event_id'],
                    'event_datetime' => date('Y-m-d H:i:s', strtotime($line['event_datetime'])),
                    'protocol' => $line['contract_effects'][$contractEffectKey]['protocol'],
                    'indicator' => $line['contract_effects'][$contractEffectKey]['indicator'],
                    'registration_entity_document_number' => $line['contract_effects'][$contractEffectKey]['registration_entity_document_number'],
                    'effect_type' => $line['contract_effects'][$contractEffectKey]['effect_type'],
                    'holder_beneficiary_document_number' => $line['contract_effects'][$contractEffectKey]['holder_beneficiary_document_number'],
                    'division_rule' => $line['contract_effects'][$contractEffectKey]['division_rule'],
                    'committed_value' => $line['contract_effects'][$contractEffectKey]['committed_value'],
                    'payment_holder_document_number' => $line['contract_effects'][$contractEffectKey]['payment_holder_document_number'],
                    'bank_account_type' => $line['contract_effects'][$contractEffectKey]['bank_account_type'],
                    'compe' => $line['contract_effects'][$contractEffectKey]['compe'],
                    'ispb' => $line['contract_effects'][$contractEffectKey]['ispb'],
                    'bank_branch' => $line['contract_effects'][$contractEffectKey]['bank_branch'],
                    'bank_account' => $line['contract_effects'][$contractEffectKey]['bank_account'],
                    'holder_name' => $line['contract_effects'][$contractEffectKey]['holder_name'],
                    'external_contract_id' => $line['contract_effects'][$contractEffectKey]['external_contract_id'],
                    'uploaded_contract_effect_file_id' => $uploadedContractEffectFileId,
                ])
            );
    }

    private function applyContractEffects(int $uploadedFileInfoId): void
    {
        app(ContractEffectApplyServiceContract::class, [
            'class' => ClassBindResolution::CONTRACT_EFFECT_APPLY_S3
        ])->apply($uploadedFileInfoId);
    }
}
