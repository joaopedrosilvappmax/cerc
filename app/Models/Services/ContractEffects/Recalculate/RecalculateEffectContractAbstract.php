<?php

namespace App\Models\Services\ContractEffects\Recalculate;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Abstracts\ContractEffectServiceAbstract;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Support\ValueObjects\Bank;

abstract class RecalculateEffectContractAbstract extends ContractEffectServiceAbstract
{
    public function hasNoValueToRecalculate(ReceivableUnit $receivableUnit) : bool
    {
        return $receivableUnit->total_constituted_value <= 0;
    }

    public function resetContractEffectOrders(ContractEffect $contractEffect, ReceivableUnit $receivableUnit, array $orders): void
    {
        $paymentContractEffect = Payment::where([
                'receivable_unit_id' => $receivableUnit->id,
                'holder_document_number' => $contractEffect->holder_beneficiary_document_number,
                'ispb' => (new Bank($contractEffect->compe))->getFormattedIspb(),
            ])->first();

        app(ContractEffectServiceContract::class, [
            'effect_type' => $contractEffect->effect_type
        ])->storeContractEffectOrders($paymentContractEffect, $receivableUnit, $contractEffect, $orders);
    }
}
