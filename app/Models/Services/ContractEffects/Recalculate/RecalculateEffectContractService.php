<?php

namespace App\Models\Services\ContractEffects\Recalculate;

use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RecalculateContractEffectContract;
use App\Support\EnumTypes\ContractEffectType;

class RecalculateEffectContractService extends RecalculateEffectContractAbstract implements RecalculateContractEffectContract
{
    public function recalculate(ReceivableUnit $receivableUnit): void
    {
        $receivableUnit->contractEffects()
            ->where('effect_type', '<>', ContractEffectType::COURT_BLOCK)
            ->where('committed_value', '>', 0)
            ->orderBy('created_at', 'desc')
            ->orderBy('indicator')
            ->get()
            ->each(function ($contractEffect) use ($receivableUnit) {
                if ($this->hasNoValueToRecalculate($receivableUnit)) {
                    $orders = $this->getOrdersWithValuesCalculatedFromContractEffectApplied($receivableUnit);
                    $this->resetContractEffectOrders($contractEffect, $receivableUnit, $orders);

                    return;
                }

                app(ContractEffectServiceContract::class, [
                    'effect_type' => $contractEffect->effect_type
                ])->apply($contractEffect, $receivableUnit);
            });
    }
}
