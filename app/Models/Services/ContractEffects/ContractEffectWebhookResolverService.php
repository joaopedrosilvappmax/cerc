<?php

namespace App\Models\Services\ContractEffects;

use App\Models\Services\Contracts\WebhookResolverServiceContract;
use App\Support\Adapters\Contracts\DataRegisterAdapterContract;
use App\Support\EnumTypes\ClassBindResolution;

class ContractEffectWebhookResolverService implements WebhookResolverServiceContract
{
    public function resolveWebhookData(array $data, int $webhookRequestId): void
    {
        if (config('cerc_ap008.inactive_webhook')) {
            return;
        }

        $data['webhook_request_id'] = $webhookRequestId;

        foreach ($data['evento']['efeitosContrato'] as $contractEffectData) {
            $webhookData = $data;
            $webhookData['evento']['efeitosContrato'] = $contractEffectData;

            app(ContractEffectRegisterService::class)
                ->store(
                    app(DataRegisterAdapterContract::class, [
                        'class' => ClassBindResolution::CONTRACT_EFFECT_DATA_ADAPTER
                    ])->addData($webhookData)
                );
        }
    }
}
