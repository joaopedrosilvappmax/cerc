<?php

namespace App\Models\Services\ContractEffects;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Abstracts\ContractEffectOnusServiceAbstract;
use App\Models\Services\Contracts\DivisionRuleApplicatorForOnusServiceContract;
use App\Models\Services\Contracts\ReceivableUnitContractContractEffectServiceApplyContract;
use App\Support\ValueObjects\Bank;

class ReceivableUnitContractEffectOnusService extends ContractEffectOnusServiceAbstract implements ReceivableUnitContractContractEffectServiceApplyContract
{
    public function apply(ContractEffect $contractEffect, ReceivableUnit $receivableUnit): ?ReceivableUnit
    {
        $payment = $this->getPaymentFromReceivableUnitHolder($receivableUnit);

        $paymentValue = app(DivisionRuleApplicatorForOnusServiceContract::class, [
            'division_rule' => $contractEffect->division_rule
        ])->apply($contractEffect, $payment);

        $paymentContractEffect = $this->storePayment($contractEffect, $receivableUnit, $paymentValue);

        $this->fillApplyDateAndReceivableUnitId($contractEffect, $receivableUnit);

        $this->storeContractEffectOrders(
            $paymentContractEffect,
            $receivableUnit,
            $contractEffect,
            $this->getOrdersWithValuesCalculatedFromContractEffectApplied($receivableUnit, $paymentValue)
        );

        return $receivableUnit;
    }

    private function storePayment(
        ContractEffect $contractEffect,
        ReceivableUnit $receivableUnit,
        int $paymentValue
    ): Payment
    {
        $payment = $this->findPaymentByHolderBeneficiary($contractEffect, $receivableUnit);

        if ($payment) {
            $payment->update(['payment_value' => $payment->payment_value + $paymentValue]);

            return $payment;
        }

        return Payment::create([
            'receivable_unit_id' => $receivableUnit->id,
            'holder_document_number' => $contractEffect->holder_beneficiary_document_number,
            'ispb' => (new Bank($contractEffect->compe))->getFormattedIspb(),
            'compe' => $contractEffect->compe,
            'bank_account_type' => $contractEffect->bank_account_type,
            'bank_branch' => $contractEffect->bank_branch,
            'bank_account' => $contractEffect->bank_account,
            'payment_value' => $paymentValue
        ]);
    }

    private function findPaymentByHolderBeneficiary(ContractEffect $contractEffect, ReceivableUnit $receivableUnit): ?Payment
    {
        return Payment::where([
            'receivable_unit_id' => $receivableUnit->id,
            'holder_document_number' => $contractEffect->holder_beneficiary_document_number,
            'ispb' => (new Bank($contractEffect->compe))->getFormattedIspb(),
        ])->first();
    }
}
