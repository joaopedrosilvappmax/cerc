<?php

namespace App\Models\Services\ContractEffects;

use App\Models\Entities\ContractEffect;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Models\Services\Contracts\RegisterStoreServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

/**
 * Class ContractEffectRegisterService
 * @package App\Models\Services\ContractEffect
 */
class ContractEffectRegisterService implements RegisterServiceContract, RegisterStoreServiceContract
{
    public function store(DataRegisterContract $data): ContractEffect
    {
        return ContractEffect::create($data->toArray());
    }
}
