<?php

namespace App\Models\Services\ContractEffects\DivisionRuleApplicators;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\DivisionRuleApplicatorForHolderChangeServiceContract;
use App\Support\EnumTypes\Math;

class DivisionRulePercentageValueApplicatorForHolderChangeService implements DivisionRuleApplicatorForHolderChangeServiceContract
{
    public function apply(
        ContractEffect $contractEffect,
        ReceivableUnit $receivableUnit,
        ReceivableUnit $receivableUnitNewHolder,
        Payment $payment): int
    {
        $committedValue = (int) ceil(
            $receivableUnit->total_constituted_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER)
        );

        $balanceHolder = $payment->payment_value - $committedValue;

        if ($balanceHolder < 0) {
            $balanceHolder = 0;
            $contractEffect->committed_value = $payment->payment_value;
            $committedValue = $payment->payment_value;
        }

        $payment->update(['payment_value' => $balanceHolder]);

        $receivableUnit->update(['total_constituted_value' => $receivableUnit->total_constituted_value - $committedValue]);

        $receivableUnitNewHolder->update(['total_constituted_value' => $committedValue]);

        return $committedValue;
    }
}
