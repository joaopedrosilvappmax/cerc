<?php

namespace App\Models\Services\ContractEffects\DivisionRuleApplicators;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Services\Contracts\DivisionRuleApplicatorForOnusServiceContract;

class DivisionRuleFixedValueApplicatorForOnusService implements DivisionRuleApplicatorForOnusServiceContract
{
    public function apply(ContractEffect $contractEffect, Payment $holderPayment, bool $updateHolderPayment = true): int
    {
        $committedValue = $this->getCalculatedCommittedValue($contractEffect);
        $balanceHolder = $holderPayment->payment_value - $committedValue;

        if ($balanceHolder < 0) {
            $committedValue = $holderPayment->payment_value;
            $balanceHolder = 0;
        }

        if (! $updateHolderPayment) {
            return $committedValue;
        }

        $holderPayment->update(['payment_value' => $balanceHolder]);

        return $committedValue;
    }

    private function getCalculatedCommittedValue(ContractEffect $contractEffect)
    {
        $contractEffectTotalAnticipated = ContractEffectOrder::where('contract_effect_id', $contractEffect->id)
            ->whereHas('receivableUnitOrder', function ($query) {
                return $query->select('external_order_id', 'anticipation_id')
                    ->whereNotNull('anticipation_id');
            })->sum('value');

        $total = $contractEffect->committed_value - $contractEffectTotalAnticipated;

        return ($total < 0) ? 0 : $total;
    }
}
