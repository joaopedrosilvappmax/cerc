<?php

namespace App\Models\Services\ContractEffects\DivisionRuleApplicators;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Services\Contracts\DivisionRuleApplicatorForOnusServiceContract;
use App\Support\EnumTypes\Math;

class DivisionRulePercentageValueApplicatorForOnusService implements DivisionRuleApplicatorForOnusServiceContract
{
    public function apply(ContractEffect $contractEffect, Payment $holderPayment, bool $updateHolderPayment = true): int
    {
        $committedValue =  (int) ceil(
            $holderPayment->receivableUnit->total_constituted_value
            * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER)
        );

        $balanceHolder = $holderPayment->payment_value - $committedValue;

        if ($balanceHolder < 0) {
            $balanceHolder = 0;
            $committedValue = $holderPayment->payment_value;
        }

        if (! $updateHolderPayment) {
            return $committedValue;
        }

        $holderPayment->update(['payment_value' => $balanceHolder]);

        return $committedValue;
    }
}
