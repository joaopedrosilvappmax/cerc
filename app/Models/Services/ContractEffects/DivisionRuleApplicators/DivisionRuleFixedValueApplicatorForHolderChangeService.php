<?php

namespace App\Models\Services\ContractEffects\DivisionRuleApplicators;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\DivisionRuleApplicatorForHolderChangeServiceContract;

class DivisionRuleFixedValueApplicatorForHolderChangeService implements DivisionRuleApplicatorForHolderChangeServiceContract
{
    public function apply(
        ContractEffect $contractEffect,
        ReceivableUnit $receivableUnit,
        ReceivableUnit $receivableUnitNewHolder,
        Payment $payment
    ): int
    {
        $committedValue = $contractEffect->committed_value;

        $balanceHolder = $payment->payment_value - $committedValue;

        if ($balanceHolder < 0) {
            $balanceHolder = 0;
           $committedValue = $payment->payment_value;
        }

        $payment->update(['payment_value' => $balanceHolder]);

        $receivableUnit->update([
            'total_constituted_value' => $receivableUnit->total_constituted_value - $committedValue
        ]);

        $receivableUnitNewHolder->update(['total_constituted_value' => $committedValue]);

        return $committedValue;
    }
}
