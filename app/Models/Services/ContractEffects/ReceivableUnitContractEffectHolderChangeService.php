<?php

namespace App\Models\Services\ContractEffects;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Abstracts\ContractEffectServiceAbstract;
use App\Models\Services\Contracts\DivisionRuleApplicatorForHolderChangeServiceContract;
use App\Models\Services\Contracts\ReceivableUnitContractContractEffectServiceApplyContract;
use App\Support\EnumTypes\OperationType;
use App\Support\ValueObjects\Bank;

class ReceivableUnitContractEffectHolderChangeService extends ContractEffectServiceAbstract implements ReceivableUnitContractContractEffectServiceApplyContract
{
    public function apply(ContractEffect $contractEffect, ReceivableUnit $receivableUnit): ?ReceivableUnit
    {
        $payment = $this->getPaymentFromReceivableUnitHolder($receivableUnit);

        $receivableUnitNewHolder = $this->updateOrCreateReceivableUnitNewHolder($contractEffect, $receivableUnit);
        $paymentNewHolder = $this->updateOrCreatePaymentForNewHolder($contractEffect, $receivableUnitNewHolder);

        $paymentValue = app(DivisionRuleApplicatorForHolderChangeServiceContract::class, [
            'division_rule' => $contractEffect->division_rule
        ])->apply($contractEffect, $receivableUnit, $receivableUnitNewHolder, $payment);

        $paymentNewHolder->update(['payment_value' => $paymentValue]);

        $this->fillApplyDateAndReceivableUnitId($contractEffect, $receivableUnit);

        $orders = $this->getOrdersWithValuesCalculatedFromContractEffectApplied($receivableUnit, $paymentValue);

        $this->storeContractEffectOrders($paymentNewHolder, $receivableUnitNewHolder, $contractEffect, $orders);

        return $receivableUnitNewHolder;
    }

    private function updateOrCreateReceivableUnitNewHolder(
        ContractEffect $contractEffect,
        ReceivableUnit $receivableUnit
    ): ReceivableUnit
    {
        return ReceivableUnit::updateOrCreate(
            [
                'final_recipient_document_number' => $receivableUnit->final_recipient_document_number,
                'holder_document_number' => $contractEffect->holder_beneficiary_document_number,
                'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
                'settlement_date' => $receivableUnit->settlement_date,
            ],
            [
                'company_id' => $receivableUnit->company_id,
                'accrediting_company_id' => $receivableUnit->accrediting_company_id,
                'receivable_unit_linked_id' => $receivableUnit->id,
                'contract_effect_id' => $contractEffect->id,
                'operation_type' => OperationType::CREATE,
                'accrediting_company_document_number' => $receivableUnit->accrediting_company_document_number,
                'constituted' => $receivableUnit->constituted,
                'total_constituted_value' => $receivableUnit->total_constituted_value,
                'pre_contracted_value' => $receivableUnit->pre_contracted_value,
                'total_gross_value' => $receivableUnit->total_gross_value,
                'blocked_value' => $receivableUnit->blocked_value,
            ]);
    }

    private function updateOrCreatePaymentForNewHolder(
        ContractEffect $contractEffect,
        ReceivableUnit $receivableUnitNewHolder
    ): Payment
    {
        return Payment::updateOrCreate([
            'receivable_unit_id' => $receivableUnitNewHolder->id,
            'holder_document_number' => $receivableUnitNewHolder->holder_document_number,
            'ispb' => (new Bank($contractEffect->compe))->getFormattedIspb(),
            'compe' => $contractEffect->compe,
            'bank_account_type' => $contractEffect->bank_account_type,
            'bank_branch' => $contractEffect->bank_branch,
            'bank_account' => $contractEffect->bank_account,
        ],
        [
            'payment_value' => 0
        ]);
    }
}
