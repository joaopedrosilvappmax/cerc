<?php

namespace App\Models\Services\CashOutPayment;

use App\Models\Entities\CashOutPayment;
use App\Models\Entities\Payment;
use App\Models\Services\Contracts\CashOutPaymentRegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\CashOutPaymentType;
use Illuminate\Database\Eloquent\Model;

class CashOutPaymentSettledRegisterService implements
    CashOutPaymentRegisterServiceContract
{
    public function store(DataRegisterContract $data): Model
    {
        $this->checkRecordExisting($data);

        $this->setExternalCashOutId($data);

        return CashOutPayment::create([
            'external_cash_out_id' => $data->external_cash_out_id,
            'external_cash_out_company_id' => $data->external_cash_out_company_id,
            'receivable_unit_id' => $data->receivable_unit_id,
            'payment_id' => $data->payment_id,
            'value' => $data->value,
            'type' => CashOutPaymentType::SETTLED
        ]);
    }

    private function checkRecordExisting(DataRegisterContract $data)
    {
        $cashOutPayment = CashOutPayment::where([
            'receivable_unit_id' => $data->receivable_unit_id,
            'payment_id' => $data->payment_id,
            'type' => CashOutPaymentType::SETTLED
        ])->get();

        if (! $cashOutPayment->isEmpty()) {
            throw new \Exception('Cash Out Payment Settled Existing!');
        }
    }

    private function setExternalCashOutId(DataRegisterContract $data)
    {
        $payment = Payment::find($data->payment_id);

        if (! $payment) {
            return;
        }

        $payment->update([
            'external_cash_out_id' => $data->external_cash_out_id
        ]);

    }
}
