<?php

namespace App\Models\Services\CashOutPayment;

use App\Exceptions\NotFoundException;
use App\Models\Entities\Anticipation;
use App\Models\Entities\CashOutPayment;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\CashOutPaymentRegisterServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\CashOutPaymentType;
use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CashOutPaymentAnticipationRegisterService implements
    RegisterServiceContract,
    CashOutPaymentRegisterServiceContract
{

    protected $isCashOutTypeHolderChange;
    /**
     * @throws NotFoundException
     */
    public function store(DataRegisterContract $data): ?Model
    {
        $cashOutPayment = CashOutPayment::create([
            'external_cash_out_id' => $data->external_cash_out_id,
            'external_cash_out_company_id' => $data->external_cash_out_company_id,
            'receivable_unit_id' => $data->receivable_unit_id,
            'payment_id' => $data->payment_id,
            'value' => $data->value,
            'type' => CashOutPaymentType::ANTICIPATION,
        ]);

        $receivableUnit = $cashOutPayment->payment->receivableUnit;
        $this->verifyCashOutIsTypeHolderChange($receivableUnit);

        if ($this->isCashOutTypeHolderChange) {
            return null;
        }

        $anticipation = $this->storeAnticipation($cashOutPayment);

        $this->updatePayment($data->payment_id, $data->value);

        $this->updateReceivableUnit($data->receivable_unit_id, $data->value);

        return $anticipation;
    }

    private function storeAnticipation(CashOutPayment $cashOutPayment): Anticipation
    {
        $receivableUnit = ReceivableUnit::find($cashOutPayment->receivable_unit_id);

        $anticipation = Anticipation::where([
            'external_cash_out_id' => $cashOutPayment->external_cash_out_company_id,
            'code_arrangement_payment' => $receivableUnit->code_arrangement_payment
        ])->first();

        if ($anticipation) {
            return $this->updateAnticipation($anticipation, $cashOutPayment);
        }

        $anticipation = Anticipation::create([
            'external_cash_out_id' => $cashOutPayment->external_cash_out_company_id,
            'company_id' => $receivableUnit->company_id,
            'accrediting_company_id' => config('accrediting_company.appmax_id'),
            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
            'final_recipient_document_number' => $receivableUnit->final_recipient_document_number,
            'holder_document_number' => ($this->isCashOutTypeHolderChange) ? $receivableUnit->holder_document_number : $receivableUnit->final_recipient_document_number,
            'operation_type' => OperationType::CREATE,
            'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
            'total_constituted_value' => $cashOutPayment->value,
            'total_value' => $cashOutPayment->value,
            'paid_value' => $cashOutPayment->value,
            'request_date' => Carbon::now(),
            'settlement_date' => $receivableUnit->settlement_date
        ]);

        $this->createAnticipationPayment($anticipation, $cashOutPayment);

        $this->createHolderAnticipationPayment($anticipation, $receivableUnit, $cashOutPayment);

        return $anticipation;
    }

    private function updateAnticipation(Anticipation $anticipation, CashOutPayment $cashOutPayment): Anticipation
    {
        $anticipation->update([
            'operation_type' => $anticipation->integrated_at ? OperationType::UPDATE : OperationType::CREATE,
            'total_constituted_value' => ($anticipation->total_constituted_value + $cashOutPayment->value),
            'total_value' => ($anticipation->total_value + $cashOutPayment->value),
            'paid_value' => ($anticipation->paid_value + $cashOutPayment->value),
        ]);

        $paymentAnticipation = $anticipation->payments()
            ->where('receivable_unit_payment_id', $cashOutPayment->payment_id)
            ->first();

        if (! $paymentAnticipation) {
            $this->createAnticipationPayment($anticipation, $cashOutPayment);

            return $anticipation->fresh('payments');
        }

        $paymentAnticipation->update(['payment_value' => ($paymentAnticipation->payment_value + $cashOutPayment->value)]);

        return $anticipation->fresh('payments');
    }

    private function createAnticipationPayment(Anticipation $anticipation, CashOutPayment $cashOutPayment): void
    {
        $paymentReceivableUnit = $cashOutPayment->payment;

        Payment::create([
            'receivable_unit_payment_id' => $cashOutPayment->payment_id,
            'external_cash_out_id' => $cashOutPayment->external_cash_out_id,
            'anticipation_id' => $anticipation->id,
            'holder_document_number' => $paymentReceivableUnit->holder_document_number,
            'ispb' => $paymentReceivableUnit->ispb,
            'compe' => $paymentReceivableUnit->compe,
            'bank_account_type' => $paymentReceivableUnit->bank_account_type,
            'bank_branch' => $paymentReceivableUnit->bank_branch,
            'bank_account' => $paymentReceivableUnit->bank_account,
            'payment_value' => $cashOutPayment->value,
        ]);
    }

    private function createHolderAnticipationPayment(Anticipation $anticipation, ReceivableUnit $receivableUnit, CashOutPayment $cashOutPayment): void
    {
        $payment = $receivableUnit->payments()
            ->where('holder_document_number', $receivableUnit->holder_document_number)
            ->first();

        Payment::create([
            'receivable_unit_payment_id' => $payment->id,
            'external_cash_out_company_id' => $cashOutPayment->external_cash_out_company_id,
            'anticipation_id' => $anticipation->id,
            'holder_document_number' => $payment->holder_document_number,
            'ispb' => $payment->ispb,
            'compe' => $payment->compe,
            'bank_account_type' => $payment->bank_account_type,
            'bank_branch' => $payment->bank_branch,
            'bank_account' => $payment->bank_account,
            'payment_value' => 0,
        ]);
    }

    private function updatePayment($paymentId, $value): void
    {
        $payment = Payment::find($paymentId);

        if (! $payment) {
            throw new NotFoundException('Payment not found.');
        }

        $payment->update([
            'payment_value' => $payment->payment_value - $value
        ]);
    }

    private function updateReceivableUnit($receivableUnitId, $value): void
    {
        $receivableUnit = ReceivableUnit::find($receivableUnitId);

        if (! $receivableUnit) {
            throw new NotFoundException('Receivable Unit not found.');
        }

        $receivableUnit->update([
            'total_constituted_value' => $receivableUnit->total_constituted_value - $value
        ]);
    }

    private function createParentAnticipation(ReceivableUnit $receivableUnitParent, CashOutPayment $cashOutPayment): Anticipation
    {
        return Anticipation::create([
            'external_cash_out_id' => $cashOutPayment->external_cash_out_company_id,
            'company_id' => $receivableUnitParent->company_id,
            'accrediting_company_id' => config('accrediting_company.appmax_id'),
            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
            'final_recipient_document_number' => $receivableUnitParent->final_recipient_document_number,
            'holder_document_number' => $receivableUnitParent->final_recipient_document_number,
            'operation_type' => OperationType::CREATE,
            'code_arrangement_payment' => $receivableUnitParent->code_arrangement_payment,
            'total_constituted_value' => 0,
            'total_value' => 0,
            'paid_value' => 0,
            'request_date' => Carbon::now(),
            'settlement_date' => $receivableUnitParent->settlement_date
        ]);
    }

    private function createParentAnticipationPayment(
        ReceivableUnit $receivableUnitParent,
        Anticipation $anticipationParent,
        CashOutPayment $cashOutPayment)
    {
        $payment = $receivableUnitParent->payments()
            ->where('holder_document_number', $receivableUnitParent->holder_document_number)->first();


        return Payment::create([
            'receivable_unit_payment_id' => $payment->id,
            'external_cash_out_id' => $cashOutPayment->external_cash_out_id,
            'anticipation_id' => $anticipationParent->id,
            'holder_document_number' => $payment->holder_document_number,
            'ispb' => $payment->ispb,
            'compe' => $payment->compe,
            'bank_account_type' => $payment->bank_account_type,
            'bank_branch' => $payment->bank_branch,
            'bank_account' => $payment->bank_account,
            'payment_value' => 0,
        ]);
    }

    private function verifyCashOutIsTypeHolderChange(ReceivableUnit $receivableUnit): void
    {
        if ($receivableUnit->receivableUnitParent) {
            $this->isCashOutTypeHolderChange = true;

            return;
        }

        $this->isCashOutTypeHolderChange = false;
    }
}
