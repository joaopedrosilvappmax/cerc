<?php

namespace App\Models\Services\Webhooks;

use App\Models\Entities\WebhookRequest;
use App\Models\Services\Contracts\ContractEffectApplyServiceContract;
use App\Models\Services\Contracts\WebhookResolverServiceContract;
use App\Models\Services\Contracts\WebhookServiceContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\WebhookEventType;
use JetBrains\PhpStorm\NoReturn;

class WebhookService implements WebhookServiceContract
{
    #[NoReturn] public function registerEvent(array $data): void
    {
        $webhookRequest = $this->storeWebhookRequest($data);

        foreach ($data as $webhookData) {
            app(WebhookResolverServiceContract::class, [
                'class' => WebhookEventType::getBindServiceClass($webhookData['tipoEvento']),
            ])->resolveWebhookData($webhookData, $webhookRequest->id);
        }

        app(ContractEffectApplyServiceContract::class, [
            'class' => ClassBindResolution::CONTRACT_EFFECT_APPLY_WEBHOOK
        ])->apply($webhookRequest->id);
    }

    private function storeWebhookRequest(array $data): WebhookRequest
    {
        return WebhookRequest::create([
            'request' => $data
        ]);
    }
}
