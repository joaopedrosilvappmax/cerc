<?php

namespace App\Models\Services\Files;

use App\Models\Services\Contracts\FileServiceContract;
use Illuminate\Support\Facades\Storage;

class S3FileService implements FileServiceContract
{
    public function getStream(string $path): mixed
    {
        return Storage::readStream($path);
    }
}
