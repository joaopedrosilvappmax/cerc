<?php

namespace App\Models\Services\Files;

use App\Models\Entities\UploadedContractEffectFile;
use App\Support\DataTransferObjects\UploadedContractEffectFile\UploadedContractEffectFileDataRegister;
use Carbon\Carbon;

class RegisterContractEffectFilesService
{
    public function register(UploadedContractEffectFileDataRegister $data): UploadedContractEffectFile
    {
        return UploadedContractEffectFile::create($data->toArray());
    }

    public function getLastSequentialNumber(): int
    {
        return UploadedContractEffectFile::where('date', Carbon::now()->format('Y-m-d'))
            ->orderBy('sequential_number', 'DESC')
            ->first()
            ->sequential_number ?? 0;
    }
}
