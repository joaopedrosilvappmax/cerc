<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ReceivableUnit;

interface ReturnValuesOfContractEffectServiceContract
{
    public function returnValues(ContractEffect $contractEffect, ReceivableUnit $receivableUnit): void;
}
