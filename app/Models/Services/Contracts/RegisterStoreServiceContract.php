<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\Company;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

interface RegisterStoreServiceContract
{
    /**
     * @param DataRegisterContract $data
     * @return Company
     */
    public function store(DataRegisterContract $data);
}
