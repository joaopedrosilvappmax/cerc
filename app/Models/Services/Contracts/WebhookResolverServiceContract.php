<?php

namespace App\Models\Services\Contracts;

interface WebhookResolverServiceContract
{
    public function resolveWebhookData(array $data, int $webhookRequestId): void;
}
