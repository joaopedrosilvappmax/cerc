<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;

interface ReduceValueEffectContractHolderChangeServiceContract
{
    public function reduceValue(
        ReceivableUnit $receivableUnit,
        ReceivableUnitOrder $receivableUnitOrder,
        int $externalOrderId
    ): void;
}
