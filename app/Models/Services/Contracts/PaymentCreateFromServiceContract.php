<?php

namespace App\Models\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface PaymentCreateFromServiceContract
{
    public function store(Model $entity): Model;
}
