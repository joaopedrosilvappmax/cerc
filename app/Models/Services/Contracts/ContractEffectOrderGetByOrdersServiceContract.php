<?php

namespace App\Models\Services\Contracts;

use Illuminate\Support\Collection;

interface ContractEffectOrderGetByOrdersServiceContract
{
    public function getByOrdersId(array $ordersId): ?Collection;
}
