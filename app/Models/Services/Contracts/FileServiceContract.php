<?php

namespace App\Models\Services\Contracts;

interface FileServiceContract extends ContractEffectServiceContract
{
    public function getStream(string $path): mixed;
}
