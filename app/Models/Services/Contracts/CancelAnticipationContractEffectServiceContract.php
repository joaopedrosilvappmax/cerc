<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ContractEffect;

interface CancelAnticipationContractEffectServiceContract
{
    public function apply(ContractEffect $contractEffect): void;
}
