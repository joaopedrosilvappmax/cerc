<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ContractEffect;

interface ContractEffectTotalValueServiceContract
{
    public function getValue(ContractEffect $contractEffect): int;
}
