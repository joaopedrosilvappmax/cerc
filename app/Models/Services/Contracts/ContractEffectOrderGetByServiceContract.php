<?php

namespace App\Models\Services\Contracts;

use Illuminate\Support\Collection;

interface ContractEffectOrderGetByServiceContract
{
    public function getBy(int $id): ?Collection;
}
