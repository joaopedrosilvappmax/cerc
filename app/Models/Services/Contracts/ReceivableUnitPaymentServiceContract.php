<?php

namespace App\Models\Services\Contracts;

interface ReceivableUnitPaymentServiceContract
{
    public function receivableUnitPaymentsSum(string $holderDocumentNumber): int;
}
