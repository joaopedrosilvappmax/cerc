<?php

namespace App\Models\Services\Contracts;

interface WebhookServiceContract
{
    public function registerEvent(array $data): void;
}
