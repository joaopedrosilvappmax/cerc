<?php

namespace App\Models\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface PaymentUpdateFromServiceContract
{
    public function update(Model $entity): ?Model;
}
