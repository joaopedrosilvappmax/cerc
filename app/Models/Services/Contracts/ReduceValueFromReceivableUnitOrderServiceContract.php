<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ReceivableUnitOrder;

interface ReduceValueFromReceivableUnitOrderServiceContract
{
    public function reduce(ReceivableUnitOrder $receivableUnitOrder): void;
}
