<?php

namespace App\Models\Services\Contracts;

use Illuminate\Support\Collection;

interface ContractEffectOrderGetByPaymentServiceContract
{
    public function getBy(int $paymentId): ?Collection;
}
