<?php

namespace App\Models\Services\Contracts;

interface GetTotalContractEffectAnticipatedServiceContract
{
    public function getTotal(int $receivableUnitId): int;
}
