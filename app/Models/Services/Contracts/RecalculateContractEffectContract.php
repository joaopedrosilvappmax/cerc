<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ReceivableUnit;

interface RecalculateContractEffectContract
{
    public function recalculate(ReceivableUnit $receivableUnit): void;
}
