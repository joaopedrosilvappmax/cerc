<?php

namespace App\Models\Services\Contracts;

use Illuminate\Support\Facades\Request;

interface ContractEffectReadFileServiceContract
{
    public function read(array $data): void;
}
