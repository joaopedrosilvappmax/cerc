<?php

namespace App\Models\Services\Contracts;

interface PaymentConfirmationServiceContract
{
    public function confirmPayment(int $externalCashOutId);
}
