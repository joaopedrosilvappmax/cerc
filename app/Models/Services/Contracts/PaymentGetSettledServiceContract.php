<?php

namespace App\Models\Services\Contracts;

interface PaymentGetSettledServiceContract
{
    public function getPayments($date);
}
