<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffect;

interface AnticipationContractContractEffectServiceApplyContract extends ContractEffectServiceContract
{
    public function apply(ContractEffect $contractEffect, Anticipation $anticipation);
}
