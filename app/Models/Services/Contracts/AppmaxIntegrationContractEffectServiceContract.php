<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ContractEffect;

interface AppmaxIntegrationContractEffectServiceContract
{
    public function send(ContractEffect $contractEffect, int $paymentValue, array $orders);
}
