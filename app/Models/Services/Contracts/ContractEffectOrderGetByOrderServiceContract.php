<?php

namespace App\Models\Services\Contracts;

use Illuminate\Support\Collection;

interface ContractEffectOrderGetByOrderServiceContract
{
    public function getBy(int $externalOrderId): ?Collection;
}
