<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ReceivableUnit;

interface ReceivableUnitContractContractEffectServiceApplyContract extends ContractEffectServiceContract
{
    public function apply(ContractEffect $contractEffect, ReceivableUnit $receivableUnit);
}
