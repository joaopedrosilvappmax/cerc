<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ReceivableUnit;

interface UpdatePaymentFromContractEffectServiceContract
{
    public function update(ReceivableUnit $receivableUnit);
}
