<?php

namespace App\Models\Services\Contracts;

interface NotificationServiceContract
{
    public function notify(string $message, array $data = null): void;
}
