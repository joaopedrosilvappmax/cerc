<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;

interface DivisionRuleApplicatorForOnusServiceContract
{
    public function apply(ContractEffect $contractEffect, Payment $holderPayment, bool $updateHolderPayment): int;
}
