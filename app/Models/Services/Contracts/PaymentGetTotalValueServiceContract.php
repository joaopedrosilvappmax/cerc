<?php

namespace App\Models\Services\Contracts;

interface PaymentGetTotalValueServiceContract
{
    public function getTotal(string $holderDocumentNumber): int;
}
