<?php

namespace App\Models\Services\Contracts;

use Illuminate\Http\Request;

interface StorageServiceContract
{
    public function upload(Request $request): string;

    public function delete(string $path): bool;
}
