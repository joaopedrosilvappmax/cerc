<?php

namespace App\Models\Services\Contracts;

use App\Support\DataTransferObjects\Contracts\FilterContract;

interface ExportServiceContract
{
    public function export(FilterContract $filters): array;
}
