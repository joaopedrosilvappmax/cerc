<?php

namespace App\Models\Services\Contracts;

interface ContractEffectApplyServiceContract
{
    public function apply(int $id): void;
}
