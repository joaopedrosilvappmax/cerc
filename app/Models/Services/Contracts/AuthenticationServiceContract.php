<?php

namespace App\Models\Services\Contracts;


interface AuthenticationServiceContract
{
    public function getToken(): string;
}
