<?php

namespace App\Models\Services\Contracts;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;

interface DivisionRuleApplicatorForHolderChangeServiceContract
{
    public function apply(
        ContractEffect $contractEffect,
        ReceivableUnit $receivableUnit,
        ReceivableUnit $receivableUnitNewHolder,
        Payment $payment
    ): int;
}
