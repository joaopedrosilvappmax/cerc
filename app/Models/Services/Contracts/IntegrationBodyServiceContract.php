<?php

namespace App\Models\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface IntegrationBodyServiceContract
{
    public function mountBody(Model $entity): array;
}
