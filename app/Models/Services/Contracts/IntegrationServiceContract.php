<?php

namespace App\Models\Services\Contracts;

use App\Http\Responses\Contracts\IntegrationResponseContract;
use Illuminate\Database\Eloquent\Model;

interface IntegrationServiceContract
{
    public function send(Model $entity): IntegrationResponseContract;
}
