<?php

namespace App\Models\Services\Contracts;

use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

interface GuaranteeProcessServiceContract
{
    public function process(DataRegisterContract $guarantees);
}
