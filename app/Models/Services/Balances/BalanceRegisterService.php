<?php

namespace App\Models\Services\Balances;

use App\Models\Entities\Balance;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Models\Services\Contracts\RegisterStoreServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

class BalanceRegisterService implements RegisterServiceContract, RegisterStoreServiceContract
{
    public function store(DataRegisterContract $data): Balance
    {
        return Balance::create($data->toArray());
    }
}
