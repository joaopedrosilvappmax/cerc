<?php

namespace App\Models\Services\Payments;

use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Abstracts\ContractEffectOnusServiceAbstract;
use App\Models\Services\ContractEffects\Cancel\ReturnValuesOfContractEffectService;
use App\Models\Services\Contracts\RecalculateContractEffectContract;
use App\Models\Services\Contracts\UpdatePaymentFromContractEffectServiceContract;
use App\Support\EnumTypes\ContractEffectType;


class UpdatePaymentFromContractEffectService extends ContractEffectOnusServiceAbstract implements UpdatePaymentFromContractEffectServiceContract
{
    public function update(ReceivableUnit $receivableUnit): void
    {
        (new ReturnValuesOfContractEffectService())->returnValues($receivableUnit);

        $this->recalculateContractEffects($receivableUnit);
    }

    public function recalculateContractEffects(ReceivableUnit $receivableUnit): void
    {
        app(RecalculateContractEffectContract::class, [
            'type' => ContractEffectType::COURT_BLOCK
        ])->recalculate($receivableUnit);

        app(RecalculateContractEffectContract::class)->recalculate($receivableUnit);
    }
}
