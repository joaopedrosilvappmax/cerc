<?php

namespace App\Models\Services\Payments;

use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\PaymentGetTotalValueServiceContract;
use App\Support\EnumTypes\ContractEffectType;
use Carbon\Carbon;

class PaymentGetTotalValueNoSettledHolderChangeService implements PaymentGetTotalValueServiceContract
{
    /**
     * @param string $holderDocumentNumber
     * @return int
     */
    public function getTotal(string $holderDocumentNumber): int
    {
        $total = ReceivableUnit::where('settlement_date', '>', Carbon::now())
            ->whereHas('receivableUnitParent', function ($receivableUnitParentQuery) use ($holderDocumentNumber) {
                return $receivableUnitParentQuery->select('id')
                    ->where('holder_document_number', '=', $holderDocumentNumber);
            })
            ->whereNull('paid_at')
            ->get(['id', 'receivable_unit_linked_id'])
            ->sum(function ($receivableUnit) {
                return $receivableUnit->payments->first()->payment_value - $this->getCalculatedAnticipatedOrders($receivableUnit->receivableUnitParent);
            });

        if ($total < 0) {
            return 0;
        }

        return $total;
    }

    private function getCalculatedAnticipatedOrders(ReceivableUnit $receivableUnit)
    {
        return $receivableUnit
            ->receivableUnitOrders()
            ->whereNotNull('anticipation_id')
            ->get(['id','external_order_id'])
            ->sum(function ($receivableUnitOrder) {
                return $receivableUnitOrder->contractEffectOrders()
                    ->whereHas('contractEffect', function ($query) {
                        return $query->where('effect_type', ContractEffectType::HOLDER_CHANGE);
                    })
                    ->sum('value');
            });
    }
}
