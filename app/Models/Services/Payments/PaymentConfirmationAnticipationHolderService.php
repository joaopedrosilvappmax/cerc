<?php

namespace App\Models\Services\Payments;

use App\Exceptions\NotFoundException;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\PaymentConfirmationServiceContract;
use Carbon\Carbon;

class PaymentConfirmationAnticipationHolderService implements PaymentConfirmationServiceContract
{
    /**
     * @param int $externalCashOutId
     * @throws NotFoundException
     */
    public function confirmPayment(int $externalCashOutId): void
    {
        $payment = Payment::where('external_cash_out_company_id', $externalCashOutId)
            ->first();

        if (! $payment) {
            throw new NotFoundException('Payment not found whit this id value.');
        }

        $payment->update([
            'paid_at' => Carbon::now()
        ]);

        $this->settlePaymentOnReceivableUnit($payment->refresh());
    }

    private function settlePaymentOnReceivableUnit(Payment $payment): void
    {
        $anticipation = $payment->anticipation;

        ReceivableUnit::firstWhere([
            'final_recipient_document_number' => $anticipation->final_recipient_document_number,
            'holder_document_number' => $anticipation->holder_document_number,
            'code_arrangement_payment' => $anticipation->code_arrangement_payment,
            'settlement_date' => $anticipation->settlement_date,
        ])->payments()->create(
            $payment->only([
                'holder_document_number',
                'ispb',
                'compe',
                'bank_account_type',
                'bank_branch',
                'bank_account',
                'payment_value',
                'paid_at',
            ])
        );
    }
}
