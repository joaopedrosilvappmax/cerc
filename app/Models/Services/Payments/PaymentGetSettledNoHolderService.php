<?php

namespace App\Models\Services\Payments;

use App\Models\Entities\Payment;
use App\Models\Services\Contracts\PaymentGetSettledServiceContract;
use Illuminate\Database\Eloquent\Collection;

class PaymentGetSettledNoHolderService implements PaymentGetSettledServiceContract
{
    public function getPayments($date)
    {
        $paymentNoHolder = $this->getPaymentsNoHolderTypeOnus($date);

        $paymentHolderChange = $this->getPaymentsTypeHolderChange($date);

        return $paymentNoHolder->merge($paymentHolderChange);
    }

    private function getPaymentsNoHolderTypeOnus($date): ?Collection
    {
        return Payment::select(
            'id',
            'receivable_unit_id',
            'holder_document_number',
            'compe',
            'bank_account_type',
            'bank_branch',
            'bank_account',
            'payment_value',
        )->whereHas('receivableUnit', function ($queryReceivableUnit) use ($date) {
            return $queryReceivableUnit
                ->whereDate('settlement_date', '=', $date);
        })->whereNull('paid_at')
            ->where('payment_value', '>', 0)
            ->with([
                'receivableUnit' => function ($query) {
                    $query->select('id', 'holder_document_number');
                }
            ])
            ->get()
            ->filter(function ($payment) {
                return ($payment->holder_document_number != $payment->receivableUnit->holder_document_number);
            });
    }

    private function getPaymentsTypeHolderChange($date): ?Collection
    {
        return Payment::select(
            'id',
            'receivable_unit_id',
            'holder_document_number',
            'compe',
            'bank_account_type',
            'bank_branch',
            'bank_account',
            'payment_value',
        )->whereHas('receivableUnit', function ($queryReceivableUnit) use ($date) {
            return $queryReceivableUnit
                ->select('id', 'holder_document_number')
                ->whereNotNull('receivable_unit_linked_id')
                ->whereDate('settlement_date', '=', $date);
        })->whereNull('paid_at')
            ->where('payment_value', '>', 0)
            ->with([
                'receivableUnit' => function ($query) {
                    $query->select('id', 'receivable_unit_linked_id', 'holder_document_number')
                    ->with([
                        'receivableUnitParent' => function ($query) {
                        $query->select('id', 'holder_document_number');
                        }
                    ]);
                },
            ])
            ->get();
    }
}
