<?php

namespace App\Models\Services\Payments;

use App\Exceptions\NotFoundException;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\PaymentConfirmationServiceContract;
use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;

class PaymentConfirmationAvailableService implements PaymentConfirmationServiceContract
{
    /**
     * @param int $externalCashOutId
     * @throws NotFoundException
     */
    public function confirmPayment(int $externalCashOutId): void
    {
        $payment = Payment::where('external_cash_out_id', $externalCashOutId)
            ->first();

        if (! $payment) {
            throw new NotFoundException('Payment not found whit this id value.');
        }

        $payment->update([
            'paid_at' => Carbon::now()
        ]);
    }
}
