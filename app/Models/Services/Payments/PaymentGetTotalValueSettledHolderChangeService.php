<?php

namespace App\Models\Services\Payments;

use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\PaymentGetTotalValueServiceContract;
use Carbon\Carbon;

class PaymentGetTotalValueSettledHolderChangeService implements PaymentGetTotalValueServiceContract
{
    /**
     * @param string $holderDocumentNumber
     * @return int
     */
    public function getTotal(string $holderDocumentNumber): int
    {
        return ReceivableUnit::where(
                    'settlement_date', '<=', Carbon::now()
               )->whereHas('receivableUnitParent', function ($receivableUnitParentQuery) use ($holderDocumentNumber) {
                   return $receivableUnitParentQuery->where('holder_document_number', '=', $holderDocumentNumber);
               })
               ->whereNull('paid_at')
               ->get()
               ->sum(function ($receivableUnit) {
                   return $receivableUnit->payments->first()->payment_value;
               });
    }
}
