<?php

namespace App\Models\Services\Payments;

use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\PaymentGetTotalValueServiceContract;
use Carbon\Carbon;

class PaymentGetTotalValueNoSettledNoHolderService implements PaymentGetTotalValueServiceContract
{
    /**
     * @param string $holderDocumentNumber
     * @return int
     */
    public function getTotal(string $holderDocumentNumber): int
    {
        return ReceivableUnit::where([
                ['holder_document_number', '=', $holderDocumentNumber],
                ['settlement_date', '>', Carbon::now()]
            ])
            ->get()
            ->sum(function ($receivableUnit) use ($holderDocumentNumber) {
                return $receivableUnit->payments
                    ->where('holder_document_number', '!=', $holderDocumentNumber)
                    ->sum('payment_value');
            });
    }
}
