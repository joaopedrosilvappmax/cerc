<?php

namespace App\Models\Services\Guarantee;

use App\Events\ReceivableUnits\ReceivableUnitUpdatedWithContractEffectEvent;
use App\Exceptions\GuaranteeAlreadyException;
use App\Exceptions\GuaranteeException;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\GuaranteeProcessServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;

class GuaranteeProcessService implements GuaranteeProcessServiceContract
{
    public function process(DataRegisterContract $guarantees)
    {
        $urOrder = $this->getOrder($guarantees);

        $this->verifyRules($urOrder, $guarantees->value);

        $this->applyValueOnUr($urOrder, $guarantees->value);
    }

    private function getOrder(DataRegisterContract $guarantees)
    {
        $urOrder = ReceivableUnitOrder::firstWhere('external_order_id', $guarantees->order_id);

        if (! $urOrder) {
            throw new GuaranteeException('Not found receivable unit order', 404);
        }

        return $urOrder;
    }

    private function verifyRules($urOrder, $value){

        if ($urOrder->guarantee_value > 0) {
            throw new GuaranteeAlreadyException('Guarantee already', 422);
        }

        if ($value > $urOrder->value) {
            throw new GuaranteeException('Guarantee value is greater than UR order', 422);
        }

        if (! $urOrder->receivableUnit) {
            throw new GuaranteeException('Receivable unit not found from order', 422);
        }
    }

    private function applyValueOnUr($urOrder, $value)
    {
        $receivableUnit =  $urOrder->receivableUnit;

        $applyValue = max($receivableUnit->total_constituted_value - $value,0);

        $receivableUnit->update([
            'total_constituted_value' =>  $applyValue
        ]);

        $urOrder->update([
            'guarantee_value' => $value
        ]);

        if (! $receivableUnit->contractEffects->isEmpty()) {
            ReceivableUnitUpdatedWithContractEffectEvent::dispatch($receivableUnit);
        };
    }
}
