<?php

namespace App\Models\Entities;

use App\Support\EnumTypes\CompanyDocumentType;
use App\Traits\DailyReport;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JetBrains\PhpStorm\Pure;

class Company extends Model
{
    use HasFactory,
        DailyReport;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_id',
        'has_critical_error',
        'name',
        'operation_type',
        'document_number',
        'ispb',
        'compe',
        'status',
        'email',
        'phone',
        'bank_account_type',
        'bank_branch',
        'bank_account',
        'integrated_at',
    ];

    protected $date = [
        'integrated_at'
    ];

    public function isLegalPerson(): bool
    {
        return strlen($this->document_number) === 14;
    }

    #[Pure] public function getDocumentTypeAttribute(): string
    {
        if ($this->isLegalPerson()) {
            return CompanyDocumentType::COMPANY;
        }

        return CompanyDocumentType::INDIVIDUAL;
    }
}
