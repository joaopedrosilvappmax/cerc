<?php

namespace App\Models\Entities;

use App\Traits\DailyReport;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ReceivableUnit extends Model
{
    use HasFactory,
        DailyReport;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'accrediting_company_id',
        'receivable_unit_linked_id',
        'contract_effect_id',
        'anticipation_id',
        'operation_type',
        'accrediting_company_document_number',
        'final_recipient_document_number',
        'holder_document_number',
        'code_arrangement_payment',
        'constituted',
        'total_constituted_value',
        'pre_contracted_value',
        'total_gross_value',
        'blocked_value',
        'settlement_date',
        'integrated_at',
        'paid_at',
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function payments(): HasMany
    {
        return $this->hasMany(Payment::class)
            ->whereNull('paid_at');
    }

    public function orders(): HasMany
    {
        return $this->hasMany(ReceivableUnitOrder::class);
    }

    public function contractEffects(): HasMany
    {
        return $this->hasMany(ContractEffect::class);
    }

    public function contractEffectOrders(): HasMany
    {
        return $this->hasMany(ContractEffectOrder::class);
    }

    public function receivableUnitParent(): BelongsTo
    {
        return $this->belongsTo(ReceivableUnit::class, 'receivable_unit_linked_id', 'id');
    }

    public function receivableUnitOrders(): HasMany
    {
        return $this->hasMany(ReceivableUnitOrder::class);
    }

    public function balances(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Balance::class);
    }

    public function hasContractEffect(): bool
    {
        return ! $this->contractEffects->isEmpty();
    }
}
