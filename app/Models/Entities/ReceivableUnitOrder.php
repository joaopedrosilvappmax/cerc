<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceivableUnitOrder extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'external_order_id',
        'receivable_unit_id',
        'anticipation_id',
        'gross_value',
        'value',
        'reversal_value',
        'guarantee_value',
        'anticipation_value',
        'debit_value',
    ];

    public function receivableUnit(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ReceivableUnit::class, 'id', 'receivable_unit_id');
    }

    public function getValueAttribute($value): int
    {
        $valueCalculated = $value
            - $this->reversal_value
            - $this->guarantee_value
            - $this->anticipation_value
            - $this->debit_value;

        if ($valueCalculated < 0) {
            return 0;
        }

        return $valueCalculated;
    }

    public function hasNegativeValue(): bool
    {
        return $this->reversal_value
            || $this->guarantee_value
            || $this->anticipation_value
            || $this->debit_value;
    }

    public function hasDebitValue(): bool
    {
        return $this->debit_value > 0;
    }

    public function getNetValueAttribute()
    {
        $value = $this->getRawOriginal('value')
            - $this->debit_value
            - ContractEffectOrder::where('external_order_id', $this->external_order_id)
            ->sum('value');

        if ($value < 0) {
            $value = 0;
        }

        return $value;
    }

    public function contractEffectOrders(): HasMany
    {
        return $this->hasMany(ContractEffectOrder::class, 'external_order_id', 'external_order_id');
    }

    public function anticipation(): BelongsTo
    {
        return $this->belongsTo(Anticipation::class);
    }
}
