<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class LogNotificationError extends Model
{
    protected $table = 'log_notification_errors';

    protected $fillable = [
        'type',
        'quantity'
    ];
}
