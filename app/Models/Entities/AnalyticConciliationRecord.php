<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnalyticConciliationRecord extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'receivable_unit_id',
        'anticipation_id',
        'value_is_divergent',
        'is_not_settled',
        'reference_date',
        'accrediting_company_document_number',
        'final_recipient_document_number',
        'code_arrangement_payment',
        'settlement_date',
        'holder_document_number',
        'total_constituted_value',
        'pre_contracted_value',
        'total_gross_value',
        'anticipation_not_settled_value',
        'real_total_constituted_value',
        'real_pre_contracted_value',
        'real_total_gross_value',
        'real_anticipation_not_settled_value',
    ];
}
