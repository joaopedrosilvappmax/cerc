<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UploadedContractEffectFile extends Model
{
    use HasFactory;

    public $fillable = [
        'file_name',
        'date',
        'sequential_number',
    ];

    public $casts = [
        'date' => 'date:Y-m-d'
    ];

    public function contractEffects(): HasMany
    {
        return $this->hasMany(ContractEffect::class, 'uploaded_contract_effect_file_id', 'id');
    }
}
