<?php

namespace App\Models\Entities;

use App\Traits\DailyReport;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractEffect extends Model
{
    use HasFactory,
        SoftDeletes,
        DailyReport;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'receivable_unit_id',
        'external_reference_receivable_unit_id',
        'external_contract_id',
        'contract_generator_event_id',
        'protocol',
        'indicator',
        'accrediting_company_document_number',
        'constituted',
        'final_recipient_document_number',
        'code_arrangement_payment',
        'settlement_date',
        'registration_entity_document_number',
        'holder_beneficiary_document_number',
        'effect_type',
        'division_rule',
        'committed_value',
        'holder_document_number',
        'holder_name',
        'bank_account_type',
        'compe',
        'ispb',
        'bank_branch',
        'bank_account',
        'indicator',
        'event_datetime',
        'applied_at',
        'uploaded_contract_effect_file_id',
        'webhook_request_id',
    ];

    public function receivableUnit(): HasOne
    {
        return $this->hasOne(ReceivableUnit::class, 'id', 'receivable_unit_id');
    }

    public function receivableUnitHolderChange(): HasOne
    {
        return $this->hasOne(ReceivableUnit::class, 'contract_effect_id', 'id');
    }

    public function contractEffectOrders(): hasMany
    {
        return $this->hasMany(ContractEffectOrder::class);
    }

    public function uploadedFile(): BelongsTo
    {
        return $this->belongsTo(
            UploadedContractEffectFile::class,
            'uploaded_contract_effect_file_id',
            'id'
        );
    }

    public function webhookRequest(): BelongsTo
    {
        return $this->belongsTo(
            WebhookRequest::class,
            'webhook_request_id',
            'id'
        );
    }
}
