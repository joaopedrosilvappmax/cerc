<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class WebhookRequest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'request',
    ];

    protected $casts = [
        'request' => 'collection'
    ];

    public function contractEffects(): HasMany
    {
        return $this->hasMany(ContractEffect::class, 'webhook_request_id', 'id');
    }
}
