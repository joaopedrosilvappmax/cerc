<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Balance extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'external_id',
        'document_number',
        'type',
        'value'
    ];

    public function receivableUnits(): BelongsToMany
    {
        return $this->belongsToMany(ReceivableUnit::class, 'balance_receivable_unit')
            ->withTimestamps();
    }

    public function anticipations(): BelongsToMany
    {
        return $this->belongsToMany(Anticipation::class, 'balance_anticipation');
    }
}
