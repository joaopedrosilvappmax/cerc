<?php

namespace App\Models\Entities;

use App\Traits\DailyReport;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SyntheticConciliationRecord extends Model
{
    use HasFactory,
        DailyReport;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference_date',
        'accrediting_company_document_number',
        'code_arrangement_payment',
        'total_gross_value',
        'total_constituted_value',
        'pre_contracted_value',
        'anticipation_not_settled_value',
        'integrated_at'
    ];

    protected $dates = [
        'integrated_at'
    ];
}
