<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class IntegrationError extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'receivable_unit_id',
        'anticipation_id',
        'contract_effect_id',
        'code',
        'request',
        'response'
    ];
}
