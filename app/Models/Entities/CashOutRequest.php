<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashOutRequest extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'external_order_id',
        'external_cash_out_id',
        'final_recipient_document_number',
        'code_arrangement_payment',
        'total_constituted_value',
        'request_date',
        'settlement_date',
        'value'
    ];
}
