<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'receivable_unit_id',
        'receivable_unit_payment_id',
        'anticipation_id',
        'external_cash_out_id',
        'external_cash_out_company_id',
        'holder_document_number',
        'ispb',
        'compe',
        'bank_account_type',
        'bank_branch',
        'bank_account',
        'payment_value',
        'paid_at',
    ];

    public function contractEffectOrders(): HasMany
    {
        return $this->hasMany(ContractEffectOrder::class, 'payment_id');
    }

    public function receivableUnit(): BelongsTo
    {
        return $this->belongsTo(ReceivableUnit::class, 'receivable_unit_id');
    }

    public function anticipation(): BelongsTo
    {
        return $this->belongsTo(Anticipation::class, 'anticipation_id');
    }

    public function cashOutPayment()
    {
        return $this->hasOne(CashOutPayment::class);
    }

    public function isFromAnticipation(): bool
    {
        return $this->anticipation && $this->receivable_unit_payment_id;
    }
}
