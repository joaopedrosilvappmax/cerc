<?php

namespace App\Models\Entities;

use App\Traits\DailyReport;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Anticipation extends Model
{
    use HasFactory,
        DailyReport;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'external_cash_out_id',
        'company_id',
        'accrediting_company_id',
        'operation_type',
        'accrediting_company_document_number',
        'final_recipient_document_number',
        'holder_document_number',
        'code_arrangement_payment',
        'total_constituted_value',
        'total_value',
        'paid_value',
        'request_date',
        'settlement_date',
        'integrated_at',
        'paid_at',
    ];

    public function payments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Payment::class);
    }

    public function company(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function getPaymentHolder(): ?Payment
    {
        return Payment::where([
            'anticipation_id' => $this->id,
            'holder_document_number' => $this->holder_document_number
        ])->first();
    }
}
