<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashOutPayment extends Model
{
    use HasFactory,
        SoftDeletes;

    protected $fillable = [
        'external_cash_out_id',
        'external_cash_out_company_id',
        'receivable_unit_id',
        'payment_id',
        'value',
        'type'
    ];

    public function payment(): BelongsTo
    {
        return $this->belongsTo(Payment::class);
    }

    public function receivableUnit(): BelongsTo
    {
        return $this->belongsTo(ReceivableUnit::class);
    }
}
