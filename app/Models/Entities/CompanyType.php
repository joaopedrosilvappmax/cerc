<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class CompanyType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
