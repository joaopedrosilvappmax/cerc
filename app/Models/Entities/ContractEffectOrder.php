<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContractEffectOrder extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'external_order_id',
        'receivable_unit_id',
        'payment_id',
        'contract_effect_id',
        'value',
        'reversal_value',
    ];

    public function payment(): BelongsTo
    {
        return $this->belongsTo(Payment::class);
    }

    public function receivableUnit(): BelongsTo
    {
        return $this->belongsTo(ReceivableUnit::class);
    }

    public function contractEffect(): BelongsTo
    {
        return $this->belongsTo(ContractEffect::class);
    }

    public function receivableUnitOrder(): HasOne
    {
        return $this->hasOne(ReceivableUnitOrder::class, 'external_order_id', 'external_order_id');
    }
}
