<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class IntegrationSuccess extends Model
{
    protected $table = 'integration_success';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'external_id',
        'contract_effect_id',
    ];
}
