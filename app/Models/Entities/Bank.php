<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = [
        'name',
        'compe',
        'ispb',
    ];
}
