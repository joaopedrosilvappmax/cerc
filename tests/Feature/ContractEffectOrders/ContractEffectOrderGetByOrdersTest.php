<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\ContractEffectOrders\ContractEffectOrderNoSettledGetByOrdersService;
use App\Models\Services\ContractEffectOrders\ContractEffectOrderSettledGetByOrdersService;
use App\Models\Services\Contracts\ContractEffectOrderGetByOrdersServiceContract;
use App\Support\EnumTypes\ContractEffectOrderType;
use Carbon\Carbon;

beforeEach(function () {
    $this->receivableUnitNoSettled = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
        'settlement_date' => Carbon::now()->addDay()->format('Y-m-d')
    ]);

    $this->contractEffectNoSettled = ContractEffect::factory()->create();

    $paymentNotSettledFirst = Payment::factory()
        ->create([
            'receivable_unit_id' => $this->receivableUnitNoSettled->id,
            'payment_value' => 5000
        ]);

    $paymentNotSettledSecond = Payment::factory()
        ->create([
            'receivable_unit_id' => $this->receivableUnitNoSettled->id,
            'payment_value' => 5000
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => 1,
            'contract_effect_id' => $this->contractEffectNoSettled->id,
            'receivable_unit_id' => $this->receivableUnitNoSettled->id,
            'payment_id' => $paymentNotSettledFirst->id,
            'value' => 5000
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => 2,
            'contract_effect_id' => $this->contractEffectNoSettled->id,
            'receivable_unit_id' => $this->receivableUnitNoSettled->id,
            'payment_id' => $paymentNotSettledSecond->id,
            'value' => 5000
        ]);

    $this->receivableUnitSettled = ReceivableUnit::factory()
        ->create([
            'total_constituted_value' => 20000,
            'settlement_date' => Carbon::now()->addDay()->format('Y-m-d')
        ]);

    $this->contractEffectSettled = ContractEffect::factory()->create();

    $paymentSettledFirst = Payment::factory()
        ->create([
            'receivable_unit_id' => $this->receivableUnitSettled->id,
            'payment_value' => 10000,
        ]);

    $paymentSettledSecond = Payment::factory()
        ->create([
            'receivable_unit_id' => $this->receivableUnitSettled->id,
            'payment_value' => 10000,
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => 3,
            'receivable_unit_id' => $this->receivableUnitSettled->id,
            'contract_effect_id' => $this->contractEffectSettled->id,
            'payment_id' => $paymentSettledFirst->id,
            'value' => 10000
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => 4,
            'receivable_unit_id' => $this->receivableUnitSettled->id,
            'contract_effect_id' => $this->contractEffectSettled->id,
            'payment_id' => $paymentSettledSecond->id,
            'value' => 10000
        ]);
});

test('Bind Resolution Contract Effect Order Get By Orders', function () {
    $service = app(ContractEffectOrderGetByOrdersServiceContract::class, [
        'type' => ContractEffectOrderType::NO_SETTLED
    ]);

    expect(get_class($service))->toBe(ContractEffectOrderNoSettledGetByOrdersService::class);

    $service = app(ContractEffectOrderGetByOrdersServiceContract::class, [
        'type' => ContractEffectOrderType::SETTLED
    ]);

    expect(get_class($service))->toBe(ContractEffectOrderSettledGetByOrdersService::class);
});

test('Contract Effect Order Get By Orders Id', function () {
    $contractEffectOrderNoSettled = (new ContractEffectOrderNoSettledGetByOrdersService())
        ->getByOrdersId([ 1, 2 ]);

    expect($contractEffectOrderNoSettled->sum('value'))->toBe(10000);
    expect($contractEffectOrderNoSettled->count())->toBe(2);

    $contractEffectOrderSettled = (new ContractEffectOrderNoSettledGetByOrdersService())->getByOrdersId([ 3, 4 ]);

    expect($contractEffectOrderSettled->sum('value'))->toBe(20000);
    expect($contractEffectOrderSettled->count())->toBe(2);

});
