<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectOrderGetByServiceContract;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create([
        'pre_contracted_value' => 55000
    ]);

    $this->anticipation = Anticipation::factory()->create([
        'total_constituted_value' => 55000,
        'total_value' => 55000
    ]);

    $this->receivableUnitOrders = ReceivableUnitOrder::factory()->count(5)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'anticipation_id' => $this->anticipation->id,
        'value' => 11000
    ]);

    $this->contractEffect = ContractEffect::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'committed_value' => 5000,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE
    ]);

    $this->receivableUnitOrders->each(function ($order) {
        ContractEffectOrder::factory()->create([
            'external_order_id' => $order->external_order_id,
            'receivable_unit_id' => $this->receivableUnit->id,
            'contract_effect_id' => $this->contractEffect->id,
            'value' => 1100
        ]);
    });
});

test('Contract Effect Orders By External Cash Out Id', function () {
    $orders = app(ContractEffectOrderGetByServiceContract::class)
        ->getBy($this->anticipation->external_cash_out_id);

    expect($orders)->toHaveCount(5);
    expect($orders->sum('value'))->toBe(5500);
})->group('contract-effect-orders');

test('Contract Effect Order With More Cash Outs', function () {
    $receivableUnit = ReceivableUnit::factory()->create([
        'pre_contracted_value' => 200000
    ]);

    $anticipation = Anticipation::factory()->create([
        'total_constituted_value' => 200000,
        'total_value' => 200000
    ]);

    $receivableUnitOrders = ReceivableUnitOrder::factory()->count(10)->create([
        'receivable_unit_id' => $receivableUnit->id,
        'anticipation_id' => $anticipation->id,
        'value' => 20000
    ]);

    $contractEffect = ContractEffect::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'committed_value' => 50000,
        'division_rule' => DivisionRule::FIXED_VALUE
    ]);

    $receivableUnitOrders->each(function ($order) use ($receivableUnit, $contractEffect) {
        ContractEffectOrder::factory()->create([
            'external_order_id' => $order->external_order_id,
            'receivable_unit_id' => $receivableUnit->id,
            'contract_effect_id' => $contractEffect->id,
            'value' => 5000
        ]);
    });

    $orders = app(ContractEffectOrderGetByServiceContract::class)
        ->getBy($anticipation->external_cash_out_id);

    $allReceivableUnitOrders =ReceivableUnitOrder::get();
    $allContractEffectOrders = ContractEffectOrder::get();

    expect($allReceivableUnitOrders)->toHaveCount(15);
    expect($allReceivableUnitOrders->sum('value'))->toBe(255000);
    expect($allContractEffectOrders)->toHaveCount(15);
    expect($allContractEffectOrders->sum('value'))->toBe(55500);
    expect($orders)->toHaveCount(10);
    expect($orders->sum('value'))->toBe(50000);

})->group('contract-effect-orders');

test('Contract Effect Order With More Cash Outs And Partial Anticipation', function () {
    $receivableUnit = ReceivableUnit::factory()->create([
        'pre_contracted_value' => 200000
    ]);

    $anticipation = Anticipation::factory()->create([
        'total_constituted_value' => 40000,
        'total_value' => 40000
    ]);

    $receivableUnitOrders = ReceivableUnitOrder::factory()->count(10)->create([
        'receivable_unit_id' => $receivableUnit->id,
        'anticipation_id' => $anticipation->id,
        'value' => 20000
    ]);

    $contractEffect = ContractEffect::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'committed_value' => 50000,
        'division_rule' => DivisionRule::FIXED_VALUE
    ]);

    $receivableUnitOrders->pop(2)->each(function ($order) use ($receivableUnit, $contractEffect) {
        ContractEffectOrder::factory()->create([
            'external_order_id' => $order->external_order_id,
            'receivable_unit_id' => $receivableUnit->id,
            'contract_effect_id' => $contractEffect->id,
            'value' => 5000
        ]);
    });

    $orders = app(ContractEffectOrderGetByServiceContract::class)
        ->getBy($anticipation->external_cash_out_id);

    $allReceivableUnitOrders =ReceivableUnitOrder::get();
    $allContractEffectOrders = ContractEffectOrder::get();

    expect($allReceivableUnitOrders)->toHaveCount(15);
    expect($allReceivableUnitOrders->sum('value'))->toBe(255000);
    expect($allContractEffectOrders)->toHaveCount(7);
    expect($allContractEffectOrders->sum('value'))->toBe(15500);
    expect($orders)->toHaveCount(2);
    expect($orders->sum('value'))->toBe(10000);

})->group('contract-effect-orders');
