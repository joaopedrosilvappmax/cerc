<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\ContractEffectOrders\ContractEffectOrderGetByPaymentNoAnticipationService;
use App\Models\Services\Contracts\ContractEffectOrderGetByPaymentServiceContract;
use App\Support\EnumTypes\ContractEffectOrderType;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create();

    $this->anticipation = Anticipation::factory()->create();

    $this->receivableUnitOrderNoAnticipationOne = ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 1000,
        'anticipation_value' => 0,
    ]);

    $this->receivableUnitOrderNoAnticipationTwo = ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 1000,
        'anticipation_value' => 0,
    ]);

    $this->receivableUnitOrderAnticipation = ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 1000,
        'anticipation_value' => 1000,
        'anticipation_id' => $this->anticipation->id,
    ]);

    $this->payment = Payment::factory()
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_value' => $this->receivableUnit->total_constituted_value
        ]);
});

test('Bind Resolution Contract Effect Order Get By Payment', function () {
    $service = app(ContractEffectOrderGetByPaymentServiceContract::class, [
        'type' => ContractEffectOrderType::NO_ANTICIPATION
    ]);

    expect(ContractEffectOrderGetByPaymentNoAnticipationService::class)
        ->toBe(get_class($service));
})->group('bind-resolution');

test('Contract Effect Order Get By Payment Id', function () {
    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => $this->receivableUnitOrderNoAnticipationOne->external_order_id,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $this->payment->id,
            'value' => 500
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => $this->receivableUnitOrderNoAnticipationTwo->external_order_id,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $this->payment->id,
            'value' => 300
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => $this->receivableUnitOrderAnticipation->external_order_id,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $this->payment->id,
            'value' => 600
        ]);

    $quantityContractEffectOrdersNoAnticipation = (new ContractEffectOrderGetByPaymentNoAnticipationService())
        ->getBy($this->payment->id)
        ->count();

    $sumContractEffectOrdersNoAnticipation = (new ContractEffectOrderGetByPaymentNoAnticipationService())
        ->getBy($this->payment->id)
        ->sum('value');

    expect($quantityContractEffectOrdersNoAnticipation)->toBe(2);
    expect($sumContractEffectOrdersNoAnticipation)->toBe(800);
});
