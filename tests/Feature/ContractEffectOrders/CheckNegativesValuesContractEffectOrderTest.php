<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 130350,
        'holder_document_number' => '0000000000',
        'final_recipient_document_number' => '0000000000',
    ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()->count(5)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 26070,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'debit_value' => 0,
    ]);

    $this->paymentHolder = Payment::factory([
        'holder_document_number' => $this->receivableUnit->holder_document_number,
        'payment_value' =>  $this->receivableUnit->total_constituted_value,
        'receivable_unit_id' => $this->receivableUnit->id,
    ])->create();
});

test('Check For Negatives Values In Contract Effect Orders', function () {

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $this->receivableUnit->holder_document_number,
        'holder_beneficiary_document_number' => '1111111111',
        'receivable_unit_id' => $this->receivableUnit->id,
        'effect_type' => ContractEffectType::COURT_BLOCK,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 2000,
        'settlement_date' => $this->receivableUnit->settlement_date,
    ])->create();

    $contractEffectTwo = ContractEffect::factory([
        'holder_document_number' => $this->receivableUnit->holder_document_number,
        'holder_beneficiary_document_number' => '1111111111',
        'receivable_unit_id' => $this->receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 1000,
        'settlement_date' => $this->receivableUnit->settlement_date,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectTwo->effect_type
    ])->apply($contractEffectTwo, $this->receivableUnit);

    $contractEffect->refresh();
    $contractEffectTwo->refresh();

    expect($contractEffect->contractEffectOrders->pluck('value'))->each->toBeGreaterThan(0);
    expect($contractEffectTwo->contractEffectOrders->pluck('value'))->each->toBeGreaterThan(0);
    expect($contractEffect->contractEffectOrders->sum('value'))->toBe(2000);
    expect($contractEffectTwo->contractEffectOrders->sum('value'))->toBe(1000);
});
