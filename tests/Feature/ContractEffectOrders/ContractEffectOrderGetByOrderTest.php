<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\ContractEffectOrders\ContractEffectOrderGetByOrderService;
use App\Models\Services\Contracts\ContractEffectOrderGetByOrderServiceContract;
use Carbon\Carbon;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
        'settlement_date' => Carbon::now()->addDay()->format('Y-m-d')
    ]);

    $this->contractEffectOne = ContractEffect::factory()->create();
    $this->contractEffectTwo = ContractEffect::factory()->create();

    $this->payment = Payment::factory()
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_value' => 10000
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => 1,
            'contract_effect_id' => $this->contractEffectOne->id,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $this->payment->id,
            'value' => 5000
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => 2,
            'contract_effect_id' => $this->contractEffectOne->id,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $this->payment->id,
            'value' => 3000
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => 1,
            'contract_effect_id' => $this->contractEffectTwo->id,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $this->payment->id,
            'value' => 2000
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => 2,
            'contract_effect_id' => $this->contractEffectTwo->id,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $this->payment->id,
            'value' => 1000
        ]);

    ContractEffectOrder::factory()
        ->create([
            'external_order_id' => 2,
            'contract_effect_id' => $this->contractEffectTwo->id,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $this->payment->id,
            'value' => 500
        ]);
});

test('Bind Resolution Contract Effect Order Get By Order', function () {
    $service = app(ContractEffectOrderGetByOrderServiceContract::class);

    expect(get_class($service))
        ->toBe(ContractEffectOrderGetByOrderService::class);
});

test('Contract Effect Order Get By Orders Id', function () {
    $service = new ContractEffectOrderGetByOrderService();

    $contractEffectOrder = $service->getBy(1);

    expect($contractEffectOrder)->toHaveCount(2);
    expect($contractEffectOrder->sum('value'))->toBe(7000);

    $contractEffectOrder = $service->getBy(2);

    expect($contractEffectOrder)->toHaveCount(3);
    expect($contractEffectOrder->sum('value'))->toBe(4500);

    $contractEffectOrder = $service->getBy(3);

    expect($contractEffectOrder)->toBeEmpty();
});
