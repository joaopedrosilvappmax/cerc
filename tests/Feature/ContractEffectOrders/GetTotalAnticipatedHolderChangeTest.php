<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\ContractEffectOrders\GetTotalContractEffectAnticipatedHolderChangeService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\GetTotalContractEffectAnticipatedServiceContract;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use Carbon\Carbon;

beforeEach(function () {
    $this->receivableUnitSettled = ReceivableUnit::factory()->create([
        'holder_document_number' => '1111111111111',
        'settlement_date' => Carbon::now()->subDay(1)->toDateString(),
        'total_constituted_value' => 10000,
        'final_recipient_document_number' => '1111111111111'
    ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()->count(5)->create([
        'receivable_unit_id' => $this->receivableUnitSettled->id,
        'value' => 2000,
        'anticipation_value' => 0,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'debit_value' => 0,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnitSettled->id,
        'holder_document_number' => $this->receivableUnitSettled->holder_document_number,
        'payment_value' => $this->receivableUnitSettled->total_constituted_value,
        'anticipation_id' => null,
        'paid_at' => null,
    ]);
});

test('Bind Resolution Get Total Contract Effect Anticipated Service', function () {
    $service = app(GetTotalContractEffectAnticipatedServiceContract::class);

    $this->expect(get_class($service))->toBe(GetTotalContractEffectAnticipatedHolderChangeService::class);
})->group('bind-resolution');

test('Get total anticipated holder change', function () {
    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $this->receivableUnitSettled->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnitSettled->code_arrangement_payment,
            'settlement_date' => $this->receivableUnitSettled->settlement_date,
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 5000,
            'division_rule' => DivisionRule::FIXED_VALUE
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnitSettled);

    $receivableUnitHolderChange = ReceivableUnit::whereNotNull('receivable_unit_linked_id')->first();

    $anticipation = Anticipation::factory()->create();

    $this->receivableUnitOrdersInitial->first()->update([
        'anticipation_id' => $anticipation->id
    ]);

    $total = app(GetTotalContractEffectAnticipatedServiceContract::class)
        ->getTotal($receivableUnitHolderChange->id);

    expect($total)->toBe(1000);
});
