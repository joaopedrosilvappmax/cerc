<?php

namespace Tests\Anticipations\Feature;

use App\Exceptions\NotFoundException;
use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Anticipations\AnticipationRefuseService;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\OperationType;
use App\Support\ValueObjects\Bank;

beforeEach(function () {
    $ispb = (new Bank(341))->getFormattedIspb();

    $this->anticipation = Anticipation::factory()->create([
        'external_cash_out_id' => 1,
        'total_constituted_value' => 20000
    ]);

    $this->receivableUnit = ReceivableUnit::factory()->create([
        'final_recipient_document_number' => $this->anticipation->final_recipient_document_number,
        'holder_document_number' => $this->anticipation->holder_document_number,
        'code_arrangement_payment' => $this->anticipation->code_arrangement_payment,
        'settlement_date' => $this->anticipation->settlement_date,
        'total_constituted_value' => 20000
    ]);

    $this->paymentFromReceivableUnitHolder = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
        'payment_value' => 10000
    ]);

    $this->paymentFromReceivableUnitContractEffect = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'compe' => 341,
        'ispb' => $ispb,
        'payment_value' => 10000
    ]);

    $this->paymentAnticipationHolder = Payment::factory()->create([
        'receivable_unit_payment_id' => $this->paymentFromReceivableUnitHolder->id,
        'anticipation_id' => $this->anticipation->id,
        'holder_document_number' => $this->anticipation->holder_document_number,
        'payment_value' => 10000
    ]);

    $this->paymentAnticipationContractEffect = Payment::factory()->create([
        'receivable_unit_payment_id' => $this->paymentFromReceivableUnitContractEffect->id,
        'anticipation_id' => $this->anticipation->id,
        'holder_document_number' => $this->paymentFromReceivableUnitContractEffect->holder_document_number,
        'compe' => 341,
        'ispb' => $ispb,
        'payment_value' => 10000
    ]);

    ReceivableUnitOrder::factory()->count(10)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 2000
    ]);

    ReceivableUnitOrder::factory()->count(10)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'anticipation_id' => $this->anticipation->id,
        'anticipation_value' => 2000,
        'value' => 2000
    ]);

    ContractEffect::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'final_recipient_document_number' => $this->anticipation->final_recipient_document_number,
        'holder_document_number' => $this->anticipation->holder_document_number,
        'code_arrangement_payment' => $this->anticipation->code_arrangement_payment,
        'settlement_date' => $this->anticipation->settlement_date,
        'holder_beneficiary_document_number' => $this->paymentFromReceivableUnitContractEffect->holder_document_number,
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'committed_value' => 5000,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'compe' => 341,
    ]);
});

test('Exception Not Found Refuse Anticipation', function () {
    (new AnticipationRefuseService())->refuse(2);
})->throws(NotFoundException::class);

test('Refuse Anticipation', function () {
    expect($this->anticipation->operation_type)
        ->toBe(OperationType::CREATE);

    expect(ReceivableUnitOrder::where('anticipation_id', $this->anticipation->id)->count())
        ->toBe(10);

    (new AnticipationRefuseService())->refuse(1);

    $this->anticipation->refresh();
    $this->receivableUnit->refresh();
    $this->paymentFromReceivableUnitHolder->refresh();
    $this->paymentFromReceivableUnitContractEffect->refresh();

    expect($this->anticipation->operation_type)
        ->toBe(OperationType::INACTIVE);

    expect(ReceivableUnitOrder::where('anticipation_id', $this->anticipation->id)->count())
        ->toBe(0);

    expect(ReceivableUnitOrder::where('anticipation_value', 0)->count())
        ->toBe(20);

    expect($this->receivableUnit->total_constituted_value)
        ->toBe(40000);

    expect($this->paymentFromReceivableUnitHolder->payment_value)
        ->toBe(20000);

    expect($this->paymentFromReceivableUnitContractEffect->payment_value)
        ->toBe(20000);

    $this->paymentAnticipationContractEffect->refresh();
    $this->paymentAnticipationHolder->refresh();

    expect($this->paymentAnticipationContractEffect->payment_value)->toBe(10000);

    expect($this->paymentAnticipationHolder->payment_value)->toBe(10000);
})->group('cancel-anticipation');
