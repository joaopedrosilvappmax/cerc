<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\CashOutPayment;
use App\Models\Entities\Company;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Anticipations\AnticipationRegisterService;
use App\Models\Services\CashOutPayment\CashOutPaymentAnticipationRegisterService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Anticipations\AnticipationDataRegister;
use App\Support\DataTransferObjects\CashOutPayments\CashOutPaymentsDataRegister;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use Tests\Stub\AnticipationDataRegisterMock;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
        'holder_document_number' => '0000000000',
        'final_recipient_document_number' => '0000000000',
    ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()->count(5)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 2000,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'debit_value' => 0,
    ]);

    $this->paymentHolder = Payment::factory([
        'holder_document_number' => $this->receivableUnit->holder_document_number,
        'payment_value' =>  $this->receivableUnit->total_constituted_value,
        'receivable_unit_id' => $this->receivableUnit->id,
    ])->create();
});

test('Bind Resolution Anticipations', function () {
    $anticipationRegisterData = app(DataRegisterContract::class, [
        'class' => ClassBindResolution::ANTICIPATION_DATA
    ]);

    expect(AnticipationDataRegister::class)->toBe(get_class($anticipationRegisterData));

    $anticipationRegisterService = app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::ANTICIPATION_SERVICE
    ]);

    expect(get_class($anticipationRegisterService))->toBe(AnticipationRegisterService::class);
});

test('Mount Anticipations Data', function () {

    $data = AnticipationDataRegisterMock::getInitialData();

    $anticipationData = (new AnticipationDataRegister())->addData($data);

    foreach ($anticipationData->toArray() as $k => $d) {
        expect($d)->toEqual($data[$k]);
    }
});

test('Store Anticipation', function () {
    $value = 1700;

    $receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => $value,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => $receivableUnit->total_constituted_value
    ]);

    $receivableUnitOrder = ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1000,
        'reversal_value' => 0
    ]);

    $receivableUnitOrderSecond = ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 700,
        'reversal_value' => 0
    ]);

    Company::factory()->create([
        'document_number' => $receivableUnit->holder_document_number
    ]);

    $data = AnticipationDataRegisterMock::getInitialData();
    $data['final_recipient_document_number'] = $receivableUnit->holder_document_number;
    $data['code_arrangement_payment'] = $receivableUnit->code_arrangement_payment;
    $data['external_order_id'] = $receivableUnitOrder->external_order_id;

    $anticipation = (new AnticipationRegisterService())
        ->store(
            (new AnticipationDataRegister())
                ->addData($data)
        );

    $receivableUnit->refresh();

    expect($anticipation->total_constituted_value)
        ->toBe($receivableUnitOrder->value);

    expect($receivableUnit->total_constituted_value)
        ->toBe($receivableUnitOrderSecond->value);

    expect($receivableUnit->payments()->first()->payment_value)
        ->toBe($receivableUnitOrderSecond->value);
});

test('Store Anticipation With Contract Effects', function () {
    $value = 1700;

    $receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => $value,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => $receivableUnit->total_constituted_value - 300
    ]);

    $paymentContractEffect = Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => 300
    ]);

    ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1000,
        'reversal_value' => 0
    ]);

    $receivableUnitOrderSecond = ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 700,
        'reversal_value' => 0
    ]);

    ContractEffectOrder::factory()->create([
        'external_order_id' => $receivableUnitOrderSecond->external_order_id,
        'receivable_unit_id' => $receivableUnit->id,
        'payment_id' => $paymentContractEffect->id,
        'value' => 300,
    ]);

    Company::factory()->create([
        'document_number' => $receivableUnit->holder_document_number
    ]);

    $valueAnticipationPaymentContractEffect = 300;
    $cashOutDataRegister = (new CashOutPaymentsDataRegister())->addData([
        'external_cash_out_id' => 1,
        'external_cash_out_company_id' => 10,
        'receivable_unit_id' => $receivableUnit->id,
        'payment_id' => $paymentContractEffect->id,
        'payment_value' => $valueAnticipationPaymentContractEffect
    ]);

    app(CashOutPaymentAnticipationRegisterService::class)->store($cashOutDataRegister);

    $receivableUnit->refresh();

    expect($receivableUnit->total_constituted_value)
        ->toBe($value - $valueAnticipationPaymentContractEffect);

    $data = AnticipationDataRegisterMock::getInitialData();
    $data['final_recipient_document_number'] = $receivableUnit->holder_document_number;
    $data['code_arrangement_payment'] = $receivableUnit->code_arrangement_payment;
    $data['external_order_id'] = $receivableUnitOrderSecond->external_order_id;

    $anticipation = (new AnticipationRegisterService())
        ->store(
            (new AnticipationDataRegister())
                ->addData($data)
        );

    $receivableUnit->refresh();

    expect($anticipation->total_constituted_value)
        ->toBe($receivableUnitOrderSecond->getRawOriginal('value'));

    expect($receivableUnit->total_constituted_value)
        ->toBe($value - $receivableUnitOrderSecond->getRawOriginal('value'));

    expect(
        $receivableUnit->payments()
            ->where('holder_document_number', $receivableUnit->holder_document_number)
            ->first()->payment_value
    )->toBe(1000);
});

test('Verify set paid at in anticipation holder change', function () {
    $receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
        'holder_document_number' => '0000000000',
        'final_recipient_document_number' => '0000000000',
    ]);

    ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 2500,
    ]);

    Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'holder_beneficiary_document_number' => '1111111111',
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 1500,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $receivableUnitHolderChange = ReceivableUnit::where('receivable_unit_linked_id', $receivableUnit->id)->first();

    $cashOutPaymentService = new CashOutPaymentAnticipationRegisterService();
    $anticipationData = new CashOutPaymentsDataRegister();

    $anticipationData->addData([
        'external_cash_out_id' => 511,
        'external_cash_out_company_id' => 1010,
        'receivable_unit_id' => $receivableUnitHolderChange->id,
        'payment_id' => $receivableUnitHolderChange->payments->first()->id,
        'payment_value' => 500,
    ]);

    $cashOutPaymentService->store($anticipationData);

    $cashOutPayment = CashOutPayment::first();

    $receivableUnit = $cashOutPayment->payment->receivableUnit;

    expect($receivableUnit->paid_at)->toBeNull();
})->group('anticipation-no-holder');

test('Check flow creation of anticipation', function ($valueAnticipationPaymentContractEffect) {
    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $this->receivableUnit->holder_document_number,
        'holder_beneficiary_document_number' => '1111111111',
        'receivable_unit_id' => $this->receivableUnit->id,
        'effect_type' => ContractEffectType::COURT_BLOCK,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 2000,
        'settlement_date' => $this->receivableUnit->settlement_date,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $paymentContractEffect = Payment::where(
        'holder_document_number', '<>', $this->receivableUnit->holder_document_number
    )->first();

    $cashOutDataRegister = (new CashOutPaymentsDataRegister())->addData([
        'external_cash_out_id' => 1,
        'external_cash_out_company_id' => 10,
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_id' => $paymentContractEffect->id,
        'payment_value' => $valueAnticipationPaymentContractEffect
    ]);

    app(CashOutPaymentAnticipationRegisterService::class)->store($cashOutDataRegister);

    $anticipation = Anticipation::where([
        'external_cash_out_id' => $cashOutDataRegister->external_cash_out_company_id,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment
    ])->first();

    $paymentAnticipationHolder = $anticipation->payments->where(
        'holder_document_number', $this->receivableUnit->holder_document_number
    )->first();

    $paymentAnticipationContractEffect = $anticipation->payments->where(
        'holder_document_number', '<>', $this->receivableUnit->holder_document_number
    )->first();

    expect($anticipation->total_constituted_value)->toBe(800);
    expect($paymentAnticipationHolder->payment_value)->toBe(0);
    expect($paymentAnticipationContractEffect->payment_value)->toBe(800);

    $anticipationData = (new AnticipationDataRegister())->addData([
        'external_order_id' => $this->receivableUnitOrdersInitial->first()->external_order_id,
        'external_cash_out_id' => 10,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'value' => $this->receivableUnitOrdersInitial->first()->value,
        'request_date' => '2022-01-21'
    ]);

    $valueOrderFirstContractEffect = ContractEffectOrder::where(
        'external_order_id', $this->receivableUnitOrdersInitial->first()->external_order_id
    )->sum('value');

    app(AnticipationRegisterService::class)->store($anticipationData);

    $anticipation->refresh();
    $paymentAnticipationHolder->refresh();
    $paymentAnticipationContractEffect->refresh();

    expect($anticipation->total_constituted_value)
        ->toBe($this->receivableUnitOrdersInitial->first()->refresh()->getRawOriginal('value'));

    expect($paymentAnticipationContractEffect->payment_value)->toBe($valueAnticipationPaymentContractEffect);
    expect($this->receivableUnitOrdersInitial->first()->refresh()->anticipation_value)
        ->toBe($this->receivableUnitOrdersInitial->first()->getRawOriginal('value'));

    $anticipationData = (new AnticipationDataRegister())->addData([
        'external_order_id' => $this->receivableUnitOrdersInitial->last()->external_order_id,
        'external_cash_out_id' => 10,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'value' => $this->receivableUnitOrdersInitial->last()->refresh()->getRawOriginal('value'),
        'request_date' => '2022-01-21'
    ]);

    $valueOrderLastContractEffect = ContractEffectOrder::where(
        'external_order_id', $this->receivableUnitOrdersInitial->last()->external_order_id
    )->sum('value');

    app(AnticipationRegisterService::class)->store($anticipationData);

    $anticipation->refresh();
    $paymentAnticipationHolder->refresh();
    $paymentAnticipationContractEffect->refresh();

    expect($anticipation->total_constituted_value)->toBe(
        $valueAnticipationPaymentContractEffect
        + $this->receivableUnitOrdersInitial->first()->refresh()->getRawOriginal('value')
        + $this->receivableUnitOrdersInitial->last()->refresh()->getRawOriginal('value')
        - $valueOrderFirstContractEffect
        - $valueOrderLastContractEffect
    );
    expect($paymentAnticipationHolder->payment_value)
        ->toBe(
            $this->receivableUnitOrdersInitial->first()->refresh()->getRawOriginal('value')
            + $this->receivableUnitOrdersInitial->last()->refresh()->getRawOriginal('value')
            - $valueOrderFirstContractEffect
            - $valueOrderLastContractEffect
        );
    expect($paymentAnticipationContractEffect->payment_value)->toBe($valueAnticipationPaymentContractEffect);
    expect($this->receivableUnitOrdersInitial->first()->refresh()->anticipation_value)
        ->toBe($this->receivableUnitOrdersInitial->first()->getRawOriginal('value'));
    expect($this->receivableUnitOrdersInitial->last()->anticipation_value)
        ->toBe($this->receivableUnitOrdersInitial->last()->refresh()->getRawOriginal('value'));
})->with([800])->group('flow-anticipation');
