<?php

namespace Tests\Feature;

use App\Models\Entities\Authentication;
use App\Models\Entities\Payment;
use App\Models\Entities\Anticipation;
use App\Models\Services\Anticipations\AnticipationIntegrateService;
use App\Support\EnumTypes\IntegrationResponseStatus;
use Illuminate\Support\Facades\Http;
use Tests\Stub\IntegrationResponseDataMock;

beforeEach(function () {
    Authentication::factory()->create();

    $this->anticipation = Anticipation::factory()->create();

    Payment::factory()->count(2)->create([
        'anticipation_id' => $this->anticipation->id,
    ]);
});

test('Success Integrate Anticipations', function () {
    Http::fake([
        config('cerc.api_url') . '*' => Http::response([
            IntegrationResponseDataMock::getSuccessResponse($this->anticipation),
        ], 207, []),
    ]);

    $response = (new AnticipationIntegrateService)->send($this->anticipation);

    expect($response->getStatus())->toBe(IntegrationResponseStatus::SUCCESS);

    $this->anticipation = $this->anticipation->fresh();

    expect($this->anticipation->integrated_at)->not()->toBeNull();
});

test('Error Invalid Operation Integrate Anticipations', function () {
    Http::fake([
        config('cerc.api_url') . '*' => Http::response(
            IntegrationResponseDataMock::getErrorOperationInvalidResponse(
                $this->anticipation,
                IntegrationResponseStatus::ANTICIPATION_INVALID_OPERATION
            ),
            207,
            []
        ),
    ]);

    $response = (new AnticipationIntegrateService)->send($this->anticipation);

    expect($response->getStatus())->toBe(IntegrationResponseStatus::FAILED);

    $this->anticipation = $this->anticipation->fresh();

    expect($this->anticipation->integrated_at)->not()->toBeNull();
});

test('Error Exists Integrate Anticipations', function () {
    Http::fake([
        config('cerc.api_url') . '*' => Http::response(
            IntegrationResponseDataMock::getErrorExistsInformationResponse(
                $this->anticipation,
                IntegrationResponseStatus::ANTICIPATION_CREATED
            ),
            207,
            []
        ),
    ]);

    $response = (new AnticipationIntegrateService)->send($this->anticipation);

    expect($response->getStatus())->toBe(IntegrationResponseStatus::FAILED);

    $this->anticipation = $this->anticipation->fresh();

    expect($this->anticipation->integrated_at)->not()->toBeNull();
});

test('Error Integrate Anticipations', function () {
    Http::fake([
        config('cerc.api_url') . '*' => Http::response(
            IntegrationResponseDataMock::getErrorResponse($this->anticipation),
            207,
            []
        ),
    ]);

    $response = (new AnticipationIntegrateService)->send($this->anticipation);

    expect($response->getStatus())->toBe(IntegrationResponseStatus::FAILED);

    $this->anticipation = $this->anticipation->fresh();

    expect($this->anticipation->integrated_at)->toBeNull();
});

