<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\Company;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Anticipations\AnticipationRegisterService;
use App\Models\Services\CashOutPayment\CashOutPaymentAnticipationRegisterService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Support\DataTransferObjects\Anticipations\AnticipationDataRegister;
use App\Support\DataTransferObjects\CashOutPayments\CashOutPaymentsDataRegister;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 8000,
        'holder_document_number' => '0000000000',
        'final_recipient_document_number' => '0000000000',
    ]);

    Company::factory()->create([
        'document_number' => $this->receivableUnit->holder_document_number
    ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()->count(4)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 2000,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'debit_value' => 0,
    ]);

    $this->paymentHolder = Payment::factory([
        'holder_document_number' => $this->receivableUnit->holder_document_number,
        'payment_value' =>  $this->receivableUnit->total_constituted_value,
        'receivable_unit_id' => $this->receivableUnit->id,
    ])->create();
});

test('Verify Payments After Anticipation Without Contract Effect', function (){

    $this->receivableUnitOrdersInitial->pop(3)->each(function ($receivableUnitOrder) {
        app(AnticipationRegisterService::class)->store(
            (new AnticipationDataRegister())->addData([
                'external_order_id' => $receivableUnitOrder->external_order_id,
                'external_cash_out_id' => 10,
                'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
                'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
                'value' => $receivableUnitOrder->value,
                'request_date' => '2022-01-21'
            ])
        );
    });

    $this->receivableUnit->refresh();
    $this->paymentHolder->refresh();

    $anticipation = Anticipation::first();

    expect($this->receivableUnit->total_constituted_value)->toBe(2000);
    expect($anticipation->total_constituted_value)->toBe(6000);
    expect($anticipation->payments->first()->payment_value)->toBe(6000);
    expect($this->paymentHolder->payment_value)->toBe(2000);
});

test('Verify Payments Before Anticipation With Contract Effect', function () {
    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $this->receivableUnit->holder_document_number,
        'holder_beneficiary_document_number' => '1111111111',
        'receivable_unit_id' => $this->receivableUnit->id,
        'effect_type' => ContractEffectType::COURT_BLOCK,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 2000,
        'settlement_date' => $this->receivableUnit->settlement_date,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $paymentContractEffect = Payment::where('holder_document_number', $contractEffect->holder_beneficiary_document_number)
        ->first();

    $this->receivableUnitOrdersInitial->pop(3)->each(function ($receivableUnitOrder) use ($paymentContractEffect) {
        $contractEffectOrder = ContractEffectOrder::where('external_order_id', $receivableUnitOrder->external_order_id)
            ->first();

        app(CashOutPaymentAnticipationRegisterService::class)->store(
            (new CashOutPaymentsDataRegister())->addData([
                'external_cash_out_id' => 1,
                'external_cash_out_company_id' => 10,
                'receivable_unit_id' => $this->receivableUnit->id,
                'payment_id' => $paymentContractEffect->id,
                'payment_value' => $contractEffectOrder->value
            ])
        );

        app(AnticipationRegisterService::class)->store(
            (new AnticipationDataRegister())->addData([
                'external_order_id' => $receivableUnitOrder->external_order_id,
                'external_cash_out_id' => 10,
                'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
                'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
                'value' => $receivableUnitOrder->value,
                'request_date' => '2022-01-21'
            ])
        );
    });

    $this->receivableUnit->refresh();
    $this->paymentHolder->refresh();
    $anticipation = Anticipation::first();
    $paymentContractEffect = $paymentContractEffect->fresh();

    $paymentAnticipationHolder = $anticipation->payments
        ->where('holder_document_number', $anticipation->holder_document_number)->first();
    $paymentAnticipationContractEffect = $anticipation->payments
        ->where('holder_document_number', "!=" , $anticipation->holder_document_number)->first();

    expect($this->receivableUnit->total_constituted_value)->toBe(2000);
    expect($anticipation->total_constituted_value)->toBe(6000);
    expect($paymentAnticipationHolder->payment_value)->toBe(4500);
    expect($paymentAnticipationContractEffect->payment_value)->toBe(1500);
    expect($this->paymentHolder->payment_value)->toBe(1500);
    expect($paymentContractEffect->payment_value)->toBe(500);
});
