<?php

namespace Tests\Feature;

use App\Models\Services\ContractEffects\ContractEffectRegisterService;
use App\Models\Services\ContractEffects\ContractEffectService;
use App\Support\DataTransferObjects\ContractEffects\ContractEffectDataRegister;
use Illuminate\Http\Request;
use Tests\Stub\ContractEffectDataRegisterMock;

test('Request Contract Effect From Orders', function () {
    $contractEffectRegisterData = (new ContractEffectDataRegister())
        ->addData(ContractEffectDataRegisterMock::getInitialData());

    $newContractEffect = (new ContractEffectRegisterService())
        ->store($contractEffectRegisterData);

    expect($newContractEffect)->not()->toBeEmpty();

    $request = new Request([
        'contract_effect_ids' => [$newContractEffect->external_contract_id]
    ]);

    $response = app(ContractEffectService::class)
        ->getContractEffects($request->get('contract_effect_ids'));

    expect($response)->not()->toBeEmpty();
});
