<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\ContractEffects\DivisionRuleApplicators\DivisionRuleFixedValueApplicatorForHolderChangeService;
use App\Models\Services\ContractEffects\DivisionRuleApplicators\DivisionRulePercentageValueApplicatorForHolderChangeService;
use App\Models\Services\ContractEffects\ReceivableUnitContractEffectHolderChangeService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\DivisionRuleApplicatorForHolderChangeServiceContract;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\Math;

test('Bind Resolution Contract Effect Apply', function () {

    $contractEffectHolderChange = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectHolderChange->effect_type
    ]);

    expect(get_class($effectService))
        ->toBe(ReceivableUnitContractEffectHolderChangeService::class);

})->group('bind-resolution');

test('Bind Resolution Division Rule For Onus', function () {
    $divisionRuleApplicator = app(DivisionRuleApplicatorForHolderChangeServiceContract::class, [
        'division_rule' => DivisionRule::FIXED_VALUE
    ]);

    expect(get_class($divisionRuleApplicator))
        ->toBe(DivisionRuleFixedValueApplicatorForHolderChangeService::class);

    $divisionRuleApplicator = app(DivisionRuleApplicatorForHolderChangeServiceContract::class, [
        'division_rule' => DivisionRule::PERCENTAGE_VALUE
    ]);

    expect(get_class($divisionRuleApplicator))
        ->toBe(DivisionRulePercentageValueApplicatorForHolderChangeService::class);

})->group('bind-resolution');

test('Contract Effect Apply Change Holder Fixed Value', function () {
    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 1000
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ]);

    $receivableUnit = ReceivableUnit::factory()
        ->create([
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'total_constituted_value' => 9000
        ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->final_recipient_document_number
    ]);

    $receivableUnitInitialValue = $receivableUnit->total_constituted_value;
    $receivableUnitNewHolder = $effectService->apply($contractEffect, $receivableUnit);

    $receivableUnit = $receivableUnit->fresh('payments');

    expect($receivableUnitNewHolder->payments->count())->toBe(1);
    expect($receivableUnit->payments->first()->payment_value)->not()->toBe($receivableUnitInitialValue);
    expect($receivableUnit->total_constituted_value)->not()->toBe($receivableUnitInitialValue);
    expect(
        $receivableUnit->payments->first()->payment_value
        + $receivableUnitNewHolder->payments->first()->payment_value
    )->toBe($receivableUnitInitialValue);

    expect($receivableUnit->payments->first()->payment_value
        + $receivableUnitNewHolder->payments->first()->payment_value
    )->toBe($receivableUnitInitialValue);

    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 8000
        ]);

    $receivableUnitInitialValue = $receivableUnit->total_constituted_value;

    $receivableUnitNewHolder = $effectService->apply($contractEffect, $receivableUnit);

    $receivableUnit->refresh();

    expect($receivableUnitNewHolder->payments->count())->toBe(1);
    expect($receivableUnit->payments->count())->toBe(1);
    expect($receivableUnit->payments->first()->payment_value)->not()->toBe($receivableUnitInitialValue);
    expect($receivableUnit->total_constituted_value)->not()->toBe($receivableUnitInitialValue);
    expect($receivableUnit->payments->first()->payment_value
        + $receivableUnitNewHolder->payments->first()->payment_value)->toBe($receivableUnitInitialValue);

    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 3000
        ]);

    $effectService->apply($contractEffect, $receivableUnit);

    $receivableUnitNewHolder = $effectService->apply($contractEffect, $receivableUnit);
    $receivableUnitNewHolder->refresh();

    expect( $receivableUnitNewHolder->payments()->first()->payment_value)->toBe(0);
});

test('Contract Effect Apply Change Holder Percentage Value', function () {
    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 4500,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ]);

    $receivableUnit = ReceivableUnit::factory()
        ->create([
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'total_constituted_value' => 10000
        ]);

    $payment = Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->final_recipient_document_number,
    ]);

    $correctValueReceivableUnit = $receivableUnit->total_constituted_value
        - (int) ceil($receivableUnit->total_constituted_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER));

    $correctValuePayment = $payment->payment_value
        - (int) ceil($payment->payment_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER));

    $receivableUnitInitialValue = $receivableUnit->total_constituted_value;

    $receivableUnitNewHolder = $effectService->apply($contractEffect, $receivableUnit);

    $receivableUnit = $receivableUnit->fresh('payments');

    expect($receivableUnitNewHolder->payments->count())->toBe(1);
    expect($receivableUnit->payments->count())->toBe(1);

    expect($receivableUnit->payments->first()->payment_value)->not()->toBe($receivableUnitInitialValue);
    expect($receivableUnit->total_constituted_value)->not()->toBe($receivableUnitInitialValue);

    expect(
        $receivableUnit->payments->first()->payment_value
        + $receivableUnitNewHolder->payments->first()->payment_value
    )->toBe($receivableUnitInitialValue);
    expect(
        $receivableUnit->total_constituted_value
        + $receivableUnitNewHolder->total_constituted_value
    )->toBe($receivableUnitInitialValue);

    expect($receivableUnit->total_constituted_value)->toBe($correctValueReceivableUnit);
    expect($receivableUnit->payments->first()->payment_value)->toBe($correctValuePayment);

    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 10000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE
        ]);

    $payment = $receivableUnit->payments->first();

    $correctValueReceivableUnit = $receivableUnit->total_constituted_value
        - (int) ceil($receivableUnit->total_constituted_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER));

    $correctValuePayment = $payment->payment_value
        - (int) ceil($payment->payment_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER));

    $receivableUnitInitialValue = $receivableUnit->total_constituted_value;

    $receivableUnitNewHolder = $effectService->apply($contractEffect, $receivableUnit);

    $receivableUnit = $receivableUnit->fresh('payments');

    expect($receivableUnitNewHolder->payments->count())->toBe(1);
    expect($receivableUnit->payments->count())->toBe(1);

    expect($receivableUnit->payments->first()->payment_value)->not()->toBe($receivableUnitInitialValue);
    expect($receivableUnit->total_constituted_value)->not()->toBe($receivableUnitInitialValue);

    expect(
        $receivableUnit->payments->first()->payment_value
        + $receivableUnitNewHolder->payments->first()->payment_value
    )->toBe($receivableUnitInitialValue);

    expect(
        $receivableUnit->total_constituted_value
        + $receivableUnitNewHolder->total_constituted_value
    )->toBe($receivableUnitInitialValue);

    expect($receivableUnit->total_constituted_value)->toBe($correctValueReceivableUnit);
    expect($receivableUnit->payments->first()->payment_value)->toBe($correctValuePayment);
});

test('Verify holder document number payment', function () {
    $receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
        'holder_document_number' => '0000000000',
        'final_recipient_document_number' => '0000000000',
    ]);

    ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 2500,
    ]);

    Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'holder_beneficiary_document_number' => '1111111111',
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 1500,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $receivableUnitHolderChange = ReceivableUnit::where('receivable_unit_linked_id', $receivableUnit->id)->first();

    expect($receivableUnitHolderChange->payments->first()->holder_document_number)
        ->toBe($contractEffect->holder_beneficiary_document_number);

});
