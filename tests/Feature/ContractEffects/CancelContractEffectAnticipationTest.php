<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Anticipations\AnticipationRegisterService;
use App\Models\Services\CashOutPayment\CashOutPaymentAnticipationRegisterService;
use App\Models\Services\ContractEffects\Cancel\CancelAnticipationContractEffectOnusService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Support\DataTransferObjects\Anticipations\AnticipationDataRegister;
use App\Support\DataTransferObjects\CashOutPayments\CashOutPaymentsDataRegister;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 65025,
    ]);

    $this->totalConstitutedValueInitial = $this->receivableUnit->total_constituted_value;

     Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);

    $receivableUnitValue = 13005;

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()->count(5)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => $receivableUnitValue,
        'anticipation_value' => 0,
        'debit_value' => 0,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ]);

    $this->contractEffectCancelled = ContractEffect::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'committed_value' => 5000,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'holder_document_number' =>  $this->receivableUnit->holder_document_number,
        'holder_beneficiary_document_number' => '1111111111',
    ]);

    $this->contractEffect = ContractEffect::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'committed_value' => 5000,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'holder_document_number' =>  $this->receivableUnit->holder_document_number,
        'holder_beneficiary_document_number' => '1111111111',
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $this->contractEffectCancelled->effect_type
    ])->apply($this->contractEffectCancelled, $this->receivableUnit);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $this->contractEffect->effect_type
    ])->apply($this->contractEffect, $this->receivableUnit);

    $this->paymentOriginalHolder = Payment::where('holder_document_number', $this->receivableUnit->holder_document_number)->first();

    $this->paymentContractEffect = Payment::where('holder_document_number',  $this->contractEffect->holder_beneficiary_document_number)->first();
});

test ('Cancel Contract Effect With Anticipation Total', function ($valueAnticipationPaymentContractEffect) {
    $cashOutDataRegister = (new CashOutPaymentsDataRegister())->addData([
        'external_cash_out_id' => 1,
        'external_cash_out_company_id' => 10,
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_id' => $this->paymentContractEffect->id,
        'payment_value' => $valueAnticipationPaymentContractEffect
    ]);

    app(CashOutPaymentAnticipationRegisterService::class)->store($cashOutDataRegister);

    $this->receivableUnitOrdersInitial->each(function ($receivableUnit) {
        $anticipationData = (new AnticipationDataRegister())->addData([
            'external_order_id' => $receivableUnit->external_order_id,
            'external_cash_out_id' => 10,
            'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'value' => $receivableUnit->value,
            'request_date' => '2022-01-21'
        ]);

        app(AnticipationRegisterService::class)->store($anticipationData);
    });

    $anticipation = Anticipation::where([
        'settlement_date' => $this->contractEffect->settlement_date,
        'code_arrangement_payment' => $this->contractEffect->code_arrangement_payment,
        'holder_document_number' => $this->contractEffect->final_recipient_document_number,
    ])->first();

    $paymentAnticipationHolder = $anticipation->payments->where(
        'holder_document_number', $this->receivableUnit->holder_document_number
    )->first();

    $paymentAnticipationContractEffect = $anticipation->payments->where(
        'holder_document_number', '<>', $this->receivableUnit->holder_document_number
    )->first();

    $this->paymentContractEffect->refresh();
    $this->receivableUnit->refresh();
    $this->paymentOriginalHolder->refresh();

    expect($paymentAnticipationContractEffect->payment_value)->toBe(10000);
    expect($paymentAnticipationHolder->payment_value)->toBe(55025);
    expect($this->paymentContractEffect->payment_value)->toBe(0);
    expect($this->receivableUnit->total_constituted_value)->toBe(0);
    expect($this->paymentOriginalHolder->payment_value)->toBe(0);

    $contractEffectCancel = ContractEffect::factory()->create([
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'committed_value' => 0,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->contractEffectCancelled->final_recipient_document_number,
        'code_arrangement_payment' => $this->contractEffectCancelled->code_arrangement_payment,
        'settlement_date' => $this->contractEffectCancelled->settlement_date,
        'holder_document_number' => $this->contractEffectCancelled->holder_document_number,
        'holder_beneficiary_document_number' => $this->contractEffectCancelled->holder_beneficiary_document_number
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectCancel->effect_type,
        'has_committed_value' => false
    ])->apply($contractEffectCancel, $this->receivableUnit);

    $this->paymentOriginalHolder->refresh();
    $this->paymentContractEffect->refresh();
    $this->contractEffectCancelled->refresh();
    $this->contractEffect->refresh();
    $anticipation->refresh();
    $paymentAnticipationHolder->refresh();
    $paymentAnticipationContractEffect->refresh();

    $contractEffectOrders = ContractEffectOrder::all();

    expect($this->contractEffectCancelled->deleted_at)->not->toBeNull();
    expect($contractEffectOrders->count())->toBe(5);
    expect($contractEffectOrders->sum('value'))->toBe(5000);
    expect($contractEffectOrders->where('value',0)->count())->toBe(0);

    expect($paymentAnticipationContractEffect->payment_value)->toBe(5000);
    expect($paymentAnticipationHolder->payment_value)->toBe(60025);
    expect($this->paymentContractEffect->payment_value)->toBe(0);
    expect($this->receivableUnit->total_constituted_value)->toBe(0);
    expect($this->paymentOriginalHolder->payment_value)->toBe(0);
})->with([[10000]])->group('contractEffect', 'contestation', 'cancel-total');

test ('Cancel Contract Effect With Anticipation Partial', function ($valueAnticipationPaymentContractEffect) {
    $cashOutDataRegister = (new CashOutPaymentsDataRegister())->addData([
        'external_cash_out_id' => 1,
        'external_cash_out_company_id' => 10,
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_id' => $this->paymentContractEffect->id,
        'payment_value' => $valueAnticipationPaymentContractEffect
    ]);

    app(CashOutPaymentAnticipationRegisterService::class)->store($cashOutDataRegister);

    $anticipationData = (new AnticipationDataRegister())->addData([
        'external_order_id' => $this->receivableUnitOrdersInitial->first()->external_order_id,
        'external_cash_out_id' => 10,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'value' => $this->receivableUnitOrdersInitial->first()->value,
        'request_date' => '2022-01-21'
    ]);

    app(AnticipationRegisterService::class)->store($anticipationData);

    $this->anticipation = Anticipation::where([
        'settlement_date' => $this->contractEffect->settlement_date,
        'code_arrangement_payment' => $this->contractEffect->code_arrangement_payment,
        'holder_document_number' => $this->contractEffect->final_recipient_document_number,
    ])->first();

    $this->paymentAnticipationHolder = $this->anticipation->payments->where(
        'holder_document_number', $this->receivableUnit->holder_document_number
    )->first();

    $this->paymentAnticipationContractEffect = $this->anticipation->payments->where(
        'holder_document_number', '<>', $this->receivableUnit->holder_document_number
    )->first();

    $this->paymentOriginalHolder->refresh();
    $this->paymentContractEffect->refresh();

    expect($this->paymentAnticipationContractEffect->payment_value)->toBe(2000);
    expect($this->paymentAnticipationHolder->payment_value)->toBe(11005);
    expect($this->paymentOriginalHolder->payment_value)->tobe(44020);
    expect($this->paymentContractEffect->payment_value)->tobe(8000);

    $contractEffectCancel = ContractEffect::factory()->create([
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'committed_value' => 0,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->contractEffectCancelled->final_recipient_document_number,
        'code_arrangement_payment' => $this->contractEffectCancelled->code_arrangement_payment,
        'settlement_date' => $this->contractEffectCancelled->settlement_date,
        'holder_document_number' => $this->contractEffectCancelled->holder_document_number,
        'holder_beneficiary_document_number' => $this->contractEffectCancelled->holder_beneficiary_document_number
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectCancel->effect_type,
        'has_committed_value' => false
    ])->apply($contractEffectCancel, $this->receivableUnit);

    $this->paymentOriginalHolder->refresh();
    $this->paymentContractEffect->refresh();
    $this->anticipation->refresh();
    $this->paymentAnticipationHolder->refresh();
    $this->paymentAnticipationContractEffect->refresh();
    $this->contractEffectCancelled->refresh();
    $this->contractEffect->refresh();

    $contractEffectOrders = ContractEffectOrder::all();

    expect($this->contractEffectCancelled->deleted_at)->not->toBeNull();
    expect($contractEffectOrders->count())->toBe(5);
    expect($contractEffectOrders->sum('value'))->toBe(5000);
    expect($contractEffectOrders->where('value', 0)->count())->toBe(0);

    expect($this->paymentAnticipationContractEffect->payment_value)->toBe(1000);
    expect($this->paymentAnticipationHolder->payment_value)->toBe(12005);

    expect($this->paymentOriginalHolder->payment_value)->toBe(48020);
    expect($this->paymentContractEffect->payment_value)->toBe(4000);

})->with([[2000]])->group('contractEffect', 'contestation', 'cancel-partial');

test('Get Contract Effect Orders Anticipated', function () {
    $receivableUnit = ReceivableUnit::factory()->create();

    $contractEffect = ContractEffect::factory()->create();

    $receivableUnitOrders = ReceivableUnitOrder::factory()->count(5)->create([
        'receivable_unit_id' => $receivableUnit
    ]);

    $receivableUnitOrders->each(function ($receivableUnitOrder) use ($contractEffect) {
        ContractEffectOrder::factory()->create([
            'external_order_id' => $receivableUnitOrder->external_order_id,
            'contract_effect_id' => $contractEffect->id,
            'value' => 1000,
        ]);
    });

    $anticipation = Anticipation::factory()->create();

    $receivableUnitOrders->first()->update([
        'anticipation_id' => $anticipation->id
    ]);

    $receivableUnitOrders->last()->update([
        'anticipation_id' => $anticipation->id
    ]);

    $contractEffectOrdersAnticipated = (new CancelAnticipationContractEffectOnusService())
        ->getContractEffectOrdersAnticipated($anticipation->id, $contractEffect->id);

    expect(2)->toBe($contractEffectOrdersAnticipated->count());
    expect(2000)->toBe($contractEffectOrdersAnticipated->sum('value'));
})->group('get-contract-effect-orders-anticipated');
