<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use Tests\Stub\ContractEffectWebhookMock;

test('Store from Webhook Contract Effect', function() {
    $data = ContractEffectWebhookMock::getWebhookData();

    $this->withHeaders([
        'Authorization' => config('cerc_ap008.webhook_token')
    ])->post(route('webhook.store'), [$data]);

    $this->assertTrue(true);
});
