<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\ContractEffects\DivisionRuleApplicators\DivisionRuleFixedValueApplicatorForOnusService;
use App\Models\Services\ContractEffects\DivisionRuleApplicators\DivisionRulePercentageValueApplicatorForOnusService;
use App\Models\Services\ContractEffects\ReceivableUnitContractEffectOnusService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\DivisionRuleApplicatorForOnusServiceContract;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\Math;

test('Bind Resolution Contract Effect Apply', function () {
    $contractEffectOnus = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectOnus->effect_type
    ]);

    expect(get_class($effectService))->toBe(ReceivableUnitContractEffectOnusService::class);

})->group('bind-resolution');

test('Bind Resolution Division Rule For Onus', function () {
    $divisionRuleApplicator = app(DivisionRuleApplicatorForOnusServiceContract::class, [
        'division_rule' => DivisionRule::FIXED_VALUE
    ]);

    expect(get_class($divisionRuleApplicator))->toBe(DivisionRuleFixedValueApplicatorForOnusService::class);

    $divisionRuleApplicator = app(DivisionRuleApplicatorForOnusServiceContract::class, [
        'division_rule' => DivisionRule::PERCENTAGE_VALUE
    ]);

    expect(get_class($divisionRuleApplicator))->toBe(DivisionRulePercentageValueApplicatorForOnusService::class);
});

test('Contract Effect Apply Onus Fixed Value|Court Block|Fiduciary', function () {
    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 10000
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ]);

    $receivableUnit = ReceivableUnit::factory()
        ->create([
            'holder_document_number' => $contractEffect->holder_document_number,
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'total_constituted_value' => 50000
        ]);

    $payment = Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->holder_document_number
    ]);

    $paymentOriginalValue = $payment->payment_value;

    $effectService->apply($contractEffect, $receivableUnit);

    $payment = $receivableUnit->payments->where('id', $payment->id)->first();
    $paymentApplied = $receivableUnit->payments->where('id', '<>', $payment->id)->first();

    expect($receivableUnit->payments->count())->toBe(2);
    expect($payment->payment_value)->not()->toBe($paymentOriginalValue);
    expect($paymentOriginalValue)->toBe($payment->payment_value + $paymentApplied->payment_value);
});

test('Contract Effect Apply Onus Percentage Value |Court Block| Fiduciary', function () {
    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 1000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ]);

    $receivableUnit = ReceivableUnit::factory()
        ->create([
            'holder_document_number' => $contractEffect->final_recipient_document_number,
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'total_constituted_value' => 10000
        ]);

    $payment = Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->holder_document_number,
    ]);

    $paymentOriginalValue = $payment->payment_value;

    $correctValue = (int) ceil($payment->payment_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER));

    $effectService->apply($contractEffect, $receivableUnit);

    $receivableUnit = $receivableUnit->fresh('payments');

    $receivableUnitPayments = $receivableUnit->payments;

    $payment = $receivableUnitPayments->where('id', $payment->id)->first();
    $paymentApplied = $receivableUnitPayments->where('id', '<>', $payment->id)->first();

    expect($receivableUnitPayments->count())->toBe(2);
    expect($paymentApplied->payment_value)->toBe($correctValue);
    expect($payment->payment_value)->toBe($paymentOriginalValue - $correctValue);
    expect($payment->payment_value)->not()->toBe($paymentOriginalValue);
    expect($payment->payment_value + $paymentApplied->payment_value)->toBe($paymentOriginalValue);

    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 10000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE
        ]);

    $receivableUnit = $receivableUnit->fresh('payments');

    $paymentOriginalValue = $payment->payment_value;

    $correctValue = (int) ceil($payment->payment_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER));

    $receivableUnit = $effectService->apply($contractEffect, $receivableUnit);

    $receivableUnit = $receivableUnit->fresh('payments');

    $receivableUnitPayments = $receivableUnit->payments;

    $payment = $receivableUnitPayments
        ->where('holder_document_number', '<>', $receivableUnit->holder_document_number)
        ->first();

    $paymentApplied = $receivableUnitPayments
        ->where('holder_document_number', $receivableUnit->holder_document_number)
        ->first();

    expect($receivableUnitPayments->count())->toBe(3);
    expect($payment->payment_value)->toBe($correctValue);
    expect($paymentApplied->payment_value)->toBe($payment->payment_value - $correctValue);
    expect($paymentApplied->payment_value)->not()->toBe($paymentOriginalValue);
    expect($payment->payment_value + $paymentApplied->payment_value)->toBe($paymentOriginalValue);

    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 20,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE
        ]);

    $receivableUnit = $effectService->apply($contractEffect, $receivableUnit);
    $receivableUnit->refresh();
    $paymentHolder = $receivableUnit->payments->firstWhere('holder_document_number', $receivableUnit->holder_document_number);
    $paymentNoHolder = $receivableUnit->payments->firstWhere('holder_document_number', $contractEffect->holder_beneficiary_document_number);

    expect($paymentHolder->payment_value)->toBe(0);
    expect($paymentNoHolder->payment_value)->toBe(0);

});

test('Contract Effect Apply Onus Fixed Value|Court Block|Fiduciary (Update Payment)', function () {
    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 10
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ]);

    $receivableUnit = ReceivableUnit::factory()
        ->create([
            'final_recipient_document_number' => $contractEffect->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffect->code_arrangement_payment,
            'settlement_date' => $contractEffect->settlement_date,
            'total_constituted_value' => 100
        ]);

    $payment = Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->final_recipient_document_number
    ]);

    $paymentOriginalValue = $payment->payment_value;

    $effectService->apply($contractEffect, $receivableUnit);

    $effectService->apply($contractEffect, $receivableUnit);

    $receivableUnit = $receivableUnit->fresh('payments');

    $receivableUnitPayments = $receivableUnit->payments;

    $payment = $receivableUnitPayments->where('id', $payment->id)->first();
    $paymentApplied = $receivableUnitPayments->where('id', '<>', $payment->id)->first();

    expect($receivableUnitPayments->count())->toBe(2);
    expect($payment->payment_value)->not()->toBe($paymentOriginalValue);
    expect(
        $payment->payment_value + $paymentApplied->payment_value
    )->toBe($paymentOriginalValue);

});
