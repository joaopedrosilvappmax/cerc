<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Anticipations\AnticipationRegisterService;
use App\Models\Services\CashOutPayment\CashOutPaymentAnticipationRegisterService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Support\DataTransferObjects\Anticipations\AnticipationDataRegister;
use App\Support\DataTransferObjects\CashOutPayments\CashOutPaymentsDataRegister;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
    ]);

    $this->totalConstitutedValueInitial = $this->receivableUnit->total_constituted_value;

    $this->receivableUnitOrders = ReceivableUnitOrder::factory()->count(5)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 2000,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ]);

    $this->paymentHolder = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test('Contract Effect Apply Holder Change And Onus With Anticipation', function () {
    $contractEffectHolderChange = ContractEffect::factory()->create([
        'id' => 1,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'committed_value' => 2000,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
    ]);

    $contractEffectOnus = ContractEffect::factory()->create([
        'id' => 2,
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'committed_value' => 1000,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'holder_beneficiary_document_number' => $contractEffectHolderChange->holder_beneficiary_document_number,
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectHolderChange->effect_type
    ])->apply($contractEffectHolderChange, $this->receivableUnit);

    $this->paymentHolder->refresh();
    $this->receivableUnit->refresh();

    $receivableUnitHolderChange = ReceivableUnit::where('receivable_unit_linked_id', $this->receivableUnit->id)->first();
    $paymentHolderChange = $receivableUnitHolderChange->payments->first();

    expect(2000)->toBe($receivableUnitHolderChange->total_constituted_value);
    expect(2000)->toBe($paymentHolderChange->payment_value);
    expect(8000)->toBe($this->receivableUnit->total_constituted_value);
    expect(8000)->toBe($this->paymentHolder->payment_value);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectOnus->effect_type
    ])->apply($contractEffectOnus, $this->receivableUnit);

    $this->paymentHolder->refresh();
    $this->receivableUnit->refresh();
    $receivableUnitHolderChange->refresh();
    $paymentHolderChange->refresh();
    $paymentContractEffectOnus = $this->receivableUnit->payments
        ->where('holder_document_number', '!=', $this->receivableUnit->holder_document_number)
        ->first();

    expect(2000)->toBe($receivableUnitHolderChange->total_constituted_value);
    expect(2000)->toBe($paymentHolderChange->payment_value);
    expect(8000)->toBe($this->receivableUnit->total_constituted_value);
    expect(7000)->toBe($this->paymentHolder->payment_value);
    expect(1000)->toBe($paymentContractEffectOnus->payment_value);

    $contractEffectOrdersOnus = ContractEffectOrder::where('contract_effect_id', $contractEffectOnus->id)->get();

    $contractEffectOrdersHolderChange = ContractEffectOrder::where('contract_effect_id', $contractEffectHolderChange->id)->get();

    $cercCashOutRequestId = 1;
    $cashOutCompanyId = 10;

    app(CashOutPaymentAnticipationRegisterService::class)->store(
        (new CashOutPaymentsDataRegister())->addData([
            'external_cash_out_id' => $cercCashOutRequestId,
            'external_cash_out_company_id' => $cashOutCompanyId,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $paymentContractEffectOnus->id,
            'payment_value' => $contractEffectOrdersOnus->first()->value
        ])
    );

    $receivableUnitOrderAnticipated = ReceivableUnitOrder::where('external_order_id', $contractEffectOrdersOnus->first()->external_order_id)->first();

    app(AnticipationRegisterService::class)->store(
        (new AnticipationDataRegister())->addData([
            'external_order_id' => $receivableUnitOrderAnticipated->external_order_id,
            'external_cash_out_id' => 10,
            'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'value' => $receivableUnitOrderAnticipated->value,
            'request_date' => '2022-01-21'
        ])
    );

    $anticipation = Anticipation::where([
        'settlement_date' => $contractEffectOnus->settlement_date,
        'code_arrangement_payment' => $contractEffectOnus->code_arrangement_payment,
        'holder_document_number' => $contractEffectOnus->final_recipient_document_number,
    ])->first();

    $paymentHolderAnticipation = Payment::where([
        'anticipation_id' => $anticipation->id,
        'holder_document_number' => $anticipation->holder_document_number
    ])->first();

    $paymentOnusAnticipation = Payment::where('anticipation_id', $anticipation->id)
        ->where('holder_document_number', '!=', $anticipation->holder_document_number)
        ->first();

    $this->paymentHolder->refresh();
    $this->receivableUnit->refresh();
    $receivableUnitHolderChange->refresh();
    $paymentHolderChange->refresh();
    $paymentContractEffectOnus->refresh();

    expect(1600)->toBe($anticipation->total_constituted_value);
    expect(1400)->toBe($paymentHolderAnticipation->payment_value);
    expect(200)->toBe($paymentOnusAnticipation->payment_value);

    expect(2000)->toBe($paymentHolderChange->payment_value);
    expect(800)->toBe($paymentContractEffectOnus->payment_value);
    expect(5600)->toBe($this->paymentHolder->payment_value);
    expect(6400)->toBe($this->receivableUnit->total_constituted_value);

    expect(8000)->toBe(
        $this->paymentHolder->payment_value
        + $paymentContractEffectOnus->payment_value
        + $anticipation->total_constituted_value
    );

    expect(10000)->toBe(
        $this->paymentHolder->payment_value
        + $paymentContractEffectOnus->payment_value
        + $anticipation->total_constituted_value
        + $paymentHolderChange->payment_value
    );

})->group('apply-contract-effect');
