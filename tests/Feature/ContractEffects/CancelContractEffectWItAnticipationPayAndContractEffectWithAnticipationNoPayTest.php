<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Anticipations\AnticipationRegisterService;
use App\Models\Services\CashOutPayment\CashOutPaymentAnticipationRegisterService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Support\DataTransferObjects\Anticipations\AnticipationDataRegister;
use App\Support\DataTransferObjects\CashOutPayments\CashOutPaymentsDataRegister;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
    ]);

    $this->totalConstitutedValueInitial = $this->receivableUnit->total_constituted_value;

    $this->receivableUnitOrders = ReceivableUnitOrder::factory()->count(5)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 2000,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test ('Cancel Contract Effect With Anticipation Pay And Anticipation No Pay', function () {
    $contractEffectOne = ContractEffect::factory()->create([
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'committed_value' => 1000,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
    ]);

    $contractEffectTwo = ContractEffect::factory()->create([
        'id' => 2,
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'committed_value' => 2000,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'holder_beneficiary_document_number' => $contractEffectOne->holder_beneficiary_document_number,
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectOne->effect_type
    ])->apply($contractEffectOne, $this->receivableUnit);

    $this->payment->refresh();
    $this->receivableUnit->refresh();
    $paymentContractEffectOne = Payment::where('holder_document_number', $contractEffectOne->holder_beneficiary_document_number)->first();

    $contractEffectOrderAnticipated = $this->receivableUnitOrders
        ->first()
        ->contractEffectOrders()
        ->where('contract_effect_id', $contractEffectOne->id)
        ->first();

    $cercCashOutRequestId = 1;
    $cashOutCompanyId = 10;

    app(CashOutPaymentAnticipationRegisterService::class)->store(
        (new CashOutPaymentsDataRegister())->addData([
            'external_cash_out_id' => $cercCashOutRequestId,
            'external_cash_out_company_id' => $cashOutCompanyId,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $paymentContractEffectOne->id,
            'payment_value' => $contractEffectOrderAnticipated->value
        ])
    );

    app(AnticipationRegisterService::class)->store(
        (new AnticipationDataRegister())->addData([
            'external_order_id' => $this->receivableUnitOrders->first()->external_order_id,
            'external_cash_out_id' => 10,
            'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'value' => $this->receivableUnitOrders->first()->value,
            'request_date' => '2022-01-21'
        ])
    );

    $anticipationOne = Anticipation::where([
        'settlement_date' => $contractEffectOne->settlement_date,
        'code_arrangement_payment' => $contractEffectOne->code_arrangement_payment,
        'holder_document_number' => $contractEffectOne->final_recipient_document_number,
    ])->first();

    $contractEffectOneCanceled = ContractEffect::factory()->create([
        'effect_type' => $contractEffectOne->effect_type,
        'committed_value' => 0,
        'division_rule' => $contractEffectOne->division_rule,
        'final_recipient_document_number' => $contractEffectOne->final_recipient_document_number,
        'code_arrangement_payment' => $contractEffectOne->code_arrangement_payment,
        'settlement_date' => $contractEffectOne->settlement_date,
        'holder_document_number' => $contractEffectOne->holder_document_number,
        'holder_beneficiary_document_number' => $contractEffectOne->holder_beneficiary_document_number
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectOneCanceled->effect_type,
        'has_committed_value' => false
    ])->apply($contractEffectOneCanceled, $this->receivableUnit);

    $contractEffectOne->refresh();
    $anticipationOne->refresh();
    $paymentHolderAnticipationOne = Payment::where('external_cash_out_company_id', $cashOutCompanyId)->first();

    $paymentContractAnticipationOne = Payment::where('external_cash_out_id', $cercCashOutRequestId)->first();

    $contractEffectOrderAnticipated->refresh();
    $contractEffectOrdersOne = ContractEffectOrder::where('contract_effect_id', $contractEffectOne->id)->get();

    expect($contractEffectOne->deleted_at)->not()->toBeNull();
    expect($contractEffectOrderAnticipated->deleted_at)->not()->toBeNull();
    expect(0)->toBe($contractEffectOrdersOne->sum('value'));
    expect(2000)->toBe($paymentHolderAnticipationOne->payment_value);
    expect(0)->toBe($paymentContractAnticipationOne->payment_value);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectTwo->effect_type
    ])->apply($contractEffectTwo, $this->receivableUnit->refresh());

    $this->payment->refresh();
    $this->receivableUnit->refresh();
    $paymentContractEffectTwo = Payment::where(
        'holder_document_number', $contractEffectTwo->holder_beneficiary_document_number
    )->first();

    $cercCashOutRequestIdTwo = 2;
    $cashOutCompanyIdTwo = 11;

    app(CashOutPaymentAnticipationRegisterService::class)->store(
        (new CashOutPaymentsDataRegister())->addData([
            'external_cash_out_id' => $cercCashOutRequestIdTwo,
            'external_cash_out_company_id' => $cashOutCompanyIdTwo,
            'receivable_unit_id' => $this->receivableUnit->id,
            'payment_id' => $paymentContractEffectTwo->id,
            'payment_value' => $paymentContractEffectTwo->payment_value
        ])
    );

    $this->receivableUnitOrders = ReceivableUnitOrder::all();

    $this->receivableUnitOrders->whereNull('anticipation_id')->each(function ($receivableUnitOrder) use ($cashOutCompanyIdTwo) {
        app(AnticipationRegisterService::class)->store(
            (new AnticipationDataRegister())->addData([
                'external_order_id' => $receivableUnitOrder->external_order_id,
                'external_cash_out_id' => $cashOutCompanyIdTwo,
                'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
                'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
                'value' => $receivableUnitOrder->value,
                'request_date' => '2022-01-21'
            ])
        );
    });

    $contractEffectTwoCanceled = ContractEffect::factory()->create([
        'effect_type' => $contractEffectTwo->effect_type,
        'committed_value' => 0,
        'division_rule' => $contractEffectTwo->division_rule,
        'final_recipient_document_number' => $contractEffectTwo->final_recipient_document_number,
        'code_arrangement_payment' => $contractEffectTwo->code_arrangement_payment,
        'settlement_date' => $contractEffectTwo->settlement_date,
        'holder_document_number' => $contractEffectTwo->holder_document_number,
        'holder_beneficiary_document_number' => $contractEffectTwo->holder_beneficiary_document_number
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectTwoCanceled->effect_type,
        'has_committed_value' => false
    ])->apply($contractEffectTwoCanceled, $this->receivableUnit);

    $contractEffectOne->refresh();
    $anticipationOne->refresh();
    $paymentHolderAnticipationOne->refresh();
    $paymentContractAnticipationOne->refresh();
    $contractEffectOrderAnticipated->refresh();

    $contractEffectTwo->refresh();
    $anticipationTwo = Anticipation::where('external_cash_out_id', $cashOutCompanyIdTwo)->first();
    $paymentHolderAnticipationTwo = Payment::where('external_cash_out_company_id', $cashOutCompanyIdTwo)->first();
    $paymentContractAnticipationTwo = Payment::where('external_cash_out_id', $cercCashOutRequestIdTwo)->first();
    $contractEffectOrdersTwo = ContractEffectOrder::where('contract_effect_id', $contractEffectTwo->id)->get();

    expect($contractEffectTwo->deleted_at)->not()->toBeNull();

    expect(8000)->toBe($anticipationTwo->total_constituted_value);
    expect(0)->toBe($contractEffectOrdersTwo->sum('value'));
    expect(0)->toBe($paymentContractAnticipationTwo->payment_value);
    expect(8000)->toBe($paymentHolderAnticipationTwo->payment_value);

})->group('flow-cancel-contract-effect');
