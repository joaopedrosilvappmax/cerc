<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\ContractEffects\Cancel\CancelAnticipationContractEffectOnusService;
use App\Models\Services\ContractEffects\Cancel\CancelContractEffectService;
use App\Models\Services\Contracts\CancelAnticipationContractEffectServiceContract;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
    ]);

    $this->totalConstitutedValueInitial = $this->receivableUnit->total_constituted_value;

    ReceivableUnitOrder::factory()->count(5)->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'value' => 2000,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test ('Bind Resolution Return Values Of Contract Effect', function () {
    $contractEffectOnus = ContractEffect::factory()->create([
        'effect_type' => ContractEffectType::ONUS_OTHERS
    ]);

    $contractEffectHolderChange = ContractEffect::factory()->create([
        'effect_type' => ContractEffectType::HOLDER_CHANGE
    ]);

    $returnValueContractEffectOnus = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectOnus->effect_type,
        'has_committed_value' => false
    ]);

    $returnValueContractEffectHolderChange = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectHolderChange->effect_type,
        'has_committed_value' => false
    ]);

    $cancelAnticipationContractEffectOnus = app(CancelAnticipationContractEffectServiceContract::class);

    expect(CancelContractEffectService::class)->toBe(get_class($returnValueContractEffectOnus));
    expect(CancelContractEffectService::class)->toBe(get_class($returnValueContractEffectHolderChange));

    expect(CancelAnticipationContractEffectOnusService::class)
        ->toBe(get_class($cancelAnticipationContractEffectOnus));
})->group('bind', 'contractEffect', 'contestation');

test ('Cancel Contract Effect Type Onus', function () {
    $contractEffectFixed = ContractEffect::factory()->create([
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'committed_value' => 6000,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
    ]);

    $contractEffectPercentage = ContractEffect::factory()->create([
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'committed_value' => 5000,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'holder_beneficiary_document_number' => $contractEffectFixed->holder_beneficiary_document_number
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectFixed->effect_type
    ])->apply($contractEffectFixed, $this->receivableUnit);

    $this->payment->refresh();
    $this->receivableUnit->refresh();

    expect($contractEffectFixed->committed_value)
        ->toBe($this->receivableUnit->contractEffectOrders->sum('value'));
    expect($this->totalConstitutedValueInitial - $contractEffectFixed->committed_value)
        ->toBe($this->payment->payment_value);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectPercentage->effect_type
    ])->apply($contractEffectPercentage, $this->receivableUnit);

    $contractEffectCancel = ContractEffect::factory()->create([
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'committed_value' => 0,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'final_recipient_document_number' => $contractEffectPercentage->final_recipient_document_number,
        'code_arrangement_payment' => $contractEffectPercentage->code_arrangement_payment,
        'settlement_date' => $contractEffectPercentage->settlement_date,
        'holder_document_number' => $contractEffectPercentage->holder_document_number,
        'holder_beneficiary_document_number' => $contractEffectFixed->holder_beneficiary_document_number
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectCancel->effect_type,
        'has_committed_value' => false
    ])->apply($contractEffectCancel, $this->receivableUnit);

    $contractEffectPercentage->refresh();
    $this->receivableUnit->refresh();
    $this->payment->refresh();

    $paymentContractEffectCanceled = $this->receivableUnit->payments()->where([
        'holder_document_number' => $contractEffectPercentage->holder_beneficiary_document_number
    ])->first();

    expect($this->totalConstitutedValueInitial)->toBe($this->receivableUnit->total_constituted_value);
    expect(2)->toBe($this->receivableUnit->payments->count());
    expect(6000)->toBe($paymentContractEffectCanceled->payment_value);
    expect($contractEffectPercentage->deleted_at)->not->toBeNull();
})->group('contractEffect', 'onus', 'contestation');

test ('Cancel Contract Effect Type Holder Change', function () {
    $contractEffectFixed = ContractEffect::factory()->create([
        'indicator' => 1,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'committed_value' => 6000,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
    ]);

    $receivableUnitNewHolderOne = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectFixed->effect_type
    ])->apply($contractEffectFixed, $this->receivableUnit);

    $this->receivableUnit->refresh();

    expect($this->totalConstitutedValueInitial - $contractEffectFixed->committed_value)
        ->toBe($this->receivableUnit->total_constituted_value);

    $contractEffectPercentage = ContractEffect::factory()->create([
        'indicator' => 2,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'committed_value' => 5000,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);

    $receivableUnitNewHolderTwo = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectPercentage->effect_type
    ])->apply($contractEffectPercentage, $this->receivableUnit);

    $this->receivableUnit->refresh();

    $contractEffectCancel = ContractEffect::factory()->create([
        'indicator' => 3,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'committed_value' => 0,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
        'holder_beneficiary_document_number' => $contractEffectPercentage->holder_beneficiary_document_number
    ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectCancel->effect_type,
        'has_committed_value' => false
    ])->apply($contractEffectCancel, $this->receivableUnit);

    $receivableUnitNewHolderTwo->refresh();
    $contractEffectPercentage->refresh();
    $contractEffectFixed->refresh();
    $this->receivableUnit->refresh();

    expect(1)->toBe($this->receivableUnit->payments->count());
    expect(0)->toBe($receivableUnitNewHolderTwo->total_constituted_value);
    expect(0)->toBe($receivableUnitNewHolderTwo->payments->first()->payment_value);
    expect($contractEffectFixed->committed_value)->toBe($receivableUnitNewHolderOne->total_constituted_value);
    expect($contractEffectFixed->committed_value)
        ->toBe(
            $receivableUnitNewHolderOne->payments()
                ->where('holder_document_number', $contractEffectFixed->holder_beneficiary_document_number)
                ->first()->payment_value
        );
    expect($contractEffectFixed->deleted_at)->toBeNull();
    expect($contractEffectPercentage->deleted_at)->not->toBeNull();
    expect($contractEffectFixed->committed_value)->toBe($receivableUnitNewHolderOne->contractEffectOrders->sum('value'));
})->group('contractEffect', 'holderChange', 'contestation');
