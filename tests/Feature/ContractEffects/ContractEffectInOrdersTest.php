<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\ContractEffects\ReceivableUnitContractEffectHolderChangeService;
use App\Models\Services\ContractEffects\ReceivableUnitContractEffectOnusService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\ReceivableUnits\ReceivableUnitRegisterService;
use App\Support\DataTransferObjects\ReceivableUnits\ReceivableUnitDataRegister;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use Tests\Stub\ReceivableUnitDataRegisterMock;

test('Get Orders And Values From Contract Effect Onus', function () {
    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 30000
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1000,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->count(3)->create();

    $contractEffect = ContractEffect::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 5000
    ])->create();

    $payment = Payment::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->final_recipient_document_number,
        'payment_value' => $receivableUnit->total_constituted_value
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $payment->refresh();

    $orders = app(ReceivableUnitContractEffectOnusService::class)
        ->getOrdersWithValuesCalculatedFromContractEffectApplied($receivableUnit, $payment->payment_value);

    $amountOrders = array_sum(array_column($orders, 'value'));

    expect($payment->payment_value)->toBe($amountOrders);
});

test('Apply Contract Effect Verify Sum Orders', function () {
    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 2198,
    ])->create();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 4000,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    $payment = Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1099,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->count(2)->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $payment = $payment->fresh();

    $orders = app(ReceivableUnitContractEffectOnusService::class)
                ->getOrdersWithValuesCalculatedFromContractEffectApplied($receivableUnit, $payment->payment_value);

    $amountOrders = array_sum(array_column($orders, 'value'));

    expect($payment->payment_value)->toBe($amountOrders);
});

test('Get Contract Effect With Decimal', function () {
    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 1698,
    ])->create();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 4000,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    $payment = Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 599,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1099,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->create();

    $receivableUnitUpdated = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $payment = $receivableUnitUpdated->payments()
        ->where('holder_document_number', '!=', $receivableUnit->holder_document_number)
        ->first();

    $contractEffectOrders = app(ContractEffectOrder::class)->all();

    app(ReceivableUnitContractEffectOnusService::class)
        ->getOrdersWithValuesCalculatedFromContractEffectApplied($receivableUnit, $payment->payment_value);

    $contractEffectOrders->sum('value');

    expect($payment->payment_value)->toBe($contractEffectOrders->sum('value'));
});

test('Store Contract Effect Order', function () {
    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 1698,
    ])->create();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 5000,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 599,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1099,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $contractEffectOrders = app(ContractEffectOrder::class)->all();

    $contractEffectOrders->sum('value');

    expect($receivableUnit->total_constituted_value/2)->toBe($contractEffectOrders->sum('value'));
});

test ('Apply More Tha One Contract Effect', function () {
    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 10000,
    ])->create();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 5000,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    $payment = Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1000,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->count(9)->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 999,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $contractEffectOrders = ContractEffectOrder::all();

    $sumOrders = $contractEffectOrders->sum('value');

    expect($receivableUnit->total_constituted_value/2)->toBe($sumOrders);

    $payment = $payment->fresh();

    expect($receivableUnit->total_constituted_value/2)->toBe( $payment->payment_value);

    $newContractEffect = ContractEffect::factory([
        'id' => 10,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 5000,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($newContractEffect, $receivableUnit);

    $contractEffectOrders = ContractEffectOrder::all();

    $payment = $payment->fresh();

    $contractEffectOrders->sum('value');

    expect($receivableUnit->total_constituted_value)->toBe($payment->payment_value + $contractEffectOrders->sum('value'));
});

test('Apply Contract Effect With Division Rule Fix', function () {
   $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 3333,
    ])->create();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 1500,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    $payment = Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1111,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->count(3)->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $contractEffectOrders = ContractEffectOrder::all();

    $payment = $payment->fresh();

    expect($contractEffect->committed_value)->toBe($contractEffectOrders->sum('value'));
    expect($receivableUnit->total_constituted_value)
        ->toBe($payment->payment_value + $contractEffectOrders->sum('value'));

});

test('Apply Contract Effect With Division Rule Fix And Refund', function ()
{
    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 3000,
    ])->create();

    $contractEffectFirst = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 1500,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    $contractEffectSecond = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 500,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    $receivableUnitOrder = ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 3000,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectFirst->effect_type
    ])->apply($contractEffectFirst, $receivableUnit);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectSecond->effect_type
    ])->apply($contractEffectSecond, $receivableUnit);

    $receivableUnitData = ReceivableUnitDataRegisterMock::getInitialData();

    $receivableUnitDataRegister = (new ReceivableUnitDataRegister())
        ->addData($receivableUnitData);

    $receivableUnitDataRegister->external_id = $receivableUnitOrder->external_order_id;
    $receivableUnitDataRegister->total_constituted_value = 0;
    $receivableUnitDataRegister->reversal_value = 3000;

    $contractEffectOrders = ContractEffectOrder::all();

    expect($contractEffectOrders->count())->toBe(2);

    (new ReceivableUnitRegisterService())
        ->store($receivableUnitDataRegister);

    $contractEffectOrders = ContractEffectOrder::all();

    expect($contractEffectOrders->count())->toBe(2);
});

test('Apply Contract Effect Type Holder Change And Verify Sum Orders', function () {
    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 40116,
    ])->create();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 5000,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    $payment = Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 20058,
        'reversal_value' => 0,
        'guarantee_value' => 0,
    ])->count(2)->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $payment = $payment->fresh();

    $orders = app(ReceivableUnitContractEffectHolderChangeService::class)
        ->getOrdersWithValuesCalculatedFromContractEffectApplied($receivableUnit, $payment->payment_value);

    $amountOrders = array_sum(array_column($orders, 'value'));

    expect($payment->payment_value)->toBe($amountOrders);
});

test('Verify Values When Effect Contract Type Holder Change', function () {
    $receivableUnitContractEffectHolderChangeService = new ReceivableUnitContractEffectHolderChangeService();

    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 40116,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 15000,
        'reversal_value' => 0,
    ])->count(2)->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 10116,
        'reversal_value' => 0
    ])->create();

    Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' =>  $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 5000,
        'settlement_date' => $receivableUnit->settlement_date,
        'holder_beneficiary_document_number' => 74689300000166
    ])->create();

    $receivableUnitContractEffectHolderChangeService->apply($contractEffect, $receivableUnit);

    $receivableUnit = $receivableUnit->fresh();
    $receivableUnitNewHolder = ReceivableUnit::where(
        'holder_document_number', '=', $contractEffect->holder_beneficiary_document_number
    )->first();
    $contractEffectOrders  = ContractEffectOrder::all();

    expect($receivableUnit->payments->count())->toBe(1);
    expect($receivableUnitNewHolder->payments->count())->toBe(1);
    expect($contractEffectOrders->count())->toBe(3);
    expect($receivableUnitNewHolder->total_constituted_value)->toBe($contractEffectOrders->sum('value'));
    expect($receivableUnitNewHolder->payments->first()->payment_value)->toBe($contractEffectOrders->sum('value'));

});

test('Test Orders with effect contract of onus and holder change', function () {
    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => 30000,
    ])->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 10000,
        'reversal_value' => 0,
    ])->count(2)->create();

    ReceivableUnitOrder::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 10000,
        'reversal_value' => 0
    ])->create();

    Payment::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => $receivableUnit->total_constituted_value,
        'receivable_unit_id' => $receivableUnit->id,
    ])->create();

    $contractEffectChangeHolder = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 5000,
        'settlement_date' => $receivableUnit->settlement_date,
        'holder_beneficiary_document_number' => 74689300000166
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectChangeHolder->effect_type
    ])->apply($contractEffectChangeHolder, $receivableUnit);

    $contractEffectOnus = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::PERCENTAGE_VALUE,
        'committed_value' => 5000,
        'settlement_date' => $receivableUnit->settlement_date,
        'holder_beneficiary_document_number' => 89043258000189
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectOnus->effect_type
    ])->apply($contractEffectOnus, $receivableUnit);

    $contractEffectHolderChangeTwo = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::HOLDER_CHANGE,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 500,
        'settlement_date' => $receivableUnit->settlement_date,
        'holder_beneficiary_document_number' => 68539584000101
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectHolderChangeTwo->effect_type
    ])->apply($contractEffectHolderChangeTwo, $receivableUnit);

    $receivableUnit = $receivableUnit->fresh();

    $receivableUnitNewHolderEffectContractOne = ReceivableUnit::where(
        'holder_document_number', '=', $contractEffectChangeHolder->holder_beneficiary_document_number
    )->first();

    $receivableUnitNewHolderEffectContractTwo = ReceivableUnit::where(
        'holder_document_number', '=', $contractEffectHolderChangeTwo->holder_beneficiary_document_number
    )->first();

    $originalPaymentUr = Payment::where(
        'holder_document_number', '=', $receivableUnit->holder_document_number
    )->first();

    $paymentEffectContractOnus = Payment::where(
        'holder_document_number', '=', $contractEffectOnus->holder_beneficiary_document_number
    )->first();

    $contractEffectOrders = ContractEffectOrder::all();

    expect(2)->toBe($receivableUnit->payments->count());
    expect(1)->toBe($receivableUnitNewHolderEffectContractOne->payments->count());
    expect(1)->toBe($receivableUnitNewHolderEffectContractTwo->payments->count());
    expect(9)->toBe($contractEffectOrders->count());

    expect(7000)->toBe($originalPaymentUr->payment_value);
    expect(7500)->toBe($paymentEffectContractOnus->payment_value);
    expect(15000)->toBe($receivableUnitNewHolderEffectContractOne->payments->first()->payment_value);
    expect(500)->toBe($receivableUnitNewHolderEffectContractTwo->payments->first()->payment_value);

    expect(15000)->toBe(
        $contractEffectOrders->filter(function($order) use ($receivableUnitNewHolderEffectContractOne) {
            return $order->receivable_unit_id == $receivableUnitNewHolderEffectContractOne->id;
        })->sum('value')
    );

    expect($contractEffectHolderChangeTwo->committed_value)->toBe(
        $contractEffectOrders->filter(function($order) use ($receivableUnitNewHolderEffectContractTwo) {
            return $order->receivable_unit_id == $receivableUnitNewHolderEffectContractTwo->id;
        })->sum('value')
    );

    expect(7500)->toBe(
        $contractEffectOrders->filter(function($order) use ($receivableUnit) {
            return $order->receivable_unit_id == $receivableUnit->id;
        })->sum('value')
    );

});
