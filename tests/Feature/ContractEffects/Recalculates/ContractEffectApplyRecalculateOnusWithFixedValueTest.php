<?php

namespace Tests\ContractEffects\Recalculates\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\Math;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()
        ->create([
            'total_constituted_value' => 100000
        ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()
        ->count(5)
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'value' => 20000,
            'reversal_value' => 0,
            'guarantee_value' => 0,
        ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test('Recalculate Contract Effect With Fixed Value After Percentage Value', function () {

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 5000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,

        ]);

    $valuePaymentEffectContract = (int) ceil($this->payment->payment_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER));

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $this->receivableUnit->refresh();

    $receivableUnitPayments = $this->receivableUnit->payments;

    $paymentApplied = $receivableUnitPayments->where('id', '<>', $this->payment->id)->first();

    $contractEffectOrders = ContractEffectOrder::all();

    expect($this->receivableUnit->payments->count())->toBe(2);
    expect($valuePaymentEffectContract)->toBe($paymentApplied->payment_value);
    expect($contractEffectOrders->count())->toBe(5);
    expect($paymentApplied->payment_value)->toBe($contractEffectOrders->sum('value'));

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 20000,
            'division_rule' => DivisionRule::FIXED_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'holder_beneficiary_document_number' => 60573798000192,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $contractEffectOrders = ContractEffectOrder::all();
    $payments = Payment::all();

    expect($contractEffectOrders->count())->toBe($this->receivableUnitOrdersInitial->count() * 2);
    expect($payments->count())->toBe(3);
    expect($this->receivableUnit->total_constituted_value)->toBe($payments->sum('payment_value'));

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 1000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'holder_beneficiary_document_number' => 60573798000164,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $valueNewOrder = 51099;

    app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ])->store(app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ])->addData([
        'external_id' => 1212121,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => $valueNewOrder,
        'blocked_value' => 0,
        'reversal_value' => 0,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => $valueNewOrder,
        'guarantee_value' => 0,
    ]));

    $this->receivableUnit->refresh();

    $contractEffectOrders = ContractEffectOrder::all();
    $payments = $this->receivableUnit->payments;

    expect($contractEffectOrders->count())->toBe(18);
    expect($this->receivableUnit->total_constituted_value)->toBe(100000 + $valueNewOrder);
    expect($this->receivableUnit->total_constituted_value)->toBe($payments->sum('payment_value'));

})->group('contractEffects', 'recalculateContractEffects');
