<?php

namespace Tests\ContractEffects\Recalculates\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\Math;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()
        ->create([
            'total_constituted_value' => 10000
        ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()
        ->count(5)
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'value' => 2000,
            'reversal_value' => 0,
            'guarantee_value' => 0,
        ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test('Recalculate Contract Effect With Percentage Value', function () {

    $paymentOriginalValue = $this->payment->payment_value;

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 2000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,

        ]);

    $valuePaymentEffectContract = (int) ceil(
        $this->payment->payment_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER)
    );

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $this->payment->refresh();
    $this->receivableUnit->refresh();

    $paymentApplied = $this->receivableUnit->payments->where('id', '<>', $this->payment->id)->first();

    $contractEffectOrders = ContractEffectOrder::all();

    expect($this->receivableUnit->payments->count())->toBe(2);
    expect($valuePaymentEffectContract)->toBe($paymentApplied->payment_value);
    expect($this->payment->payment_value)->toBe($paymentOriginalValue - $valuePaymentEffectContract);
    expect($contractEffectOrders->count())->toBe(5);
    expect($paymentApplied->payment_value)->toBe($contractEffectOrders->sum('value'));

    $valueNewOrder = 5000;
    app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ])->store(app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ])->addData([
        'external_id' => 1212121,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => $valueNewOrder,
        'blocked_value' => 0,
        'reversal_value' => 0,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => $valueNewOrder,
        'guarantee_value' => 0,
    ]));

    $contractEffectOrders = ContractEffectOrder::all();

    $this->payment->refresh();
    $paymentApplied = $paymentApplied->fresh();

    $totalConstitutedValueOld = $this->receivableUnit->total_constituted_value;
    $this->receivableUnit->refresh();

    expect($contractEffectOrders->count())->toBe($this->receivableUnitOrdersInitial->count() + 1);
    expect($this->receivableUnit->total_constituted_value)->toBe($totalConstitutedValueOld + $valueNewOrder);
    expect($paymentApplied->payment_value)->toBe($contractEffectOrders->sum('value'));
    expect($this->receivableUnit->total_constituted_value)->toBe($paymentApplied->payment_value + $this->payment->payment_value);

})->group('contractEffects', 'recalculateContractEffects');
