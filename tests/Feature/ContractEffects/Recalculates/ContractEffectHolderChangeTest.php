<?php

namespace Tests\ContractsEffects\Recalculates\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\Math;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()
        ->create([
            'total_constituted_value' => 10000
        ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()
        ->count(5)
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'value' => 2000,
            'reversal_value' => 0,
            'guarantee_value' => 0,
        ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test('Recalculate Contract Effect Holder Change', function () {

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 2000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    $valuePaymentEffectContract = (int) ceil(
        $this->payment->payment_value * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER)
    );

    $receivableUnitNewHolder = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $this->receivableUnit->refresh();

    $contractEffectOrders = ContractEffectOrder::all();

    expect($this->receivableUnit->payments()->count())->toBe(1);
    expect($receivableUnitNewHolder->payments()->count())->toBe(1);
    expect($contractEffectOrders->count())->toBe(5);
    expect($this->receivableUnit->total_constituted_value)
        ->toBe($this->receivableUnit->payments->first()->payment_value);
    expect($valuePaymentEffectContract)
        ->toBe($receivableUnitNewHolder->payments->first()->payment_value);
    expect($this->payment->payment_value - $valuePaymentEffectContract)
        ->toBe($this->receivableUnit->payments->first()->payment_value);
    expect($valuePaymentEffectContract)
        ->toBe($contractEffectOrders->sum('value'));

    $referenceOrderId = $this->receivableUnitOrdersInitial->first()->external_order_id;

    $contractEffectOrdersTotalValue = ContractEffectOrder::where('external_order_id', $referenceOrderId)
        ->sum('value');

    $reversalValue = $this->receivableUnitOrdersInitial->first()->value;
    app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ])->store(app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ])->addData([
            'external_id' => $referenceOrderId,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'holder_document_number' => $this->receivableUnit->holder_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'pre_contracted_value' => 0,
            'total_gross_value' => 0,
            'blocked_value' => 0,
            'reversal_value' => $reversalValue,
            'settlement_date' => $this->receivableUnit->settlement_date,
            'paid_at' => null,
            'value' => 0,
            'guarantee_value' => 0,
    ]));

    $contractEffectOrders = ContractEffectOrder::all();

    $expectedValue = $this->receivableUnit->total_constituted_value - ($reversalValue - $contractEffectOrdersTotalValue);
    $expectedValueNewHolder = $receivableUnitNewHolder->total_constituted_value - $contractEffectOrdersTotalValue;

    $this->receivableUnit->refresh();
    $receivableUnitNewHolder = $receivableUnitNewHolder->fresh('payments');

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::all();
    $expectedValuePaymentHolder = $this->receivableUnitOrdersInitial->sum('value');

    expect($this->receivableUnitOrdersInitial->count())
        ->toBe($contractEffectOrders->count());
    expect($this->receivableUnit->total_constituted_value)
        ->toBe($expectedValue);
    expect($receivableUnitNewHolder->total_constituted_value)
        ->toBe($expectedValueNewHolder);
    expect($expectedValuePaymentHolder)
        ->toBe(
            $this->receivableUnit->payments->sum('payment_value')
            + $receivableUnitNewHolder->payments()->sum('payment_value')
        );

});
