<?php

namespace Tests\ContractEffects\Recalculates\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\Math;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()
        ->create([
            'total_constituted_value' => 10000
        ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()
        ->count(5)
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'value' => 2000,
            'reversal_value' => 0,
            'guarantee_value' => 0,
        ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test('Recalculate Contract Effect When Refuse Withdraw', function () {

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 5000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,

        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $this->receivableUnit->refresh();

    $contractEffectOrders = ContractEffectOrder::all();

    $paymentApplied = $this->receivableUnit->payments->where('id', '<>', $this->payment->id)->first();

    $this->assertEquals(2, $this->receivableUnit->payments->count());
    $this->assertEquals(5, $contractEffectOrders->count());
    $this->assertEquals($contractEffectOrders->sum('value'), $paymentApplied->payment_value);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $this->receivableUnitRegisterService = app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ]);

    $order = ReceivableUnitOrder::all()->first();

    app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ])->store(app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ])->addData([
        'external_id' => $order->external_order_id,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => $order->value,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => 0,
        'guarantee_value' => 0,
    ]));

    $receivableUnitOld = $this->receivableUnit;
    $this->receivableUnit = $this->receivableUnit->fresh();

    $valuePaymentEffectContract = (int) ceil(
        $this->receivableUnit->total_constituted_value
        * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER)
    );

    $paymentApplied = Payment::where('holder_document_number', '<>', $this->receivableUnit->holder_document_number)->first();

    $contractEffectOrders = ContractEffectOrder::all();

    $expectedTotalConstitutedValue = $receivableUnitOld->total_constituted_value - $order->value;

    expect($this->receivableUnitOrdersInitial->count())->toBe($contractEffectOrders->count());
    expect($expectedTotalConstitutedValue)->toBe($this->receivableUnit->total_constituted_value);
    expect($this->receivableUnit->total_constituted_value)->toBe(Payment::all()->sum('payment_value'));
    expect($valuePaymentEffectContract)->toBe($paymentApplied->payment_value);
    expect($paymentApplied->payment_value)->toBe( $contractEffectOrders->sum('value'));

})->group('contractEffects', 'recalculateContractEffects');
