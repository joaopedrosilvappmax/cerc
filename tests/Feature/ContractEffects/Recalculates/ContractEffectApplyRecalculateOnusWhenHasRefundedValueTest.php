<?php

namespace Tests\ContractEffects\Recalculates\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Models\Services\ReceivableUnits\ReceivableUnitRegisterService;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\DataTransferObjects\ReceivableUnits\ReceivableUnitDataRegister;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()
        ->create([
            'total_constituted_value' => 10000
        ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()
        ->count(5)
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'value' => 2000,
            'reversal_value' => 0,
            'guarantee_value' => 0,
            'gross_value' => 2000,
        ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test('Recalculate Contract Effect Continuous When Has Refunded Value', function () {

    $totalConstitutedValueInitial = $this->receivableUnit->total_constituted_value;

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 5000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $this->receivableUnit->refresh();
    $this->payment->refresh();
    $contractEffectOrders = ContractEffectOrder::all();

    $paymentApplied = $this->receivableUnit->payments->where('id', '<>', $this->payment->id)->first();

    expect($this->receivableUnit->payments->count())->toBe(2);
    expect($contractEffectOrders->count())->toBe(5);
    expect($paymentApplied->payment_value)->toBe($contractEffectOrders->sum('value'));

    $contractEffectFixed = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 500,
            'division_rule' => DivisionRule::FIXED_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectFixed->effect_type
    ])->apply($contractEffectFixed, $this->receivableUnit);

    $this->receivableUnit->refresh();
    $contractEffectOrders = ContractEffectOrder::all();

    expect($this->receivableUnit->payments->count())->toBe(3);
    expect($contractEffectOrders->count())->toBe(10);

    $totalRefunded = 0;
    $this->receivableUnitOrdersInitial->each(function ($order) use (&$totalRefunded) {

        app(RegisterServiceContract::class, [
            'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
        ])->store( app(DataRegisterContract::class, [
            'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
        ])->addData([
            'external_id' => $order->external_order_id,
            'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'pre_contracted_value' => 0,
            'total_gross_value' => 0,
            'blocked_value' => 0,
            'reversal_value' => $order->value,
            'settlement_date' => $this->receivableUnit->settlement_date,
            'paid_at' => null,
            'value' => 0,
            'guarantee_value' => 0,
        ]));

        $totalRefunded += $order->value;
    });

    $this->receivableUnit->refresh();
    $contractEffectOrders = ContractEffectOrder::all();

    expect($contractEffectOrders->count())->toBe(10);
    expect($this->receivableUnit->total_constituted_value)->toBe($contractEffectOrders->sum('value'));
    expect($this->receivableUnit->total_constituted_value)
        ->toBe($totalConstitutedValueInitial - $totalRefunded);
    expect($this->receivableUnit->total_constituted_value)
        ->toBe($this->receivableUnit->payments->sum('payment_value'));
})->group('contractEffects', 'recalculateContractEffects');

test('Recalculate Contract Effect Onus Others With Value Fixed And Refund Order', function () {
    $receivableUnitValueInitial = $this->receivableUnit->total_constituted_value;

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_OTHERS,
            'committed_value' => 6000,
            'division_rule' => DivisionRule::FIXED_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $this->receivableUnit->refresh();

    $contractEffectOrders = ContractEffectOrder::all();

    $paymentApplied = $this->receivableUnit->payments->where('id', '<>', $this->payment->id)->first();

    expect($this->receivableUnit->payments->count())->toBe(2);
    expect($contractEffectOrders->count())->toBe(5);
    expect($paymentApplied->payment_value)->toBe($contractEffectOrders->sum('value'));

    $totalRefunded = $this->receivableUnitOrdersInitial->first()->value;
    $newTotalConstitutesValue = $receivableUnitValueInitial - $totalRefunded;

    app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ])->store(app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ])->addData([
        'external_id' => $this->receivableUnitOrdersInitial->first()->external_order_id,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => $this->receivableUnitOrdersInitial->first()->value,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => 0,
        'guarantee_value' => 0,
    ]));

    $this->receivableUnit->refresh();
    $paymentApplied->refresh();
    $contractEffectOrders = $contractEffectOrders->fresh();

    expect($newTotalConstitutesValue)->toBe($this->receivableUnit->total_constituted_value);
    expect($newTotalConstitutesValue)->toBe(Payment::all()->sum('payment_value'));
    expect($paymentApplied->payment_value)->toBe($contractEffectOrders->sum('value'));
})->group('contractEffects', 'recalculateContractEffects');

test('Refunded Partial Receivable Unit With Contract Effect', function () {
    $valueOrderInitial = 2000;

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_OTHERS,
            'committed_value' => 1000,
            'division_rule' => DivisionRule::FIXED_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $reversalValue = 500;

    (new ReceivableUnitRegisterService())->store(
        (new ReceivableUnitDataRegister())->addData([
            'external_id' => $this->receivableUnitOrdersInitial->last()->external_order_id,
            'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'pre_contracted_value' => 0,
            'total_gross_value' => 0,
            'blocked_value' => 0,
            'reversal_value' => $reversalValue,
            'settlement_date' => $this->receivableUnit->settlement_date,
            'paid_at' => null,
            'value' => 0,
            'guarantee_value' => 0,
        ])
    );

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::all();
    $this->receivableUnit->refresh();

    expect($reversalValue)->not()->toBe($this->receivableUnitOrdersInitial->last()->reversal_value);
    expect($valueOrderInitial)->toBe($this->receivableUnitOrdersInitial->last()->reversal_value);
    expect($valueOrderInitial)->toBe($this->receivableUnitOrdersInitial->last()->gross_value);
    expect($this->receivableUnitOrdersInitial->last()->value)->toBe(0);
    expect($this->receivableUnit->total_constituted_value)
        ->toBe(10000 - $this->receivableUnitOrdersInitial->last()->reversal_value);

    (new ReceivableUnitRegisterService())->store(
        (new ReceivableUnitDataRegister())->addData([
            'external_id' => $this->receivableUnitOrdersInitial->first()->external_order_id,
            'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'pre_contracted_value' => 0,
            'total_gross_value' => 0,
            'blocked_value' => 0,
            'reversal_value' => $reversalValue,
            'settlement_date' => $this->receivableUnit->settlement_date,
            'paid_at' => null,
            'value' => 0,
            'guarantee_value' => 0,
        ])
    );

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::all();
    $this->receivableUnit->refresh();

    expect($reversalValue)->not()->toBe($this->receivableUnitOrdersInitial->first()->reversal_value);
    expect($valueOrderInitial)->toBe($this->receivableUnitOrdersInitial->first()->reversal_value);
    expect($valueOrderInitial)->toBe($this->receivableUnitOrdersInitial->first()->gross_value);
    expect($this->receivableUnitOrdersInitial->first()->value)->toBe(0);
    expect($this->receivableUnit->total_constituted_value)
        ->toBe(
            10000
            - $this->receivableUnitOrdersInitial->last()->reversal_value
            - $this->receivableUnitOrdersInitial->first()->reversal_value
        );
})->group('contractEffects', 'recalculateContractEffects');
