<?php

namespace Tests\ContractEffects\Recalculates\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\Math;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()
        ->create([
            'total_constituted_value' => 10000
        ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()
        ->count(5)
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'value' => 2000,
            'reversal_value' => 0,
            'guarantee_value' => 0,
        ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test('Recalculate Contract Effect When Receive Guarantee Value', function () {
    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 5000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,

        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $this->receivableUnit->refresh();

    $contractEffectOrders = ContractEffectOrder::all();

    $guaranteeValue = 500;

    app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ])->store(app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ])->addData([
        'external_id' => $this->receivableUnitOrdersInitial->first()->external_order_id,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => 0,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => 0,
        'guarantee_value' => $guaranteeValue,
    ]));

    $this->receivableUnit->refresh();

    $this->receivableUnitOrdersInitial->first()->refresh();

    $contractEffectOrders = $contractEffectOrders->fresh();
    $contractEffectOrderGuarantee = $contractEffectOrders->where('external_order_id', $this->receivableUnitOrdersInitial->first()->external_order_id)->first();
    $newValueOrderContractEffet = (int) ceil(($this->receivableUnitOrdersInitial->first()->value) * ($contractEffect->committed_value * Math::DIVISION_PERCENTAGE_INTEGER));

    expect($contractEffectOrders->count())->toBe(5);
    expect($this->receivableUnit->total_constituted_value)->toBe(10000 - $guaranteeValue);
    expect($this->receivableUnitOrdersInitial->first()->value)->toBe(2000 - $guaranteeValue);
    expect($this->receivableUnit->total_constituted_value)->toBe($this->receivableUnit->payments->sum('payment_value'));
    expect($newValueOrderContractEffet)->toBe($contractEffectOrderGuarantee->value);
})->group('contractEffects', 'recalculateContractEffects');
