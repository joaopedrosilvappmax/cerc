<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()
        ->create([
            'total_constituted_value' => 10000
        ]);

    $this->receivableUnitOrdersInitial = ReceivableUnitOrder::factory()
        ->count(5)
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'value' => 2000,
            'reversal_value' => 0,
            'guarantee_value' => 0,
        ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test('Contract Effect Continuous With Constituted Total Not Available', function () {

    $contractEffect = ContractEffect::factory()
        ->create([
            'id' => 1,
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 5000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $contractEffectFixed = ContractEffect::factory()
        ->create([
            'id' => 2,
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 5000,
            'division_rule' => DivisionRule::FIXED_VALUE,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffectFixed, $this->receivableUnit);

    $contractEffectOrders = ContractEffectOrder::all();

    expect($this->receivableUnit->total_constituted_value)->toBe($contractEffectOrders->sum('value'));

    $orderFirst = $this->receivableUnitOrdersInitial->first();
    $orderLast = $this->receivableUnitOrdersInitial->last();

    app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ])->store(app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ])->addData([
        'external_id' => $orderFirst->external_order_id,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => $orderFirst->value,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => 0,
        'guarantee_value' => 0,
    ]));

    app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ])->store(app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ])->addData([
        'external_id' => $orderLast->external_order_id,
        'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
        'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => 0,
        'settlement_date' => $this->receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => 0,
        'guarantee_value' => 1000,
    ]));

    $this->receivableUnit->refresh();

    $contractEffectOrders = $this->receivableUnit->contractEffectOrders;

    $paymentsNoHolder = Payment::where('holder_document_number', '<>', $this->receivableUnit->holder_document_number)->get();
    $contractEffectOrdersPercentage = $paymentsNoHolder->first()->contractEffectOrders;
    $contractEffectOrdersFixed = $paymentsNoHolder->last()->contractEffectOrders;

    expect($contractEffectOrders->count())->toBe(10);
    expect($contractEffectOrdersPercentage->sum('value'))->toBe(3500);
    expect($contractEffectOrdersFixed->sum('value'))->toBe(3500);
    expect($paymentsNoHolder->sum('payment_value'))->toBe($contractEffectOrders->sum('value'));
    expect($this->receivableUnit->total_constituted_value)->toBe($this->receivableUnit->payments->sum('payment_value'));

})->group('contractEffects', 'recalculateContractEffects');
