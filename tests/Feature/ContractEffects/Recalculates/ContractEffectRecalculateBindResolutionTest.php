<?php

namespace Tests\ContractEffects\Recalculates\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Services\ContractEffects\Recalculate\RecalculateEffectContractCourtBlockService;
use App\Models\Services\ContractEffects\Recalculate\RecalculateEffectContractService;
use App\Models\Services\Contracts\RecalculateContractEffectContract;
use App\Support\EnumTypes\ContractEffectType;

test ('Bind Resolution Recalculate Contract Effect', function () {

    $contractEffectOnus = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT
        ]);

    $recalculateService = app(RecalculateContractEffectContract::class, [
        'type' => $contractEffectOnus->effect_type
    ]);

    expect(get_class($recalculateService))->toBe(RecalculateEffectContractService::class);

    $contractEffectCourtBlock = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::COURT_BLOCK
        ]);

    $recalculateService = app(RecalculateContractEffectContract::class, [
        'type' => $contractEffectCourtBlock->effect_type
    ]);

    expect(get_class($recalculateService))->toBe(RecalculateEffectContractCourtBlockService::class);

})->group('contractEffects', 'recalculateContractEffects', 'bind-resolution');
