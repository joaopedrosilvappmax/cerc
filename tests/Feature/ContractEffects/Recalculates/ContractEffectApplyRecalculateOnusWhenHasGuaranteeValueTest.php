<?php

namespace Tests\ContractEffects\Recalculates\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

beforeEach(function () {
    $this->receivableUnit = ReceivableUnit::factory()
        ->create([
            'total_constituted_value' => 10000
        ]);

    $this->receivableUnitOrders = ReceivableUnitOrder::factory()
        ->count(5)
        ->create([
            'receivable_unit_id' => $this->receivableUnit->id,
            'value' => 2000,
            'reversal_value' => 0,
            'guarantee_value' => 0,
        ]);

    $this->payment = Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_value' => $this->receivableUnit->total_constituted_value,
        'holder_document_number' => $this->receivableUnit->holder_document_number,
    ]);
});

test('Recalculate Contract Effect When Has Guarantee Value', function () {

    $totalConstitutedValueInitial = $this->receivableUnit->total_constituted_value;

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 5000,
            'division_rule' => DivisionRule::PERCENTAGE_VALUE,
            'holder_document_number' => $this->receivableUnit->holder_document_number,
            'final_recipient_document_number' => $this->receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'settlement_date' => $this->receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnit);

    $order = $this->receivableUnitOrders->first();
    $guaranteeValue = 2000;
    $totalGuarantee = 0;

    $this->receivableUnitOrders->each(function ($order) use ($guaranteeValue, &$totalGuarantee) {

        $dataRegister = app(DataRegisterContract::class, [
            'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
        ])->addData([
            'external_id' => $order->external_order_id,
            'final_recipient_document_number' => $this->receivableUnit->holder_document_number,
            'code_arrangement_payment' => $this->receivableUnit->code_arrangement_payment,
            'pre_contracted_value' => 0,
            'total_gross_value' => 0,
            'blocked_value' => 0,
            'reversal_value' => 0,
            'settlement_date' => $this->receivableUnit->settlement_date,
            'paid_at' => null,
            'value' => 0,
            'guarantee_value' => $guaranteeValue,
        ]);

        app(RegisterServiceContract::class, [
            'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
        ])->store($dataRegister);

        $totalGuarantee += $guaranteeValue;
    });

    $this->receivableUnit->refresh();
    $order->refresh();
    $receivableUnitPayments = $this->receivableUnit->payments;
    $contractEffectOrders = ContractEffectOrder::all();

    expect($contractEffectOrders->count())->toBe(5);
    expect($this->receivableUnit->total_constituted_value)->toBe($contractEffectOrders->sum('value'));
    expect($this->receivableUnit->total_constituted_value)->toBe($totalConstitutedValueInitial - $totalGuarantee);
    expect($this->receivableUnit->total_constituted_value)->toBe($receivableUnitPayments->sum('payment_value'));
    expect($contractEffectOrders->sum('value'))->toBe(0);

})->group('contractEffects', 'recalculateContractEffects');
