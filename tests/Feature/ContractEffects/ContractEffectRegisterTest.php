<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Services\ContractEffects\ContractEffectRegisterService;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\DataTransferObjects\ContractEffects\ContractEffectDataRegister;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use Tests\Stub\ContractEffectDataRegisterMock;

test('Bind Resolution Contract Effect', function () {
    $contractEffectRegisterData = app(DataRegisterContract::class, [
        'class' => ClassBindResolution::CONTRACT_EFFECT_DATA
    ]);

    expect(get_class($contractEffectRegisterData))->toBe(ContractEffectDataRegister::class);

    $contractEffectRegisterService = app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::CONTRACT_EFFECT_SERVICE
    ]);

    expect(get_class($contractEffectRegisterService))->toBe(ContractEffectRegisterService::class);

})->group('bind-resolution');

test('Store Contract Effect', function() {
    $contractEffectRegisterData = (new ContractEffectDataRegister())->addData(ContractEffectDataRegisterMock::getInitialData());

    $contractEffect = (new ContractEffectRegisterService())->store($contractEffectRegisterData);

    expect(get_class($contractEffect))->toBe(ContractEffect::class);
})->group('bind-resolution');
