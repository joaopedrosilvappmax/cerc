<?php

namespace Tests\Feature;

use App\Models\Entities\Balance;
use App\Models\Services\Balances\BalanceRegisterService;
use App\Support\EnumTypes\BalanceType;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\DataTransferObjects\Balances\BalanceDataRegister;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use Tests\Stub\BalanceDataRegisterMock;

beforeEach(function () {
    $this->balanceDataMocked = BalanceDataRegisterMock::getInitialData();
    $this->balanceRegisterData = (new BalanceDataRegister())->addData($this->balanceDataMocked);
});

test('Bind Resolution Company', function () {
    $balanceDataRegister = app(DataRegisterContract::class, [
        'class' => ClassBindResolution::BALANCE_DATA
    ]);

    expect(get_class($balanceDataRegister))->toBe(BalanceDataRegister::class,);
})->group('balances');


test('Mount Balance Data', function () {
    foreach ($this->balanceRegisterData->toArray() as $k => $d) {

        expect($d)->toBe($this->balanceDataMocked[$k]);
    }
})->group('balances');


test('Store Balance', function () {

    $balance = (new BalanceRegisterService())->store($this->balanceRegisterData);

    expect(get_class($balance))->toBe(Balance::class);

    expect($balance->type)->toBe(BalanceType::DEBIT);
})->group('balances');
