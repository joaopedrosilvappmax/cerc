<?php

namespace Tests\Feature;

use App\Models\Entities\Balance;
use App\Models\Entities\Company;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\ReceivableUnits\ReceivableUnitRegisterService;
use App\Support\DataTransferObjects\ReceivableUnits\ReceivableUnitDataRegister;
use App\Support\EnumTypes\BalanceType;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use Tests\Stub\ReceivableUnitDataRegisterMock;

beforeEach(function () {
    $this->company = Company::factory()->create();

    $this->receivableUnitData = (new ReceivableUnitDataRegister())
        ->addData(ReceivableUnitDataRegisterMock::getInitialData());
});

test('Receivable Unit Apply Debit', function ($balanceValue, $totalConstitutedValue) {
    $balance = Balance::factory()->create([
        'document_number' => $this->company->document_number,
        'value' => $balanceValue
    ]);

    $this->receivableUnitData->holder_document_number = $balance->document_number;
    $this->receivableUnitData->final_recipient_document_number = $balance->document_number;
    $this->receivableUnitData->total_constituted_value = $totalConstitutedValue;

    $receivableUnit = (new ReceivableUnitRegisterService())
        ->store($this->receivableUnitData);

    $receivableUnit->refresh();
    $balance->refresh();

    expect($receivableUnit->receivableUnitOrders->first()->debit_value)->toBe($balanceValue);
    expect($receivableUnit->receivableUnitOrders->first()->debit_value)->toBe($balanceValue);
    expect($balance->value)->toBe(0);

})->with([
    [1000, 1000]
])->group('balances', 'receivableUnitApplyDebits');

test('Receivable Unit Apply Debit With Non-Full Amount', function ($balanceValue, $totalConstitutedValue) {
    $balance = Balance::factory()->create([
        'document_number' => $this->company->document_number,
        'value' => $balanceValue
    ]);

    $this->receivableUnitData->holder_document_number = $balance->document_number;
    $this->receivableUnitData->final_recipient_document_number = $balance->document_number;
    $this->receivableUnitData->total_constituted_value = $totalConstitutedValue;

    $receivableUnit = (new ReceivableUnitRegisterService())
        ->store($this->receivableUnitData);

    $balance->refresh();
    $receivableUnit->refresh();

    expect($receivableUnit->total_constituted_value)
        ->toBe($this->receivableUnitData->total_constituted_value - $balanceValue);

    expect($receivableUnit->receivableUnitOrders->first()->debit_value)->toBe($balanceValue);
    expect($receivableUnit->receivableUnitOrders->first()->debit_value)->toBe($balanceValue);
    expect($balance->value)->toBe(0);
    expect($balance->receivableUnits->isEmpty())->toBe(false);

})->with([
    [800, 1000]
])->group('balances', 'receivableUnitApplyDebits');


test('Receivable Unit Apply Debit With Amount', function ($balanceValue, $totalConstitutedValue) {
    $balance = Balance::factory()->create([
        'document_number' => $this->company->document_number,
        'value' => $balanceValue
    ]);

    $this->receivableUnitData->holder_document_number = $balance->document_number;
    $this->receivableUnitData->final_recipient_document_number = $balance->document_number;
    $this->receivableUnitData->total_constituted_value = $totalConstitutedValue;

    $receivableUnit = (new ReceivableUnitRegisterService())
        ->store($this->receivableUnitData);

    $balance->refresh();

    expect($receivableUnit->receivableUnitOrders->first()->debit_value)
        ->toBe($this->receivableUnitData->total_constituted_value);

    expect($receivableUnit->receivableUnitOrders->first()->debit_value)->toBe(1000);
    expect($balance->value)->toBe(1000);
    expect($balance->receivableUnits->isEmpty())->toBe(false);

})->with([
    [2000, 1000]
])->group('balances', 'receivableUnitApplyDebits');


test('Receivable Unit Apply With Two Debits When No Available Value', function ($balanceValue, $totalConstitutedValue) {
    $firstBalance = Balance::factory()->create([
        'document_number' => $this->company->document_number,
        'value' => $balanceValue
    ]);

    $secondBalance = Balance::factory()->create([
        'document_number' => $this->company->document_number,
        'value' => $balanceValue
    ]);

    $this->receivableUnitData->holder_document_number = $firstBalance->document_number;
    $this->receivableUnitData->final_recipient_document_number = $firstBalance->document_number;
    $this->receivableUnitData->total_constituted_value = $totalConstitutedValue;

    $receivableUnit = (new ReceivableUnitRegisterService())
        ->store($this->receivableUnitData);

    $firstBalance->refresh();
    $secondBalance->refresh();

    expect($receivableUnit->receivableUnitOrders->first()->debit_value)
        ->toBe($this->receivableUnitData->total_constituted_value);

    expect($receivableUnit->receivableUnitOrders->first()->debit_value)->toBe(1000);
    expect($receivableUnit->balances->count())->toBe(1);
    expect($secondBalance->value)->toBe(1000);
    expect($firstBalance->receivableUnits->isEmpty())->toBe(false);

})->with([
    [1000, 1000]
])->group('balances', 'receivableUnitApplyDebits');

test('Receivable Unit Apply With Multi Debits When No Available Value', function ($firstBalanceValue, $secondBalanceValue, $totalConstitutedValue) {

    $firstBalance = Balance::factory()->create([
        'document_number' => $this->company->document_number,
        'value' => $firstBalanceValue
    ]);

    $secondBalance = Balance::factory()->create([
        'document_number' => $this->company->document_number,
        'value' => $secondBalanceValue
    ]);

    $this->receivableUnitData->holder_document_number = $firstBalance->document_number;
    $this->receivableUnitData->final_recipient_document_number = $firstBalance->document_number;
    $this->receivableUnitData->total_constituted_value = $totalConstitutedValue;

    $receivableUnit = (new ReceivableUnitRegisterService())
        ->store($this->receivableUnitData);

    $firstBalance->refresh();
    $secondBalance->refresh();

    expect($receivableUnit->total_constituted_value)->toBe(0);
    expect($firstBalance->value)->toBe(0);
    expect($secondBalance->value)->toBe(200);
    expect($receivableUnit->balances->count())->toBe(2);

})->with([
    [200, 1000, 1000]
])->group('balances', 'receivableUnitApplyDebits');

test('Check Debit Application With Contract Effect', function () {
    $documentNumber = '72861111000160';

    Company::factory()->create([
        'document_number' => $documentNumber
    ]);

    $dataRegisterReceivableUnit = new ReceivableUnitDataRegister();
    $receivableUnitRegisterService = new ReceivableUnitRegisterService();

    $orderOne = [
        'external_id' => '0001',
        'final_recipient_document_number' => $documentNumber,
        'code_arrangement_payment' => 'VCC',
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => 0,
        'settlement_date' => '2021-12-01',
        'paid_at' => null,
        'value' => 2000,
    ];

    $dataRegisterReceivableUnit->addData($orderOne);

    $receivableUnitRegisterService
        ->store($dataRegisterReceivableUnit);

    $debit = Balance::factory()->create([
        'document_number' => $documentNumber,
        'value' => 500,
        'type' => BalanceType::DEBIT
    ]);

    $orderTwo = [
        'external_id' => '0002',
        'final_recipient_document_number' => $documentNumber,
        'code_arrangement_payment' => 'VCC',
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => 0,
        'settlement_date' => '2021-12-01',
        'paid_at' => null,
        'value' => 3000,
    ];

    $dataRegisterReceivableUnit->addData($orderTwo);

    $receivableUnitRegisterService
        ->store($dataRegisterReceivableUnit);

    $receivableUnit = ReceivableUnit::where('final_recipient_document_number', $documentNumber)->first();

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 3000,
            'division_rule' => DivisionRule::FIXED_VALUE,
            'final_recipient_document_number' => $receivableUnit->final_recipient_document_number,
            'holder_beneficiary_document_number' => 60573798000164,
            'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
            'settlement_date' => $receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, ['effect_type' => $contractEffect->effect_type])
        ->apply($contractEffect, $receivableUnit);

    $receivableUnit->refresh();
    $receivableUnitOrders = ReceivableUnitOrder::all();

    $paymentsHolder = $receivableUnit->payments
        ->where('holder_document_number', $receivableUnit->holder_document_number)->first();

    $paymentContractEffect = $receivableUnit->payments
        ->where('holder_document_number', '<>', $receivableUnit->holder_document_number)->first();

    expect($receivableUnitOrders->count())
        ->toBe(2);

    expect($receivableUnit->total_constituted_value)
        ->toBe(
            $orderOne['value'] + $orderTwo['value'] - $debit->value
        );

    expect($receivableUnitOrders->sum('value'))
        ->toBe($receivableUnit->total_constituted_value);

    expect(Payment::all()->sum('payment_value'))
        ->toBe($receivableUnit->total_constituted_value);

    expect($paymentsHolder->payment_value)
        ->toBe(1500);

    expect($paymentContractEffect->payment_value)
        ->toBe($contractEffect->committed_value);
});
