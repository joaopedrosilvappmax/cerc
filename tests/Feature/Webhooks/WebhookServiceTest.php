<?php

namespace Tests\Feature;

use App\Models\Services\Contracts\WebhookServiceContract;
use App\Models\Services\Webhooks\WebhookService;

test('Bind Resolution Webhook Service', function () {
    $webhookService = app(WebhookServiceContract::class);

    expect(get_class($webhookService))->toBe(WebhookService::class);

});
