<?php

namespace Tests\Feature\Notifications;

use App\Models\Entities\Anticipation;
use App\Models\Entities\Company;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\UploadedContractEffectFile;
use App\Models\Services\ContractEffects\ContractEffectService;
use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;

test('Generate companies data to notifications test', function () {
    Company::factory()->count(5)->create([
        'created_at' => Carbon::now()->subDay(),
        'updated_at' => Carbon::now()->subDay()
    ]);

    Company::factory()->count(5)->create([
        'updated_at' => Carbon::now()->subDay()
    ]);

    Company::factory()->count(5)->create([
        'integrated_at' => Carbon::now()
    ]);

    expect(Company::dailyCreated()->count())->toBe(5);
    expect(Company::dailyUpdated()->count())->toBe(5);
    expect(Company::dailyIntegrated()->count())->toBe(5);
})->group('notifications');

test('Generate receivable units data to notifications test', function () {
    ReceivableUnit::factory()->count(5)->create([
        'integrated_at' => Carbon::now(),
        'created_at' => Carbon::now()->subDay(),
        'updated_at' => Carbon::now()->subDay(),
    ]);

    ReceivableUnit::factory()->count(5)->create([
        'integrated_at' => Carbon::now(),
        'updated_at' => Carbon::now()->subDay()
    ]);

    ReceivableUnit::factory()->count(5)->create([
        'settlement_date' => Carbon::now()->subDay()->format('Y-m-d'),
        'integrated_at' => Carbon::now(),
        'operation_type' => OperationType::WRITE_OFF
    ]);

    expect(ReceivableUnit::dailyCreated()->count())->toBe(5);
    expect(ReceivableUnit::dailyUpdated()->count())->toBe(5);
    expect(ReceivableUnit::dailyWriteOff()->count())->toBe(5);
    expect(ReceivableUnit::dailyIntegrated()->count())->toBe(15);
})->group('notifications');

test('Generate contract effects data to notifications test', function () {
    $uploadedContractEffectFile = UploadedContractEffectFile::create([
        'file_name' => 'test',
        'date' => Carbon::now()->format('Y-m-d'),
        'sequential_number' => 1,
    ]);

    ContractEffect::factory()->count(5)->create([
        'uploaded_contract_effect_file_id' => $uploadedContractEffectFile->id
    ]);

    ContractEffect::factory()->count(5)->create([
        'applied_at' => Carbon::now()->subDay()
    ]);

    expect(ContractEffect::dailyApplied()->count())->toBe(5);

    expect(
        app(ContractEffectService::class)
            ->getDailyContractEffectsFromWebhookOrFile()
            ->count()
    )->toBe(5);
});

test('Generate anticipation data to notifications test', function () {
    Anticipation::factory()->count(5)->create([
        'created_at' => Carbon::now()->subDay(),
        'updated_at' => Carbon::now()->subDay(),
        'integrated_at' => Carbon::now(),
    ]);

    Anticipation::factory()->count(5)->create([
        'updated_at' => Carbon::now()->subDay(),
        'integrated_at' => Carbon::now(),
    ]);

    Anticipation::factory()->count(5)->create([
        'settlement_date' => Carbon::now()->subDay()->format('Y-m-d'),
        'integrated_at' => Carbon::now(),
        'operation_type' => OperationType::WRITE_OFF
    ]);

    expect(Anticipation::dailyCreated()->count())->toBe(5);
    expect(Anticipation::dailyUpdated()->count())->toBe(5);
    expect(Anticipation::dailyWriteOff()->count())->toBe(5);
    expect(Anticipation::dailyIntegrated()->count())->toBe(15);
});
