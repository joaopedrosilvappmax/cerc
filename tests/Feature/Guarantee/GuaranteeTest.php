<?php

namespace Tests\Feature;

use App\Models\Entities\Company;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;

test('Receivable unit order process', function() {

    $orderId = 1;
    $guaranteeValue = 50;

    $receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 1000
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->holder_document_number,
    ]);

    $receivalbeUnitOrder = ReceivableUnitOrder::create(
        [
            'external_order_id' => $orderId,
            'receivable_unit_id' => $receivableUnit->id,
            'value' => 500,
            'guarantee_value' => 0
        ]
    );

    app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ])->store(app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ])->addData([
        'external_id' => $orderId,
        'final_recipient_document_number' => $receivableUnit->holder_document_number,
        'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => 0,
        'settlement_date' => $receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => 0,
        'guarantee_value' => $guaranteeValue,
    ]));

    $receivableUnit->refresh();

    $receivalbeUnitOrder->refresh();

    expect($receivalbeUnitOrder->guarantee_value)->toBe(50);
    expect($receivableUnit->total_constituted_value)->toBe(950);

});

test('Guarantee with Contract Effect', function() {
    $receivableUnit = ReceivableUnit::factory()->create([
        'holder_document_number' => 7286144100160,
        'code_arrangement_payment' => 'VCC',
        'settlement_date' => '2021-12-12',
        'final_recipient_document_number' => 7286144100160,
        'total_constituted_value' => 12971,
    ]);

    $receivableUnitOrders = ReceivableUnitOrder::factory()->count(7)->create([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => 1853,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'anticipation_value' => 0,
        'debit_value' => 0,
    ]);

    Company::factory()->create([
        'document_number' => $receivableUnit->holder_document_number,
    ]);

    $payment = Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->final_recipient_document_number,
        'payment_value' => $receivableUnit->total_constituted_value,
    ]);

    expect($receivableUnit->total_constituted_value)->toBe(12971);
    expect($payment->payment_value)->toBe(12971);

    $receivableUnitOrders->each(function ($receivableUnitOrder, &$count) use ($receivableUnit) {
        if ($count < 3) {
            app(RegisterServiceContract::class, [
                'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
            ])->store(app(DataRegisterContract::class, [
                'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
            ])->addData([
                'external_id' => $receivableUnitOrder->external_order_id,
                'final_recipient_document_number' => $receivableUnit->holder_document_number,
                'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
                'pre_contracted_value' => 0,
                'total_gross_value' => 0,
                'blocked_value' => 0,
                'reversal_value' => 0,
                'settlement_date' => $receivableUnit->settlement_date,
                'paid_at' => null,
                'value' => 0,
                'guarantee_value' => $receivableUnitOrder->value,
            ]));

            $count ++;

            return;
        }

        return false;
    });

    $receivableUnit->refresh();
    $payment->refresh();

    $contractEffect = ContractEffect::factory([
        'holder_document_number' => $receivableUnit->holder_document_number,
        'receivable_unit_id' => $receivableUnit->id,
        'effect_type' => ContractEffectType::ONUS_OTHERS,
        'division_rule' => DivisionRule::FIXED_VALUE,
        'committed_value' => 100000,
        'settlement_date' => $receivableUnit->settlement_date,
    ])->create();

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $receivableUnit->refresh();

    $paymentHolder = Payment::where('holder_document_number', '=', $receivableUnit->holder_document_number)->first();

    $paymentContractEffect = Payment::where('holder_document_number', '!=', $receivableUnit->holder_document_number)->first();

    expect($paymentHolder->payment_value)->toBe(0);
    expect($receivableUnit->total_constituted_value)->toBe($paymentContractEffect->payment_value);

});

