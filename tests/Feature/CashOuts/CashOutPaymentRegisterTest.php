<?php

namespace Tests\Feature;

use App\Models\Entities\CashOutPayment;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\CashOutPayment\CashOutPaymentAnticipationRegisterService;
use App\Models\Services\CashOutPayment\CashOutPaymentSettledRegisterService;
use App\Models\Services\Contracts\CashOutPaymentRegisterServiceContract;
use App\Support\DataTransferObjects\CashOutPayments\CashOutPaymentsDataRegister;
use App\Support\EnumTypes\CashOutPaymentType;
use Exception;

beforeEach(function() {
    $this->payment = Payment::factory()->create();
    $this->receivableUnit = ReceivableUnit::factory()->create();

    $this->data = app(CashOutPaymentsDataRegister::class)->addData([
        'external_cash_out_id' => 1,
        'external_cash_out_company_id' => null,
        'receivable_unit_id' => $this->receivableUnit->id,
        'payment_id' => $this->payment->id,
        'payment_value' => 50000,
    ]);
});

test('Bind resolution', function () {
    $registerService = app(CashOutPaymentRegisterServiceContract::class, [
        'type' => CashOutPaymentType::SETTLED
    ]);

    expect(get_class($registerService))->toBe(CashOutPaymentSettledRegisterService::class);

    $registerService = app(CashOutPaymentRegisterServiceContract::class, [
        'type' => CashOutPaymentType::ANTICIPATION
    ]);

    expect(get_class($registerService))->toBe(CashOutPaymentAnticipationRegisterService::class);
});

test('Register Cash Out Payment Settled', function () {
    (new CashOutPaymentSettledRegisterService())->store($this->data);

    $cashOutPayment = CashOutPayment::all();

    expect($cashOutPayment->count())->toBe(1);

    (new CashOutPaymentSettledRegisterService())->store($this->data);
})->throws(Exception::class);

