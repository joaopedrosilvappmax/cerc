<?php

namespace Tests\Feature\LogNotificationErrors;

use App\Models\Entities\LogNotificationError;
use App\Models\Services\Notifications\LogNotificationErrorRegisterService;
use App\Support\EnumTypes\LogNotificationErrorType;

test('Increment error critical', function() {
    $logCritical =  LogNotificationError::firstWhere('type', LogNotificationErrorType::CRITICAL);

    expect($logCritical->quantity)->toBe(0);

    app(LogNotificationErrorRegisterService::class)->incrementCriticalError();

    $logCritical->refresh();

    expect($logCritical->quantity)->toBe(1);
});
