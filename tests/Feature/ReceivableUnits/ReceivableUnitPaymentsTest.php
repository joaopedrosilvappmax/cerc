<?php

namespace Tests\Unit;

use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\ReceivableUnits\ReceivableUnitPaymentService;
use Carbon\Carbon;

test('Get Sum Payments Is Not Holder', function () {
    $paymentNoHolderValue = 20000;
    $numberPayments = 5;

    $receivableUnit = ReceivableUnit::factory([
        'id' => 1,
        'total_constituted_value' => 20000,
        'holder_document_number' => '11111111111',
        'settlement_date' => Carbon::now()->addDay(),
    ])->create();

    Payment::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => $receivableUnit->total_constituted_value/2
    ])->create();

    Payment::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => 1111125,
        'payment_value' => $paymentNoHolderValue
    ])->count($numberPayments)->create();

    $receivableUnitPaymentsSum = app(ReceivableUnitPaymentService::class)
        ->receivableUnitPaymentsSum($receivableUnit->holder_document_number);

    expect($receivableUnitPaymentsSum)->toBe($paymentNoHolderValue * $numberPayments);
});
