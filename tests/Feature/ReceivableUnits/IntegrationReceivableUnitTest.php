<?php

namespace Tests\Feature;

use App\Models\Services\Contracts\IntegrationBodyServiceContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\EnumTypes\RoutesCERC;
use App\Support\EnumTypes\VersionsCERC;
use App\Support\ValueObjects\ReceivableUnitIntegrationBodyV14;

test('Bind Resolution Integration ReceivableUnit', function () {
    $class = app(IntegrationBodyServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_INTEGRATION_BODY_V14
    ]);

    expect(get_class($class))->toBe(ReceivableUnitIntegrationBodyV14::class);
});

test('Route version test', function () {
    expect(RoutesCERC::RECEIVABLE_UNITS)->toBe(cerc_route_by_config_version(RoutesCERC::RECEIVABLE_UNITS));

    config(['cerc.api_version' => VersionsCERC::V15]);

    expect(VersionsCERC::V15 . '/' . RoutesCERC::RECEIVABLE_UNITS)->toBe(cerc_route_by_config_version(RoutesCERC::RECEIVABLE_UNITS));
});

