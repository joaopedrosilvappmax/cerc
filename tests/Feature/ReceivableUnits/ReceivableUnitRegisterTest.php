<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\Balance;
use App\Models\Entities\Company;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Models\Services\ReceivableUnits\ReceivableUnitRegisterService;
use App\Support\EnumTypes\CompanyType;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\DataTransferObjects\ReceivableUnits\ReceivableUnitDataRegister;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use Tests\Stub\ReceivableUnitDataRegisterMock;

test('Bind Resolution Receivable Unit', function () {
    $receivableUnitDataRegister = app(DataRegisterContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_DATA
    ]);

    expect( get_class($receivableUnitDataRegister))->toBe(ReceivableUnitDataRegister::class);

    $receivableUnitRegisterService = app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::RECEIVABLE_UNIT_SERVICE
    ]);

    expect(get_class($receivableUnitRegisterService))->toBe(ReceivableUnitRegisterService::class);
})->group('bind-resolution');


test('Mount Receivable Unit Data', function () {

    $company = Company::factory()->create();

    $accreditingCompany = Company::factory()->create([
        'type_id' => CompanyType::INTERNAL
    ]);

    $data = ReceivableUnitDataRegisterMock::getInitialData();

    $receivableUnitDataRegister = (new ReceivableUnitDataRegister())->addData($data);

    $data = array_merge($data, [
        'company_id' => $company->id,
        'accrediting_company_id' => $accreditingCompany->id,
        'accrediting_company_document_number' => $accreditingCompany->document_number,
        'receivable_unit_linked_id' => null,
        'contract_effect_id' => null,
    ]);

    $receivableUnitDataRegister->company_id = $company->id;
    $receivableUnitDataRegister->accrediting_company_id = $accreditingCompany->id;
    $receivableUnitDataRegister->accrediting_company_document_number = $accreditingCompany->document_number;
    $receivableUnitDataRegister->receivable_unit_linked_id = null;
    $receivableUnitDataRegister->contract_effect_id = null;

    foreach ($receivableUnitDataRegister->toArray() as $k => $d) {

        expect($d)->toEqual($data[$k]);
    }
});

test('Store Receivable Unit', function () {

    $receivableUnitDataRegister = (new ReceivableUnitDataRegister())
        ->addData(ReceivableUnitDataRegisterMock::getInitialData());

    $receivableUnit = (new ReceivableUnitRegisterService())
        ->store($receivableUnitDataRegister);

    $receivableUnit = $receivableUnit->fresh('payments');

    expect(get_class($receivableUnit))->toBe(ReceivableUnit::class);

    expect($receivableUnit->payments->count())->toBe(1);

    expect($receivableUnit->total_constituted_value)->toBe($receivableUnit->payments->first()->payment_value);

    expect( $receivableUnit->total_constituted_value)->toBe( $receivableUnit->orders->first()->value);

    $receivableUnitDataRegister->external_id = 12312322222;

    $receivableUnit = (new ReceivableUnitRegisterService())->store($receivableUnitDataRegister);

    $receivableUnit = $receivableUnit->fresh('payments', 'orders');

    expect($receivableUnit->total_constituted_value)->toBe($receivableUnit->payments->first()->payment_value);

    expect($receivableUnit->total_constituted_value)->toBe($receivableUnit->orders->sum('value'));

});

test('Store Receivable Unit Negative Value', function () {

    $receivableUnitDataRegister = (new ReceivableUnitDataRegister())
        ->addData(ReceivableUnitDataRegisterMock::getInitialData());

    $receivableUnitDataRegister->total_constituted_value = 0;
    $receivableUnitDataRegister->pre_contracted_value = 0;
    $receivableUnitDataRegister->total_gross_value = 0;
    $receivableUnitDataRegister->reversal_value = 100;

    $receivableUnit = (new ReceivableUnitRegisterService())->store($receivableUnitDataRegister);

    $receivableUnit = $receivableUnit->fresh('payments');

    expect($receivableUnit->payments->first()->payment_value)->toBe(0);
    expect($receivableUnit->total_constituted_value)->toBe(0);
    expect($receivableUnit->total_constituted_value)->toBe($receivableUnit->payments->first()->payment_value);

    $receivableUnitFactory = ReceivableUnit::factory()->create([
        'id'  => 777,
        'total_constituted_value' => 20,
        'settlement_date' => '2021-12-31',
    ]);

    ReceivableUnitOrder::factory()->create([
        'external_order_id' => 777,
        'receivable_unit_id' => $receivableUnitFactory->id,
        'value' => $receivableUnitFactory->total_constituted_value,
        'gross_value' => $receivableUnitFactory->total_constituted_value,
        'guarantee_value' => 0,
        'reversal_value' => 0,
        'anticipation_value' => 0,
        'debit_value' => 0,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnitFactory->id,
        'holder_document_number' => $receivableUnitFactory->holder_document_number,
        'payment_value' => $receivableUnitFactory->total_constituted_value
    ]);

    Company::factory()->create([
        'document_number' => $receivableUnitFactory->holder_document_number
    ]);

    $receivableUnitDataRegister = (new ReceivableUnitDataRegister())
        ->addData(ReceivableUnitDataRegisterMock::getInitialData());

    $receivableUnitDataRegister->external_id = '777';
    $receivableUnitDataRegister->total_constituted_value = 0;
    $receivableUnitDataRegister->final_recipient_document_number = $receivableUnitFactory->final_recipient_document_number;
    $receivableUnitDataRegister->holder_document_number = $receivableUnitFactory->holder_document_number;
    $receivableUnitDataRegister->code_arrangement_payment = $receivableUnitFactory->code_arrangement_payment;
    $receivableUnitDataRegister->settlement_date = $receivableUnitFactory->settlement_date;
    $receivableUnitDataRegister->reversal_value = 100;
    $receivableUnit = (new ReceivableUnitRegisterService())->store($receivableUnitDataRegister);

    $receivableUnit = $receivableUnit->fresh('payments');

    expect($receivableUnit->payments->first()->payment_value)->toBe(0);
    expect($receivableUnit->total_constituted_value)->toBe(0);
    expect($receivableUnit->total_constituted_value)->toBe($receivableUnit->payments->first()->payment_value);

});


test('Store Receivable Unit Guarantee Value', function () {

    $receivableUnitDataRegister = (new ReceivableUnitDataRegister())
        ->addData(ReceivableUnitDataRegisterMock::getInitialData());

    $receivableUnitDataRegister->total_constituted_value = 100;

    $receivableUnit = (new ReceivableUnitRegisterService())->store($receivableUnitDataRegister);

    $receivableUnit = $receivableUnit->fresh('payments');

    expect($receivableUnit->total_constituted_value)->toBe(100);
    expect($receivableUnit->total_constituted_value)->toBe($receivableUnit->payments->first()->payment_value);

    $receivableUnitDataRegister = (new ReceivableUnitDataRegister())
        ->addData(ReceivableUnitDataRegisterMock::getInitialData());

    $receivableUnitDataRegister->final_recipient_document_number = $receivableUnit->final_recipient_document_number;
    $receivableUnitDataRegister->holder_document_number = $receivableUnit->holder_document_number;
    $receivableUnitDataRegister->total_constituted_value = 0;
    $receivableUnitDataRegister->guarantee_value = 25;

    $receivableUnitGuarantee = (new ReceivableUnitRegisterService())->store($receivableUnitDataRegister);

    $receivableUnitGuarantee = $receivableUnitGuarantee->fresh('payments');

    expect($receivableUnitGuarantee->payments->first()->payment_value)->toBe(75);
    expect($receivableUnitGuarantee->total_constituted_value)->toBe(75);
    expect($receivableUnit->fresh()->total_constituted_value)->toBe($receivableUnitGuarantee->total_constituted_value);
});

test('Store Receivable Unit Excess Negative Value', function () {

    $totalValue = 1000;

    $receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => $totalValue
    ]);

    Company::factory()->create([
        'document_number' => $receivableUnit->holder_document_number
    ]);

    $receivableUnitOrder = ReceivableUnitOrder::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'value' => $totalValue,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'anticipation_value' => 0,
        'debit_value' => 800,
    ]);

    $receivableUnitDataRegister = (new ReceivableUnitDataRegister())
        ->addData(ReceivableUnitDataRegisterMock::getInitialData());

    $receivableUnitDataRegister->external_id = $receivableUnitOrder->external_order_id;
    $receivableUnitDataRegister->final_recipient_document_number = $receivableUnit->final_recipient_document_number;
    $receivableUnitDataRegister->holder_document_number = $receivableUnit->holder_document_number;
    $receivableUnitDataRegister->settlement_date = $receivableUnit->settlement_date;
    $receivableUnitDataRegister->total_constituted_value = 0;
    $receivableUnitDataRegister->reversal_value = $totalValue;

    $receivableUnit = (new ReceivableUnitRegisterService())
        ->store($receivableUnitDataRegister);

    $receivableUnitOrder = $receivableUnitOrder->fresh();

    expect(0)
        ->toBe($receivableUnit->total_constituted_value);

    expect($totalValue)
        ->toBe($receivableUnitOrder->reversal_value);

    expect(Balance::first()->value)
        ->toBe($receivableUnitOrder->debit_value);

});

test('Refunded Partial Receivable Unit', function () {
    $receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
        'total_gross_value' => 10000,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->holder_document_number,
    ]);

    $valueOrderInitial = 2500;

    $receivableUnitOrders = ReceivableUnitOrder::factory()->count(4)->create([
        'receivable_unit_id' => $receivableUnit->id,
        'gross_value' => $valueOrderInitial,
        'value' => $valueOrderInitial,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'anticipation_value' => 0,
        'debit_value' => 0,
    ]);

    $dataRegisterReceivableUnit = new ReceivableUnitDataRegister();
    $receivableUnitRegisterService = new ReceivableUnitRegisterService();

    $reversalValue = 500;

    $dataRegisterReceivableUnit->addData([
        'external_id' => $receivableUnitOrders->first()->external_order_id,
        'final_recipient_document_number' => $receivableUnit->holder_document_number,
        'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => $reversalValue,
        'settlement_date' => $receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => 0,
        'guarantee_value' => 0,
    ]);

    $receivableUnitRegisterService->store($dataRegisterReceivableUnit);

    $receivableUnitOrders = $receivableUnitOrders->fresh();
    $receivableUnit->refresh();

    expect($receivableUnitOrders->first()->reversal_value)->toBe($valueOrderInitial);
    expect($receivableUnitOrders->first()->gross_value)->toBe($valueOrderInitial);
    expect($receivableUnitOrders->first()->value)->toBe(0);
    expect($receivableUnit->total_constituted_value)->toBe(10000 - $receivableUnitOrders->first()->reversal_value);

});

test('Test Partial Reversals And Total', function () {
    $receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 10000,
        'total_gross_value' => 10000,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->holder_document_number,
    ]);

    $valueOrderInitial = 2500;

    $receivableUnitOrders = ReceivableUnitOrder::factory()->count(4)->create([
        'receivable_unit_id' => $receivableUnit->id,
        'gross_value' => $valueOrderInitial,
        'value' => $valueOrderInitial,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'anticipation_value' => 0,
        'debit_value' => 0,
    ]);

    $dataRegisterReceivableUnit = new ReceivableUnitDataRegister();
    $receivableUnitRegisterService = new ReceivableUnitRegisterService();

    $reversalValue = $valueOrderInitial;

    $dataRegisterReceivableUnit->addData([
        'external_id' => $receivableUnitOrders->last()->external_order_id,
        'final_recipient_document_number' => $receivableUnit->holder_document_number,
        'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 0,
        'reversal_value' => $reversalValue,
        'settlement_date' => $receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => 0,
        'guarantee_value' => 0,
    ]);

    $receivableUnitRegisterService->store($dataRegisterReceivableUnit);

    $receivableUnitOrders = $receivableUnitOrders->fresh();
    $receivableUnit->refresh();

    expect($receivableUnitOrders->last()->reversal_value)->toBe($reversalValue);
    expect($receivableUnitOrders->last()->gross_value)->toBe($valueOrderInitial);
    expect($receivableUnitOrders->last()->value)->toBe($valueOrderInitial - $reversalValue);
    expect($receivableUnit->total_constituted_value)->toBe(10000 - $receivableUnitOrders->last()->reversal_value);

    $dataRegisterReceivableUnit->addData([
        'external_id' => $receivableUnitOrders->last()->external_order_id,
        'final_recipient_document_number' => $receivableUnit->holder_document_number,
        'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
        'pre_contracted_value' => 0,
        'total_gross_value' => 0,
        'blocked_value' => 77,
        'reversal_value' => $reversalValue,
        'settlement_date' => $receivableUnit->settlement_date,
        'paid_at' => null,
        'value' => 0,
        'guarantee_value' => 0,
    ]);

    $receivableUnitRegisterService->store($dataRegisterReceivableUnit);

    $receivableUnitOrders = $receivableUnitOrders->fresh();
    $receivableUnit->refresh();

    expect($receivableUnitOrders->last()->reversal_value)->toBe($reversalValue);
    expect($receivableUnitOrders->last()->value)->toBe($valueOrderInitial - $reversalValue);
    expect($receivableUnitOrders->last()->gross_value)->toBe($valueOrderInitial);
    expect($receivableUnit->total_constituted_value)->toBe(10000 - $receivableUnitOrders->last()->reversal_value);
    expect($receivableUnit->payments->sum('payment_value'))->toBe(10000 - $receivableUnitOrders->last()->reversal_value);
    expect(Balance::all()->count())->toBe(0);

});

test('Test Reversal Value In Receivable Unit Order Anticipated', function () {
    $receivableUnit = ReceivableUnit::factory()->create([
        'total_constituted_value' => 1000,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => 2000,
        'holder_document_number' => $receivableUnit->holder_document_number
    ]);

     $receivableUnitOrders = ReceivableUnitOrder::factory()->count(2)->create([
        'receivable_unit_id' => $receivableUnit->id,
        'gross_value' => 1000,
        'value' => 1000,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'anticipation_value' => 0,
        'debit_value' => 0,
    ]);

     $anticipation = Anticipation::factory()->create();

     $receivableUnitOrders->first()->update([
         'anticipation_id' => $anticipation->id,
         'anticipation_value' => 1000
     ]);

    (new ReceivableUnitRegisterService())->store(
        (new ReceivableUnitDataRegister())->addData(
            [
                'external_id' => $receivableUnitOrders->first()->external_order_id,
                'final_recipient_document_number' => $receivableUnit->holder_document_number,
                'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
                'pre_contracted_value' => 0,
                'total_gross_value' => 0,
                'blocked_value' => 0,
                'reversal_value' => 1000,
                'settlement_date' => $receivableUnit->settlement_date,
                'paid_at' => null,
                'value' => 0,
                'guarantee_value' => 0,
            ]
        )
    );

    $receivableUnitOrders = $receivableUnitOrders->fresh();
    $receivableUnit->refresh();

    expect($receivableUnit->total_constituted_value)->toBe(1000);
    expect($receivableUnitOrders->first()->anticipation_value)->toBe(1000);
    expect($receivableUnitOrders->first()->anticipation_value)->toBe(1000);

});

