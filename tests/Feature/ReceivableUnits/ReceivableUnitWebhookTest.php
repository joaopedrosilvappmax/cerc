<?php

namespace Tests\Feature;

use Tests\Stub\ReceivableUnitWebhookMock;

test('Update Receivable Unit integrated_at from Webhook', function() {
    $data = ReceivableUnitWebhookMock::getWebhookData();

    $this->withHeaders([
        'Authorization' => config('cerc_ap008.webhook_token')
    ])->post(route('webhook.store'), $data);

    $this->assertTrue(true);
});
