<?php

namespace Tests\Feature;

use App\Models\Entities\Company;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Anticipations\AnticipationRegisterService;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use App\Support\EnumTypes\ClassBindResolution;
use Tests\Stub\AnticipationDataRegisterMock;

test('Receivable Anticipation In Receivable Unit Order', function () {
    $receivableUnit = ReceivableUnit::factory()->create([
        'holder_document_number' => 72861461000170,
        'code_arrangement_payment' => "VCC",
        'settlement_date' => '2021-12-07',
    ]);

    Company::factory()->create([
        'document_number' => $receivableUnit->holder_document_number,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
    ]);

    ReceivableUnitOrder::factory()->count(3)->create([
        'receivable_unit_id' => $receivableUnit->id,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'anticipation_value' => 0,
        'value' => 20000,
    ]);

    $receivableUnitOrder = ReceivableUnitOrder::factory()->create([
        'external_order_id' => 77,
        'receivable_unit_id' => $receivableUnit->id,
        'reversal_value' => 0,
        'guarantee_value' => 0,
        'anticipation_value' => 0,
        'value' => 10000,
    ]);

    $anticipationData = AnticipationDataRegisterMock::getInitialData();

    $anticipationData['external_order_id'] = $receivableUnitOrder->external_order_id;
    $anticipationData['holder_document_number'] = $receivableUnit->holder_document_number;
    $anticipationData['final_recipient_document_number'] = $receivableUnit->holder_document_number;
    $anticipationData['code_arrangement_payment'] = $receivableUnit->code_arrangement_payment;
    $anticipationData['settlement_date'] = $receivableUnit->settlement_date;
    $anticipationData['value'] = $receivableUnitOrder->value;

    $data = app(DataRegisterContract::class, [
        'class' => ClassBindResolution::ANTICIPATION_DATA
    ])->addData($anticipationData);

    $anticipationRegisterService = new AnticipationRegisterService();

    $anticipationRegisterService->store($data);

    $receivableUnitOrder = $receivableUnitOrder->fresh();

    expect(0)->toBe($receivableUnitOrder->value);
});

