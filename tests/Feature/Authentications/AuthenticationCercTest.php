<?php

namespace Tests\Feature;

use App\Models\Services\Authentications\AuthenticationService;
use App\Models\Services\Contracts\AuthenticationServiceContract;
use Illuminate\Support\Facades\Http;
use Tests\Stub\AuthenticationMock;

beforeEach(function() {
    $this->authenticationService = app(AuthenticationServiceContract::class);

    $this->authenticationResponse = AuthenticationMock::getAuthenticationResponse();
});

test('Bind Resolution Authentication Service CERC', function () {

    expect(get_class($this->authenticationService))->toBe(AuthenticationService::class);
});


test('Authenticate CERC', function () {

    Http::fake([
        config('cerc.api_url') . '*' => Http::response($this->authenticationResponse, 207, []),
    ]);

    $token = $this->authenticationService->getToken();

    expect($token)
        ->toBe("{$this->authenticationResponse['token_type']} {$this->authenticationResponse['access_token']}");
});
