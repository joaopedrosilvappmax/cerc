<?php

namespace Tests\Feature;

use App\Models\Entities\Company;
use App\Models\Services\Companies\CompanyRegisterService;
use App\Models\Services\Contracts\RegisterServiceContract;
use App\Support\EnumTypes\ClassBindResolution;
use App\Support\DataTransferObjects\Companies\CompanyDataRegister;
use App\Support\DataTransferObjects\Contracts\DataRegisterContract;
use Tests\Stub\CompanyDataRegisterMock;

test('Bind Resolution Company', function () {
    $companyDataRegister = app(DataRegisterContract::class, [
        'class' => ClassBindResolution::COMPANY_DATA
    ]);

    expect(get_class($companyDataRegister))->toBe(CompanyDataRegister::class);

    $companyRegisterService = app(RegisterServiceContract::class, [
        'class' => ClassBindResolution::COMPANY_SERVICE
    ]);

    expect(get_class($companyRegisterService))->toBe(CompanyRegisterService::class);
});


test('Mount Company Data', function () {

    $data = CompanyDataRegisterMock::getInitialData();

    $companyRegisterData = (new CompanyDataRegister())->addData($data);

    foreach ($companyRegisterData->toArray() as $k => $d) {

        expect($data[$k])->toEqual($d);
    }
});

test('Store Company', function () {

    $companyRegisterData = (new CompanyDataRegister())
        ->addData(CompanyDataRegisterMock::getInitialData());

    $company = (new CompanyRegisterService())->store($companyRegisterData);

    expect(get_class($company))->toBe(Company::class);
});
