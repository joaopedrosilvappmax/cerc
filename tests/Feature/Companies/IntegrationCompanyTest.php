<?php

namespace Tests\Feature;

use App\Models\Entities\Authentication;
use App\Models\Entities\Company;
use App\Models\Services\Companies\CompanyIntegrateService;
use App\Support\EnumTypes\IntegrationResponseStatus;
use Illuminate\Support\Facades\Http;
use Tests\Stub\IntegrationResponseDataMock;

beforeEach(function() {
    Authentication::factory()->create();

    $this->company = Company::factory()->create();
});

test('Success Integrate Company', function () {
    Http::fake([
        config('cerc.api_url') . '*' => Http::response([
            IntegrationResponseDataMock::getSuccessResponse($this->company),
        ], 207, []),
    ]);

    $response = (new CompanyIntegrateService)->send($this->company);

    expect($response->getStatus())->toBe(IntegrationResponseStatus::SUCCESS);

    $this->company->refresh();

    expect($this->company->integrated_at)->not()->toBeNull();
});

test('Error Invalid Operation Integrate Company', function () {
    Http::fake([
        config('cerc.api_url') . '*' => Http::response(
            IntegrationResponseDataMock::getErrorOperationInvalidResponse(
                $this->company,
                IntegrationResponseStatus::ESTABLISHMENT_INVALID_OPERATION
            ),
            207,
            []
        ),
    ]);

    $response = (new CompanyIntegrateService)->send($this->company);

    expect($response->getStatus())->toBe(IntegrationResponseStatus::FAILED);

    $this->company->refresh();

    expect($this->company->integrated_at)->not()->toBeNull();
});

test('Error Exists Integrate Company', function () {
    Http::fake([
        config('cerc.api_url') . '*' => Http::response(
            IntegrationResponseDataMock::getErrorExistsInformationResponse(
                $this->company,
                IntegrationResponseStatus::ESTABLISHMENT_CREATED
            ),
            207,
            []
        ),
    ]);

    $response = (new CompanyIntegrateService)->send($this->company);

    expect($response->getStatus())->toBe(IntegrationResponseStatus::FAILED);

    $this->company->refresh();

    expect($this->company->integrated_at)->not()->toBeNull();
});

test('Error Integrate Company', function () {
    Http::fake([
        config('cerc.api_url') . '*' => Http::response(
            IntegrationResponseDataMock::getErrorResponse($this->company),
            207,
            []
        ),
    ]);

    $response = (new CompanyIntegrateService)->send($this->company);

    expect($response->getStatus())->toBe(IntegrationResponseStatus::FAILED);

    $this->company->refresh();

    expect($this->company->integrated_at)->toBeNull();
});

