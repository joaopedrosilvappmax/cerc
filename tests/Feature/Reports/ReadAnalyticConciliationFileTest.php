<?php

namespace Tests\Feature\Reports;

use App\Models\Entities\AnalyticConciliationRecord;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Reports\ReadAnalyticConciliationService;
use App\Support\EnumTypes\OperationType;
use Carbon\Carbon;

test('Analytic Conciliation - Get Mounted Filename', function () {
    $date = Carbon::createFromFormat('Y-m-d', '2021-01-01');

    $fileName = (new ReadAnalyticConciliationService())
        ->getMountedFileName($date);

    expect($fileName)->toBe('CERC-AP009_27000511_20210101_0000001_ret.csv');

});

test('Analytic Conciliation - Check Divergent Values', function () {

    $receivableUnit = ReceivableUnit::factory()->create([
        'operation_type' => OperationType::WRITE_OFF,
        'total_constituted_value' => 1000,
        'pre_contracted_value' => 1000,
        'total_gross_value' => 1000
    ]);

    $conciliationRecord = AnalyticConciliationRecord::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'total_constituted_value' => 2000,
        'pre_contracted_value' => 2000,
        'total_gross_value' => 2000
    ]);

    $columns = config('cerc_ap009.file_columns');

    $line = [
        $columns['reference_external_id'] => $receivableUnit->id,
        $columns['reference_date'] => Carbon::now()->format('Y-m-d'),
        $columns['accrediting_company_document_number'] => config('accrediting_company.appmax_document_number'),
        $columns['final_recipient_document_number'] => $receivableUnit->final_recipient_document_number,
        $columns['code_arrangement_payment'] => $receivableUnit->code_arrangement_payment,
        $columns['settlement_date'] => $receivableUnit->settlement_date,
        $columns['holder_document_number'] => $receivableUnit->holder_document_number,
        $columns['total_constituted_value'] => $receivableUnit->total_constituted_value,
        $columns['pre_contracted_value'] => $receivableUnit->pre_contracted_value,
        $columns['total_gross_value'] => $receivableUnit->total_gross_value,
        $columns['anticipation_not_settled_value'] => 0,
    ];

    $result = (new ReadAnalyticConciliationService())
        ->checkDivergenceInValues($conciliationRecord, $line, $columns);

    expect($result)->toBeTrue();

});


test('Analytic Conciliation - Get Receivable Unit To Verify', function () {
    $receivableUnit = ReceivableUnit::factory()->create();

    $receivableUnitFromAnalyticConciliationService = (new ReadAnalyticConciliationService())
        ->getReceivableUnit($receivableUnit->id);

    $this->assertEquals(
        $receivableUnit->total_constituted_value,
        $receivableUnitFromAnalyticConciliationService->total_constituted_value
    );
});

test('Analytic Conciliation - Create Not Settled', function () {
    $receivableUnit = ReceivableUnit::factory()->create([
        'operation_type' => OperationType::WRITE_OFF
    ]);

    $columns = config('cerc_ap009.file_columns');

    $line = [
        $columns['reference_external_id'] => $receivableUnit->id,
        $columns['reference_date'] => Carbon::now()->format('Y-m-d'),
        $columns['accrediting_company_document_number'] => config('accrediting_company.appmax_document_number'),
        $columns['final_recipient_document_number'] => $receivableUnit->final_recipient_document_number,
        $columns['code_arrangement_payment'] => $receivableUnit->code_arrangement_payment,
        $columns['settlement_date'] => $receivableUnit->settlement_date,
        $columns['holder_document_number'] => $receivableUnit->holder_document_number,
        $columns['total_constituted_value'] => $receivableUnit->total_constituted_value,
        $columns['pre_contracted_value'] => $receivableUnit->pre_contracted_value,
        $columns['total_gross_value'] => $receivableUnit->total_gross_value,
        $columns['anticipation_not_settled_value'] => 0,
    ];

    $analyticConciliationRecord = (new ReadAnalyticConciliationService())
        ->createNotSettledRecord($line, $columns);

    expect(get_class($analyticConciliationRecord))->toBe(AnalyticConciliationRecord::class);

});

