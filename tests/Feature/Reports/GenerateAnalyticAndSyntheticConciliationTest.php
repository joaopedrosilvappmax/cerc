<?php

namespace Tests\Feature;

use App\Models\Entities\AnalyticConciliationRecord;
use App\Models\Entities\Anticipation;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\SyntheticConciliationRecord;
use App\Models\Services\Reports\GenerateAnalyticConciliationFromReceivableUnitValueService;
use App\Models\Services\Reports\GenerateSyntheticConciliationService;
use App\Support\EnumTypes\PaymentArrangement;
use Carbon\Carbon;

test('Generate Analytic Conciliation', function (
    $firstReceivableUnitsValues,
    $secondReceivableUnitsValues,
    $counter,
    $date
) {
    ReceivableUnit::factory()->count($counter)->create([
        'created_at' => $date->copy()->subDay()->startOfDay(),
        'code_arrangement_payment' => PaymentArrangement::MASTERCARD_ARRANGEMENT,
        'total_constituted_value' => $firstReceivableUnitsValues['total_constituted_value'],
        'total_gross_value' => $firstReceivableUnitsValues['total_gross_value'],
        'pre_contracted_value' => $firstReceivableUnitsValues['pre_contracted_value'],
        'integrated_at' => Carbon::now()
    ]);

    ReceivableUnit::factory()->count($counter)->create([
        'created_at' => $date->copy()->subDay()->startOfDay(),
        'code_arrangement_payment' => PaymentArrangement::VISA_ARRANGEMENT,
        'total_constituted_value' => $secondReceivableUnitsValues['total_constituted_value'],
        'total_gross_value' => $secondReceivableUnitsValues['total_gross_value'],
        'pre_contracted_value' => $secondReceivableUnitsValues['pre_contracted_value'],
        'integrated_at' => Carbon::now()
    ]);

    (new GenerateAnalyticConciliationFromReceivableUnitValueService())->process();

    $records = AnalyticConciliationRecord::all();

    $receivableUnitMasterCreditCard = $records->filter(function ($record) {
        return $record->code_arrangement_payment == PaymentArrangement::MASTERCARD_ARRANGEMENT;
    });

    $sumTotalGrossValueMasterCreditCard = $receivableUnitMasterCreditCard->sum('total_gross_value');
    $sumTotalConstitutedValueMasterCreditCard = $receivableUnitMasterCreditCard->sum('total_constituted_value');
    $sumPreContractedValueMasterCreditCard = $receivableUnitMasterCreditCard->sum('pre_contracted_value');

    $receivableUnitVisaCreditCard = $records->filter(function ($record) {
        return $record->code_arrangement_payment == PaymentArrangement::VISA_ARRANGEMENT;
    });

    $sumTotalGrossValueVisaCreditCard = $receivableUnitVisaCreditCard->sum('total_gross_value');
    $sumTotalConstitutedValueVisaCreditCard = $receivableUnitVisaCreditCard->sum('total_constituted_value');
    $sumPreContractedValueVisaCreditCard = $receivableUnitVisaCreditCard->sum('pre_contracted_value');

    expect($receivableUnitMasterCreditCard->sum('total_gross_value'))
        ->toBe($firstReceivableUnitsValues['total_gross_value'] * $counter);

    expect($receivableUnitMasterCreditCard->sum('total_constituted_value'))
        ->toBe($firstReceivableUnitsValues['total_constituted_value'] * $counter);

    expect($receivableUnitMasterCreditCard->sum('pre_contracted_value'))
        ->toBe($firstReceivableUnitsValues['pre_contracted_value'] * $counter);


    expect($receivableUnitVisaCreditCard->sum('total_gross_value'))
        ->toBe($secondReceivableUnitsValues['total_gross_value'] * $counter);

    expect($receivableUnitVisaCreditCard->sum('total_constituted_value'))
        ->toBe($secondReceivableUnitsValues['total_constituted_value'] * $counter);

    expect($receivableUnitVisaCreditCard->sum('pre_contracted_value'))
        ->toBe($secondReceivableUnitsValues['pre_contracted_value'] * $counter);


    $syntheticConciliationService = new GenerateSyntheticConciliationService();

    $syntheticConciliationService->process();

    $records = SyntheticConciliationRecord::all();

    $recordMaster = $records->filter(function ($record) {
        return $record->code_arrangement_payment == PaymentArrangement::MASTERCARD_ARRANGEMENT;
    });

    $recordVisa = $records->filter(function ($record) {
        return $record->code_arrangement_payment == PaymentArrangement::VISA_ARRANGEMENT;
    });

    expect($recordMaster->sum('total_gross_value'))
        ->toBe($sumTotalGrossValueMasterCreditCard);

    expect($recordMaster->sum('total_constituted_value'))
        ->toBe($sumTotalConstitutedValueMasterCreditCard);

    expect($recordMaster->sum('pre_contracted_value'))
        ->toBe($sumPreContractedValueMasterCreditCard);


    expect($recordVisa->sum('total_gross_value'))
        ->toBe($sumTotalGrossValueVisaCreditCard);

    expect($recordVisa->sum('total_constituted_value'))
        ->toBe($sumTotalConstitutedValueVisaCreditCard);

    expect($recordVisa->sum('pre_contracted_value'))
        ->toBe($sumPreContractedValueVisaCreditCard);

})->with([
    [
        [
            'total_constituted_value' => 1000,
            'total_gross_value' => 2000,
            'pre_contracted_value' => 1000
        ],
        [
            'total_constituted_value' => 1200,
            'total_gross_value' => 2312,
            'pre_contracted_value' => 1200
        ],
        5,
        Carbon::now()
    ]
]);
