<?php

namespace Tests\Feature;

use App\Models\Entities\Authentication;
use App\Models\Entities\SyntheticConciliationRecord;
use App\Models\Services\Abstracts\IntegrationServiceAbstract;
use App\Models\Services\Reports\SyntheticConciliationIntegrateService;
use App\Support\EnumTypes\IntegrationResponseStatus;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Tests\Stub\IntegrationResponseDataMock;

test('Success Integrate Synthetic Conciliation', function () {
    Authentication::factory()->create();

    $syntheticConciliationRecord = SyntheticConciliationRecord::factory()->create([
        'reference_date' => Carbon::now()->subDay()
    ]);

    Http::fake([
        config('cerc.api_url') . '*' => Http::response([
            IntegrationResponseDataMock::getSuccessResponse($syntheticConciliationRecord),
        ], 207, []),
    ]);

    (new SyntheticConciliationIntegrateService())->send();

    $syntheticConciliationRecord->refresh();

    expect($syntheticConciliationRecord->integrated_at)->not()->toBeNull();

});

test('Error Invalid Operation Integrate Synthetic Conciliation', function () {
    Authentication::factory()->create();

    $syntheticConciliationRecord = SyntheticConciliationRecord::factory()->create([
        'reference_date' => Carbon::now()->subDay()
    ]);

    Http::fake([
        config('cerc.api_url') . '*' => Http::response(
            IntegrationResponseDataMock::getErrorOperationInvalidResponse(
                $syntheticConciliationRecord,
                IntegrationResponseStatus::SCHEDULE_CONCILIATION_INVALID_OPERATION
            ),
            207,
            []
        ),
    ]);

    (new SyntheticConciliationIntegrateService())->send();

    $syntheticConciliationRecord->refresh();

    expect($syntheticConciliationRecord->integrated_at)->not()->toBeNull();

});
