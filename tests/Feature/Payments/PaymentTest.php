<?php

namespace Tests\Feature;

use App\Models\Entities\Payment;

test('SoftDelete Payments', function () {

    $payment = Payment::factory()->create();

    Payment::destroy([$payment->id]);

    $paymentCount = Payment::onlyTrashed()->where('id', $payment->id)->count();

    expect($paymentCount)->toBe(1);

});
