<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\CashOutPayment;
use App\Models\Entities\Company;
use App\Models\Entities\ContractEffect;
use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Entities\ReceivableUnitOrder;
use App\Models\Services\Anticipations\AnticipationRegisterService;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\PaymentGetTotalValueServiceContract;
use App\Models\Services\Payments\PaymentGetTotalValueNoSettledHolderChangeService;
use App\Models\Services\Payments\PaymentGetTotalValueNoSettledNoHolderService;
use App\Models\Services\Payments\PaymentGetTotalValueSettledHolderChangeService;
use App\Models\Services\Payments\PaymentGetTotalValueSettledNoHolderService;
use App\Support\DataTransferObjects\Anticipations\AnticipationDataRegister;
use App\Support\EnumTypes\CashOutPaymentType;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\Math;
use App\Support\EnumTypes\PaymentTotalValueCalculateType;
use Carbon\Carbon;

test('Bind Resolution Payment Get Total Value', function () {
    $paymentGetTotalValueService = app(PaymentGetTotalValueServiceContract::class, [
        'calculate_type' => PaymentTotalValueCalculateType::NO_HOLDER
    ]);

    expect(get_class($paymentGetTotalValueService))->toBe(PaymentGetTotalValueNoSettledNoHolderService::class);

    $paymentGetTotalValueService = app(PaymentGetTotalValueServiceContract::class, [
        'calculate_type' => PaymentTotalValueCalculateType::SETTLED_NO_HOLDER
    ]);

    expect(get_class($paymentGetTotalValueService))->toBe(PaymentGetTotalValueSettledNoHolderService::class);

    $paymentGetTotalValueService = app(PaymentGetTotalValueServiceContract::class, [
        'calculate_type' => PaymentTotalValueCalculateType::SETTLED_HOLDER_CHANGE
    ]);

    expect(get_class($paymentGetTotalValueService))->toBe(PaymentGetTotalValueSettledHolderChangeService::class);

    $paymentGetTotalValueService = app(PaymentGetTotalValueServiceContract::class, [
        'calculate_type' => PaymentTotalValueCalculateType::NO_SETTLED_HOLDER_CHANGE
    ]);

    expect(get_class($paymentGetTotalValueService))->toBe(PaymentGetTotalValueNoSettledHolderChangeService::class);

})->group('bind-resolution');

test('Payment Get Total Value No Settled And No Holder - Error', function () {

    $paymentValueNoHolder = 3000;

    $receivableUnit = ReceivableUnit::factory()->create([
        'settlement_date' => Carbon::now()->subHour()->format('Y-m-d')
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => 7000
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $paymentValueNoHolder
    ]);

    $total = (new PaymentGetTotalValueNoSettledNoHolderService())->getTotal($receivableUnit->holder_document_number);

    expect($paymentValueNoHolder)->not()->toBe($total);
});

test('Payment Get Total Value Settled And No Holder - Error', function () {

    $paymentValueNoHolder = 2000;

    $receivableUnit = ReceivableUnit::factory()->create([
        'settlement_date' => Carbon::now()->format('Y-m-d')
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => 8000
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $paymentValueNoHolder
    ]);

    $total = (new PaymentGetTotalValueSettledNoHolderService())->getTotal($receivableUnit->holder_document_number);

    expect($paymentValueNoHolder)->toBe($total);

});


test('Payment Get Total Value Settled And No Holder Diff In Hours', function () {

    $paymentValueNoHolder = 2000;

    $receivableUnit = ReceivableUnit::factory()->create([
        'settlement_date' => Carbon::now()->subHour()->format('Y-m-d')
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => 8000
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $paymentValueNoHolder
    ]);

    $total = (new PaymentGetTotalValueSettledNoHolderService())->getTotal($receivableUnit->holder_document_number);

    expect($paymentValueNoHolder)->toBe($total);

});


test('Payment Get Total Value No Settled And No Holder', function () {

    $paymentValueNoHolder = 3000;

    $receivableUnit = ReceivableUnit::factory()->create([
        'settlement_date' => Carbon::now()->addDay()->format('Y-m-d')
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => 7000
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $paymentValueNoHolder
    ]);

    $total = (new PaymentGetTotalValueNoSettledNoHolderService())->getTotal($receivableUnit->holder_document_number);

    expect($paymentValueNoHolder)->toBe($total);

});


test('Payment Get Total Value Settled And No Holder', function () {

    $paymentValueNoHolder = 2000;

    $receivableUnit = ReceivableUnit::factory()->create([
        'settlement_date' => Carbon::now()->subDay()->format('Y-m-d')
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => 8000
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $paymentValueNoHolder
    ]);

    $total = (new PaymentGetTotalValueSettledNoHolderService())->getTotal($receivableUnit->holder_document_number);

    expect($paymentValueNoHolder)->toBe($total);

});

test('Payment Get Total Value No Settled With Holder Change And Holder Not Exist', function () {

    ReceivableUnit::factory()->count(3)->create();

    Payment::factory()->count(3)->create();

    $totalNoSettled = (new PaymentGetTotalValueNoSettledHolderChangeService())->getTotal(72869959000160);
    $totalSettled = (new PaymentGetTotalValueSettledHolderChangeService())->getTotal(72869959000160);

    expect($totalNoSettled)->toBe(0);
    expect($totalSettled)->toBe(0);

});

test('Payment Get Total Value No Settled With Holder Change', function () {

    $totalConstitutedValue = 1000;

    $contractEffectFixed = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => $totalConstitutedValue,
            'settlement_date' => Carbon::now()->addMonth(),
        ]);

    $contractEffectPercentage = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 5000,
            'settlement_date' => Carbon::now()->addMonth(),
            'division_rule' => DivisionRule::PERCENTAGE_VALUE
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectFixed->effect_type
    ]);

    $receivableUnit = ReceivableUnit::factory()
        ->create([
            'id' => 1,
            'receivable_unit_linked_id' => null,
            'final_recipient_document_number' => $contractEffectFixed->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffectFixed->code_arrangement_payment,
            'settlement_date' => $contractEffectFixed->settlement_date,
            'total_constituted_value' => 9000
        ]);

    ReceivableUnit::factory()->count(3)->create();

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->final_recipient_document_number
    ]);

    Payment::factory()->count(3)->create();

    $effectService->apply($contractEffectFixed, $receivableUnit);

    $paymentGetTotalValueService = new PaymentGetTotalValueNoSettledHolderChangeService();

    $total = $paymentGetTotalValueService->getTotal($receivableUnit->holder_document_number);

    expect($contractEffectFixed->committed_value)->toBe($total);

    $receivableUnit = $receivableUnit->fresh();

    $totalPayment = $totalConstitutedValue +
        (int) ceil($receivableUnit->total_constituted_value * ($contractEffectPercentage->committed_value * MATH::DIVISION_PERCENTAGE_INTEGER));

    $effectService->apply($contractEffectPercentage, $receivableUnit);

    $total = $paymentGetTotalValueService->getTotal($receivableUnit->holder_document_number);

    expect($totalPayment)->toBe($total);

    $contractEffectFixed = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => $totalConstitutedValue,
            'settlement_date' => Carbon::now()->addMonth(1),
        ]);

    $newContractEffectFixed = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 8000,
            'settlement_date' => Carbon::now()->addMonth(1),
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectFixed->effect_type
    ]);

    $newReceivableUnit = ReceivableUnit::factory()
        ->create([
            'receivable_unit_linked_id' => null,
            'final_recipient_document_number' => $newContractEffectFixed->final_recipient_document_number,
            'code_arrangement_payment' => $newContractEffectFixed->code_arrangement_payment,
            'settlement_date' => $newContractEffectFixed->settlement_date,
            'total_constituted_value' => 9000
        ]);

    Payment::factory()->create([
        'receivable_unit_id' => $newReceivableUnit->id,
        'payment_value' => $newReceivableUnit->total_constituted_value,
        'holder_document_number' => $newReceivableUnit->final_recipient_document_number
    ]);

    $effectService->apply($newContractEffectFixed, $newReceivableUnit);

    $totalOldEffectContract = $paymentGetTotalValueService->getTotal($receivableUnit->holder_document_number);
    $totalNewEffectContract = $paymentGetTotalValueService->getTotal($newReceivableUnit->holder_document_number);

    expect($newContractEffectFixed->committed_value)->toBe($totalNewEffectContract);
    expect($totalOldEffectContract)->toBe($totalPayment);

});

test('Payment Get Total Value Settled With Holder Change', function () {

    $totalConstitutedValue = 1000;

    $contractEffectFixed = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => $totalConstitutedValue,
            'settlement_date' => Carbon::now()->subMonth(1),
        ]);

    $contractEffectPercentage = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 5000,
            'settlement_date' => Carbon::now()->subMonth(1),
            'division_rule' => DivisionRule::PERCENTAGE_VALUE
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectFixed->effect_type
    ]);

    $receivableUnit = ReceivableUnit::factory()
        ->create([
            'id' => 1,
            'receivable_unit_linked_id' => null,
            'final_recipient_document_number' => $contractEffectFixed->final_recipient_document_number,
            'code_arrangement_payment' => $contractEffectFixed->code_arrangement_payment,
            'settlement_date' => $contractEffectFixed->settlement_date,
            'total_constituted_value' => 9000
        ]);

    ReceivableUnit::factory()->count(3)->create();

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->final_recipient_document_number
    ]);

    Payment::factory()->count(3)->create();

    $effectService->apply($contractEffectFixed, $receivableUnit);

    $paymentGetTotalValueService = new PaymentGetTotalValueSettledHolderChangeService();

    $total = $paymentGetTotalValueService->getTotal($receivableUnit->holder_document_number);

    expect($contractEffectFixed->committed_value)->toBe($total);

    $receivableUnit = $receivableUnit->fresh();

    $totalPayment = $totalConstitutedValue +
        (int) ceil($receivableUnit->total_constituted_value * ($contractEffectPercentage->committed_value * MATH::DIVISION_PERCENTAGE_INTEGER));

    $effectService->apply($contractEffectPercentage, $receivableUnit);

    $total = $paymentGetTotalValueService->getTotal($receivableUnit->holder_document_number);

    expect($totalPayment)->toBe($total);

    $contractEffectFixed = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => $totalConstitutedValue,
            'settlement_date' => Carbon::now()->subMonth(1),
        ]);

    $newContractEffectFixed = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 8000,
            'settlement_date' => Carbon::now()->addMonth(1),
        ]);

    $effectService = app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffectFixed->effect_type
    ]);

    $newReceivableUnit = ReceivableUnit::factory()
        ->create([
            'receivable_unit_linked_id' => null,
            'final_recipient_document_number' => $newContractEffectFixed->final_recipient_document_number,
            'code_arrangement_payment' => $newContractEffectFixed->code_arrangement_payment,
            'settlement_date' => $newContractEffectFixed->settlement_date,
            'total_constituted_value' => 9000
        ]);

    Payment::factory()->create([
        'receivable_unit_id' => $newReceivableUnit->id,
        'payment_value' => $newReceivableUnit->total_constituted_value,
        'holder_document_number' => $newReceivableUnit->final_recipient_document_number
    ]);

    $effectService->apply($newContractEffectFixed, $newReceivableUnit);

    $totalOldEffectContract = $paymentGetTotalValueService->getTotal($receivableUnit->holder_document_number);
    $totalNewEffectContract = $paymentGetTotalValueService->getTotal($newReceivableUnit->holder_document_number);

    expect($totalNewEffectContract)->toBe(0);
    expect($totalPayment)->toBe($totalOldEffectContract);

});

test('Payment Get Total Value Settled And No Holder With CashOutPayment', function () {

    $paymentValueNoHolder = 2000;

    $receivableUnit = ReceivableUnit::factory()->create([
        'settlement_date' => Carbon::now()->subDay()->format('Y-m-d')
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => 8000
    ]);

    $paymentNoHolder = Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $paymentValueNoHolder
    ]);

    $newPaymentsNoHolder = Payment::factory()->count(2)->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => 1000
    ]);

    $paymentValueNoHolder += $newPaymentsNoHolder->sum('payment_value');

    $cashOutValue = 1000;

    CashOutPayment::factory()->create([
       'receivable_unit_id' => $receivableUnit->id,
       'payment_id' => $paymentNoHolder->id,
       'value' => $cashOutValue,
       'type' => CashOutPaymentType::SETTLED
    ]);

    $paymentGetTotalValueService = new PaymentGetTotalValueSettledNoHolderService();

    $total = $paymentGetTotalValueService->getTotal($receivableUnit->holder_document_number);

    expect($total)->toBe($paymentValueNoHolder - $cashOutValue);

});

test('Payment Get Total Value No Settled Holder Change With CashOutPayment', function () {
    Company::factory()->create();

    $receivableUnit = ReceivableUnit::factory()
        ->create([
            'id' => 1,
            'total_constituted_value' => 10000,
            'settlement_date' => Carbon::now()->addMonth()->toDateString()
        ]);

    $receivableUnitOrders = ReceivableUnitOrder::factory()
        ->count(5)
        ->create([
            'receivable_unit_id' => $receivableUnit->id,
            'gross_value' => 2000,
            'value' => 2000,
            'reversal_value' => 0,
            'guarantee_value' => 0,
        ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'payment_value' => $receivableUnit->total_constituted_value,
        'holder_document_number' => $receivableUnit->holder_document_number,
    ]);

    $contractEffect = ContractEffect::factory()
        ->create([
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 5000,
            'division_rule' => DivisionRule::FIXED_VALUE,
            'final_recipient_document_number' => $receivableUnit->final_recipient_document_number,
            'code_arrangement_payment' => $receivableUnit->code_arrangement_payment,
            'settlement_date' => $receivableUnit->settlement_date,
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $receivableUnit);

    $anticipation = Anticipation::factory()->create();

    $receivableUnitOrders->first()->update([
        'anticipation_id' => $anticipation->id,
        'anticipation_value' => $receivableUnitOrders->first()->value
    ]);

    expect(4000)
        ->toBe((new (PaymentGetTotalValueNoSettledHolderChangeService::class))->getTotal($receivableUnit->holder_document_number));
})->group('payment-total-holder-change');

