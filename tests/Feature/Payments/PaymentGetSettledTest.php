<?php

namespace Tests\Feature;

use App\Models\Entities\ContractEffect;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\ContractEffectServiceContract;
use App\Models\Services\Contracts\PaymentGetSettledServiceContract;
use App\Models\Services\Payments\PaymentGetSettledNoHolderService;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use Carbon\Carbon;

beforeEach(function () {
    $this->receivableUnitSettled = ReceivableUnit::factory()->create([
        'holder_document_number' => '1111111111111',
        'settlement_date' => Carbon::now()->subDay(1)->toDateString(),
        'total_constituted_value' => 10000,
        'final_recipient_document_number' => '1111111111111'
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnitSettled->id,
        'holder_document_number' => $this->receivableUnitSettled->holder_document_number,
        'payment_value' => $this->receivableUnitSettled->total_constituted_value,
        'anticipation_id' => null,
        'paid_at' => null,
    ]);
});

test('Bind Resolution Payment Settled', function () {
    $paymentGetSettledService = app(PaymentGetSettledServiceContract::class);

    expect(get_class($paymentGetSettledService))->toBe(PaymentGetSettledNoHolderService::class);
})->group('bind-resolution');

test('Get Payments Settled', function ($amountPaymentsNoHolder) {
    Payment::factory()->count($amountPaymentsNoHolder)->create([
        'receivable_unit_id' => $this->receivableUnitSettled->id,
        'payment_value' => 200,
        'paid_at' => null,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnitSettled->id,
        'payment_value' => 200,
        'paid_at' => Carbon::now()->subDay(3),
    ]);

    $receivableUnit = ReceivableUnit::factory()->create([
        'holder_document_number' => '1111111111111',
        'settlement_date' => Carbon::now()->addMonth(1),
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'paid_at' => null,
    ]);

    Payment::factory()->count(5)->create([
        'receivable_unit_id' => $receivableUnit->id,
        'anticipation_id' => null,
        'paid_at' => null,
    ]);

   $paymentGetSettledService = app(PaymentGetSettledServiceContract::class);

   $paymentsSettled = $paymentGetSettledService->getPayments(Carbon::now()->subDay(1)->toDateString());

   expect($paymentsSettled->count())->toBe($amountPaymentsNoHolder);
})->with([
    [3]
]);

test('Get Payments Settled Type Holder Change', function () {
    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $this->receivableUnitSettled->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnitSettled->code_arrangement_payment,
            'settlement_date' => $this->receivableUnitSettled->settlement_date,
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 4000,
            'division_rule' => DivisionRule::FIXED_VALUE
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnitSettled);

    $this->assertEquals(2, ReceivableUnit::all()->count());
    $this->assertEquals(2, Payment::all()->count());

    $getSettledNoHolderService = new PaymentGetSettledNoHolderService();

    $payments = $getSettledNoHolderService->getPayments(Carbon::now()->subDay(1)->toDateString());

    expect($payments->count())->toBe(1);
    expect($payments->sum('payment_value'))->toBe(4000);
});


test('Get Payments Settled Onus and Holder Change', function () {
    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $this->receivableUnitSettled->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnitSettled->code_arrangement_payment,
            'settlement_date' => $this->receivableUnitSettled->settlement_date,
            'effect_type' => ContractEffectType::ONUS_FIDUCIARY_ASSIGNMENT,
            'committed_value' => 1000,
            'division_rule' => DivisionRule::FIXED_VALUE
        ]);


    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnitSettled);

    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $this->receivableUnitSettled->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnitSettled->code_arrangement_payment,
            'settlement_date' => $this->receivableUnitSettled->settlement_date,
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 4000,
            'division_rule' => DivisionRule::FIXED_VALUE
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnitSettled);

    $contractEffect = ContractEffect::factory()
        ->create([
            'final_recipient_document_number' => $this->receivableUnitSettled->final_recipient_document_number,
            'code_arrangement_payment' => $this->receivableUnitSettled->code_arrangement_payment,
            'settlement_date' => $this->receivableUnitSettled->settlement_date,
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'committed_value' => 3000,
            'division_rule' => DivisionRule::FIXED_VALUE
        ]);

    app(ContractEffectServiceContract::class, [
        'effect_type' => $contractEffect->effect_type
    ])->apply($contractEffect, $this->receivableUnitSettled);

    expect(ReceivableUnit::all()->count())->toBe(3);
    expect(Payment::all()->count())->toBe(4);

    $getSettledNoHolderService = new PaymentGetSettledNoHolderService();

    $payments = $getSettledNoHolderService->getPayments(Carbon::now()->subDay(1)->toDateString());

    expect($payments->count())->toBe(3);
    expect($payments->sum('payment_value'))->toBe(8000);
});

test('Test Get Payments Greater Than Zero', function () {
    Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnitSettled->id,
        'holder_document_number' => '72861001000161',
        'payment_value' => 0,
        'anticipation_id' => null,
        'paid_at' => null,
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnitSettled->id,
        'holder_document_number' => '72861001000161',
        'payment_value' => 1000,
        'anticipation_id' => null,
        'paid_at' => null,
    ]);

    $paymentsSettled = (new PaymentGetSettledNoHolderService())->getPayments(Carbon::now()->subDay(1)->toDateString());

    expect(Payment::all()->count())->toBe(3);
    expect($paymentsSettled->count())->toBe(1);
})->group('tsa');
