<?php

namespace Tests\Feature;

use App\Models\Entities\Anticipation;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use App\Models\Services\Contracts\PaymentConfirmationServiceContract;
use App\Support\EnumTypes\PaymentConfirmationType;
use Carbon\Carbon;

beforeEach(function () {
    $anticipationTotalConstitutedValue = 5000;
    $receivableUnitTotalConstitutedValue = 10000;

    $this->anticipation = Anticipation::factory()->create([
        'total_constituted_value' => $anticipationTotalConstitutedValue,
        'paid_value' => $anticipationTotalConstitutedValue,
        'total_value' => $anticipationTotalConstitutedValue
    ]);

    $this->receivableUnit = ReceivableUnit::factory()->create([
        'final_recipient_document_number' => $this->anticipation->final_recipient_document_number,
        'holder_document_number'  => $this->anticipation->holder_document_number,
        'code_arrangement_payment' => $this->anticipation->code_arrangement_payment,
        'settlement_date' => $this->anticipation->settlement_date,
        'total_constituted_value' => $receivableUnitTotalConstitutedValue,
        'pre_contracted_value' => $receivableUnitTotalConstitutedValue,
        'total_gross_value' => $receivableUnitTotalConstitutedValue
    ]);

    Payment::factory()->create([
        'receivable_unit_id' => $this->receivableUnit->id,
        'holder_document_number' => $this->anticipation->holder_document_number,
        'payment_value' => 10000
    ]);
});

test('Confirm payment anticipation', function ($externalCashOutId) {
    $payment = Payment::factory([
        'anticipation_id' => $this->anticipation->id,
        'external_cash_out_id' => $externalCashOutId
    ])->create();

    app(PaymentConfirmationServiceContract::class, [
        'class' => PaymentConfirmationType::getBindServiceClass(PaymentConfirmationType::ANTICIPATION)
    ])->confirmPayment($externalCashOutId);

    $payment->refresh();

    expect($payment->paid_at)->not->toBeNull();

    $this->receivableUnit->refresh();

    $payments = $this->receivableUnit->payments;

    expect($payments->count())->toBe(1);
    expect($payments->first()->paid_at)->toBeNull();

})->with([
    [123456]
]);

test('Confirm payment anticipation holder', function ($externalCashOutId) {
    $payment = Payment::factory([
        'anticipation_id' => $this->anticipation->id,
        'external_cash_out_company_id' => $externalCashOutId
    ])->create();

    app(PaymentConfirmationServiceContract::class, [
        'class' => PaymentConfirmationType::getBindServiceClass(PaymentConfirmationType::ANTICIPATION_HOLDER)
    ])->confirmPayment($externalCashOutId);

    $payment->refresh();

    expect($payment->paid_at)->not->toBeNull();

    $this->receivableUnit->refresh();

    $payments = $this->receivableUnit->payments;

    expect($payments->count())->toBe(1);
    expect($payments->first()->paid_at)->toBeNull();

})->with([
    [123456]
]);

test('Confirm payment available', function (
    $externalCashOutId,
    $totalConstitutedValue
) {

    $receivableUnit = ReceivableUnit::factory([
        'total_constituted_value' => $totalConstitutedValue,
        'settlement_date' => Carbon::now()->addDay(),
    ])->create();

    $payment = Payment::factory([
        'receivable_unit_id' => $receivableUnit->id,
        'holder_document_number' => $receivableUnit->holder_document_number,
        'payment_value' => $receivableUnit->total_constituted_value / 2,
        'external_cash_out_id' => $externalCashOutId
    ])->create();

    app(PaymentConfirmationServiceContract::class, [
        'class' => PaymentConfirmationType::getBindServiceClass(PaymentConfirmationType::AVAILABLE)
    ])->confirmPayment($externalCashOutId);

    $payment->refresh();

    $this->assertNotEmpty($payment->paid_at);

})->with([
    [123456, 20000]
]);
