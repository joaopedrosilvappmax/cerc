<?php

namespace Tests\Feature;

use App\Support\EnumTypes\Math;
use App\Support\ValueObjects\Money;

test('Test To Decimal', function () {

    $value = 15420;

    $money = (new Money($value));

    expect($money->toDecimal())
        ->toBe($value * Math::DECIMAL);
});


test('Test To Integer With Float', function () {
    $money = (new Money(129.09));

    expect($money->toInteger())
        ->toBe(12909);

    $money = (new Money('129'));

    expect($money->toInteger())
        ->toBe(12900);


    $money = (new Money('1309.61'));

    expect($money->toInteger())
        ->toBe(130961);
});
