<?php

namespace Tests\Stub;

class AuthenticationMock
{

    public static function getAuthenticationResponse(): array
    {
        return [
            "access_token" => "3604be0c-c572-4dc1-be1b-95594d30122e",
            "token_type" => "bearer",
            "expires_in" => 60,
            "scope" => "resource-server-read resource-server-write"
        ];
    }
}
