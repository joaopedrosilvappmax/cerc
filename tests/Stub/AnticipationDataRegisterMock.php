<?php

namespace Tests\Stub;

use App\Models\Entities\Company;
use App\Support\EnumTypes\OperationType;

class AnticipationDataRegisterMock
{

    public static function getInitialData(): array
    {
        $company = Company::factory()->create();

        return [
            'external_order_id' => 1,
            'external_cash_out_id' => 1,
            'accrediting_company_id' => config('accrediting_company.appmax_id'),
            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
            'operation_type' => OperationType::CREATE,
            'final_recipient_document_number' => $company->document_number,
            'holder_document_number' => $company->document_number,
            'code_arrangement_payment' => 'MCC',
            'total_constituted_value' => 123456789,
            'total_value' => 123456789,
            'paid_value' => 123456789,
            'request_date' => '2021-07-09',
            'settlement_date' => null,
            'value' => 123456789,
        ];
    }
}
