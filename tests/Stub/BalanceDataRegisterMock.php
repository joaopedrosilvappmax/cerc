<?php

namespace Tests\Stub;

use App\Support\EnumTypes\BalanceType;

class BalanceDataRegisterMock
{

    public static function getInitialData(): array
    {
        return [
            'external_id' => '123123',
            'document_number' => '23400511000161',
            'type' => BalanceType::DEBIT,
            'value' => '30.01'
        ];
    }
}
