<?php

namespace Tests\Stub;

use App\Models\Entities\Company;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\ReceivableUnitConstituteStatus;
use Carbon\Carbon;

class ReceivableUnitDataRegisterMock
{

    public static function getInitialData(): array
    {
        $company = Company::factory()->create();

        return [
            'external_id' => '12312123',
            'operation_type' => OperationType::CREATE,
            'accrediting_company_id' => config('accrediting_company.appmax_id'),
            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
            'final_recipient_document_number' => $company->document_number,
            'holder_document_number' => $company->document_number,
            'code_arrangement_payment' => 'MCC',
            'constituted' => ReceivableUnitConstituteStatus::CONSTITUTED,
            'total_constituted_value' => 20.00,
            'pre_contracted_value' => 20.00,
            'total_gross_value' => 20.00,
            'blocked_value' => 0,
            'settlement_date' => Carbon::now()->format('Y-m-d'),
            'integrated_at' => null,
            'paid_at' => null,
            'value' => 20.00,
            'reversal_value' => 0.00,
            'guarantee_value' => 0,
            'debit_value' => 0,
        ];
    }
}
