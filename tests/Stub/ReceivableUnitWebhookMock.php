<?php

namespace Tests\Stub;

class ReceivableUnitWebhookMock
{
    public static function getWebhookData(): array
    {
        return [
            [
                "tipoEvento" => "unidadeRecebivel",
                "dataHoraEvento" => "2021-12-02T17:53:50.480+00:00",
                "evento" => [
                    "referenciaExterna" => "",
                      "protocolo" => "d0a52985-1cfc-4c45-92c4-574d87f13163",
                      "dataHoraProcessamento" => "2021-01-09T23:45:09.353Z",
                      "status" => "0",
                      "erros" => []
                ]
            ]
        ];
    }
}
