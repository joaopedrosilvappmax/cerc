<?php

namespace Tests\Stub;

class ContractEffectWebhookMock
{
    public static function getWebhookData(): array
    {
        return [
            "tipoEvento" => "efeitoContrato",
            "dataHoraEvento" => "2021-07-12T21:03:36.803+00:00",
            "evento" => [
                "referenciaExternaUnidadeRecebivel" => "15",
                "cnpjCredenciadora" => "08561701000101",
                "constituicaoUnidadeRecebivel" => "2",
                "identificadorContratoGeradorEvento" => "341e9640-48a0-4756-ab76-7e3b08161316",
                "documentoUsuarioFinalRecebedor" => "42889916090",
                "codigoArranjoPagamento" => "VCD",
                "dataLiquidacao" => "2022-09-22",
                "documentoTitular" => "42889916090",
                "efeitosContrato" => [
                    [
                        "protocolo" => "d38d3e02-5b08-40bc-a880-1cc89ac0d40f",
                        "indicadorEfeitosContrato" => "1",
                        "cnpjEntidadeRegistradora" => "23399607000191",
                        "tipoEfeito" => "3",
                        "regraDivisao" => "1",
                        "cnpjBeneficiarioTitular" => "10264663000177",
                        "valorComprometido" => "900.0",
                        "identificadorContrato" => "341e9640-48a0-4756-ab76-7e3b08161316",
                        "domicilioPagamento" => [
                            "documentoTitularDomicilio" => "42889916090",
                            "nomeTitularDomicilio" => "João dos Santos",
                            "tipoConta" => "CC",
                            "compe" => "341",
                            "ispb" => "60701190",
                            "agencia" => "0910",
                            "numeroConta" => "093973"
                        ]
                    ]
                ]
            ]
        ];
    }
}
