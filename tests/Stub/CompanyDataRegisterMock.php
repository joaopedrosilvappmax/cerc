<?php

namespace Tests\Stub;

use App\Support\EnumTypes\CompanyType;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\Status;
use App\Support\ValueObjects\Bank;

class CompanyDataRegisterMock
{
    public static function getInitialData(): array
    {
        $documentNumber = '23400511000161';

        return [
            'type_id' => CompanyType::PLATFORM,
            'name' => 'Empresa Test',
            'operation_type' => OperationType::CREATE,
            'document_number' => $documentNumber,
            'ispb' => (new Bank(654))->getFormattedIspb(),
            'compe' => 654,
            'status' => Status::ACTIVE,
            'email' => 'test@test.com',
            'phone' => '982469535',
            'bank_account_type' => 'CC',
            'bank_branch' => '11111',
            'bank_account' => '123123',
            'integrated_at' => null
        ];
    }
}
