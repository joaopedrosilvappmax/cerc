<?php

namespace Tests\Stub;

use Illuminate\Database\Eloquent\Model;

class IntegrationResponseDataMock
{

    public static function getSuccessResponse(Model $data): array
    {
        return [
            "protocolo" => "ccbf365b-dc81-4770-bd99-4af864c7fa54",
            "referenciaExterna" => $data->id,
            "status" => "0",
            "dataHoraProcessamento" => "2021-07-09T15:59:32.062Z",
            "erros" => []
        ];
    }

    public static function getErrorOperationInvalidResponse(Model $data, string $errorCode): array
    {
        return [
            [
                "referenciaExterna" => $data->id,
                "protocolo" => "4777d006-8ba0-4ca0-8a94-39176edfb943",
                "dataHoraProcessamento" => "2021-07-10T15:03:48.804Z",
                "status" => "1",
                "erros" => [
                    [
                        "codigo" => $errorCode,
                        "mensagem" => ": OPERACAO INVALIDA PARA O REGISTRO ATUAL"
                    ]
                ]
            ]
        ];
    }

    public static function getErrorExistsInformationResponse(Model $data, string $errorCode): array
    {
        return [
            [
                "referenciaExterna" => $data->id,
                "protocolo" => "4777d006-8ba0-4ca0-8a94-39176edfb943",
                "dataHoraProcessamento" => "2021-07-10T15:03:48.804Z",
                "status" => "1",
                "erros" => [
                    [
                        "codigo" => $errorCode,
                        "mensagem" => ": OPERACAO INVALIDA PARA O REGISTRO ATUAL"
                    ]
                ]
            ]
        ];
    }

    public static function getErrorResponse(Model $data): array
    {
        return [
            [
                "referenciaExterna" => $data->id,
                "protocolo" => "4777d006-8ba0-4ca0-8a94-39176edfb943",
                "dataHoraProcessamento" => "2021-07-10T15:03:48.804Z",
                "status" => "1",
                "erros" => [
                    [
                        "codigo" => "12312",
                        "mensagem" => ": ERRO GENERICO"
                    ]
                ]
            ]
        ];
    }
}
