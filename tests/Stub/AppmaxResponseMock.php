<?php

namespace Tests\Stub;

class AppmaxResponseMock
{
    public static function contractEffectApplySuccess(): array
    {
        return [
            'status' => 'success',
            'result' => rand(1, 1000)
        ];
    }

    public static function contractEffectApplyError(): array
    {
        return [
            'status' => 'error',
            'result' => 'Internal service error'
        ];
    }

    public static function accessDenied(): array
    {
        return [
            'message' => 'Access Denied'
        ];
    }
}
