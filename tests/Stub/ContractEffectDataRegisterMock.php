<?php

namespace Tests\Stub;

use App\Support\EnumTypes\AccreditingCompany;
use App\Support\EnumTypes\OperationType;

class ContractEffectDataRegisterMock
{

    public static function getInitialData(): array
    {
        return [
            'external_reference_receivable_unit_id' => 15,
            'external_contract_id' => '123123123123',
            'contract_generator_event_id' => 'asdasdasd',
            'protocol' => 'asdasdasd',
            'indicator' => 1,
            'accrediting_company_document_number' => 'abc',
            'constituted' => 3,
            'final_recipient_document_number' => 'abc',
            'code_arrangement_payment' => 'abc',
            'settlement_date' => '2020-01-01 00:00:00',
            'registration_entity_document_number' => 'abc',
            'holder_beneficiary_document_number' => 'abc',
            'effect_type' => 1,
            'division_rule' => 1,
            'committed_value' => 1234,
            'holder_document_number' => '123123123123',
            'holder_name' => 'asdasdasdas',
            'bank_account_type' => 'CC',
            'compe' => '654',
            'ispb' => 'abc',
            'bank_branch' => 'abc',
            'bank_account' => 'abc',
            'event_datetime' => '2020-01-01 00:00:00',
            'applied_at' => '2020-01-01 00:00:00',
        ];
    }
}
