<?php

return [
    'appmax_id' => env('APPMAX_ID', 1),
    'appmax_document_number' => env('APPMAX_DOCUMENT_NUMBER'),
];
