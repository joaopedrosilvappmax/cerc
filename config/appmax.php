<?php

return [
    'api_url' => env('APPMAX_API_URL'),
    'api_token' => env('APPMAX_API_TOKEN')
];
