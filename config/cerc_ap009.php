<?php

return [
    'sequential_number_file' => env('SEQUENTIAL_NUMBER_AP009', '0000001'),
    'file_columns' => [
        'reference_external_id' => 0,
        'reference_date' => 1,
        'accrediting_company_document_number' => 2,
        'final_recipient_document_number' => 3,
        'code_arrangement_payment' => 4,
        'settlement_date' => 5,
        'holder_document_number' => 6,
        'total_gross_value' => 7,
        'total_constituted_value' => 8,
        'pre_contracted_value' => 9,
        'anticipation_not_settled_value' => 10
    ]
];
