<?php

return [
    'holder_document_number' => env('CERC_HOLDER_DOCUMENT_NUMBER'),
    'client_id' => env('CERC_CLIENT_ID'),
    'client_secret' => env('CERC_CLIENT_SECRET'),
    'api_url' => env('CERC_API_URL'),
    'api_version' => env('CERC_API_VERSION', 'v14'),
    'webhook_token' =>env('CERC_WEBHOOK_TOKEN'),
    'company_hour_integrate' => env('COMPANY_HOUR_INTEGRATE', '00:05'),
    'receivable_unit_hour_write_off' => env('RECEIVABLE_UNIT_HOUR_WRITE_OFF', '00:00'),
    'receivable_unit_hour_integrate' => env('RECEIVABLE_UNIT_HOUR_INTEGRATE', '01:30'),
    'receivable_unit_hour_integrate_retry' => env('RECEIVABLE_UNIT_HOUR_INTEGRATE_RETRY', '03:00'),
    'anticipation_hour_integrate' => env('ANTICIPATION_HOUR_INTEGRATE', '09:05'),
    'anticipation_hour_write_off' => env('ANTICIPATION_HOUR_WRITE_OFF', '00:01'),
    'timeout' => env('INTEGRATION_TIMEOUT', 300),
    'synthetic_conciliation_hour_integrate' => env('SYNTHETIC_CONCILIATION_HOUR_INTEGRATE', '08:35'),
    'synthetic_conciliation_hour_generate' => env('SYNTHETIC_CONCILIATION_HOUR_GENERATE', '23:57'),
    'sleep_seconds_update_competition' => env('SLEEP_SECONDS_UPDATE_COMPETITION', 1),
    'analytic_conciliation' => [
        'receivable_unit_hour_generate' => env('ANALYTIC_CONCILIATION_RECEIVABLE_UNIT_HOUR_GENERATE', '01:30'),
        'anticipation_hour_generate' => env('ANALYTIC_CONCILIATION_ANTICIPATION_HOUR_GENERATE', '20:00'),
    ],
    'anticipation_limit_time' => [
        'start' => '09:00',
        'end' => '19:59'
    ]
];
