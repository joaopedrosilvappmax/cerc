<?php

return [
    'max_critical_count' => env('MAX_CRITICAL_COUNT', 10),
];
