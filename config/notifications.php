<?php

return [
    'daily_reports_time' => env('NOTIFICATION_REPORT_TIME', '08:40'),

    'slack' => [
        'channels' => [
            'cerc_notifications' => [
                'webhook_url' => env('LOG_SLACK_WEBHOOK_URL'),
            ],
        ],
    ],

    'discord' => [
        'servers' => [
            'cerc_notifications' => [
                'webhook_url' => env('LOG_DISCORD_WEBHOOK_URL'),
            ]
        ]
    ]
];
