<?php

return [
    'rate_limit' => env('API_RATE_LIMIT', 600),
    'debits_rate_limit' => env('API_DEBITS_RATE_LIMIT', 600)
];
