<?php

use App\Http\Controllers\AnticipationController;
use App\Http\Controllers\AnticipationRefuseController;
use App\Http\Controllers\CashOutPaymentController;
use App\Http\Controllers\ContractEffectOrderController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ReceivableUnitOrderController;
use App\Http\Controllers\WebhookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('client')->group(function() {
    Route::post('contract-effects/upload', [\App\Http\Controllers\ContractEffectController::class, 'upload'])
        ->name('contract-effects.upload');

    Route::get('contract-effects', [\App\Http\Controllers\ContractEffectController::class , 'index'])
        ->name('contract-effects.index');

    Route::post('companies', [\App\Http\Controllers\CompanyController::class, 'store'])
        ->name('companies.store');

    Route::post('receivable-units', [\App\Http\Controllers\ReceivableUnitController::class, 'store'])
        ->name('receivable-units.store');

    Route::get('receivable-unit-orders/total-anticipated/', [ReceivableUnitOrderController::class, 'getTotalAnticipated'])
        ->name('receivable-units.total-anticipated');

    Route::post('debits', [\App\Http\Controllers\DebitController::class, 'store'])
        ->name('debits.store')
        ->middleware('throttle:' . config('api.debits_rate_limit') . ',1');

    Route::post('guarantees', [\App\Http\Controllers\GuaranteeController::class, 'process'])
        ->name('guarantee.process');

    Route::post('cash-out-payments', [CashOutPaymentController::class, 'store'])
        ->name('cash-out-payments.store');

    Route::get('cash-out-payments', [CashOutPaymentController::class, 'index'])
        ->name('cash-out-payments.index');

    Route::get('payments/setlled', [PaymentController::class, 'paymentsSettled'])
        ->name('payments.settled');

    Route::get('payments/{id}', [PaymentController::class, 'show'])
        ->name('payments.show');

    Route::post('payments/totals', [PaymentController::class, 'getPaymentTotals'])
        ->name('payments.totals');

    Route::post('payments/confirmation', [PaymentController::class, 'paymentConfirmation'])
        ->name('payments.confirmation');

    Route::post('anticipations', [AnticipationController::class, 'store'])
        ->name('anticipations.store');

    Route::post('anticipations/refuse', [AnticipationRefuseController::class, 'store'])
        ->name('anticipations.refuse.store');

    Route::get('anticipations/{externalCashOutId}/contract-effect-orders', [ContractEffectOrderController::class, 'getByExternalCashOutId'])
        ->name('contract-effect-orders.by-external-cash-out-id');

    Route::post('contract-effects/orders', [ContractEffectOrderController::class, 'getByOrders'])
        ->name('contract-effects.orders');

    Route::get('contract-effects/orders/{external_order_id}', [ContractEffectOrderController::class, 'getByOrder'])
        ->name('contract-effects.orders.by-external-order-id');

    Route::get('contract-effects/payment/{id}', [ContractEffectOrderController::class, 'getByPaymentId'])
        ->name('contract-effects.payment');
});

Route::post('webhook', [WebhookController::class, 'store'])
    ->name('webhook.store')
    ->middleware('webhook');
