<?php

namespace Database\Seeders;

use App\Models\Entities\Company;
use App\Models\Entities\CompanyType;
use Illuminate\Database\Seeder;

class CompanyAppmaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documentNumber = '27000511000160';

        Company::updateOrCreate(
            ['id' => 1],
            [
                'type_id' => CompanyType::where('name', 'internal')->first()->id,
                'name' => 'APPMAX PLATAFORMA DE VENDAS LTDA',
                'document_number' => $documentNumber,
                'ispb' => substr($documentNumber, 0, 8)
            ]
        );
    }
}
