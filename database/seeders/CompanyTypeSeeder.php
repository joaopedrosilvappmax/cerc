<?php

namespace Database\Seeders;

use App\Models\Entities\CompanyType;
use Illuminate\Database\Seeder;

class CompanyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            ['id' => 1, 'name' => 'platform'],
            ['id' => 2, 'name' => 'internal']
        ];

        foreach ($types as $type) {
            CompanyType::updateOrCreate(
                ['id' => $type['id']],
                $type
            );
        }
    }
}
