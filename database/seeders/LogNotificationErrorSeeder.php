<?php

namespace Database\Seeders;

use App\Models\Entities\LogNotificationError;
use App\Support\EnumTypes\LogNotificationErrorType;
use Illuminate\Database\Seeder;

class LogNotificationErrorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LogNotificationError::updateOrCreate(
            [ 'type' => LogNotificationErrorType::CRITICAL ],
            [ 'type' => LogNotificationErrorType::CRITICAL ]
        );
    }
}
