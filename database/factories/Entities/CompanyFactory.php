<?php

namespace Database\Factories\Entities;

use App\Models\Entities\Company;
use App\Support\EnumTypes\CompanyType;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\Status;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $documentNumber = $this->faker->numerify('##############');

        return [
            'type_id' => CompanyType::PLATFORM,
            'name' => $this->faker->name(),
            'document_number' => $documentNumber,
            'operation_type' => OperationType::CREATE,
            'ispb' => substr($documentNumber, 0, 8),
            'compe' => 654,
            'status' => Status::ACTIVE,
            'email' => $this->faker->safeEmail(),
            'phone' => '51999999999',
            'bank_account_type' => 'CC',
            'bank_branch' => '11111',
            'bank_account' => '1231',
        ];
    }
}
