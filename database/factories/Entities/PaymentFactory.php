<?php

namespace Database\Factories\Entities;

use App\Models\Entities\Anticipation;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Payment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $documentNumber = $this->faker->numerify('##############');

        return [
            'receivable_unit_id' => null,
            'anticipation_id' => null,
            'holder_document_number' => $documentNumber,
            'ispb' => substr($documentNumber, 0, 8),
            'compe' => 654,
            'bank_account_type' => 'CC',
            'bank_branch' => $this->faker->numerify('#####'),
            'bank_account' => $this->faker->numerify('#####'),
            'payment_value' => $this->faker->numerify('#####'),
        ];
    }
}
