<?php

namespace Database\Factories\Entities;

use App\Models\Entities\ReceivableUnitOrder;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReceivableUnitOrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ReceivableUnitOrder::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $value = $this->faker->numerify('######');

        return [
            'external_order_id' => $this->faker->numerify('######'),
            'receivable_unit_id' => null,
            'gross_value' => $value,
            'value' => $value,
            'reversal_value' => 0,
            'guarantee_value' => 0,
        ];
    }
}
