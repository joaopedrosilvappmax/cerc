<?php

namespace Database\Factories\Entities;

use App\Models\Entities\SyntheticConciliationRecord;
use App\Support\EnumTypes\PaymentArrangement;
use Illuminate\Database\Eloquent\Factories\Factory;

class SyntheticConciliationRecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SyntheticConciliationRecord::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'reference_date' => $this->faker->date('Y-m-d'),
            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
            'code_arrangement_payment' => PaymentArrangement::HIPER_ARRANGEMENT,
            'total_gross_value' => $this->faker->numerify('######'),
            'total_constituted_value' => $this->faker->numerify('######'),
            'pre_contracted_value' => $this->faker->numerify('######'),
            'anticipation_not_settled_value' => $this->faker->numerify('######')
        ];
    }
}
