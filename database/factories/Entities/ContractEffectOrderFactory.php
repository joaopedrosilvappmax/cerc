<?php

namespace Database\Factories\Entities;

use App\Models\Entities\ContractEffectOrder;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContractEffectOrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ContractEffectOrder::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $receivableUnit = ReceivableUnit::factory()->create();

        return [
            'external_order_id' => $this->faker->numerify('########'),
            'receivable_unit_id' => $receivableUnit->id,
            'payment_id' => Payment::factory()->create(['receivable_unit_id' => $receivableUnit->id])->id,
            'value' => $this->faker->numerify('######'),
            'reversal_value' => 0
        ];
    }
}
