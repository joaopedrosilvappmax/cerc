<?php

namespace Database\Factories\Entities;

use App\Models\Entities\Balance;
use App\Support\EnumTypes\BalanceType;
use Illuminate\Database\Eloquent\Factories\Factory;

class BalanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Balance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'external_id' => $this->faker->numerify('######'),
            'document_number' => $this->faker->numerify('##############'),
            'value' => $this->faker->numerify('######'),
            'type' => BalanceType::DEBIT,
        ];
    }
}
