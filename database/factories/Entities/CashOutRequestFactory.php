<?php

namespace Database\Factories\Entities;

use App\Models\Entities\CashOutRequest;
use App\Models\Entities\Company;
use App\Support\EnumTypes\PaymentArrangement;
use Illuminate\Database\Eloquent\Factories\Factory;

class CashOutRequestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CashOutRequest::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $company = Company::factory()->create();

        $requestDate = $this->faker->date('Y-m-d');

        return [
            'external_cash_out_id' => $this->faker->numerify('######'),
            'external_order_id' => $this->faker->numerify('######'),
            'final_recipient_document_number' => $company->document_number,
            'code_arrangement_payment' => PaymentArrangement::HIPER_ARRANGEMENT,
            'value' => $this->faker->numerify('######'),
            'request_date' => $requestDate,
            'settlement_date' => $requestDate
        ];
    }
}
