<?php

namespace Database\Factories\Entities;

use App\Models\Entities\Company;
use App\Models\Entities\ReceivableUnit;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\PaymentArrangement;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReceivableUnitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ReceivableUnit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $documentNumber = $this->faker->numerify('##############');
        $totalConstitutedValue = $this->faker->numerify('######');

        return [
            'company_id' => Company::factory()->create()->id,
            'accrediting_company_id' => config('accrediting_company.appmax_id'),
            'receivable_unit_linked_id' => null,
            'contract_effect_id' => null,
            'operation_type' => OperationType::CREATE,
            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
            'final_recipient_document_number' => $documentNumber,
            'holder_document_number' => $documentNumber,
            'code_arrangement_payment' => PaymentArrangement::HIPER_ARRANGEMENT,
            'constituted' => 1,
            'total_constituted_value' => $totalConstitutedValue,
            'pre_contracted_value' => $this->faker->numerify('######'),
            'total_gross_value' => $this->faker->numerify('######'),
            'blocked_value' => 0,
            'settlement_date' => $this->faker->date('Y-m-d'),
            'integrated_at' => null,
            'paid_at' => $this->faker->dateTime(),
        ];
    }
}
