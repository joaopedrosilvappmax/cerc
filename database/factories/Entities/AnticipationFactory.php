<?php

namespace Database\Factories\Entities;

use App\Models\Entities\Anticipation;
use App\Models\Entities\Company;
use App\Support\EnumTypes\OperationType;
use App\Support\EnumTypes\PaymentArrangement;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnticipationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Anticipation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $company = Company::factory()->create();

        return [
            'operation_type' => OperationType::CREATE,
            'company_id' => $company->id,
            'external_cash_out_id' => $this->faker->numerify('######'),
            'accrediting_company_id' => config('accrediting_company.appmax_id'),
            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
            'final_recipient_document_number' => $company->document_number,
            'holder_document_number' => $company->document_number,
            'code_arrangement_payment' => PaymentArrangement::HIPER_ARRANGEMENT,
            'total_constituted_value' => $this->faker->numerify('######'),
            'total_value' => $this->faker->numerify('######'),
            'paid_value' => $this->faker->numerify('######'),
            'request_date' => $this->faker->dateTime(),
            'settlement_date' => $this->faker->date('Y-m-d'),
        ];
    }
}
