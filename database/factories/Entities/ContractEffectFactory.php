<?php

namespace Database\Factories\Entities;

use App\Models\Entities\ContractEffect;
use App\Support\EnumTypes\ContractEffectType;
use App\Support\EnumTypes\DivisionRule;
use App\Support\EnumTypes\ReceivableUnitConstituteStatus;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContractEffectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ContractEffect::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $documentNumber = $this->faker->numerify('##############');

        return [
            'external_reference_receivable_unit_id' => $this->faker->numerify('#'),
            'external_contract_id' => $this->faker->numerify('##############'),
            'contract_generator_event_id' => $this->faker->numerify('##############'),
            'protocol' => $this->faker->numerify('##############'),
            'indicator' => 1,
            'accrediting_company_document_number' => $this->faker->numerify('##############'),
            'constituted' => ReceivableUnitConstituteStatus::CONSTITUTED,
            'final_recipient_document_number' => $documentNumber,
            'code_arrangement_payment' => 'VCC',
            'settlement_date' => $this->faker->date('Y-m-d'),
            'registration_entity_document_number' => $this->faker->numerify('##############'),
            'holder_beneficiary_document_number' => $documentNumber,
            'effect_type' => ContractEffectType::HOLDER_CHANGE,
            'division_rule' => DivisionRule::FIXED_VALUE,
            'committed_value' => $this->faker->numerify('######'),
            'holder_document_number' => $documentNumber,
            'holder_name' => $this->faker->name(),
            'bank_account_type' => 'CC',
            'compe' => 654,
            'ispb' => substr($documentNumber, 0, 8),
            'bank_branch' => $this->faker->numerify('#####'),
            'bank_account' => $this->faker->numerify('#####'),
            'event_datetime' => Carbon::now(),
            'applied_at' => $this->faker->dateTime,
        ];
    }
}
