<?php

namespace Database\Factories\Entities;

use App\Models\Entities\AnalyticConciliationRecord;
use App\Models\Entities\ReceivableUnit;
use App\Support\EnumTypes\PaymentArrangement;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnalyticConciliationRecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AnalyticConciliationRecord::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $documentNumber = $this->faker->numerify('##############');

        return [
            'reference_date' => $this->faker->date('Y-m-d'),
            'receivable_unit_id' => ReceivableUnit::factory()->create()->id,
            'accrediting_company_document_number' => config('accrediting_company.appmax_document_number'),
            'final_recipient_document_number' => $documentNumber,
            'holder_document_number' => $documentNumber,
            'code_arrangement_payment' => PaymentArrangement::HIPER_ARRANGEMENT,
            'total_constituted_value' => $this->faker->numerify('######'),
            'pre_contracted_value' => $this->faker->numerify('######'),
            'total_gross_value' => $this->faker->numerify('######'),
            'settlement_date' => $this->faker->date('Y-m-d'),
        ];
    }
}
