<?php

namespace Database\Factories\Entities;

use App\Models\Entities\CashOutPayment;
use App\Models\Entities\Payment;
use App\Models\Entities\ReceivableUnit;
use Illuminate\Database\Eloquent\Factories\Factory;

class CashOutPaymentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CashOutPayment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $receivableUnit = ReceivableUnit::factory()->create();
        $payment = Payment::factory()->create();

        return [
            'external_cash_out_id' => $this->faker->numerify('#####'),
            'external_cash_out_company_id' => $this->faker->numerify('#####'),
            'receivable_unit_id' => $receivableUnit->id,
            'payment_id' => $payment->id,
            'value' =>  $this->faker->numerify('#######'),
        ];
    }
}
