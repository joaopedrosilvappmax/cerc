<?php

namespace Database\Factories\Entities;

use App\Models\Entities\Authentication;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class AuthenticationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Authentication::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $seconds = 60;

        return [
            'access_token' => '3604be0c-c572-4dc1-be1b-95594d30122e',
            'token_type' => 'bearer',
            'expires_in' => $seconds,
            'scope' => 'resource-server-read resource-server-write',
            'updated_at' => Carbon::now()->addSeconds($seconds)
        ];
    }
}
