<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRealValuesColumnsToAnalyticConciliationRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analytic_conciliation_records', function (Blueprint $table) {
            $table->bigInteger('real_total_gross_value')
                ->default(0)
                ->after('total_gross_value');

            $table->bigInteger('real_total_constituted_value')
                ->default(0)
                ->after('total_constituted_value');

            $table->bigInteger('real_pre_contracted_value')
                ->default(0)
                ->after('pre_contracted_value');

            $table->bigInteger('real_anticipation_settled_value')
                ->default(0)
                ->after('anticipation_settled_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analytic_conciliation_records', function (Blueprint $table) {
            $table->dropColumn('real_total_gross_value');
            $table->dropColumn('real_total_constituted_value');
            $table->dropColumn('real_pre_contracted_value');
            $table->dropColumn('real_anticipation_settled_value');
        });
    }
}
