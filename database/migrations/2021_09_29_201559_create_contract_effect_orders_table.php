<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractEffectOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_effect_orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('external_order_id');
            $table->unsignedBigInteger('receivable_unit_id');
            $table->unsignedBigInteger('payment_id');
            $table->bigInteger('value');
            $table->bigInteger('reversal_value')->default(0);

            $table->timestamps();

            $table->foreign('receivable_unit_id')
                ->references('id')
                ->on('receivable_units');

            $table->foreign('payment_id')
                ->references('id')
                ->on('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_effect_orders');
    }
}
