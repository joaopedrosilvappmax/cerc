<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContractEffectIdToIntegrationErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('integration_errors', function (Blueprint $table) {
            $table->unsignedBigInteger('contract_effect_id')
                ->after('anticipation_id')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('integration_errors', function (Blueprint $table) {
            $table->dropColumn('contract_effect_id');
        });
    }
}
