<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnticipationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anticipations', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('accrediting_company_id');
            $table->char('operation_type', 1);
            $table->char('accrediting_company_document_number', 14);
            $table->char('final_recipient_document_number', 14);
            $table->char('holder_document_number', 14);
            $table->char('code_arrangement_payment', 3);
            $table->bigInteger('total_constituted_value')->default(0);
            $table->bigInteger('total_value')->default(0);
            $table->bigInteger('paid_value')->default(0);
            $table->date('request_date');
            $table->date('settlement_date');

            $table->timestamp('integrated_at')->nullable();
            $table->timestamp('paid_at')->nullable();

            $table->timestamps();

            $table->foreign('company_id')
                ->references('id')
                ->on('companies');

            $table->foreign('accrediting_company_id')
                ->references('id')
                ->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anticipations');
    }
}
