<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashOutPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_out_payments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('external_cash_out_id');
            $table->foreignId('receivable_unit_id')
                ->nullable()
                ->constrained();
            $table->foreignId('payment_id')
                ->nullable()
                ->constrained();
            $table->float('value')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_out_payments');
    }
}
