<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeRealAnticipationSettledValueToRealAnticipationNotSettledValueToAnalyticConciliationRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analytic_conciliation_records', function (Blueprint $table) {
            $table->renameColumn('real_anticipation_settled_value', 'real_anticipation_not_settled_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analytic_conciliation_records', function (Blueprint $table) {
            $table->renameColumn('real_anticipation_not_settled_value', 'real_anticipation_settled_value');
        });
    }
}
