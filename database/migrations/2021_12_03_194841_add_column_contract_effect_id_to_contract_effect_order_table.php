<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnContractEffectIdToContractEffectOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_effect_orders', function (Blueprint $table) {
            $table->foreignId('contract_effect_id')
                ->nullable()
                ->after('payment_id')
                ->constrained('contract_effects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_effect_orders', function (Blueprint $table) {
            $table->dropForeign(['contract_effect_id']);
            $table->dropColumn('contract_effect_id');
        });
    }
}
