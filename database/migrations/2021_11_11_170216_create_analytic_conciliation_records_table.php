<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalyticConciliationRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytic_conciliation_records', function (Blueprint $table) {
            $table->id();

            $table->foreignId('receivable_unit_id')
                ->constrained('receivable_units');

            $table->date('reference_date');
            $table->char('accrediting_company_document_number', 14);
            $table->char('final_recipient_document_number', 14);
            $table->char('code_arrangement_payment', 3);
            $table->date('settlement_date');
            $table->char('holder_document_number', 14);
            $table->bigInteger('total_gross_value')->default(0);
            $table->bigInteger('total_constituted_value')->default(0);
            $table->bigInteger('pre_contracted_value')->default(0);
            $table->bigInteger('anticipation_settled_value')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytic_conciliation_records');
    }
}
