<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexToReceivableUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receivable_units', function (Blueprint $table) {
            $table->index([
                'final_recipient_document_number',
                'code_arrangement_payment',
                'settlement_date',
                'holder_document_number'
            ], 'receivable_units_index_01');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receivable_units', function (Blueprint $table) {
            $table->dropIndex('receivable_units_index_01');
        });
    }
}
