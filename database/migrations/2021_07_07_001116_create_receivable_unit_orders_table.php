<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceivableUnitOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivable_unit_orders', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('external_order_id');
            $table->unsignedBigInteger('receivable_unit_id');
            $table->bigInteger('value')->default(0);
            $table->bigInteger('reversal_value')->default(0);

            $table->timestamps();

            $table->foreign('receivable_unit_id')
                ->references('id')
                ->on('receivable_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivable_unit_orders');
    }
}
