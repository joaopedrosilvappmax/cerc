<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReceivableUnitOrdersExternalOrderIdIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receivable_unit_orders', function (Blueprint $table) {
            $table->index('external_order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receivable_unit_orders', function (Blueprint $table) {
            $table->dropIndex(['external_order_id']);
        });
    }
}
