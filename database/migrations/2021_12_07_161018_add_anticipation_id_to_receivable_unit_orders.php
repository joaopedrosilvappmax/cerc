<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAnticipationIdToReceivableUnitOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receivable_unit_orders', function (Blueprint $table) {
            $table->foreignId('anticipation_id')
                ->nullable()
                ->after('receivable_unit_id')
                ->constrained('anticipations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receivable_unit_orders', function (Blueprint $table) {
            $table->dropForeign(['anticipation_id']);
            $table->dropColumn('anticipation_id');
        });
    }
}
