<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDebitValueToReceivableUnitOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receivable_unit_orders', function (Blueprint $table) {
            $table->unsignedBigInteger('debit_value')
                ->default(0)
                ->after('anticipation_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receivable_unit_orders', function (Blueprint $table) {
            $table->dropColumn('debit_value');
        });
    }
}
