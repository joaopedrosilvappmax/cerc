<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropAnticipationOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('anticipation_orders', function (Blueprint $table) {
            Schema::dropIfExists('anticipation_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('anticipation_orders', function (Blueprint $table) {
            Schema::dropIfExists('anticipation_orders');
        });
    }
}
