<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalanceReceivableUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_receivable_unit', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('balance_id');
            $table->unsignedBigInteger('receivable_unit_id');
            $table->bigInteger('applied_value');

            $table->timestamps();

            $table->foreign('balance_id')
                ->references('id')
                ->on('balances');

            $table->foreign('receivable_unit_id')
                ->references('id')
                ->on('receivable_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_receivable_unit');
    }
}
