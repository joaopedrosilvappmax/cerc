<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalanceAnticipationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_anticipation', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('balance_id');
            $table->unsignedBigInteger('anticipation_id');
            $table->bigInteger('applied_value');

            $table->timestamps();

            $table->foreign('balance_id')
                ->references('id')
                ->on('balances');

            $table->foreign('anticipation_id')
                ->references('id')
                ->on('anticipations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_anticipation');
    }
}
