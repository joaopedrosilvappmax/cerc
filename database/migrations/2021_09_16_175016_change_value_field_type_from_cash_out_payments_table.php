<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeValueFieldTypeFromCashOutPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_out_payments', function (Blueprint $table) {
            $table->bigInteger('value')
                ->default(0)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_out_payments', function (Blueprint $table) {
            $table->float('value')
                ->default(0)
                ->change();
        });
    }
}
