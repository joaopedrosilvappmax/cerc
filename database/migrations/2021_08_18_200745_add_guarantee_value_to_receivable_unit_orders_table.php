<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGuaranteeValueToReceivableUnitOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('receivable_unit_orders', function (Blueprint $table) {
            $table->bigInteger('guarantee_value')->after('reversal_value')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receivable_unit_orders', function (Blueprint $table) {
            $table->dropColumn('guarantee_value');
        });
    }
}
