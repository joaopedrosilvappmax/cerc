<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('receivable_unit_id')->nullable();
            $table->unsignedBigInteger('anticipation_id')->nullable();

            $table->char('holder_document_number', 14);
            $table->char('ispb', 8);
            $table->char('compe', 3);
            $table->char('bank_account_type', 2);
            $table->char('bank_branch', 6);
            $table->char('bank_account', 12);
            $table->bigInteger('payment_value')->default(0);

            $table->timestamps();

            $table->foreign('receivable_unit_id')
                ->references('id')
                ->on('receivable_units');

            $table->foreign('anticipation_id')
                ->references('id')
                ->on('anticipations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
