<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToContractEffectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_effects', function (Blueprint $table) {
            $table->unsignedBigInteger('uploaded_contract_effect_file_id')
                ->nullable()
                ->after('id');

            $table->unsignedBigInteger('webhook_request_id')
                ->unsigned()
                ->nullable()
                ->after('id');

            $table->foreign('uploaded_contract_effect_file_id')
                ->references('id')
                ->on('uploaded_contract_effect_files');

            $table->foreign('webhook_request_id')
                ->references('id')
                ->on('webhook_requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_effects', function (Blueprint $table) {
            $table->dropForeign('contract_effects_uploaded_contract_effect_file_id_foreign');
            $table->dropForeign('contract_effects_webhook_request_id_foreign');

            $table->dropColumn('uploaded_contract_effect_file_id');
            $table->dropColumn('webhook_request_id');
        });
    }
}
