<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExternalCashoutCompanyIdInCashOutPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_out_payments', function (Blueprint $table) {
            $table->unsignedBigInteger('external_cash_out_company_id')
                ->after('external_cash_out_id')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_out_payments', function (Blueprint $table) {
            $table->dropColumn('external_cash_out_company_id');
        });
    }
}
