<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSyntheticConciliationRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('synthetic_conciliation_records', function (Blueprint $table) {
            $table->id();

            $table->date('reference_date');
            $table->char('accrediting_company_document_number', 14);
            $table->char('code_arrangement_payment', 3);
            $table->bigInteger('total_gross_value')->default(0);
            $table->bigInteger('total_constituted_value')->default(0);
            $table->bigInteger('pre_contracted_value')->default(0);
            $table->bigInteger('anticipation_settled_value')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('synthetic_conciliation_records');
    }
}
