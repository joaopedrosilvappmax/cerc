<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeReceivableUnitIdNullableToAnalyticConciliationRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analytic_conciliation_records', function (Blueprint $table) {
            $table->unsignedBigInteger('receivable_unit_id')
                ->nullable(true)
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analytic_conciliation_records', function (Blueprint $table) {
            $table->unsignedBigInteger('receivable_unit_id')
                ->nullable(false)
                ->change();
        });
    }
}
