<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropCashOutCercTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('cash_out_cerc');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('cash_out_cerc', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('external_cash_out_company_id');
            $table->string('company_name');
            $table->string('cnpj', 14);
            $table->string('account');
            $table->float('value');
            $table->tinyInteger('status');
            $table->tinyInteger('type');
            $table->timestamp('approved_date')
                ->nullable();
            $table->timestamp('payment_date')
                ->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
