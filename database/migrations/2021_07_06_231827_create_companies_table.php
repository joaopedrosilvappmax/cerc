<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('type_id');

            $table->string('name');
            $table->char('operation_type', 1)->nullable();
            $table->char('document_number', 14);
            $table->char('ispb', 8);
            $table->char('compe', 3)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->char('bank_account_type', 2)->nullable();
            $table->char('bank_branch', 6)->nullable();
            $table->char('bank_account', 12)->nullable();

            $table->timestamp('integrated_at')->nullable();
            $table->timestamps();

            $table->foreign('type_id')
                ->references('id')
                ->on('company_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
