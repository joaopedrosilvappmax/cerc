<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAnticipationSettledValueToAnticipationNotSettledValueToSyntheticConciliationRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('synthetic_conciliation_records', function (Blueprint $table) {
            $table->renameColumn('anticipation_settled_value', 'anticipation_not_settled_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('synthetic_conciliation_records', function (Blueprint $table) {
            $table->renameColumn('anticipation_not_settled_value', 'anticipation_settled_value');
        });
    }
}
