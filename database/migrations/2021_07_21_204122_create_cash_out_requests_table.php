<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashOutRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_out_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('external_order_id');
            $table->unsignedBigInteger('external_cash_out_id');
            $table->char('final_recipient_document_number', 14);
            $table->char('code_arrangement_payment', 3);
            $table->bigInteger('value');
            $table->date('request_date');
            $table->date('settlement_date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_out_requests');
    }
}
