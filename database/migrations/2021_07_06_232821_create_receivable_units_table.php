<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceivableUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receivable_units', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('accrediting_company_id');
            $table->unsignedBigInteger('receivable_unit_linked_id')->nullable();
            $table->unsignedBigInteger('contract_effect_id')->nullable();
            $table->unsignedBigInteger('anticipation_id')->nullable();

            $table->char('operation_type', 1);
            $table->char('accrediting_company_document_number', 14);
            $table->char('final_recipient_document_number', 14);
            $table->char('holder_document_number', 14);
            $table->char('code_arrangement_payment', 3);
            $table->tinyInteger('constituted')->default(1);
            $table->bigInteger('total_constituted_value')->default(0);
            $table->bigInteger('pre_contracted_value')->default(0);
            $table->bigInteger('total_gross_value')->default(0);
            $table->bigInteger('blocked_value')->default(0);
            $table->date('settlement_date');

            $table->timestamp('integrated_at')->nullable();
            $table->timestamp('paid_at')->nullable();

            $table->timestamps();

            $table->foreign('company_id')
                ->references('id')
                ->on('companies');

            $table->foreign('accrediting_company_id')
                ->references('id')
                ->on('companies');

            $table->foreign('receivable_unit_linked_id')
                ->references('id')
                ->on('receivable_units');

            $table->foreign('contract_effect_id')
                ->references('id')
                ->on('contract_effects');

            $table->foreign('anticipation_id')
                ->references('id')
                ->on('anticipations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receivable_units');
    }
}
