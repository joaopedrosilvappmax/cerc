<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractEffectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_effects', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('external_reference_receivable_unit_id')->nullable();
            $table->string('contract_generator_event_id', 255)->nullable();
            $table->string('external_contract_id')->nullable();
            $table->string('protocol', 255);
            $table->char('accrediting_company_document_number', 14);
            $table->tinyInteger('constituted')->default(1);
            $table->char('final_recipient_document_number', 14);
            $table->char('code_arrangement_payment', 3);
            $table->date('settlement_date');
            $table->char('registration_entity_document_number', 14);
            $table->char('holder_beneficiary_document_number', 14);
            $table->tinyInteger('effect_type');
            $table->tinyInteger('division_rule');
            $table->integer('indicator');

            $table->bigInteger('committed_value')->default(0);
            $table->char('holder_document_number', 14);
            $table->string('holder_name', 255)->nullable();
            $table->char('bank_account_type', 2);
            $table->char('compe', 3)->nullable();
            $table->char('ispb', 8);
            $table->char('bank_branch', 6);
            $table->char('bank_account', 12);

            $table->timestamp('event_datetime')->nullable();
            $table->timestamp('applied_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_effects');
    }
}
