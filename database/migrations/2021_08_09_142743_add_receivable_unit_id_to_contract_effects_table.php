<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReceivableUnitIdToContractEffectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contract_effects', function (Blueprint $table) {
            $table->foreignId('receivable_unit_id')
                ->nullable()
                ->after('id')
                ->constrained('receivable_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contract_effects', function (Blueprint $table) {
            $table->dropForeign(['receivable_unit_id']);
            $table->dropColumn('receivable_unit_id');
        });
    }
}
