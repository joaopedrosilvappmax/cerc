<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIntegratedAtToSyntheticConciliationRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('synthetic_conciliation_records', function (Blueprint $table) {
            $table->timestamp('integrated_at')->before('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('synthetic_conciliation_records', function (Blueprint $table) {
            $table->dropColumn('integrated_at');
        });
    }
}
