<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReferenceDateIndexToAnalyticConciliationRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analytic_conciliation_records', function (Blueprint $table) {
            $table->index(['reference_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analytic_conciliation_records', function (Blueprint $table) {
            $table->dropIndex(['reference_date']);
        });
    }
}
