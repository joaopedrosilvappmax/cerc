# Appmax - CERC Integration Application

## About CERC Integration Application

CERC Integration Application

Distributed service for registration of receivable units and integration with CERC system.

## Contributing

Thank you for considering contributing to the CERC Integration Application!

## Code of Conduct

Always try to exercise SOLID.

In object-oriented computer programming, SOLID is a mnemonic acronym for five design principles intended to make software designs more understandable, flexible, and maintainable. The principles are a subset of many principles promoted by American software engineer and instructor Robert C. Martin, first introduced in his 2000 paper Design Principles and Design Patterns.

The SOLID concepts are

- The Single-responsibility principle: "There should never be more than one reason for a class to change."In other words, every class should have only one responsibility.
- The Open–closed principle: "Software entities ... should be open for extension, but closed for modification."
- The Liskov substitution principle: "Functions that use pointers or references to base classes must be able to use objects of derived classes without knowing it".
- The Interface segregation principle: "Many client-specific interfaces are better than one general-purpose interface."
- The Dependency inversion principle: "Depend upon abstractions, [not] concretions."

Use:

- Service layer for business rules

Create:
- Feature Tests
- Unit Tests

## Security Vulnerabilities

Think security, act fast!

## CLI Commands

- Installation (Run CLI commands in root directory of project)
- copy to your bash profile
    ```
        alias sail='bash vendor/bin/sail'
    ```

- and then:
    ```
        sail up -d
    ```

- in case of needing to install composer dependencies, run:
    ```
        docker run --rm \
            -u "$(id -u):$(id -g)" \
            -v $(pwd):/opt \
            -w /opt \
            laravelsail/php80-composer:latest \
            composer install --ignore-platform-reqs
    ```

- Composer

        sail composer

- Tests

        sail tests

- Artisan commands

        sail artisan <command>

- Build Project

        sail build --no-cache  

- Down

        sail down

- Down (remove all cache)

        sail down --rmi all -v --remove-orphans 

- Migrations

        sail artisan migrate --seed

- Migrations (Test)

        sail artisan migrate --seed --env=testing 

More details: https://laravel.com/docs/8.x/sail

### Comunicating Appmax system to Cerc SD

- Check your CERC network with:

        docker network ls

- Get the network name and use it on that command to conect the containers:

        docker network connect {network} {container}

- Example:

        docker network connect cerc_sail appmax-php-dev

### Comunicating Cerc SD to Appmax system

- Check id Cerc Container:

        docker ps

- Access container Cerc:

        docker exec -it {container} bash

- Example:

       docker exec -it c2e7900a8c79 bash

- Install Vim:

      apt-get update
      apt-get install vim

- Edit file /etc/hosts:

      vim /etc/hosts

- Get ip adress System Appmax docker

    docker ps

    docker inspect 13edc19c656f | grep IPAddress

- Press "i" to edit and add to end of file:
  
    <ip_container_appmax_system>     sistema.localhost

- Example:

      172.22.0.4    sistema.localhost

- Save and exit file

### Generating credentials for authentication

- Run this command to generate the Client ID and Secret

        sail artisan passport:install

- Put that credentials on APPMAX System .env file

        CERC_SD_CLIENT_ID=
        CERC_SD_CLIENT_SECRET=

- Define the API url GATEWAY (final 1) on .env file as well:

        docker inspect 13edc19c656f | grep IPAddress
  
        CERC_SD_ENDPOINT=

- Example:
  
        CERC_SD_ENDPOINT=http://172.19.0.1:80/

### To tests

#### Cerc Sd reicevable effect contracts
- Config key WEBHOOK_TOKEN in .env
- Example:

       WEBHOOK_TOKEN = abc123

#### Send Effect Contract AppMax system
- config key APPMAX_API_URL and APPMAX_API_TOKEN in CERC SD .env 
- Example:

        APPMAX_API_URL=http://sistema.local:8080/

APPMAX_API_TOKEN equal CERC_ACCESS_TOKEN key in .env System AppMax.

    (CERC SD) APPMAX_API_TOKEN = (System) CERC_ACCESS_TOKEN
    APPMAX_API_TOKEN=abc123


